<?php
namespace Statflow\Service;

class GlobalManagement
{
    const KPI_SATGLO = 1;
    const KPI_SATDET = 2;
    const KPI_NPSGLO = 3;
    const KPI_NPSDET = 4;
    const KPI_EXPGLO = 5;
    
    private $model;

    private $validator;

    function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/kpiManagement');
    }

    
    public function getValidator($parameters)
    {
        if (! $this->validator) {
            $filters = array(
                new \Zend_Filter_Alnum(array(
                    'allowwhitespace' => true
                )),
                new \Zend_Filter_StringTrim(),
                new \Zend_Filter_StripTags(),
                new \Zend_Filter_StripNewlines()
            );
            $validators = array(
                    'name' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 255
                        ))
                    ),
                    'type_kpi' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_Int()
                    ),
                    'question' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength()
                    ),
                    'form' => array(
                        'presence' => 'required',
                        new \Zend_Validate_Int()
                    ),
                    'ignore' => array(
                        new \Zend_Validate_StringLength()
                    )
                );
            
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator;
    }
    
    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }

    public function getValidated($is_open,$meta=null, $id = null)
    {
        if ($id) {
            $kpiManagement = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $kpiManagement = $this->model->createRow();
        }
        
        $kpiManagement->name = $this->validator->name;
        $kpiManagement->meta = $meta;
        $kpiManagement->type = $this->validator->type_kpi;
        if(is_array($this->validator->question))
            $kpiManagement->question = implode(',', $this->validator->question );
        else
            $kpiManagement->question = $this->validator->question;
        if(is_array($this->validator->ignore))
            $kpiManagement->question_ignore = implode(',', $this->validator->ignore );
        else
            $kpiManagement->question_ignore = $this->validator->ignore;
        $kpiManagement->feedback_form_id = $this->validator->form;
        $kpiManagement->is_open = $is_open;
 
        $kpiManagement->save();
        
        return $kpiManagement;
    }
    
    public function getById($id){
        return $this->model->get(array('id'=>$id));
    }
    
    public function getAllKpiByFeedbackForm($id, $where=null){
        if(!$where){
          $where = "1=1";  
        }
        $select = $this->model->select()
                              ->where('feedback_form_id = ?',$id)
                              ->where($where);
        return $this->model->fetchAll($select);
    }
    
   
}

?>