<?php
namespace Statflow\service;

class Preview
{
    private $model;
    private $Language;

    
    public function __construct()
    {
        $this->model    = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->Language = \Centurion_Db::getSingleton('translation/language');
    }
    
    public  function getAllLanguages(){
        return $this->Language->fetchAll();
    }
    
    public function gestionErreur($formId, $rid)
    {     
        if($formId == null)
           die("No form id specified");      
        $form = $this->getFormById($formId);
        if($form == null) 
            die("No form matching this id found") ;
        $now = new \DateTime();
        $error = false;
        $errors = array('error'=>array());
        if ($form->study->is_published != true) {
            $errors['error'][] = 'study-not-published';
        }
        if($form->publication_start) {
            $pubStart = \DateTime::createFromFormat('Y-m-d H:i:s', $form->publication_start);
            if($pubStart > $now) {
                $errors['error'][] = 'publication-not-started';
            }
        }
        if($form->publication_end) {
            $pubEnd = \DateTime::createFromFormat('Y-m-d H:i:s', $form->publication_end);
            if($pubEnd < $now) {
                $errors['error'][] = 'publication-ended';
            }
        }
        if($form->is_published != true){
            $errors['error'][] = 'not-published';
        }
        
        $data = array('form' => $form->toArray());       
        $data['user'] = array('id' => 0);
        $data['store'] = array('id' => 0);
        
        if($form->is_open == false ){
            if(intval($rid ) < 1){
                $errors['error'][] = 'missing-respondent-id';
            } else {
                $respondentId = intval($rid);
                if($respondentId > 0) {
                    $respondent = $this->getRespendenetById($respondentId);
                    if($respondent == null) {
                        $errors['error'][] = 'respondent-not-found';
                    } else {
                        $validity_days = $form->validity_days;
                        $date = new \Zend_Date($respondent->created_at, 'YYYY-MM-dd HH:mm:ss');
                        $date->addDay($validity_days);
                        if($date->isEarlier(\Zend_Date::now())) { $errors['error'][] = 'is_invalid'; }
                        $data['user'] = $respondent->toArray();
                        $fields = $respondent->findDependentRowset('Statflow_Model_DbTable_RespondentExternalField')->toArray();
                        foreach($fields as $field){
                            $data['user'][str_replace(' ', '_', trim($field['label']))] = $field['value'];
                        }
                        $store = $this->getByExternalIdStructure($data['user']['structure_belonging']);
                        if($store) 
                            $data['store'] = $store->toArray();
                        else 
                            $errors['error'][] = 'respondents-structure-not-found';
                    }
                }
            }
        }
        if (count($errors['error']) > 0) {
            return $errors;
        }
        else
            return $data;
    }
    
    public function getFormById($formId)
    {
        return $this->model->fetchRow("id = $formId");
    }
    
    public function getRespendenetById($respondentId)
    {
        return \Centurion_Db::getSingleton('statflow/respondent')->fetchRow("id = ".$respondentId);
    }
    
    public function getByExternalIdStructure($data)
    {
        return \Centurion_Db::getSingleton('statflow/structure')->fetchRow("external_id = '{$data}'");
    }
    
    public function getFormWhereParentIdNull(){
        return $this->model->select()->where('parent_id is null')->order('order ASC');
    }
    
    public  function getAllPageByForm($form)
    {
        return $form->findDependentRowset(
                'Statflow_Model_DbTable_FeedbackFormPage',
                'feedback_form',
                $this->model->select()->order('order ASC')
            );
    }
}
