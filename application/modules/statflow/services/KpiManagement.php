<?php
namespace Statflow\Service;

class KpiManagement
{
    const KPI_SATGLO = 1;
    const KPI_SATDET = 2;
    const KPI_NPSGLO = 3;
    const KPI_NPSDET = 4;
    const KPI_EXPGLO = 5;
    
    private $model;

    private $validator;

    function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/kpiManagement');
    }

    
    public function getValidator($parameters)
    {
        if (! $this->validator) {
            $filters = array(
                new \Zend_Filter_Alnum(array(
                    'allowwhitespace' => true
                )),
                new \Zend_Filter_StringTrim(),
                new \Zend_Filter_StripTags(),
                new \Zend_Filter_StripNewlines()
            );
            $validators = array(
                    'name' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 255
                        ))
                    ),
                    'type_kpi' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_Int()
                    ),
                    'question' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength()
                    ),
                    'form' => array(
                        'presence' => 'required',
                        new \Zend_Validate_Int()
                    ),
                    'ignore' => array(
                        new \Zend_Validate_StringLength()
                    )
                );
            
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator;
    }
    
    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }

    public function getValidated($is_open,$meta=null, $id = null)
    {
        if ($id) {
            $kpiManagement = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $kpiManagement = $this->model->createRow();
        }
        
        $kpiManagement->name = $this->validator->name;
        $kpiManagement->meta = $meta;
        $kpiManagement->type = $this->validator->type_kpi;
        if(is_array($this->validator->question))
            $kpiManagement->question = implode(',', $this->validator->question );
        else
            $kpiManagement->question = $this->validator->question;
        if(is_array($this->validator->ignore))
            $kpiManagement->question_ignore = implode(',', $this->validator->ignore );
        else
            $kpiManagement->question_ignore = $this->validator->ignore;
        $kpiManagement->feedback_form_id = $this->validator->form;
        $kpiManagement->is_open = $is_open;
 
        $kpiManagement->save();
        
        return $kpiManagement;
    }
    
    public function getById($id){
        return $this->model->get(array('id'=>$id));
    }
    
    public function getAllKpiByFeedbackForm($id, $where=null){
        if(!$where){
          $where = "1=1";  
        }
        $select = $this->model->select()
                              ->where('feedback_form_id = ?',$id)
                              ->where($where);
        return $this->model->fetchAll($select);
    }
    
    public function getFeedbackByTypeAndStudyId($type, $studyId, $userId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                    ->distinct()
                    ->from(['CKM' => 'statflow_kpi_management'],[])
                    ->join(['CFF' => 'statflow_feedback_form'], 'CFF.id = CKM.feedback_form_id',['id', 'name as FeedbackName'])
                    ->join(['CUF' => 'statflow_user_feedback_form'], 'CUF.feedback_form_id = CFF.id',[])
                    ->where('CKM.type = ?', $type)
                    ->where('CFF.study_id = ?', $studyId)
                    ->where('CFF.is_visible = 1')
                    ->where('CUF.user_id = ?', $userId);        
        return $adapter->fetchAll($select);
    }
    
    public function getKpiByFeedbackAndType($type, $form)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from(['CKM' => 'statflow_kpi_management'],['CKM.id as idKpi', 'name as Kpiname'])
        ->join(['CFF' => 'statflow_feedback_form'], 'CFF.id = CKM.feedback_form_id',[])
        ->where('CKM.type = ?', $type)
        ->where('CKM.is_open = 1')
        ->where('CFF.id = ?', $form);
        return $adapter->fetchAll($select);
    }
    
    public function getQuestionByTypeAndFeedbackForm($feedback_form_id,$type =null){
        if(!$type)
            $where = '1=1';
        else if($type == 5)
            $where = 'parent_id is NOT NULL';
        else
            $where = 'question_type_id= '.$type;
        
        $model  = \Centurion_Db::getSingleton('statflow/question');
        $select = $model->select();
        $select->where('feedback_form_id = ? ', $feedback_form_id)
               ->where($where);
        return $model->fetchAll($select)->toArray();
    }
    
//     public function questionIntervalWithOneSubquestion($feedback_form_id){
//        $data = array();
//         foreach ($this->getAllQuestionInterval($feedback_form_id) as $value){
           
//        }
//     }
    
    public function getSubquestion($parent_id,$feedback_form_id){
        $model  = \Centurion_Db::getSingleton('statflow/question');
        $select = $model->select()
                        ->where('parent_id= ?',$parent_id)
                        ->where('feedback_form_id= ?',$feedback_form_id);
        return $model->fetchAll($select)->toArray();
    }
    
    public function getAllQuestionParent($feedback_form_id,$where=null){
        if(!$where)
           $where =  '1=1';
        $model  = \Centurion_Db::getSingleton('statflow/question');
        $select = $model->select()
                        ->where('parent_id is NULL')
                        ->where('feedback_form_id =? ',$feedback_form_id)
                        ->where($where);
        return $model->fetchAll($select)->toArray();
    }
    
    public function writeFileConfig($feedback_form_id)
    {
        $path = APPLICATION_PATH.'/modules/costumer/configs/satisfaction-limits-'.$feedback_form_id.'.ini';
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                            ->from('statflow_kpi_management')
                            ->where('feedback_form_id = ?',$feedback_form_id);
        $assoc_arr = $adapter->fetchAll($select); 
  
        $question_satisfaction_global   =array();
        $question_satisfaction_detaille =array();
        $question_nps_global            =array();
        $question_nps_detaille          =array();
        $question_exp_global            =array();
        $borne_sat_global               =array();
        $borne_sat_detail               =array();
        $borne_exp_global               =array();
        $content = "[production]\n";
        foreach ($assoc_arr as $key=>$elem) {
            if(is_array($elem)){
                if($elem['type'] == self::KPI_SATGLO){
                   $question_satisfaction_global[]   = $this->getCodeQuestion($elem['question']);
                   $borne_sat_global[]               = \Zend_Json::decode($elem['meta']);
                }elseif($elem['type'] == self::KPI_SATDET){
                   $question_satisfaction_detaille[] = $elem['question'];
                   $question_satisfaction_ignore[]   = $elem['question_ignore'];
                   $borne_sat_detail[]               = \Zend_Json::decode($elem['meta']);
               }elseif($elem['type'] == self::KPI_NPSGLO)
                   $question_nps_global[]            = $this->getCodeQuestion($elem['question']);
               elseif($elem['type'] == self::KPI_NPSDET)
                   $question_nps_detaille[]          = $this->getCodeQuestion($elem['question']);
               elseif($elem['type'] == self::KPI_EXPGLO){
                   $question_exp_global[]            = $this->getCodeQuestion($elem['question']);
                   $borne_exp_global[]               =\Zend_Json::decode($elem['meta']);
               }
             }else if($elem=="") 
                 $content .= $key." = \n";
             else 
                 $content .= $key." = \"".$elem."\"\n";
        }
        
        if(!empty($question_satisfaction_global)){
            $i=0;
            foreach ($question_satisfaction_global as $element){
               //var_dump( $borne_sat_global[$i]['bornes']);
                $content .= "\n;;".$element."\n";
                $content .= "sat-global.value=".$element."\n";
                $content .= "sat-global.".$element.".very_unsatisfied.from=".$borne_sat_global[$i]['bornes']['bornePDTSMin']." \n";
                $content .= "sat-global.".$element.".very_unsatisfied.to=".$borne_sat_global[$i]['bornes']['bornePDTSMax']." \n";
                $content .= "sat-global.".$element.".unsatisfied.from=".$borne_sat_global[$i]['bornes']['bornePSMin']." \n";
                $content .= "sat-global.".$element.".unsatisfied.to=".$borne_sat_global[$i]['bornes']['bornePSMax']." \n";
                $content .= "sat-global.".$element.".satisfied.from=".$borne_sat_global[$i]['bornes']['borneSMin']." \n";
                $content .= "sat-global.".$element.".satisfied.to=".$borne_sat_global[$i]['bornes']['borneSMax']." \n";
                $content .= "sat-global.".$element.".very_satisfied.from=".$borne_sat_global[$i]['bornes']['borneTSMin']." \n";
                $content .= "sat-global.".$element.".very_satisfied.to=".$borne_sat_global[$i]['bornes']['borneTSMax']." \n";
                $i++;
            }
        }
        if(!empty($question_exp_global)){
            $i=0;
            foreach ($question_exp_global as $element){
//              var_dump($borne_exp_global[$i]['bornes']);
                $content .= "\n;;".$element."\n";
                $content .= "exp-global.value=".$element."\n";
                $content .= "exp-global.".$element.".values.very_good=".$borne_exp_global[$i]['bornes']['borneVeryGood']." \n";
                $content .= "exp-global.".$element.".values.good=".$borne_exp_global[$i]['bornes']['borneGood']." \n";
                $content .= "exp-global.".$element.".values.poor=".$borne_exp_global[$i]['bornes']['bornePoor']." \n";
                $content .= "exp-global.".$element.".values.very_poor=".$borne_exp_global[$i]['bornes']['borneVeryPoor']." \n";
                $i++;
            }
        }
        if(!empty($question_nps_global)){
            foreach ($question_nps_global as $element){
                $content .= "\n;;".$element."\n";
                $content .= "nps-global.value=".$element."\n";
                $content .= "nps-global.".$element.".promoters.from=9 \n";
                $content .= "nps-global.".$element.".promoters.to=10 \n";
                $content .= "nps-global.".$element.".passive.from=7 \n";
                $content .= "nps-global.".$element.".passive.to=8 \n";
                $content .= "nps-global.".$element.".detractors.from=0 \n";
                $content .= "nps-global.".$element.".detractors.to=6 \n";
            }
        }
        $question  =array();
        $question_id_array= array();
        $question_ignore = array();

          //var_dump($question_satisfaction_detaille);die;
        if(!empty($question_satisfaction_detaille)){
            foreach ($question_satisfaction_detaille as $element){
                $question_id_array[] = explode(',', $element); 
            }
            foreach ($question_satisfaction_ignore as $element){
                $question_ignore_array[] = explode(',', $element);
            }
            for($i=0;$i<count($question_id_array);$i++){
                foreach ($question_id_array[$i] as $question_id)
                     $question[$i][] = $this->getCodeQuestion($question_id);
                
                $prefix = implode('_', $question[$i]);
                //var_dump($borne_sat_detail[$i]['bornes']);
                 $content .="\n;;".$prefix."\n";
                 $content .= "sat-detail.value=".$prefix."\n";
                 $content .= "sat-detail.".$prefix.".very_unsatisfied.from=".$borne_sat_detail[$i]['bornes']['bornePDTSMin']." \n";
                 $content .= "sat-detail.".$prefix.".very_unsatisfied.to=".$borne_sat_detail[$i]['bornes']['bornePDTSMax']." \n";
                 $content .= "sat-detail.".$prefix.".unsatisfied.from=".$borne_sat_detail[$i]['bornes']['bornePSMin']." \n";
                 $content .= "sat-detail.".$prefix.".unsatisfied.to=".$borne_sat_detail[$i]['bornes']['bornePSMax']." \n";
                 $content .= "sat-detail.".$prefix.".satisfied.from=".$borne_sat_detail[$i]['bornes']['borneSMin']." \n";
                 $content .= "sat-detail.".$prefix.".satisfied.to=".$borne_sat_detail[$i]['bornes']['borneSMax']." \n";
                 $content .= "sat-detail.".$prefix.".very_satisfied.from=".$borne_sat_detail[$i]['bornes']['borneTSMin']." \n";
                 $content .= "sat-detail.".$prefix.".very_satisfied.to=".$borne_sat_detail[$i]['bornes']['borneTSMax']." \n";
                 $question_id='';
                 if(!empty($question_ignore_array[$i]))
                     foreach ($question_ignore_array[$i] as $question_id){
                         $question_ignore[$i][] = $this->getCodeQuestion($question_id);
                         if($question_id)
                            $content .= "sat-detail.".$prefix.".".$this->getCodeQuestion($question_id).".ignore=".$question_id." \n";
                     }
                 $ignore='';
                 if(!empty($question_ignore[$i])){
                     $ignore = implode('_', $question_ignore[$i]);
                     if($ignore){
                         $content .="\n;;".$ignore."\n";
                         $content .= "sat-detail.".$prefix.".ignore.value=".$ignore."\n";
                     }
                 }
            }
        }
        
        $content .= "\n[pre-prod : production]\n\n[development : production]\n\n[staging : production]\n\n[testing : production]\n\n[local : production]\n\n";
        if (!$handle = fopen($path, 'w')) {
           return false;
        }  
        $success = fwrite($handle, $content);
        fclose($handle);
        
        return $success;
        
    }
    
    static public function getCodeQuestion($id){
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_question',array('code'=>'code'))
                           ->where('id = ?',$id);
        $code = $adapter->fetchOne($select);
        return $code;
    }
    
    static public function getColeurByKpi($id){
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_kpi_management',array('meta'=>'meta'))
                            ->where('id = ?',$id);
        $meta = $adapter->fetchOne($select);
        return \Zend_Json::decode($meta)['style'];
        
    }
    
    public function getQuestionBykpi($kpiId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from('statflow_kpi_management', ['question'])
        ->where('id = ?', $kpiId);
        return $adapter->fetchOne($select);
    }
}

?>