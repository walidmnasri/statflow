<?php
namespace Statflow\Service;

class XmlConfigSolr
{
    private $models = array();
    
    private $core;
    
    public function getCore(){
        return $this->core;
    }
    
    public function setCore($val){
        $this->core = $val;
    }
    
    function __construct()
    {
        $this->models['structure'] = \Centurion_Db::getSingleton('statflow/structure');
        $this->models['study'] = \Centurion_Db::getSingleton('statflow/study');
        $this->models['respondent'] = \Centurion_Db::getSingleton('statflow/respondent');
        $this->models['answer'] = \Centurion_Db::getSingleton('statflow/answer');
        $this->models['question'] = \Centurion_Db::getSingleton('statflow/question');
        $this->models['feedbackForm'] = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->models['feedbackFormPage'] = \Centurion_Db::getSingleton('statflow/feedbackFormPage');
    }

    public function generateSqlCode($feedback_form_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $feedbackForm = $this->models['feedbackForm']->get(array(
            'id' => $feedback_form_id
        ));
        
        $meta = \Zend_Json::decode($feedbackForm->meta ? $feedbackForm->meta : '{}');
        if(!isset($meta['respondentInfosOrder'])){
            $meta['respondentInfosOrder'] = array();
            $feedbackForm->meta = \Zend_Json::encode($meta);
            $feedbackForm->save();
        }     
                
        // main request
        $select = $adapter->select();
        $select->from('statflow_respondent')
        ->reset(\Zend_Db_Select::COLUMNS)
        ->where('statflow_respondent.feedback_form_id = ?', $feedback_form_id);
            
        $postStructures = array();
        
        $selectExternal_id = $adapter->select();
        $selectExternal_id->from('statflow_feedback_form',array('external'=>'external'))
        ->where('statflow_feedback_form.id = ?',  $feedback_form_id)
        ->limit(1);
        //echo $selectExternal_id;die;
        $external_id = $adapter->fetchOne($selectExternal_id);
        
        $select->columns('statflow_respondent.id as respondent_id')
        ->columns('statflow_respondent.status as status')
        ->columns("DATE_FORMAT(statflow_respondent.answered_at, '%Y-%m-%dT%H:%i:%sZ')  as answer_date")
        ->columns('statflow_respondent.alert as alert')
        ->columns('statflow_respondent.action as action')
        ->columns('statflow_respondent.token as token')
        ->columns('statflow_respondent.email as email')
        ->columns('statflow_respondent.firstname as firstname')
        ->columns('statflow_respondent.lastname as lastname')
        ->columns('statflow_respondent.completion_duration as completion_time')
        ->columns('statflow_respondent.locale as language')
        ->columns('statflow_respondent.structure_belonging as structure_belonging')
        ->columns("DATE_FORMAT(statflow_respondent.created_at, '%Y-%m-%d')  as imported_at")
        ->columns('statflow_respondent.external_id as '.$external_id)
        ->columns('statflow_respondent.terminal as terminal')
        ->columns('statflow_respondent.country_code as country_code');
        
        // TODO Fix Import Field List + External ID Key (from config file)
        $keysToIgnore = array(
            'email',
            'Name',
            'Surname',
            'language',
            'structure_belonging',
            'comment'
        );
        // $select->columns('statflow_respondent.external_id as ID_Club_Member');
        $externalFieldsSelect = $adapter->select()
        ->from('statflow_import_field', array(
            'id',
            'label'
        ))
        ->where('statflow_import_field.feedback_form_id = ?', $feedback_form_id)
        
        // Remove THAT
        ->where('statflow_import_field.label NOT IN ("' . join('","', $keysToIgnore) . '")');
        
        $fields = $adapter->fetchPairs($externalFieldsSelect);
        
        foreach ($fields as $id => $field) {
            $subSelect = $adapter->select();
            $subSelect->from('statflow_respondent_external_field', array(
                'statflow_respondent_external_field.value'
            ))
            ->joinInner('statflow_import_field', 'statflow_import_field.id = statflow_respondent_external_field.import_field_id', array())
            ->where('statflow_respondent_external_field.respondent_id = statflow_respondent.id')
            ->where('statflow_import_field.id = ?', $id)
            ->limit(1);
            $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($field)));
        }

        $respondentInfosOrder = array();
        $headers_init = array();
        if (count($meta['respondentInfosOrder']) > 0){
            $respondentInfosOrder = $meta['respondentInfosOrder'];
            foreach ($respondentInfosOrder as $respondentInfo){
                if ($respondentInfo == "respondent_id" || $respondentInfo == "status"){
                    $headers_init[] = 'respondent@int@'.$respondentInfo;
                }
                elseif ($respondentInfo == "answer_date"){
                    $headers_init[] = 'respondent@date@'.$respondentInfo;
                }
                else {
                    $headers_init[] = 'respondent@string@'.$respondentInfo;
                }
            }
        }
        else {
             $headers_init = array('respondent@int@respondent_id',
                                   'respondent@int@status',
                                   'respondent@date@answer_date',
                                   'respondent@string@alert',
                                   'respondent@string@action',
                                   'respondent@string@token',
                                   'respondent@string@email',
                                   'respondent@string@firstname',
                                   'respondent@string@lastname',
                                   'respondent@string@completion_time',
                                   'respondent@string@language',
                                   'respondent@string@structure_name',
                                   'respondent@string@structure_belonging',
                                   'respondent@string@imported_at',
                                   'respondent@string@'.$external_id,
                                   'respondent@string@terminal',
                                   'respondent@string@country_code');   

             foreach ($fields as $field) {
             if( !in_array('respondent@string@'.$field, $headers_init))
                 $headers_init[] = 'respondent@string@'.$field;
             }
        }

        $headers = $headers_init;

        $modalityQuestions      = array();
        $modalityQuestionsCodes = array();
        $openQuestions          = array();

        foreach ($feedbackForm->feedback_form_pages as $feedback_form_page) {
             foreach ($feedback_form_page->questions as $question) {
            $code =  explode('-',trim($question->code));
            $reference = $adapter->select();
            $reference->from(array('CQE'=>'statflow_question_entry'), array('ref'=>'MIN(`CQE`.id)'))
            ->joinInner(array('CQ'=>'statflow_question'), '`CQ`.id =`CQE`.question_id',array())
            ->where("CQ.code like ". $adapter->quote($code[0]) )
            ->where("`CQ`.`feedback_form_id` = '".$feedback_form_id."'");
            if ($question->parent_id != null) {
                if(!$question->parent) {
                    continue;
                }
                $subSelect = $adapter->select();
                $subSelect->from('statflow_answer', array(
                    '(statflow_answer.value )'
                ))
                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                ->where('statflow_question.code = ?', $question->code)
                ->limit(1);
                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                
                $subSelect = $adapter->select();
                $subSelect->from('statflow_answer', array(
                    '(statflow_answer.value - ('.$reference.'))'
                ))
                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                ->where('statflow_question.code = ?', $question->code)
                ->limit(1);
                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code).'-Value'));
                $headers[] = 'question@int@'.$question->code.'@tab';
                $headers[] = 'question@int@'.$question->code.'-Value';
                $modalityQuestions[] = $question->parent->code;
                $modalityQuestionsCodes[$question->code] = $question->parent->code;
                
            } else {
                switch ($question->question_type_id) {
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY:
                        break;
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ: // Q1 Q7 Q8 Q12
                        $subSelect = $adapter->select();
                        $subSelect->from('statflow_answer_entry', array())
                        ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                        ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                        ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array(
                            'id'
                        ))
                        ->where('statflow_answer.respondent_id = statflow_respondent.id')
                        ->where('statflow_question.code = ?', $question->code)
                        ->limit(1);
                        $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                        $headers[] = 'question@int@'.$question->code;
                        foreach ($question->question_entrys as $modality) {
                            if ($modality->entry_type_id == \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT) {
                                $subSelect = $adapter->select();
                                $subSelect->from('statflow_answer_entry', array(
                                    'content'
                                ))
                                ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                                ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array())
                                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                                ->where('statflow_question.code = ?', $question->code)
                                ->limit(1);
                                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code . '-Content')));
                                $headers[] = 'question@string@'.$question->code . '-Content';    
                            }
                        }
                        break;
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE:
                        $subSelect = $adapter->select();
                        $subSelect->from("statflow_answer_entry", "GROUP_CONCAT( statflow_answer_entry.question_entry_id SEPARATOR '|')")
                            ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                            ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                            ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array())
                            ->where('statflow_answer.respondent_id = statflow_respondent.id')
                            ->where('statflow_question.code = ?', $question->code);
                        $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                        $headers[]                     = 'question@string@'.$question->code.'@multiple';
                        foreach ($question->question_entrys as $modality) {
                            if ($modality->entry_type_id == \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT) {
                                $subSelect = $adapter->select();
                                $subSelect->from('statflow_answer_entry', array(
                                    'content'
                                ))
                                ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                                ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array())
                                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                                ->where('statflow_question.code = ?', $question->code)
                                ->limit(1);
                                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code . '-Content')));
                                $headers[] = 'question@string@'.$question->code . '-Content';
                            }
                        }
                        break;
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED:
                            $meta = \Zend_Json::decode($question->meta);
                            if (! isset($meta['max']))
                                $meta['max'] = 0;
                            $modalityQuestions[] = $question->code;
                            for ($i = 0; $i < $meta['max']; $i ++) {
                                $subSelect = $adapter->select();
                                $subSelect->from('statflow_answer_entry', array())
                                ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                                ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array(
                                    'id'
                                ))
                                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                                ->where('statflow_question.code = ?', $question->code)
                                ->limit(1, ($i > 0) ? $i : null);
                                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code) . '-' . ($i + 1)));
                                $headers[] = 'question@int@'.$question->code . '-' . ($i + 1).'@order';
                            }
                            break;
                       case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                        $subSelect = $adapter->select();
                        $subSelect->from('statflow_answer', array(
                            'value'
                        ))
                        ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                        ->where('statflow_answer.respondent_id = statflow_respondent.id')
                        ->where('statflow_question.code = ?', $question->code)
                        ->limit(1);
                        $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                        $headers[]                               = 'question@string@'.$question->code.'@open';
                        //$headers[]                               = 'question@string@'.$question->code;
                        break;
                    }
                }
            }
        }

        return array('sql' => $select, 'header' => $headers) ;
    }
    
    public function WriteXMlFileSchema($feedback_form_id)
    {
        header('Content-Type: text/html; charset=utf-8');
        $sql = $this->generateSqlCode($feedback_form_id);

        $xml = '
                    <schema name="statflow__'.$this->getCore().'" version="1.1" >
                      <fields>';
                        foreach ($sql['header'] as $elmt)
                        {
                            $type_data = explode('@', $elmt);
                            $name = str_replace(' ', '_', $type_data[2]);
                            if ($type_data[1]=='string'){
                                    if ($type_data[2] == "Date d'achat")
                                        $name = 'Date_achat';                                        
                                $xml .='<field name="'.$name.'" type="string" indexed="true" stored="true" multiValued="false" required="false"  />';
                                $xml .="\r\n";
                            }
                            elseif ($type_data[1]=='int'){
                                if($type_data[2]==='respondent_id')
                                    $xml .='<field name="'.$name.'" type="tint" indexed="true" stored="true" multiValued="false" required="false" />';
                                else 
                                    $xml .='<field name="'.$name.'" type="tint" indexed="true" stored="true" multiValued="false" required="false"  />';
                                $xml .="\r\n";
                            }
                            elseif ($type_data[1]=='date') {
                                $xml .='<field name="'.$name.'" type="date" indexed="true" stored="true" multiValued="false" required="false"  />';
                                $xml .="\r\n";
                            }
                        } 
                     $xml .=' <field name="_version_" type="long" indexed="true" stored="true"/>
                      </fields>
                    
                      <uniqueKey>respondent_id</uniqueKey>
                        
                      <solrQueryParser defaultOperator="AND"/>
                      <types>
                        <fieldType name="string" class="solr.StrField" sortMissingLast="true" />
                        <fieldType name="long" class="solr.TrieLongField" precisionStep="0" positionIncrementGap="0"/>
                        <fieldType name="tint" class="solr.TrieIntField" precisionStep="8" positionIncrementGap="0"/>
                        <fieldType name="date" class="solr.TrieDateField" precisionStep="0" positionIncrementGap="0"/>
                        <fieldType name="tdate" class="solr.TrieDateField" omitNorms="true" precisionStep="6" positionIncrementGap="0"/>
                        <fieldType name="random" class="solr.RandomSortField" indexed="true" />
                      </types>
                    </schema>';
        $path = APPLICATION_PATH."/../bin/solr/example/solr/".$this->getCore()."/conf/schema.xml";

        $xmlWriter = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.$xml);
        $xmlWriter->asXML($path);

    }
    
    public function WriteXMlFileDataConf($feedback_form_id){
        header('Content-Type: text/html; charset=utf-8');
        $sql = $this->generateSqlCode($feedback_form_id);
        
        $xml ='
            <dataConfig>
    <document name="content">

        <entity
                name="id"
                query="'.$sql['sql'].'
                        ORDER BY `statflow_respondent`.`created_at` DESC, `statflow_respondent`.`answered_at` DESC"
                deltaImportQuery="'.$sql['sql'].'
                    AND (`statflow_respondent`.`id` = ${dataimporter.delta.respondent_id})
                    AND (
                            (
                                SELECT Count(0)
                                FROM `statflow_answer`
                                WHERE `statflow_answer`.`respondent_id` = `statflow_respondent`.`id`
                            ) &gt; 0
                        )
                    GROUP  BY `statflow_respondent`.`id`
                    ORDER BY `statflow_respondent`.`created_at` DESC, `statflow_respondent`.`answered_at` DESC"
                deltaQuery="SELECT `id` as `respondent_id` FROM `statflow_respondent` WHERE `id` = ${dataimporter.request.respondent_id}"
            >';
        foreach ($sql['header'] as $elmt)
        {
            $type_data = explode('@', $elmt);
            if ($type_data[2] == "Date d'achat")
                $name = 'Date_achat';
            else {
                $name = str_replace(' ', '_', $type_data[2]);
            }
            $xml .='<field column="'.$type_data[2].'"      name="'.$name.'" />';
            $xml .="\r\n";
        }
       $xml .='</entity>
                </document>
            </dataConfig>';
        $path = APPLICATION_PATH."/../bin/solr/example/solr/".$this->getCore()."/conf/data-config.xml";

        $xmlWriter = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.$xml);
        $xmlWriter->asXML($path);
    }

    public function getQuestionByFeedbackForm($feedbackForm_id){
        $select =  $this->models['question']->select()
                                           ->where('feedback_form_id = ?',$feedbackForm_id);
        return $this->models['question']->fetchAll($select)->toArray();
        
    }
}

?>