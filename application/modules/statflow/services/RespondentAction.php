<?php
namespace Statflow\Service;

class RespondentAction
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
    
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/RespondentAction');
        }
        $this->model = $model;
    }
    
    public function getRespondentActionById($id)
    {
        return $this->model->select(true)
                           ->where('id = ' . $id)
                           ->fetchAll();
    }
    
    public function getRespondentActionByRespondentId($respondent_id)
    {
        return $this->model->select(true)
                           ->where('respondent_id = ' . $respondent_id)
                           ->order(array('id ASC'))
                           ->fetchAll();
    }
    
    public function saveRespondentAction($respondent_id, $feedback_form_id, $action, $comment)
    {
          $respondentAction = $this->model->createRow(array('respondent_id'    => $respondent_id,
                                                            'feedback_form_id' => $feedback_form_id,
                                                            'action'           => $action,
                                                            'comment'          => $comment));
        
            $respondentAction->save();
            return $respondentAction;
    }
    
    public function updateRespondentAction($respondent_id, $action)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $data = array(
            'action' => $action
        );
        
        $adapter->update('statflow_respondent', $data, 'id = '.$respondent_id);

    }
    
    public function getCurrentAction($respondent_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $selectQuery = $adapter->select();
                   $selectQuery->from('statflow_respondent',array('action'=>'action'))
                               ->where('statflow_respondent.id = ?',  $respondent_id)
                               ->limit(1);
        return $adapter->fetchOne($selectQuery);
    }
}

?>