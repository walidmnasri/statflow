<?php
namespace Statflow\Service;

class RespondentFieldHidden
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
    
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/RespondentFieldHidden');
        }
        $this->model = $model;
    }
    
    public function getRespondentFieldHiddenByFeedbackFormId($feedbackFormId)
    {
        $select=  $this->model->select(true)
                       ->where('feedback_form_id ='.$feedbackFormId)
                       ->fetchAll()->toArray();
    
        $data = array();
        foreach ($select as $field){
            $data[] = str_replace(' ', '_', $field['respondent_field']);
        }
    
        return $data;
    }
        
    public function saveRespondentFieldHidden($feedbackFormId, $respondentField)
    {
        $hiddenFields = $this->model->createRow( array( 'feedback_form_id' => $feedbackFormId,
                                                        'respondent_field' => $respondentField
                                               ));
        $hiddenFields->save();
        return $hiddenFields;  
    }
    
    public function deleteRespondentFieldHiddenByFeedbackFormId($feedbackFormId)
    {
        return $this->model->deleteRow("feedback_form_id = ".$feedbackFormId);
    }
    
    public function getRespondentQuestions($headers)
    {
        $respondentQuestions = array();
     
        foreach ($headers as $header){
            $data_header = explode('@', $header);
            if (!in_array('Value', explode('-', explode('@', $header)[2]))){
                if(explode('@', $header)[0] == 'question'){
                    $respondentQuestions[] = $data_header[2];
                }
            }
        }

        return $respondentQuestions;
    }

    public function getRespondentExternalFields($externalFields)
    {
        $respondentExternalFields = array();
        
        foreach ($externalFields as $externalField) {
            $respondentExternalFields[] = str_replace(' ', '_', $externalField['label']);
        }
        
        return $respondentExternalFields;
    }
    
    public function getRespondentInfos($headers, $externalFields)
    {
        $respondentInfos = array();
       
        $respondentExternalFields = $this->getRespondentExternalFields($externalFields);
        foreach ($headers as $header){
            $data_header = explode('@', $header);
            if (explode('@', $header)[0] == 'respondent' && !in_array(str_replace(' ', '_', $data_header[2]), $respondentExternalFields)){
                $respondentInfos[] = $data_header[2];
            }
            
        }
        
        return $respondentInfos;
    }
}

?>