<?php
namespace Statflow\Service;

class FeedbackForm
{

    private $model;

    private $studyModel;

    private $validator;

    private $imageForm;

    function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->studyModel = \Centurion_Db::getSingleton('statflow/study');
    }

    public function addImageForm()
    {
        $this->imageForm = new \Centurion_Form();
        $fileInput = new \Zend_Form_Element_File('image');
        $fileInput->addValidator('Count', false, 1);
        $fileInput->addValidator('Extension', false, array('jpg', 'jpeg','png'));
        $fileInput->addValidator('Size', array('min' => 0));
        $fileInput->setRequired(true);
        return $this->imageForm->addElement($fileInput);
    }
    
    public function getValidator($parameters, $action)
    {
        if (! $this->validator) {
            $filters = array(
		      'feedback' => array(
//                 new \Zend_Filter_Alnum(array(
//                     'allowwhitespace' => true
//                 )),
                new \Zend_Filter_StringTrim(),
                new \Zend_Filter_StripTags(),
                new \Zend_Filter_StripNewlines()
		      ),
              'manage-import' => array(
                  'rule_numdays' => array(
                        
                        'messages' => array(\Zend_Validate_Int::NOT_INT => 'A numeric value is required')
                    )
              )
            );
            $validators = array(
		        'feedback' => array(
                    'name' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 45
                        ))
                    ),
                    'external' => array(
                        'presence' => 'required',
                            new \Zend_Validate_NotEmpty(),
                            new \Zend_Validate_StringLength(array(
                                'max' => 255
                            ))
                    ),
		            'process_time' => array(
		                'presence' => 'required',
                        new \Zend_Validate_Int(),
                        new \Zend_Validate_Digits(),
                        'messages' => array(\Zend_Validate_Int::NOT_INT => 'A numeric value is required')
		            ),
                    'introduction_text' => array(
                        new \Zend_Validate_NotEmpty()
                    ),
                    'study_id' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_Int()
                    )
		        ),
                
                'manage-import' => array(
                    'rule_numdays' => array(
                        new \Zend_Validate_Int(),
                        new \Zend_Validate_Digits(),
                        'allowEmpty' => true,
                        'messages' => array(\Zend_Validate_Int::NOT_INT => 'A numeric value is required')
                    ),
                    'column_separator' => array(
                        'allowEmpty' => true,
                        new \Zend_Validate_StringLength(array('max' => 1))
                        
                    ),
                    'label_info' => array(
                        'allowEmpty' => true,
                        new \Zend_Validate_StringLength(array('max' => 20)),
                    )
                )
            );
            
            $this->validator = new \Zend_Filter_Input($filters[$action], $validators[$action], $parameters);
        }
        
        return $this->validator;
    }
    
    public function isValid($parameters, $action)
    {
        return $this->getValidator($parameters, $action)->isValid();
    }

    public function getValidated($study_id, $is_published, $is_open, $publication_start, $publication_end, $introduction_is_visible, $meta, $image, $id = null)
    {
        if ($id) {
            $feedbackForm = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $feedbackForm = $this->model->createRow();
        }
        
        $feedbackForm->name = html_entity_decode($this->validator->name);
        $feedbackForm->external = html_entity_decode($this->validator->external);
        $feedbackForm->process_time = $this->validator->process_time;
        $feedbackForm->study_id = $this->validator->study_id;
        $feedbackForm->is_published = $is_published;
        $feedbackForm->is_open = $is_open;
        $feedbackForm->publication_start = $publication_start;
        $feedbackForm->publication_end = $publication_end;
        $feedbackForm->introduction_is_visible = $introduction_is_visible;
        $feedbackForm->meta = $meta;
        $feedbackForm->image = $image;
        $feedbackForm->save();
        
        return $feedbackForm;
    }

    public function getById($id)
    {
        return $this->model->get(array(
            'id' => $id
        ));
    }

    public function getAll()
    {
        return $this->model->fetchAll();
    }

    public function getAllRoots($study_id)
    {
        $select = $this->model->select()
                       ->where('study_id = ' . $study_id);
          return  $this->model->fetchAll($select);
    }
    

    public function getAllStructuresById($study_id)
    {
        $select = $this->model->select()
                       ->where('study_id = ' . $study_id);
        return  $this->model->fetchAll($select);
    }
    
    public function getStructuresByIdFeedback($feedbackId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
                          ->from(array('ff' => 'statflow_feedback_form'),[])
                          ->joinInner(array('cs' => 'statflow_study'), 'cs.id = ff.study_id',[])
                          ->joinInner(array('str' => 'statflow_structure'), 'str.id = cs.structure_id',['external_id'])
                          ->where('ff.id ='.$feedbackId);
    
        $result =  $adapter->fetchOne($select);
        return $result;
    }
    
    public function getStructureIDByFeedbackFormId($feedbackId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
                          ->from(array('ff' => 'statflow_feedback_form'),[])
                          ->joinInner(array('cs' => 'statflow_study'), 'cs.id = ff.study_id',[])
                          ->joinInner(array('str' => 'statflow_structure'), 'str.id = cs.structure_id',['id'])
                          ->where('ff.id ='.$feedbackId);
    
        $result =  $adapter->fetchOne($select);
        return $result;
    }
        
    public function getAllFeedbackByStudyId($studyId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select = $adapter->select()
        ->from('statflow_feedback_form', ['ID'])
        ->where('study_id = ' . $studyId);
        $result =  $adapter->fetchAll($select);
        $resultId = [];
        foreach ($result as $element) {
            $resultId[] = $element['ID'];
        }
        return $resultId;
    }

    public function getFeedbacksByStudyId($studyId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select = $adapter->select()
            ->from('statflow_feedback_form', ['id', 'name'])
            ->where('study_id = ' . $studyId);
        $result =  $adapter->fetchAll($select);
        $resultId = [];
        foreach ($result as $element) {
            $resultId[] = $element;
        }
        return $resultId;
    }
    
    public function getAllFeedbackFormForRespondent($external_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
                          ->from(array('res' => 'statflow_respondent'))
                          ->joinInner(array('fee' => 'statflow_feedback_form'), 'fee.id = res.feedback_form_id')
                          ->where("res.external_id ='$external_id'")
                          ->where('res.status = 20');
    
        $result =  $adapter->fetchAll($select);
        return $result;
    }  
    
    /**
     * Generatting CSV formatted string from an array.
     * By Sergey Gurevich.
     */
    public function _arrayToCSV($array, $header_row = true, $col_sep = ";", $row_sep = "\n", $qut = '')
    {
        if (! is_array($array) or ! is_array($array[0])) {
            return false;
        }
        $output = '';
        // Header row.
        if ($header_row) {
            foreach ($array[0] as $key => $val) {
                // Escaping quotes.
                $key = str_replace($qut, "$qut$qut", $key);
                $output .= "$col_sep$qut$key$qut";
            }
            $output = substr($output, 1) . "\n";
        }
        // Data rows.
        foreach ($array as $key => $val) {
            $tmp = '';
            foreach ($val as $cell_key => $cell_val) {
                // Escaping quotes.
                $cell_val = str_replace($qut, "$qut$qut", $cell_val);
                $tmp .= "$col_sep$qut$cell_val$qut";
            }
            $output .= substr($tmp, 1) . $row_sep;
        }
        return $output;
    }

    public function uploadImage($uploadPath = null)
    {
        if (!$uploadPath) {
            $uploadPath = realpath(
                APPLICATION_PATH . '/../public/uploads/forms'
            );
        }
        
        $upload = new \Zend_File_Transfer_Adapter_Http();
        $upload->setDestination($uploadPath);
        $upload->addFilter('Rename', array('target' => $uploadPath.'/'.$upload->getFileName(null,false), 'overwrite' => true));
        $upload->receive();
        $image = $upload->getFileName(null,false);
        
        return $image;
    }
    
    public function formatDate($date)
    {
        $dateFormat = new \Zend_Date($date, 'dd/MM/yyyy');
        $publicationDate = $dateFormat->get('yyyy-MM-dd HH:mm:ss');
        unset($dateFormat);
        
        return $publicationDate;
    }
    
    public function addDirectorySolr($name){
        
        $directories_solr = array(
            $name,
            $name . '/data',
            $name . '/conf',
        );
        foreach ($directories_solr as $directory) {
            $path = APPLICATION_PATH . '/../bin/solr/example/solr/' . $directory;
            //die($path);
            if (!\Sayyes_Tools_Filesystem::directoryExist($path)) {
                \Sayyes_Tools_Filesystem::createdir($path);
                
            }
        }
        return true;
    }
    
    public function addfileConfigSolr($name){
        $error =array();
        $solrconfig     = APPLICATION_PATH . '/../bin/solr/example/solr/originConfig/solrconfig.xml';
        $solrcore       = APPLICATION_PATH . '/../bin/solr/example/solr/originConfig/solrcore.properties';
        $iniFile        = APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$name.'.ini';
        
        if (\Sayyes_Tools_Filesystem::fileExist($solrconfig)) {
            $importDirectory = dirname($solrconfig);
            copy($solrconfig, $importDirectory.'/../'.$name .'/conf/'. basename($solrconfig));
        }
        else {
            $error[]= 'No file solrconfig.xml in directory'.$solrconfig;
        }
        if (\Sayyes_Tools_Filesystem::fileExist($solrcore)) {
            $importDirectory = dirname($solrcore);
            copy($solrcore, $importDirectory.'/../'.$name .'/conf/'. basename($solrcore));
        }
        else {
            $error[]= 'No file solrcore.properties in directory';
        }
        if (!\Sayyes_Tools_Filesystem::fileExist($iniFile)) {
            \Sayyes_Tools_Filesystem::createFile($iniFile, null);
        }

        if($error){
            var_dump($error);
            die;
        }
        return true;
    }
    
    
    public function duplicateFeedbackForm($formId)
    {
         
        $formRow = $this->model->fetchRow($this->model->select()->where('id = ?',$formId));
        $data = $formRow->toArray();
        $data['id']= null;
        $data['created_at']= null;
        $data['updated_at']= null;
        $data['is_visible']= 0;
        $newForm = $this->model->createRow($data);
        $newForm->name  = $formRow->name.'cp';
        $newForm->save();
    
        return $newForm;
    }
    
    public function createFeedbackFormDirectories($studyId, $feedbackFormId)
    { 
        $directories = array(
            $studyId,
            $studyId . '/' . $feedbackFormId
        );
        foreach ($directories as $directory) {
            $path = APPLICATION_PATH . '/modules/statflow/configs/streams/' . $directory;
            if (! \Sayyes_Tools_Filesystem::directoryExist($path)) {
                \Sayyes_Tools_Filesystem::createdir($path);
                chmod($path,0777);
            }
        }
        // import/export/logs
        $directories = array(
            $studyId,
            $studyId . '/' . $feedbackFormId,
            $studyId . '/' . $feedbackFormId . '/export',
            $studyId . '/' . $feedbackFormId . '/import',
            $studyId . '/' . $feedbackFormId . '/import/archives',
            $studyId . '/' . $feedbackFormId . '/logs',
            $studyId . '/' . $feedbackFormId . '/logs/sql',
            $studyId . '/' . $feedbackFormId . '/flux'
        );
        foreach ($directories as $directory) {
            $path = APPLICATION_PATH . '/../public/ftp/' . $directory;
            if (! \Sayyes_Tools_Filesystem::directoryExist($path)) {
                \Sayyes_Tools_Filesystem::createdir($path);
                chmod($path,0777);
            }
        }
    }
    
    public function getAnonymousQuestionByFeedbackFormId($feedbackFormId)
    {        
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_feedback_form', array('anonymous_question'))
                           ->where('id = ?', $feedbackFormId)
                           ->limit(1);
        
        return $adapter->fetchOne($select);
    }

    public function getAnonymousModalityByFeedbackFormId($feedbackFormId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_feedback_form', array('anonymous_modality'))
                           ->where('id = ?', $feedbackFormId)
                           ->limit(1);
    
        return $adapter->fetchOne($select);
    }
       
    public function updateAnonymousQuestion($feedbackFormId, $question, $modality)
    {   
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $data = array( 'anonymous_question' => $question,
                       'anonymous_modality' => $modality );
        
        $adapter->update('statflow_feedback_form', $data, 'id = '.$feedbackFormId);
    }
    
    
    
    public function getNominativeQuestionByFeedbackFormId($feedbackFormId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from('statflow_feedback_form', array('nominative_question'))
        ->where('id = ?', $feedbackFormId)
        ->limit(1);
    
        return $adapter->fetchOne($select);
    }
    
    public function getNominativeModalityByFeedbackFormId($feedbackFormId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from('statflow_feedback_form', array('nominative_modality'))
        ->where('id = ?', $feedbackFormId)
        ->limit(1);
    
        return $adapter->fetchOne($select);
    }
    
    public function updateNominativeQuestion($feedbackFormId, $question, $modality)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $data = array( 'nominative_question' => $question,
            'nominative_modality' => $modality );
    
        $adapter->update('statflow_feedback_form', $data, 'id = '.$feedbackFormId);
    }
    
    public function getAnonymousFilter($feedbackFormId)
    {
        $question = $this->getAnonymousQuestionByFeedbackFormId($feedbackFormId);
        $modality = $this->getAnonymousModalityByFeedbackFormId($feedbackFormId);
        
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        if (isset($question) && isset($modality)){
            return "-".$question.":".$modality;
        }
        
        return false;
    }   

    function CleanData() {
        ini_set('memory_limit', '-1');
        $config  =  new \Zend_Config_Ini( APPLICATION_PATH .'/configs/local.ini', 'local');
        $servername = $config->resources->db->params->host;
        $username   = $config->resources->db->params->username;
        $password   = $config->resources->db->params->password;
        $dbname     = $config->resources->db->params->dbname;
        
        $conn = new \mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sqlDoublant = "SELECT `id`, `respondent_id`, `question_id`, `value`, `created_at`, `updated_at`
            FROM `statflow_answer`
            WHERE
                `id` NOT IN ( SELECT id FROM
                            `statflow_answer`
                          GROUP BY
                            `question_id`, `respondent_id`
                          HAVING
                             COUNT(`question_id`) = 1
                        )
            ORDER BY `respondent_id` , `question_id`, `created_at`
        ";
        $result = $conn->query($sqlDoublant);
        $question = '';
        for ($resultSet = []; $row = $result->fetch_assoc(); $resultSet[] = $row );
        foreach($resultSet as $key => $element) {
            if ($question == $element['question_id']) {
                if ($resultSet[$key-1]['value'] === $element['value']) {
                    $deleteSqlAnswer  = "DELETE FROM `statflow_answer` WHERE id = " . $element['id'] ;
                   $conn->query($deleteSqlAnswer);
                    $deleteSqlAnswerEntry   = "DELETE FROM `statflow_answer_entry` WHERE answer_id = " . $element['id'] ;
                   $conn->query($deleteSqlAnswerEntry); 
                }
                else {
                    if (isset($element['value']) && $element['value'] !== NULL) {
                        $deleteSqlAnswer        = "DELETE FROM `statflow_answer` WHERE id = " . $resultSet[$key-1]['id'] ;
                        $conn->query($deleteSqlAnswer); 
                        $deleteSqlAnswerEntry   = "DELETE FROM `statflow_answer_entry` WHERE answer_id = " . $resultSet[$key-1]['id'] ;
                        $conn->query($deleteSqlAnswerEntry);
                    }
                    else {
                        $deleteSqlAnswer        = "DELETE FROM `statflow_answer` WHERE id = " . $element['id'] ;
                        $conn->query($deleteSqlAnswer);
                        $deleteSqlAnswerEntry   = "DELETE FROM `statflow_answer_entry` WHERE answer_id = " . $element['id'] ;
                        $conn->query($deleteSqlAnswerEntry);
                    }
                }
            } else {
                $question = $element['question_id'];
            }
        }
        $responseEmpty = "SELECT id FROM `statflow_answer` WHERE `value` = ''";
        $resultEmptyValue = $conn->query($responseEmpty);
        for ($resultEmptyValueSet = []; $row = $resultEmptyValue->fetch_assoc(); $resultEmptyValueSet[] = $row );
        foreach ($resultEmptyValueSet as $key => $value ) {
            $cleanTableEntryAnswerSql = " DELETE FROM `statflow_answer_entry`  WHERE `answer_id` = {$value['id']} ";
            if ($conn->query($cleanTableEntryAnswerSql) == FALSE) {
                 echo "\nerror to clean data !\n";
            }
        }
        $cleanTableAnswerSql = " DELETE FROM `statflow_answer` WHERE `value` = '' ";
        
        if ($conn->query($cleanTableAnswerSql) == FALSE) {
            echo "\nerror to clean data !\n";
        }
    
    
        $conn->close();
    }
    
    public function getFeedbackFormParameterExternalField($feedback_form_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
                          ->from(array('cif' => 'statflow_import_field'))
                          ->where("cif.is_parameter ='1'")
                          ->where("cif.feedback_form_id ='$feedback_form_id'");
    
        $result =  $adapter->fetchAll($select);
        return $result;
    }
    
    public function insertImportInfos($study_id, $feedback_form_id, $external_id, $label)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $import = array( 'study_id'         => $study_id,
                         'feedback_form_id' => $feedback_form_id,
                         'number'           => 1,
                         'created_at'       => date("Y-m-d H:i:s"));      
            
        $adapter->insert('statflow_import', $import);
        
        $import_id = $adapter->lastInsertId();
        
        $importField = array( 'import_id'        => $import_id,
                              'study_id'         => $study_id,
                              'feedback_form_id' => $feedback_form_id,
                              'label'            => $label,
                              'external_id'      => $external_id,
                              'created_at'       => date("Y-m-d H:i:s"),
                              'is_parameter'     => '1');
        
        $adapter->insert('statflow_import_field', $importField);
    }
    
    public function insertRespondentExternalField($respondent_id, $import_field_id, $label, $value)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $data = array( 'respondent_id'   => $respondent_id,
                       'import_field_id' => $import_field_id,
                       'label'           => $label,
                       'value'           => $value,
                       'created_at'      => date("Y-m-d H:i:s"));
    
        $adapter->insert('statflow_respondent_external_field', $data);
    }
    
    public function updateImportInfos($import_field_id, $external_id, $label, $import_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $importField = array( 'external_id' => $external_id,
                              'label'       => $label );
        
        $adapter->update('statflow_import_field', $importField, 'id = '.$import_field_id);
        
        $number = (int)$this->getImportNumber($import_id) + 1;
        $import = array( 'number' => $number);
        
        $adapter->update('statflow_import', $import, 'id = '.$import_id);
    }
    
    public function getImportNumber($import_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $select = $adapter->select()
                          ->from(array('cim' => 'statflow_import'))
                          ->where("cim.id ='$import_id'");
        
        $result =  $adapter->fetchRow($select);
        return $result['number'];        
        
    }
    
    public function getImportFieldInfosByID($import_field_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $select = $adapter->select()
                          ->from(array('cif' => 'statflow_import_field'))
                          ->where("cif.id ='$import_field_id'");
        
        $result =  $adapter->fetchRow($select);
        return $result;
        
    }
    
    public function deleteImportField($import_field_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $adapter->delete("statflow_import_field", "id = ".$import_field_id);
    }
        
}

?>
