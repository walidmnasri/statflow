<?php

namespace Statflow\Service;

class Structure 
{
    private $model;
    
    private $studyModel;
    
    private $validator;
    
    // Trace suite à import 0: insert, 1:update, 2:error
    private $import_status;   
    
    private $export; 
    
    public function __construct()
    {

        $this->model = \Centurion_Db::getSingleton('statflow/structure');
        $this->studyModel = \Centurion_Db::getSingleton('statflow/study');
    }
    
    /**
     *
     * set $export
     */
    public function setExport($val)
    {
        $this->export = $val;
    }
    
    /**
     * get $export
     */
    public function getExport()
    {
        return $this->export;
    }
    
    private function getValidator($parameters)
    {
        if (!$this->validator) {

            $filters = array(
                '*' => array(
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines(),
                )
            );
            
            $validators = array(
                    'name' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 255)),
                    ),
                    'external_id' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 255)),
                    )
            );
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator; 
    }
    
    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }
    
    public function getValidated($parent_id, $id = null)
    {
        if ($id) {
            $structure = $this->model->get(array('id' => $id));
        } else {
            $structure = $this->model->createRow();
        }
        
        $structure->name = $this->validator->name;
        $structure->parent_id = $parent_id;
        $structure->external_id = $this->validator->external_id;
        
        return $structure;
    }
    
    public function getById($id)
    {
        return $this->model->get(array('id' => $id));
    }
    
    public function getAll()
    {
        return $this->model->fetchAll();
    }
    
    public function getAllRoots()
    {
        return $this->model->select(true)->where('parent_id IS NULL')->fetchAll();
    }
    
    
    public function getAllStructureByUser($user_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select= $adapter->select()
                         ->from('statflow_structure')
                         ->join('statflow_structure_user', '`statflow_structure`.id=`statflow_structure_user`.structure_id')
                         ->where('`statflow_structure_user`.user_id ='.$user_id);
        return  $adapter->fetchAll($select);
    }
    

    public function getAllStructureOnlyByUser($user_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select= $adapter->select()
                         ->from('statflow_structure')
                         ->join('statflow_structure_user', '`statflow_structure`.id=`statflow_structure_user`.structure_id', array())
                         ->where('`statflow_structure_user`.user_id ='.$user_id);
        return  $adapter->fetchAll($select);
    }
    
    public function getStructureIdbyExternalId($external_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $select = $adapter->select()
                          ->from(array('stu' => 'statflow_structure'),array('id'))
                          ->where('stu.external_id = ?', "$external_id");
        
        $result =  $adapter->fetchAll($select);
        return $result[0]['id'];
    }
    
    public function getExternalIdbyStructureId($structure_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
                          ->from(array('stu' => 'statflow_structure'),array('external_id'))
                          ->where('stu.id = ?', "$structure_id");
    
        $result =  $adapter->fetchAll($select);
        return $result[0]['external_id'];
    }     
       
    public function getAllStructureAttachedByUserId($userId)
    {
        //get Structure attached
        $allStructureAttached = $this->getAllStructureByUser($userId);
        $strStructure = "";
        foreach ($allStructureAttached as $structure){
            $strStructure.= $structure['name'].", ";
        }
    
        $strStructure = substr($strStructure, 0, -2);
        return html_entity_decode(strip_tags($strStructure));
    }
    
    public function exportStructure($root_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        // main request
        $select = $adapter->select();
        $select->from('statflow_structure')
               ->where('id ='.$root_id);
    
        $headers = array(
            'ID',
            'Name',
            'Parent',
            'External ID'
        );
    
        $export = $this->getExport();
        if ($export) {
          $this->exportCSV($adapter, $select, $headers, $root_id);
        }
    }
    
    
    
    
    public function exportCSV($adapter, $select, $headers, $root_id)
    {
        $currentDateTime = date("dmYHis");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition:inline; filename="' . 'Export-Structure_' . $currentDateTime . '.csv"');
        header("Pragma: ");
        header("Cache-Control: ");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
    
        echo implode(',', str_replace(array(
            '|',
            "\r\n",
            "\r",
            "\n",
            "<br>",
            "<br/>",
            "<br />"
        ), array(
            ';',
            ''
        ), $headers)) . "\r\n";
    
        $dataUser = $adapter->fetchAll($select);
        
        // Iterate Each Root
        foreach ($dataUser as $line) {
            $tmpArray = array();
            $tmpArray[] = $line['id'];
            $tmpArray[] = html_entity_decode(strip_tags($line['name']));
            $tmpArray[] = $line['parent_id'];
            $tmpArray[] = $line['external_id'];
            
            echo implode(',', str_replace(array(
                '|',
                "\r\n",
                "\r",
                "\n",
                "<br>",
                "<br/>",
                "<br />"
            ), array(
                ';',
                ''
            ), $tmpArray)) . "\r\n";
            
            // BEGIN OF CHILD
            $structure = $this->getById($line['id']);
            $this->getChildStructure($structure);
            // END OF CHILD
        }
        die();
    }
    
    
    
    public function getChildStructure($structure, $key = null)
    {
        if (is_object($structure)) {
                foreach ($structure->structures as $x => $value) {
                    $tmpChild1 = array();
                    $tmpChild1[] = $value['id'];
                    $tmpChild1[] = html_entity_decode(strip_tags($value['name']));
                    $tmpChild1[] = $value['parent_id'];
                    $tmpChild1[] = $value['external_id'];
                    
                    echo implode(',', str_replace(array(
                        '|',
                        "\r\n",
                        "\r",
                        "\n",
                        "<br>",
                        "<br/>",
                        "<br />"
                    ), array(
                        ';',
                        ''
                    ), $tmpChild1)) . "\r\n";
                    
                    $this->getChildStructure($value, $x);
                }
            } else {
                echo "Key1: $key, value: $structure";
            }
    }
    
      public function getImportStatus()
    {
        return $this->import_status;
    }   
     
    // Fonction d'import avec statut 0: insert, 1:update, 2:error
    public function importCsvStructure($csv_structure)
    {
           
        $this->import_status = 0;        
        $parent_id = $csv_structure[2] == '' ? NULL : $csv_structure[2];
        
//         if ($parent_id) {
//             //ligne en échec, car parent_id introuvable dans la base            
//             if ($this->model->select(true)->where('id = ' . $parent_id)->fetchAll()->count() == 0){
//                 $this->import_status = 2;
//                 return NULL;
//             }
//         }
            
        $id = $csv_structure[0];
        if ($id) {
            $resultSet = $this->model->select()
                        ->where('id = ' . $id)
                        ->fetchAll(); 
            $result = $resultSet->toArray();
            if (empty($result)) {
                //ligne en échec, car id introuvable dans la base
                $this->import_status = 0;
                $data = [
                    'id'            => $csv_structure[0],
                    'name'          => $csv_structure[1],
                    'parent_id'     => $csv_structure[2],
                    'external_id'   => $csv_structure[3]
                ];
                $structure = $this->model->createRow($data);
            }else{
                $structure = $this->model->get(array('id' => $id));
                $this->import_status = 1;
            }
           
        } else {
            $structure = $this->model->createRow();
        }

        $structure->name = $csv_structure[1];
        $structure->parent_id = $parent_id;
        $structure->external_id = $csv_structure[3];            

        return $structure;
    }    
    
    //Fonction de lecture du fichier structure csv
    public function readCSV($csv_file){
        //vérif nombre de colonne
        $column_count = 4;
        //vérif nombre de ligne
        $line_count=2;
        //vérif du header et ne pas le garder
        $row_count=1;
        
        $file_handle = fopen($csv_file, "r");
        while (($single_line = fgetcsv($file_handle, 1024)) !== FALSE) {
            // vérif format du fichier
            if(count($single_line) != $column_count){
                $line_of_text=NULL;
                break;
            }            
            // sauvegarde données csv dans array
            if($row_count > 1) $line_of_text[] = $single_line;             

            $row_count++;
        }
        fclose($file_handle);    

        return $line_of_text;
    }
    
    //Fonction de lecture du fichier log suite à l'import
    public function getImportResult(){
        $result_file = "/tmp/import_structure_log.txt";
        if (file_exists($result_file)){
            $file_handle = fopen($result_file, "r");
            while (($result_line = fgetcsv($file_handle, 1024)) !== FALSE) {
                $result[] = $result_line;
            }
            fclose($file_handle);
            unlink($result_file);
            return $result;
        }
        return NULL;
    }    
    
    public function fetch_recursive($src_arr, $currentid, $parentfound = false, $cats = array())
    {
        foreach($src_arr as $row)
        {
            if((!$parentfound && $row['id'] == $currentid) || $row['parent_id'] == $currentid)
            {
                $rowdata = array();
                foreach($row as $k => $v)
                    $rowdata[$k] = $v;
                $cats[] = $rowdata;
                if($row['parent_id'] == $currentid)
                    $cats = array_merge($cats, $this->fetch_recursive($src_arr, $row['id'], true));
            }
        }
        return $cats;
    }
    
    //recursive function
    public function renderMenu($items,$structureActive = null)
    {
        $render = "<ul>";
        $active = "data-jstree='{}'";
        if ($structureActive) {
            $active = "data-jstree='{ \"selected\" : true }'";
        }

        foreach ($items as $item) {
            if (!empty($item->subs)) {
                $render .= "<li id=\"{$item->external_id}\" ".$active."><a href=\"javascript:;\">" . $item->name . "</a>";
                $render .= $this->renderMenu($item->subs);
                $render .= "</li>";
            }
            else
            {
                $render .= "<li id=".$item->external_id." ".$active." ><a href='javascript:;'>" . $item->name;
                $render .= "</a></li>";
            }
        }
        return $render . "</ul>";
    }
     
    public function getFilterStructure($user_id, $structureActive = null)
    {
        $all_structures = $this->getAll()->toArray();
        $structures_user = $this->getAllStructureOnlyByUser($user_id);
    
        $structures = array();
        $list = array();
        if(!empty($structures_user)){
            foreach ($structures_user as $structure_user) {
                $structures = array_merge($structures, $this->fetch_recursive($all_structures, $structure_user['id']));
            }
      
            foreach ($structures as $structure) {
                if (!in_array($structure, $list))
                {
                    $list[] = $structure;
                }
            }
        
            //index elements by id
            foreach ($list as $item) {
                if(in_array($item, $structures_user))
                {
                    $item['parent_id'] = null;
                }
                 
                $item['subs'] = array();
                $indexedItems[$item['id']] = (object) $item;
            }
        
            //assign to parent
            $topLevel = array();
            foreach ($indexedItems as $item) {
                if ($item->parent_id == null) {
                    $topLevel[] = $item;
                } else {
                    $indexedItems[$item->parent_id]->subs[] = $item;
                }
            }
        
            return $this->renderMenu($topLevel,$structureActive);
        }       
    }
    
    public function getAllChildStructuresByExternalId($external_id)
    {
        $all_structures = $this->getAll()->toArray();
        $id_filter_structure_selected = $this->getStructureIdbyExternalId($external_id);
        
        $structures = array();
        $structures = array_merge($structures, $this->fetch_recursive($all_structures, $id_filter_structure_selected));
         
        $list = array();
        foreach ($structures as $structure) {
            if (!in_array($structure, $list))
            {
                $list[] = $structure['external_id'];
            }
        }
         
        return $list;        
    }
    
    public function getAllChildStructuresById($structure_id)
    {
        $all_structures = $this->getAll()->toArray();
    
        $structures = array();
        $structures = array_merge($structures, $this->fetch_recursive($all_structures, $structure_id));
         
        $list = array();
        foreach ($structures as $structure) {
            if (!in_array($structure, $list))
            {
                $list[] = $structure['id'];
            }
        }
         
        return $list;
    }   
    
    public function getAllChildStructuresByUserId($user_id)
    {
        $all_structures = $this->getAll()->toArray();
        $structures_user = $this->getAllStructureOnlyByUser($user_id);

        $structures = array();
        $list = array();
        if(!empty($structures_user)){
            foreach ($structures_user as $structure_user) {
                $structures = array_merge($structures, $this->fetch_recursive($all_structures, $structure_user['id']));
            }

            foreach ($structures as $structure) {
                if (!in_array($structure, $list))
                {
                    $list[] = $structure['external_id'];
                }
            }
        }
        return $list;
    }    
    
    public function isParent($structure) {
        $data =  $this->model->get(array('external_id' => $structure));
        return (!$data->parent_id)?true:false;
    }
    
    public function getStructureName($structure_belonging, $studyId)
    {
        $result   = [];
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $selectQuery = $adapter->select();
        $selectQuery->from(array('struct' => 'statflow_structure'))
        ->where('struct.external_id = ?',  $structure_belonging);
        $structures =  $adapter->fetchAll($selectQuery);
    
        if (count($structures) == 1) {
            $result = $structures[0];
        }
        else {
            foreach ($structures as $value) {
                $parentId = $this->getParentById($value['id']);
                if ($parentId && $studyId == $this->getStudyByStruct($parentId)) {
                    $result =  $value ;
                    break;
                }
            }
        }
    
        return $result;
    }
    
    public function getStudyByStruct($struct) {
        $result = $this->studyModel->get(['structure_id' => $struct])->toArray();
        return $result['id'];
    }
    
    public function getParentById($id)
    {
        $allStructInStudyTable = [];
        $getAllStudyTable = $this->studyModel->fetchAll()->toArray();
        foreach ($getAllStudyTable as $element) {
            $allStructInStudyTable[] = $element['structure_id'];
        }
        if (in_array($id, $allStructInStudyTable)) {
            return $id;
        }
        else {
            $result = $this->model->get(['id' => $id])->toArray();
            if ($result['parent_id'] == NULL) {
                if (in_array($result['parent_id'], $allStructInStudyTable)) {
                    return $result['id'];
                }
                return false;
            }
            else {
                return $this->getParentById($result['parent_id']);
            }
        }
    
    }
}