<?php
namespace Statflow\Service;

class StudyUser
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
    
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/studyUser');
        }
        $this->model = $model;
    }
    
    public function getAllStudyByUserId($userId)
    {
        return $this->model->select(true)
                            ->where('user_id = ' . $userId)
                            ->fetchAll();
    }
    
    public function getAllFeedbackFormByUserId($userId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select = $adapter->select()
                          ->from(array('CFF'=>'statflow_feedback_form'))
                          ->joinInner(array('CS'=>'statflow_study'),'`CFF`.`study_id` = `CS`.`id`',array())
                          ->joinInner(array('CSU'=>'statflow_study_user'),'`CS`.`id` = `CSU`.`study_id`')
                          ->joinInner(array('CSFF'=>'statflow_user_feedback_form'),'`CFF`.`id` = `CSFF`.`feedback_form_id`')
                          ->where('`CSU`.`user_id` = ' . $userId)
                          ->where('`CSFF`.`user_id` = ' . $userId)
                          ->where('`CFF`.`is_visible` = 1');
        //var_dump($adapter->fetchAll($select))  ;die;                                               
        return $adapter->fetchAll($select);
    }
    
    public function checkRecordExist($userId, $studyId)
    {
        $result =  $this->model->select(true)
                           ->where('user_id = ' . $userId. ' and study_id = '.$studyId)
                            ->fetchAll();
        
        return $result->count();
    }
    
    public function saveStudyUser($id, $studyId)
    { 
        if($this->checkRecordExist($id, $studyId) == 0){     
          $studyUser = $this->model->createRow(array('user_id' => $id,
                                                     'study_id' => $studyId));
    
          $studyUser->save();
          return $studyUser;
        }
    
    }
    

    public function deleteStudyByUserId($user_id)
    {
        
        return $this->model->deleteRow("user_id = ".$user_id);
    
    }
    
    public function deleteStudyByStudyId($study_id)
    {
    
        return $this->model->deleteRow("study_id = ".$study_id);
    
    }
    
    public function getFirstStudyByUser($userId)
    {
        $select = $this->model->select();
        $select->where('user_id = ' . $userId);
        return $this->model->fetchRow($select);
    }
    
    // Delete a list of user id
    public function deleteByListUserId($listid)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        return $adapter->delete('statflow_study_user','user_id in ('.$listid.')');
    }
}

?>