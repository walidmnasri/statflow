<?php
namespace Statflow\Service;

class StructureUser
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
    
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/structureUser');
        }
        $this->model = $model;
    }
    
    public function getAllStructureByUserId($userId)
    {
        return $this->model->select(true)
                            ->where('user_id = ' . $userId)
                            ->fetchAll();
    }
    
    public function saveStructureUser($id, $structureId)
    {
        $structureUser = $this->model->createRow(array(
                                                        'user_id' => $id,
                                                        'structure_id' => $structureId
                                                    ));
    
        $structureUser->save();
        return $structureUser;
    
    }
    
    
    public function deleteStructureByUserId($user_id)
    {
    
        return $this->model->deleteRow("user_id = ".$user_id);
    
    }
    
}

?>