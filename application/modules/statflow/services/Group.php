<?php
namespace Statflow\Service;

class Group
{
    private $model;
    
    public function __construct($model = null)
    {    
        $this->setModel($model);
    }
   

    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('auth/group');
        }
        $this->model = $model;
    }
    
    public function getAllGroup()
    {
        return $this->model->fetchAll();
         
    }
    public function  getGroupInfoByUserId($user_id)
    {
        $select= $this->model->select()
        ->setIntegrityCheck(false)
        ->from(array('Auth_Group' => 'auth_group'))
        ->join(array('Auth_Belong' => 'auth_belong'), 'id=group_id')
        ->where('user_id ='.$user_id);
        return  $this->model->fetchAll($select);
    }
    
    public function getGroupIdByName($group_name)
    {
        $select= $this->model->select()
        ->where('name ='."'".$group_name."'");
        return  $this->model->fetchAll($select);
    }
}

?>