<?php
namespace Statflow\Service;

class QuestionEntry
{

    private $imageForm;
    private $validator;
    private $model;
    private $question;
    private $translation;
    private $transTag;
    private $translationUid;
    private $translationTagUid;
    private $translationService;

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/questionEntry');
        $this->question = \Centurion_Db::getSingleton('statflow/question');
        $this->translationTagUid = \Centurion_Db::getSingleton('translation/tagUid');
        $this->translation = \Centurion_Db::getSingleton('translation/translation');
        $this->transTag = \Centurion_Db::getSingleton('translation/tag');
        $this->translationUid = \Centurion_Db::getSingleton('translation/uid');
    }

    /**
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (!$this->translationService){
            $this->translationService = new Translation();
        }
        return $this->translationService;
    }
    /**
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }
    
    public function addImageForm()
    {
        $this->imageForm = new \Centurion_Form();
        $fileInput = new \Zend_Form_Element_File('image');
        $fileInput->addValidator('Count', false, 1);
        $fileInput->addValidator('Extension', false, array(
            'jpg',
            'jpeg',
            'png'
        ));
        $fileInput->addValidator('Size', array(
            'min' => 0
        ));
        $fileInput->setRequired(true);
        return $this->imageForm->addElement($fileInput);
    }

    public function getValidator($parameters)
    {
        if (! $this->validator) {
            $filters = array(
                '*' => array(
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines()
                )
            );
            $validators = array(
                'question_id' => array(
                    'presence' => 'required',
                    new \Zend_Validate_Int()
                ),
                'entry_type_id' => array(
                    'presence' => 'required',
                    new \Zend_Validate_Int()
                )
            );
            
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator;
    }

    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }
    
    public function getQuestionEntryById($id)
    {
        return $this->model->get(array(
            'id' => $id
        ));
    }
    
    public function getArrayQuestionTypes()
    {
        return array(
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ => 'QUESTION_TYPE_MCQ',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE => 'QUESTION_TYPE_MULTIPLE_CHOICE',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL => 'QUESTION_TYPE_INTERVAL',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED => 'QUESTION_TYPE_ORDERED',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN => 'QUESTION_TYPE_OPEN',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY => 'QUESTION_TYPE_ARRAY',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_IMAGES => 'QUESTION_TYPE_MULTIPLE_IMAGES',
        );
    }
    
    public function getValidated($question_id, $entry_type_id)
    {
        $questionEntry = $this->model->createRow();
        $questionEntry->question_id = $question_id;
        $questionEntry->entry_type_id = $entry_type_id;
    
        return $questionEntry;
    }
    
    public function uploadImage()
    {
        $uploadPath = realpath(
            APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR
            . 'uploads' . DIRECTORY_SEPARATOR . 'modalities'
        );
        
        $upload = new \Zend_File_Transfer_Adapter_Http();
        $upload->setDestination($uploadPath);
        $upload->addFilter('Rename', array('target' => $uploadPath.'/'.$upload->getFileName(null, false), 'overwrite' => true));
        $upload->receive();
        
        return $upload->getFileName(null,false);
    }
    

    public function duplicateModalities($idQuestion, $idQuestionCloned)
    {
         
        $translationService = $this->getTranslationService();
        $modalities = $this->model->fetchAll($this->model->select()->where('question_id = ?',$idQuestion));
        $oldquestionRow = $this->question->fetchRow($this->question->select()->where('id = ?',$idQuestion));
        $newquestionRow = $this->question->fetchRow($this->question->select()->where('id = ?',$idQuestionCloned));
        $tagId= $translationService->getTagId('form_'.$newquestionRow->feedback_form_id);
       
        if (($modalities->count()) > 0){
            foreach ($modalities as $modality) {
                $dataModality = $modality->toArray();
                $dataModality['id']= null;
                $dataModality['created_at']= null;
                $dataModality['updated_at']= null;
                $dataModality['question_id']= $idQuestionCloned;
                $newModality = $this->model->createRow($dataModality);
                $newModality->save();
                                
               //duplicate translations
                $newQuestionTranslationUid = $translationService->insertRowTranslationUid('form__question_'.$newquestionRow->code.'_modality-'.$newModality->id);
                
                $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newQuestionTranslationUid->id,$tagId);
                
                $oldTranslationUidRow= $translationService->getTranslationUidRow('form__question_'.$oldquestionRow->code.'_modality-'.$modality->id);
                if($oldTranslationUidRow){
                    $translationService->duplicateTranslation ($oldTranslationUidRow->id, $newQuestionTranslationUid->id);
                }
                
            }
        }
    }
    
    public function getModalitiesByQuestionId($questionId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_question_entry',array('id'))
                           ->where('question_id = ' . $questionId);
        $results = $adapter->fetchAll($select);
        
        $modalities = array();
        foreach ($results as $result){
            $modalities[] = $result['id'];
        }
        
        return $modalities;        
    }
    
}

?>