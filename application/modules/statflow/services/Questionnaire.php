<?php
namespace Statflow\Service;
require_once APPLICATION_PATH . "/../data/includes/Mobile_Detect.php";
require_once APPLICATION_PATH . "/../data/includes/geoip.inc";
require_once __DIR__ . '/Structure.php';

use Statflow\Service\Structure;
use GuzzleHttp\json_decode;

class Questionnaire
{

    private $model;

    private $respondent;

    private $structure;

    private $token;
    /**
     * @var Structure
     */
    private $structureService;

    public function getConfig()
    {
        return \Zend_Controller_Front::getInstance()->getParam('bootstrap');
    }
    
    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
    
        return $this->structureService;
    }

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->respondent = \Centurion_Db::getSingleton('statflow/respondent');
        $this->structure = \Centurion_Db::getSingleton('statflow/structure');
    }

    /**
     * $this->_getParam('token')
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     *
     * @param string $val            
     */
    public function setToken($val)
    {
        $this->token = $val;
    }

    /**
     *
     * @param integer $formId            
     *
     */
    public function gestionErreur($formId)
    {    
        if ($formId == null)
            die("No form id specified");
        $form = $this->getFormById($formId);
        if ($form == null)
            die("No form matching this id found");
        $now = new \DateTime();
        $error = false;
        $errors = array('error'=>array());
        
        if ($form->study->is_published != true) {
            $errors['error'][] = 'study-not-published';
        }
        if ($form->is_published != true) {
            $errors['error'][] = 'not-published';
        }
        if ($form->publication_start) {
            $pubStart = \DateTime::createFromFormat('Y-m-d H:i:s', $form->publication_start);
            if ($pubStart > $now) {
                $errors['error'][] = 'publication-not-started';
            }
        }
        
        if ($form->publication_end) {
            $pubEnd = \DateTime::createFromFormat('Y-m-d H:i:s', $form->publication_end);
            if ($pubEnd < $now) {
                $errors['error'][] = 'publication-ended';
            }
        }
        
        if ($form->is_open == false && $this->token == false) {
            $errors['error'][] = 'missing-respondent-token';
        }
        
        $data = array(
            'form' => $form->toArray()
        );
        
        $data['user'] = array(
            'id' => 0
        );
        $data['store'] = array(
            'id' => 0
        );
        $structService = $this->getStructureService();
        $respondentToken = $this->token;
        if ($respondentToken) {
            $respondent = $this->respondent->fetchRow('token = ' . \Zend_Db_Table_Abstract::getDefaultAdapter()->quote($respondentToken));
            if ($respondent) {
                    $validity_days = $form->validity_days;
                    $date = new \Zend_Date($respondent->created_at, 'YYYY-MM-dd HH:mm:ss');
                    $date->addDay($validity_days);
                    if($date->isEarlier(\Zend_Date::now())) { $errors['error'][] = 'is_invalid'; }
                    $store = $structService->getStructureName($respondent->structure_belonging, $form->study_id);
                    if (!empty($store)) {
                        $data['store'] = $store;
                        $data['store']['name'] = 'structure__' . ($store['id']) . '__' . \Centurion_Inflector::slugify($store['external_id']) . '@Structures';
                    } else {
                        //$errors['error'][] = 'respondents-structure-not-found';
                        $errors['error'][] = 'bad_structure_id';
                    }
                
                switch ($respondent->status) {
                    case 20:
                        $errors['error'][] = 'already-answered';
                        break;
                    case 10:
                        break;
                    default:
                        $respondent->status = 10;
                        $now = new \DateTime();
                        $respondent->started_answering_at = $now->format('Y-m-d H:i:s');
                        $respondent->save();
                }
                
                $data['user'] = $respondent->toArray();
                $data['answers'] = \Zend_Json::decode($respondent->current_state);
                $data['last_page'] = $respondent->last_page;
                $fields = $respondent->findDependentRowset('Statflow_Model_DbTable_RespondentExternalField')->toArray();
                foreach ($fields as $field) {
                    $data['user'][str_replace(' ', '_', trim($field['label']))] = $field['value'];
                }
            } else {
                $errors['error'][] = 'respondent-not-found';
            }
        }
        if (count($errors['error']) > 0) {
            return $errors;
        }
        else
            return $data;
    }
    
    public function saveQuestionnaire($model,$formId, $headers_infos, $nominative_ignore, $nominative_check)
    {
        
        
        $form = $this->getFormById($formId);
        $respondentToken = $this->token;
        if ($respondentToken) {
            $respondent = $this->respondent->fetchRow('token = ' . \Zend_Db_Table_Abstract::getDefaultAdapter()->quote($respondentToken));
        } else {
            die('Error, no respondent');
        }

        $data = \Zend_Json::decode($model);

        if($respondent->status == 20) die('already answered');
        $now = new \DateTime();
        $respondent->answered_at    = $now->format('Y-m-d H:i:s');
        $questionModel              = \Centurion_Db::getSingleton('statflow/question');
        $answerModel                = \Centurion_Db::getSingleton('statflow/answer');
        $answerEntryModel           = \Centurion_Db::getSingleton('statflow/answerEntry');
        $recoveryModel              = \Centurion_Db::getSingleton('statflow/recovery');
        
        $answers = array();
        $answersRule = array();
        
        $completeAnswers = array(); //for excel
        
        
        foreach($data as $qId => $answerData) {
                       
            $question = $questionModel->fetchRow('id = '.$qId);
        
            $answers[$question->code] = $answerData;
            list($answer, $created) = $answerModel->getOrCreate(array('respondent_id'=>$respondent->id, 'feedback_form_id'=>$form->id, 'question_id'=>$question->id));

            $answerDetail = array(
                'question' => $question,
                'answer' => $answer,
                'answerEntries' => array()
            );
        
            switch($question->question_type_id){
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL:
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                    $answer->value = $answerData;
                    $answer->save();
                    break;
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ:
                    $answers[$question->code] = $answerData[0]['value'];
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE:
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_IMAGES:
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED:
                    if($question->parent_id){
                        $answer->value = $answerData;
                        $answer->save();
                    }else{
                        $answer->save();
                        if(is_array($answerData)){
                            foreach($answerData as $aData){                                
                                list($answerEntry , $created) = $answerEntryModel->getOrCreate(array('question_entry_id' => $aData['value'], 'answer_id' => $answer->id));
                                $answerEntry->content = isset($aData['content']) ? $aData['content']: '';
                                $answerEntry->save();   
                                $answerDetail['answerEntries'][] = $answerEntry;
                            }
                        }
                    }
        
                    break;
                default:
                    $answerDetail['answer'] = null;
            }
            $completeAnswers[$qId] = $answerDetail;
        }

        $solrConfig = \Statflow_Model_Solr::getDefaultConfig();
        $solrConfig = $solrConfig['endpoint']['localhost'];
        $duration = $now->format('U') - \DateTime::createFromFormat('Y-m-d H:i:s',$respondent->started_answering_at)->format('U');
        $respondent->completion_duration = $duration;
        $respondent->status = 20;
        
        $formMeta = \Zend_Json::decode($form->meta);
        $sendMail = false;
        $alertIDs = array();
        $alertLibelles = array();
        $anwserMc = array();
        
        foreach ($answers as $keyAns=>$valAns){
            if (is_array($valAns)){
                foreach ($valAns as $val){
                    $anwserMc[] = $val['value'];
                }
                $answersRule[$keyAns] = $anwserMc;
            }
            else{
                $answersRule[$keyAns] = $valAns;
            }
        }       

        if(isset($formMeta['alerts']) && is_array($formMeta['alerts'])){
            foreach($formMeta['alerts'] as $key => $alertRule){
                if( !isset($alertRule['enabled']) || $alertRule['enabled'] == false ) continue;
                $flagged = false;
                $rule = $alertRule['rule'];

                $rule = preg_replace('/({[^{}]*?}) IN\(\[(.*?)\]\)/',
                    '(is_array($1)) ? (count(array_intersect($1, array($2))) > 0) :  in_array($1, array($2))',
                    $rule);
                
                $rule = preg_replace('/({[^{}]*?}) MATCH\((.*?)\)/',' preg_match(\'#$2#\', $1) === 1 ', $rule);
                $rule = preg_replace('/(\s+AND\s+)/',' && ', $rule);
                $rule = preg_replace('/(\s+OR\s+)/',' || ', $rule);
                $rule = preg_replace('/{([^{}]*?)}/','$answersRule[\'$1\']', $rule);
                $rule.=';';
                $rule = '$flagged = '.$rule;
                
                eval($rule);
                if($flagged){
                    $alertLibelles[] = $alertRule['libelle'];
                    if ($alertRule['send_email']) {
                        $alertIDs[] = $key;
                    }
                }
            }
        }
        
        if (count($alertLibelles) > 0){
            $respondent->alert = implode("/", $alertLibelles);
        }

        //ip address
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
    
        $respondent_ip = $ipaddress;
        
        //host
        $respondent_host='';
        if (getenv('REMOTE_HOST'))
            $respondent_host = getenv('REMOTE_HOST');
        
        //user agent
        $user_agent = '';
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
       
        //determine the country code
        $country_code = '';
        $gi = geoip_open(APPLICATION_PATH . '/../data/includes/GeoIP.dat',GEOIP_STANDARD);
        $country_code = geoip_country_code_by_addr($gi, $respondent_ip);     
        geoip_close($gi);
        
        //determine terminal
        $terminal = '';
        $detect = new \Mobile_Detect();
        $terminal = ($detect->isMobile() ? ($detect->isTablet() ? 'Tablette' : 'Téléphone') : 'Ordinateur');
        
        $respondent->respondent_ip = $respondent_ip;
        $respondent->respondent_host = $respondent_host;
        $respondent->user_agent = $user_agent;
        $respondent->country_code = $country_code;
        $respondent->terminal = $terminal;

        $respondent->save();

        // send voucher
        $externalConf = $this->getConfig()->getOption('external');
        if (in_array($formId, $externalConf['voucher']['questionnaireId'])) {
            file_get_contents($externalConf['voucher_url'] . '/' . $respondent->token);
        }

        file_get_contents('http://'.$solrConfig['host'].':'.$solrConfig['port'].$solrConfig['path']. $formId . '/dataimport?command=delta-import&synchronous=true&respondent_id='.$respondent->id);

        if (count($alertIDs) > 0){
            foreach ($alertIDs as $alertID) {
                $formMetaAlert = $formMeta['alerts'][$alertID];
                $format_pj = isset($formMetaAlert['format_pj']) ?  (($formMetaAlert['format_pj'] != FALSE) ? 1 : 0): '1';
                if ($format_pj == 1 ){
                    $attachementPath = $this->writeAttachement($form, $respondent, $completeAnswers, $form->study_id, $answers, $headers_infos, $alertID, $nominative_ignore, $nominative_check);              
                } else {
                    $attachementPath = $this->writeAttachementXML($form, $respondent, $completeAnswers, $form->study_id, $answers, $headers_infos, $alertID, $nominative_ignore, $nominative_check);                   
                }
                $this->sendMail($form, $respondent, $attachementPath, $alertID, $form->study_id); 
            }
        }
        die();
    }
    
    public function savePage($last_page, $data, $formId, $respondentToken)
    {   
        $form = $this->getFormById($formId);
        $respondentToken = $this->token;
        if ($respondentToken) {
            $respondent = $this->respondent->fetchRow('token = ' . \Zend_Db_Table_Abstract::getDefaultAdapter()->quote($respondentToken));
        }else {
            die('Error, no respondent');
        }
        
        if($respondent->status == 20) die('already answered');
        
        $questionModel = \Centurion_Db::getSingleton('statflow/question');
        $answerModel = \Centurion_Db::getSingleton('statflow/answer');
        $answerEntryModel = \Centurion_Db::getSingleton('statflow/answerEntry');
        $answers = array();

        foreach($data as $qId => $answerData) {
            $question = $questionModel->fetchRow('id = '.$qId);
        
            $answers[$question->code] = $answerData;
            list($answer , $created) = $answerModel->getOrCreate(
                array(
                    'respondent_id'=>$respondent->id,
                    'feedback_form_id'=>$form->id,
                    'question_id'=>$question->id
                )
            );
            $answerDetail = array(
                'question' => $question,
                'answer' => $answer,
                'answerEntries' => array()
            );
            switch($question->question_type_id){
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL:
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                    $answer->value = $answerData;
                    $answer->save();
                    break;
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ:
                    $answers[$question->code] = !empty($answerData['0']['value']) ? $answerData['0']['value'] : "";
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE:
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_IMAGES:
                case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED:
                    if($question->parent_id){
                        $answer->value = $answerData;
                        $answer->save();
                    }else{
                        $answer->save();
                        if(is_array($answerData)){
                            foreach($answer->answer_entrys as $entry)
                                $entry->delete();
                            foreach($answerData as $aData){
                                list($answerEntry , $created) = $answerEntryModel->getOrCreate(array('question_entry_id' => $aData['value'], 'answer_id' => $answer->id));
                                $answerEntry->content = isset($aData['content']) ? $aData['content']: '';
                                $answerEntry->save();
                                $answerDetail['answerEntries'][] = $answerEntry;
                            }
                        }
                    }
        
                    break;
                default:
                    $answerDetail['answer'] = null;
            }
            $completeAnswers[$qId] = $answerDetail;
        }
        
        if($respondent->full_state){
            $respondent->full_state = \Zend_Json::encode(\Zend_Json::decode($respondent->full_state,true) + $data);
        }
        else
        {
            $respondent->full_state = \Zend_Json::encode($data);
        }
        
        $respondent->last_page = $last_page;
        $respondent->current_state = \Zend_Json::encode($data);
        $respondent->save();       
        echo \Zend_Json::encode($data);
        die();
    }
    
    
    function getColumnNominative($formData, $nominative_check, $nominative_ignore)
    {
        $questionRows = [];
        foreach($formData as $answer){
            if($answer['answer'] == null) continue;
            $question = $answer['question'];

            if($question->parent_id){
                if(!isset($formData[$question->parent_id])){
                    $formData[$question->parent_id] = array(
                        'question' => \Centurion_Db::getSingleton('statflow/question')->fetchRow('id = '.$question->parent_id)
                    );
                }
                $excelRow = $answer['answer']->value;
            } else {  
                $entryText= array();
                foreach($answer['answerEntries'] as $entry){
                        $entryText[] = $entry->question_entry_id;
                }
                 $excelRow = $entryText[0];          
            }
            $questionRows[] = $question->code.$excelRow;
        
        }
        
        if(in_array($nominative_check, $questionRows))
            return true;
        else
            return false;
        
    }
    
    protected function writeAttachement($form, $respondent, $formData, $studyId, $modalities, $headers_infos, $alertID, $nominative_ignore, $nominative_check)
    {
        require_once(APPLICATION_PATH . '/../library/PHPExcel/PHPExcel.php');
        require_once(APPLICATION_PATH . '/../library/PHPExcel/PHPExcel/Writer/Excel5.php');

        $formMeta = \Zend_Json::decode($form->meta);
        $formMetaAlert = $formMeta['alerts'][$alertID];       

        $objPHPExcel = new \PHPExcel();
        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->getStyle('A1:C200')
                    ->getNumberFormat()
                    ->setFormatCode( \PHPExcel_Style_NumberFormat::FORMAT_TEXT );
               
        
        $translator = \Zend_Registry::get('Zend_Translate');
        $activeSheet->setCellValueByColumnAndRow(0, 1, $translator->translate('label__study_'.$studyId.'__alerte__piecejointe__profil_repondant@'.$studyId));
                    $activeSheet->mergeCells('A1:C1');
                    $activeSheet->getStyle('A1:C1')
                               ->getAlignment()
                               ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    
                    $activeSheet->getStyle('A1:C1')
                        ->applyFromArray(
                            array(
                                'borders' => array(
                                    'inside' => array(
                                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('argb' => 'FFF'),
                                    ),
                                    'outline' => array(
                                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('argb' => 'FFF'),
                                    )
                                ),
                                'fill' => array(
                                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'C0C0C0')
                                ),
                                'font' => array(
                                    'bold' => true,
                                    'name' => 'Arial',
                                    'size' => 8
                                ),
                                'alignment' => array(
                                    'wrap' => true,
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                )
                            )
                        );
        // respondent data
        $line = 2;
        $respondentData = $respondent->toArray();

        $headersIgnore = $this->getRespondentHeadersIgnore($form->id);
        $headersRespondents = array( 'id',
                                    'answered_at',
                                    'completion_duration',
                                    'locale',
                                    'created_at',
                                    'external_id');

        $external_label = $this->getExternalLabel($respondent->feedback_form_id);
          

        foreach ($headers_infos as $headerKey => $headerValue){
            if ($headerValue == 'respondent_id'){
                $headers_infos[$headerKey] = 'id';
            }
            if ($headerValue == 'answer_date'){
                $headers_infos[$headerKey] = 'answered_at';
            }
            if ($headerValue == 'completion_time'){
                $headers_infos[$headerKey] = 'completion_duration';
            }
            if ($headerValue == 'language'){
                $headers_infos[$headerKey] = 'locale';
            }
            if ($headerValue == 'imported_at'){
                $headers_infos[$headerKey] = 'created_at';
            }
            if ($headerValue == $external_label){
                $headers_infos[$headerKey] = 'external_id';
            }            
        }
        foreach ($headersIgnore as $headerKey => $headerValue){
            if ($headerValue == 'respondent_id'){
                $headersIgnore[$headerKey] = 'id';
            }
            if ($headerValue == 'answer_date'){
                $headersIgnore[$headerKey] = 'answered_at';
            }
            if ($headerValue == 'completion_time'){
                $headersIgnore[$headerKey] = 'completion_duration';
            }
            if ($headerValue == 'language'){
                $headersIgnore[$headerKey] = 'locale';
            }
            if ($headerValue == 'imported_at'){
                $headersIgnore[$headerKey] = 'created_at';
            }
            if ($headerValue == $external_label){
                $headersIgnore[$headerKey] = 'external_id';
            }
        } 

        foreach ($nominative_ignore as $headerKey => $headerValue){
            if ($headerValue == 'respondent_id'){
                $nominative_ignore[$headerKey] = 'id';
            }
            if ($headerValue == 'answer_date'){
                $nominative_ignore[$headerKey] = 'answered_at';
            }
            if ($headerValue == 'completion_time'){
                $nominative_ignore[$headerKey] = 'completion_duration';
            }
            if ($headerValue == 'language'){
                $nominative_ignore[$headerKey] = 'locale';
            }
            if ($headerValue == 'imported_at'){
                $nominative_ignore[$headerKey] = 'created_at';
            }
            if ($headerValue == $external_label){
                $nominative_ignore[$headerKey] = 'external_id';
            }
        }
        
        
        $actions = array('0'=> $translator->translate('label__study_'.$studyId.'__list__index__action-none@'.$studyId),
                         '1'=> $translator->translate('label__study_'.$studyId.'__list__index__action-progress@'.$studyId),
                         '2'=> $translator->translate('label__study_'.$studyId.'__list__index__action-close@'.$studyId),
                         );
        
        $headers = $this->buildQueryHeaders($respondent->feedback_form_id, $respondent->id);
        
        $resultCheck = $this->getColumnNominative($formData, $nominative_check, $nominative_ignore);
        
        if (array_key_exists ("structure_belonging", $respondentData)) {
            $new = array ();
            foreach ($respondentData as $k => $value) {
                if ($k === "structure_belonging") {
                    $new["structure_name"] = $this->getStructureName($value, $studyId);
                }
                $new[$k] = $value;
            }
            $respondentData = $new;
        }
        
        $respondentInfosOrder = array();
        foreach ($headers_infos as $header_info){
            $respondentInfosOrder[$header_info]=$respondentData[$header_info];
        }        
		 	
        $respondentF1data = \Centurion_Db::getSingleton('statflow/respondentExternalField')->fetchAll('respondent_id = '.$respondent->id);
        
        $externalFields = array();
        
        foreach ($respondentF1data as $f1Entry){
            $externalFields[$f1Entry->label] = $f1Entry->value;
        }

        foreach($respondentInfosOrder as $name => $value){		       
                $value = (string) $value;
                if (!in_array($name, $headersIgnore)){       
                     if($resultCheck && in_array($name, $nominative_ignore)){
                         $activeSheet->fromArray(array($name, $translator->translate('label__study_'.$studyId.'__nominative_substitute_text__'.$form->id.'@'.$studyId)), null, 'A'.$line);
                         
                     }else{
                         
                        $name_xml = $name;
                        
                        if ($name=='external_id') {
                            $name = $external_label;
                            $name_xml = $external_label;
                        }
                        else {
                            if ($name == "action"){
                                $value = $actions[$value];
                            }
                            elseif ($name == "structure_name"){
                                $value = $this->getStructureName($respondentInfosOrder['structure_belonging'], $studyId);
                            }
                            elseif ($name == "answered_at") {
                                $value = str_replace(['T', 'Z'], ' ', $value);
                            }
							elseif (array_key_exists($name, $externalFields)){
							    $value = $externalFields[$name];
							}
							else{
							   $name = $translator->translate('label__study_'.$studyId.'__alerte__piecejointe__'.$name.'@'.$studyId);
							}
                        }                                              
                        if(is_numeric($value)) {
                            $value =  '="'.$value.'"';                          
                        }
                        $activeSheet->fromArray(array($name, (string)$value), null, 'A'.$line);	                       
                    }
                    $line++;
                                       
                } 
        }              
        
        $styleArray = array(
                    'borders' => array(
                            'outline' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => 'FFF'),
                            ),
                            'inside' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('argb' => 'FFF'),
                            ),
                        ),
                    'font' => array(
                            'name' => 'Arial',
                            'size' => 8
                        ),
                    'alignment' => array(
                            'wrap' => true,
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                );
                $activeSheet->getStyle('A2:C'.$line)->applyFromArray($styleArray);
                $activeSheet->mergeCells('A'.($line+1).':C'.($line+1));
                $activeSheet->setCellValueByColumnAndRow(0, ($line+1), $translator->translate('label__study_'.$studyId.'__alerte__piecejointe__reponse_du_client@'.$studyId));
        
               $activeSheet->getStyle('A'.($line+1).':C'.($line+1))
                ->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
                $activeSheet->getStyle('A'.($line+1).':C'.($line+1))
                ->applyFromArray(
                        array(
                                'borders' => array(
                                        'inside' => array(
                                                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                            'color' => array('argb' => 'FFF'),
                                        ),
                                        'outline' => array(
                                                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                            'color' => array('argb' => 'FFF'),
                                        )
                                    ),
                            'fill' => array(
                                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'C0C0C0')
                                ),
                                    'font' => array(
                                            'bold' => true,
                                            'name' => 'Arial',
                                            'size' => 8
                                        ),
                                'alignment' => array(
                                        'wrap' => true,
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                )
                                )
                            );
        $line = $line+2;
        $debutQuestion = $line+1;//'A'.($line+1).':B'.($line+1);

        $activeSheet->fromArray(array($translator->translate('label__study_'.$studyId.'__alerte__piecejointe__code@'.$studyId), $translator->translate('label__study_'.$studyId.'__alerte__piecejointe__label@'.$studyId), $translator->translate('label__study_'.$studyId.'__alerte__piecejointe__answer@'.$studyId)), null, 'A'.$line);
        $activeSheet->getStyle('A'.$line.':C'.$line)
        ->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $activeSheet->getStyle('A'.$line.':C'.$line)
        ->applyFromArray(
            array(
                'borders' => array(
                    'inside' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => 'FFF'),
                    ),
                    'outline' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => 'FFF'),
                    )
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C0C0C0')
                ),
                'font' => array(
                    'bold' => true,
                    'name' => 'Arial',
                    'size' => 8
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            )
        );
        $line++;
        
        $questionRows = array();
        $openModalities = array();               
        
        foreach($formData as $answer){
            if($answer['answer'] == null) continue;
            $question = $answer['question'];
            $excelRow = array(
                $question->code,
                strip_tags($translator->translate('form__'.$question->feedback_form_id.'__question_title-'.$question->code.'@Form_'.$question->feedback_form_id))
                //$question->parent_id ? $this->view->translate('form__question_title-'.$question->code.'@Form_'.$question->feedback_form_id) : $this->view->translate('form__'.$question->feedback_form_id.'__question_title-'.$question->code.'@Form_'.$question->feedback_form_id)
            );
           
            if($question->parent_id){
                if(!isset($formData[$question->parent_id])){
                    $formData[$question->parent_id] = array(
                        'question' => \Centurion_Db::getSingleton('statflow/question')->fetchRow('id = '.$question->parent_id)
                    );
                }
                $excelRow[] = strip_tags($translator->translate('form__question_'.($formData[$question->parent_id]['question']->code).'_modality-'.$answer['answer']->value.'@Form_'.$question->feedback_form_id));
            } else {
                switch($question->question_type_id) {
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL:
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                        $excelRow[] = $answer['answer']->value;
                        $openModalities[$question->code] = $question->id;                       
                        break;
                    
                    default:
                        $entryText= array();
                        foreach($answer['answerEntries'] as $entry){
                             if($entry->content && $this->getTypeModality($entry->question_entry_id) == 3) 
                                $entryText[]= strip_tags($translator->translate('form__question_'.$question->code.'_modality-'.$entry->question_entry_id.'@Form_'.$question->feedback_form_id))." : ".$entry->content;
                            else
                                $entryText[] = strip_tags($translator->translate('form__question_'.$question->code.'_modality-'.$entry->question_entry_id.'@Form_'.$question->feedback_form_id));
                        }
                        if(count($entryText)>1){
                            $excelRow[] = implode (' | ',$entryText);
                        }else {
                            $excelRow[] = $entryText[0];
                        }
                }
            }
            $questionRows[$question->code] = $excelRow;

        }
        
        
   
      foreach($headers as $header)
        {
            if(array_key_exists($header, $questionRows)){
                if (!in_array($questionRows[$header][0], $headersIgnore)){                                                                 
                    if($resultCheck && in_array($questionRows[$header][0], $nominative_ignore)){
                        $valueHeader_3 = $translator->translate('label__study_'.$studyId.'__nominative_substitute_text__'.$form->id.'@'.$studyId);
                        if(is_numeric($valueHeader_3)) {
                            $valueHeader_3 = '="'.$valueHeader_3.'"';
                        }
                        $activeSheet->fromArray(array($questionRows[$header][0],$questionRows[$header][1],$valueHeader_3), null, 'A'.$line);                     
                        
                    }else{
                        if(is_numeric($questionRows[$header][2])) {
                            $questionRows[$header][2] = '="'.$questionRows[$header][2].'"';
                        }
                        $activeSheet->fromArray(array($questionRows[$header][0], $questionRows[$header][1], $questionRows[$header][2]), null, 'A'.$line);
                        
                    }
                    $line++;
                }
            }
        }
        
        $styleArray = array(
                    'borders' => array(
                            'outline' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => 'FFF'),
                            ),
                            'inside' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('argb' => 'FFF'),
                            ),
                        ),
                    'font' => array(
                            'name' => 'Arial',
                            'size' => 8
                        ),
                    'alignment' => array(
                            'wrap' => true,
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                );
        
                $activeSheet->getStyle('A'.$debutQuestion.':C'.$line)->applyFromArray($styleArray);
        
                $styleArray = array(
                        'alignment' => array(
                                'wrap' => true,
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                );
                $activeSheet->getStyle('A'.$debutQuestion.':A'.$line)->applyFromArray($styleArray);
                $activeSheet->getStyle('A2:C'.($debutQuestion-1))->getAlignment()->setIndent(2);
                $activeSheet->getStyle('A'.$debutQuestion.':A'.$line)->getAlignment()->setIndent(2);
                foreach ($activeSheet->getRowDimensions() as $rd) { 
                    $rd->setRowHeight(-1); 
                }  

                $activeSheet->getColumnDimension('A')->setWidth(20);
                $activeSheet->getColumnDimension('B')->setWidth(32);
                $activeSheet->getColumnDimension('C')->setWidth(32);
                
                $j=0;
                foreach($headers as $header)
                {
                    if (!in_array($header, $headersIgnore)){
                        
                        
                        if (array_key_exists($header, $modalities)) {
                            
                            if (is_array($modalities[$header])) {
                                foreach ($modalities[$header] as $element) {
                                    if ($this->getColorByModality($element['value'])) {
                                        $styleModalities = array( 'font'  => [ 'name' => 'Arial',
                                            'color' => ['rgb' => str_replace('#', '', $this->getColorByModality($element['value']))],
                                            'size'  => 8 ] );
                                        $activeSheet->getStyle('C'.($debutQuestion + $j))->applyFromArray($styleModalities);
                                    }
                                }
                                
                            }
                            else {
                                if (array_key_exists($header, $openModalities)){
                                    $styleModalities = array( 'font'  => [ 'name' => 'Arial',
                                        'color' => ['rgb' => str_replace('#', '', $this->getColorOpenQuestion($openModalities[$header]))],
                                        'size'  => 8 ] );
                                    $activeSheet->getStyle('C'.($debutQuestion + $j))->applyFromArray($styleModalities);        
                                }
                                else {
                                    $styleModalities = array( 'font'  => [ 'name' => 'Arial',
                                        'color' => ['rgb' => str_replace('#', '', $this->getColorByModality($modalities[$header]))],
                                        'size'  => 8 ] );
                                    $activeSheet->getStyle('C'.($debutQuestion + $j))->applyFromArray($styleModalities);                                      
                                }
                            }
                            $j++;
                        }                    
                        
                    }        
                }
                $activeSheet->getPageSetup()->setPrintArea('A1:C'.$line);
                $activeSheet->getStyle('A1:C'.$line)->getAlignment()->setWrapText(true);
        
        $excelSavePath = APPLICATION_PATH . '/../public/ftp/costumer/alerts/';

        //saving and writting in XML file
        $excelFileName = (isset($formMetaAlert['attachementName']) && $formMetaAlert['attachementName'] ? $formMetaAlert['attachementName'] : 'alert').'-'.$respondent->id.'.xls';
        
        $excelWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);
        $excelWriter->save($excelSavePath . $excelFileName);
        chmod($excelSavePath . $excelFileName, 0777);
        return $excelSavePath . $excelFileName;           
        
    }

    protected function writeAttachementXML($form, $respondent, $formData, $studyId, $modalities, $headers_infos, $alertID, $nominative_ignore, $nominative_check)
    {
        $translator = \Zend_Registry::get('Zend_Translate');
        
        $formMeta = \Zend_Json::decode($form->meta);
        $formMetaAlert = $formMeta['alerts'][$alertID];
    
        $format_pj = isset($formMetaAlert['format_pj']) ?  (($formMetaAlert['format_pj'] != FALSE) ? 1 : 0): '1';
        $attribut_xml = isset($formMetaAlert['attribut_xml']) && $formMetaAlert['attribut_xml'] ? $formMetaAlert['attribut_xml'] : '';
        $all_attribut_xml = explode("=",$attribut_xml);
        $attribut_xml_name =  $all_attribut_xml[0];
        $attribut_xml_value = $all_attribut_xml[1];
    
        $xml = new \DOMDocument("1.0","UTF-8");
        $xml->formatOutput = true;
        $questionnaire = $xml->createElement("questionnaire");
        $xml->appendChild($questionnaire);
        $questionnaire->setAttribute($attribut_xml_name, $attribut_xml_value);
        $questionnaire->setAttribute("id", $form->id);
        $Champ_System =$xml->createElement("champs_systeme");
        $questionnaire->appendChild($Champ_System);
        $Champ_Crm = $xml->createElement("champs_crm");
        $questionnaire->appendChild($Champ_Crm);
        $Champ_questionnaire = $xml->createElement("champs_questionnaire");
        $questionnaire->appendChild($Champ_questionnaire);
        $respondentData = $respondent->toArray();
    
        $headersIgnore = $this->getRespondentHeadersIgnore($form->id);
        $headersRespondents = array( 'id',
            'answered_at',
            'completion_duration',
            'locale',
            'created_at',       
           'external_id');    
            $external_label = $this->getExternalLabel($respondent->feedback_form_id);
              
    
            foreach ($headers_infos as $headerKey => $headerValue){
                if ($headerValue == 'respondent_id'){
                    $headers_infos[$headerKey] = 'id';
                }
                if ($headerValue == 'answer_date'){
                    $headers_infos[$headerKey] = 'answered_at';
                }
                if ($headerValue == 'completion_time'){
                    $headers_infos[$headerKey] = 'completion_duration';
                }
                if ($headerValue == 'language'){
                    $headers_infos[$headerKey] = 'locale';
                }
                if ($headerValue == 'imported_at'){
                    $headers_infos[$headerKey] = 'created_at';
                }
                if ($headerValue == $external_label){
                    $headers_infos[$headerKey] = 'external_id';
                }            
            }
            foreach ($headersIgnore as $headerKey => $headerValue){
                if ($headerValue == 'respondent_id'){
                    $headersIgnore[$headerKey] = 'id';
                }
                if ($headerValue == 'answer_date'){
                    $headersIgnore[$headerKey] = 'answered_at';
                }
                if ($headerValue == 'completion_time'){
                    $headersIgnore[$headerKey] = 'completion_duration';
                }
                if ($headerValue == 'language'){
                    $headersIgnore[$headerKey] = 'locale';
                }
                if ($headerValue == 'imported_at'){
                    $headersIgnore[$headerKey] = 'created_at';
                }
                if ($headerValue == $external_label){
                    $headersIgnore[$headerKey] = 'external_id';
                }
            } 
    
            foreach ($nominative_ignore as $headerKey => $headerValue){
                if ($headerValue == 'respondent_id'){
                    $nominative_ignore[$headerKey] = 'id';
                }
                if ($headerValue == 'answer_date'){
                    $nominative_ignore[$headerKey] = 'answered_at';
                }
                if ($headerValue == 'completion_time'){
                    $nominative_ignore[$headerKey] = 'completion_duration';
                }
                if ($headerValue == 'language'){
                    $nominative_ignore[$headerKey] = 'locale';
                }
                if ($headerValue == 'imported_at'){
                    $nominative_ignore[$headerKey] = 'created_at';
                }
                if ($headerValue == $external_label){
                    $nominative_ignore[$headerKey] = 'external_id';
                }
            }
            
            $actions = array('0'=> $translator->translate('label__study_'.$studyId.'__list__index__action-none@'.$studyId),
                             '1'=> $translator->translate('label__study_'.$studyId.'__list__index__action-progress@'.$studyId),
                             '2'=> $translator->translate('label__study_'.$studyId.'__list__index__action-close@'.$studyId),
                             );
            
            $headers = $this->buildQueryHeaders($respondent->feedback_form_id, $respondent->id);
            
            $resultCheck = $this->getColumnNominative($formData, $nominative_check, $nominative_ignore);
            
            if (array_key_exists ("structure_belonging", $respondentData)) {
                $new = array ();
                foreach ($respondentData as $k => $value) {
                    if ($k === "structure_belonging") {
                        $new["structure_name"] = $this->getStructureName($value, $studyId);
                    }
                    $new[$k] = $value;
                }
                $respondentData = $new;
            }           
    		
            foreach($respondentData as $name => $value){		       
                    $value = (string) $value;
                    if (in_array($name, $headers_infos) && !in_array($name, $headersIgnore)){       
                         if($resultCheck && in_array($name, $nominative_ignore)){
                             $trans = $translator->translate('label__study_'.$studyId.'__nominative_substitute_text__'.$form->id.'@'.$studyId);
                             
                             $New_Node = $xml->createElement($name,$trans);
                             $Champ_System->appendChild($New_Node);
                         }else{
                             
                            $name_xml = $name;
                            
                            if ($name=='external_id') {
                                $name = $external_label;
                                $name_xml = $external_label;
                            }
                            else {
                                if ($name == "action"){
                                    $value = $actions[$value];
                                }
                                elseif ($name == "structure_name"){
                                    $value = $this->getStructureName($respondentData['structure_belonging'], $studyId);
                                }
                                elseif ($name == "answered_at") {
                                    $value = str_replace(['T', 'Z'], ' ', $value);
                                }
    							
    							   $name = $translator->translate('label__study_'.$studyId.'__alerte__piecejointe__'.$name.'@'.$studyId);
                            }						
                            
                            $New_Node = $xml->createElement($name_xml,$value);
                            $Champ_System->appendChild($New_Node);
                        }               
                    } 
            }
    		
    		$respondentF1data = \Centurion_Db::getSingleton('statflow/respondentExternalField')->fetchAll('respondent_id = '.$respondent->id);
            
            $externalFields = array();
            
            foreach ($respondentF1data as $f1Entry){
                $externalFields[$f1Entry->label] = $f1Entry->value;
            }
            

            foreach($externalFields as $label=>$value){
                $value = (string) $value;
                if (!in_array($label, $headersIgnore)){
                     if($resultCheck && in_array($label, $nominative_ignore)){
                         $f1Entry_value = $translator->translate('label__study_'.$studyId.'__nominative_substitute_text__'.$form->id.'@'.$studyId);
                         if(is_numeric($f1Entry_value)) {
                             $f1Entry_value = '="'.$f1Entry_value.'"';
                         }
     
                         $crm = $xml->createElement('crm');
                         $Champ_Crm->appendChild($crm);
                         $labelCRM = $xml->createElement('label',$label);
                         $crm->appendChild($labelCRM);
                         $valueCRM = $xml->createElement('value',$f1Entry_value);
                         $crm->appendChild($valueCRM);
                          
                     }else{
                        if(is_numeric($value)) {
                             $value = '="'.$value.'"';
                        }
    
                        $crm = $xml->createElement('crm');
                        $Champ_Crm->appendChild($crm);
                        $labelCRM = $xml->createElement('label',$label);
                        $crm->appendChild($labelCRM);
                        $valueCRM = $xml->createElement('value',$value);
                        $crm->appendChild($valueCRM);                    
                     }
                }
            }
            
            $questionRows = array();
            $openModalities = array();                      
            
            foreach($formData as $answer){
                if($answer['answer'] == null) continue;
                $question = $answer['question'];
                $excelRow = array(
                    $question->code,
                    strip_tags($translator->translate('form__'.$question->feedback_form_id.'__question_title-'.$question->code.'@Form_'.$question->feedback_form_id))
                    //$question->parent_id ? $this->view->translate('form__question_title-'.$question->code.'@Form_'.$question->feedback_form_id) : $this->view->translate('form__'.$question->feedback_form_id.'__question_title-'.$question->code.'@Form_'.$question->feedback_form_id)
                );
                 
                if($question->parent_id){
                    if(!isset($formData[$question->parent_id])){
                        $formData[$question->parent_id] = array(
                            'question' => \Centurion_Db::getSingleton('statflow/question')->fetchRow('id = '.$question->parent_id)
                        );
                    }
                    $excelRow[] = strip_tags($translator->translate('form__question_'.($formData[$question->parent_id]['question']->code).'_modality-'.$answer['answer']->value.'@Form_'.$question->feedback_form_id));
                } else {
                    switch($question->question_type_id) {
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL:
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                            $excelRow[] = $answer['answer']->value;
                            $openModalities[$question->code] = $question->id;
                            break;
            
                        default:
                            $entryText= array();
                            foreach($answer['answerEntries'] as $entry){
                                if($entry->content && $this->getTypeModality($entry->question_entry_id) == 3)
                                    $entryText[]= strip_tags($translator->translate('form__question_'.$question->code.'_modality-'.$entry->question_entry_id.'@Form_'.$question->feedback_form_id))." : ".$entry->content;
                                else
                                    $entryText[] = strip_tags($translator->translate('form__question_'.$question->code.'_modality-'.$entry->question_entry_id.'@Form_'.$question->feedback_form_id));
                            }
                            if(count($entryText)>1){
                                $excelRow[] = implode (' | ',$entryText);
                            }else {
                                $excelRow[] = $entryText[0];
                            }
                    }
                }
                $questionRows[$question->code] = $excelRow;
            
            }
            
            foreach($headers as $header)
            {
                if(array_key_exists($header, $questionRows)){
                    if (!in_array($questionRows[$header][0], $headersIgnore)){             
                        //question id     
                        $qid = $this->getQuestionIdByCode($form->id, $questionRows[$header][0]);
            
                        //Color by modalities
                        if($openModalities[$questionRows[$header][0]]){
            
                            //answer id
                            $aid = $openModalities[$questionRows[$header][0]];
            
                            if (is_array($aid)){
                                $aid = $aid[0]['value'];
                            }
                        }elseif($modalities[$questionRows[$header][0]]){
            
                            $aid = $modalities[$questionRows[$header][0]];
            
                            if (is_array($aid)){
                                $aid = $aid[0]['value'];
                            }
                        }else{
            
                            $aid = "";
                        }
            
                        if ($this->getColorByModality($aid)) {
                            $color = $this->getColorByModality($aid);
                        }elseif($this->getColorOpenQuestion($aid)){
                            $color = $this->getColorOpenQuestion($aid);
                        }else{
                            $color = "";
                        }
                    
                        if($resultCheck && in_array($questionRows[$header][0], $nominative_ignore)){
                            $valueHeader_3 = $translator->translate('label__study_'.$studyId.'__nominative_substitute_text__'.$form->id.'@'.$studyId);
                            $que = $xml->createElement('question');
                            $Champ_questionnaire->appendChild($que);
                            $id = $xml->createElement('id',$qid);
                            $que->appendChild($id);
                            $code = $xml->createElement('code',$questionRows[$header][0]);
                            $que->appendChild($code);
                            $couleur = $xml->createElement('couleur',$color);
                            $que->appendChild($couleur);
                            $title = $xml->createElement('title',$questionRows[$header][1]);
                            $que->appendChild($title);
                            $reponse = $xml->createElement('answer',$valueHeader_3);
                            $que->appendChild($reponse);
            
                        }else{         
                            $que = $xml->createElement('question');
                            $Champ_questionnaire->appendChild($que);
                            $id = $xml->createElement('id',$qid);
                            $que->appendChild($id);
                            $code = $xml->createElement('code',$questionRows[$header][0]);
                            $que->appendChild($code);
                            $couleur = $xml->createElement('couleur',$color);
                            $que->appendChild($couleur);
                            $title = $xml->createElement('title',$questionRows[$header][1]);
                            $que->appendChild($title);
                            $reponse = $xml->createElement('answer',$questionRows[$header][2]);
                            $que->appendChild($reponse);
                        }
                    }
                }
            }
                        
            $excelSavePath = APPLICATION_PATH . '/../public/ftp/costumer/alerts/';
            //saving and writting in XML file
            $xmlFilename =(isset($formMetaAlert['attachementName']) && $formMetaAlert['attachementName'] ? $formMetaAlert['attachementName'] : 'alert').'-'.$respondent->id.'.xml';;
            $xml ->save($excelSavePath."$xmlFilename");
            chmod($excelSavePath . "$xmlFilename", 0777);            
            return $excelSavePath . $xmlFilename;		
        }
    
    protected function sendMail($form, $respondent, $attachementPath, $alertID, $studyId)
    {
        $structService = $this->getStructureService();
        $translator = \Zend_Registry::get('Zend_Translate');
        $formMeta = \Zend_Json::decode($form->meta);
        $formMetaAlert = $formMeta['alerts'][$alertID];
        $recipients = isset($formMetaAlert['alertRecipients']) && is_array($formMetaAlert['alertRecipients']) ? $formMetaAlert['alertRecipients'] : array();
    
        $html_subject = $translator->translate('alert_'.$alertID.'_email_subject_form_'.$form->id.'@Form_'.$form->id);
        $html_body = $translator->translate('alert_'.$alertID.'_email_body_form_'.$form->id.'@Form_'.$form->id);
        $txt = $translator->translate('alert_'.$alertID.'_email_body-text_form_'.$form->id.'@Form_'.$form->id);
        
        // if no recipients, quit
        if( count($recipients) == 0) return;
        $Structure = $structService->getStructureName($respondent->structure_belonging, $studyId);
        $respondentStructure = $this->structure->fetchRow("id = ".$Structure['id']);
        //match email recipients with users attached to respondents structure and/or parents recursively
        do {
            $structureUsers = $respondentStructure->users;
            foreach($structureUsers as $user){
                if(in_array($user->email, $recipients)) $finalRecipients[] = $user->email;
            }
            $respondentStructure = $respondentStructure->parent;
        } while($respondentStructure != null);   

        if( count($finalRecipients)){

            $smtpHost = 'pro1.mail.ovh.net';
            $smtpConf = array(
                'port' => '587',
                'ssl' => 'tls',
                'auth' => 'login',
                'username' => 'webmaster@it-prospect.com',
                'password' => 'itprospect@@',
                'timeout' => 60
            );
            $transport = new \Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
    
            $mail = new \Zend_Mail('UTF-8');
            $mail->setType(\Zend_Mime::MULTIPART_RELATED);          
            $mail->setFrom( isset($formMetaAlert['senderEmail']) ? $formMetaAlert['senderEmail']  : 'webmaster@it-prospect.com',
                            isset($formMetaAlert['senderName']) ? $formMetaAlert['senderName'] : 'StatFlow');
   
            foreach($finalRecipients as $recipient)
                $mail->addBcc($recipient);
            
            $attachementContents = file_get_contents($attachementPath);
            $attachement = $mail->createAttachment($attachementContents);
            $attachement->filename = basename($attachementPath);
    
            $mail->setSubject( $html = $html_subject );
    
            $html = $html_body;
    
            $mail->setBodyHtml($html);
            $mail->setBodyText($txt);
    
            try{
                $mail->send($transport);
            } catch (\Exception $e){
                \Zend_Debug::dump($e->getMessage());
                \Zend_Debug::dump($e->getTraceAsString());
            }
        }
    }
    
    public function getFormById($formId)
    {
        return $this->model->fetchRow("id = $formId");
    }

    public function addRespondent($formId)
    {
        $form = $this->getFormById($formId);
        $respondent = $this->respondent->createRow();
        $token = sha1(uniqid());
        $respondent->token = $token;
        $respondent->status = 0;
        $respondent->feedback_form_id = $form->id;
        $respondent->study_id = $form->study->id;
        $respondent->email = "form__{$form->id}__email__open__$token";
        $respondent->firstname = "form__{$form->id}__firstname__open__$token";
        $respondent->lastname = "form__{$form->id}__lastname__open__$token";
        $respondent->external_id = "form__{$form->id}__external_id__open__$token";
        $respondent->save();
    }

    public function getAllPageByForm($form)
    {
        return $form->findDependentRowset('Statflow_Model_DbTable_FeedbackFormPage', 'feedback_form', $this->model->select()
            ->order('order ASC'));
    }

    public function getAllPageByQuestion($page)
    {
        return $page->findDependentRowset('Statflow_Model_DbTable_Question', null, $this->model->select()
            ->where('parent_id is null')
            ->order('order ASC'));
    }
    
    public function getAllSubQuestion($question)
    {
        return $question->findDependentRowset('Statflow_Model_DbTable_Question');
    }
       
    public function getAllQuestionEntry($question)
    {
        return $question->findDependentRowset('Statflow_Model_DbTable_QuestionEntry');
    }
    
    public function getExternalLabel($feedback_form_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $selectExternal_id = $adapter->select();
                   $selectExternal_id->from('statflow_feedback_form',array('external'=>'external'))
                                     ->where('statflow_feedback_form.id = ?',  $feedback_form_id)
                                     ->limit(1);
        return $adapter->fetchOne($selectExternal_id);        
    }
    /**
     *
     * @param integer $feedback_form_id
     * @param string $serverUrl
     * @return multitype:unknown \Zend_Paginator multitype: multitype:string NULL  multitype:NULL  multitype:string NULL Ambigous <multitype:, multitype:mixed >
     */
    public function buildQueryHeaders($feedback_form_id, $respondent_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();

        $feedbackFormModel = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $feedbackForm = $feedbackFormModel->get(array(
            'id' => $feedback_form_id
        ));
    
        // main request
        $select = $adapter->select();
        $select->from('statflow_respondent')
        ->reset(\Zend_Db_Select::COLUMNS)
        ->where('statflow_respondent.feedback_form_id = ?', $feedback_form_id)
        ->where('statflow_respondent.id = ?', $respondent_id);
    
        $headers = array();
        foreach ($feedbackForm->feedback_form_pages as $feedback_form_page) {
            foreach ($feedback_form_page->questions as $question) {
                if ($question->parent_id != null) {
                    if(!$question->parent) {
                        continue;
                    }
                    $headers[] = $question->code;
                } else {
                    switch ($question->question_type_id) {
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY:
                            break;
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ: // Q1 Q7 Q8 Q12
                            $headers[] = $question->code;
                            foreach ($question->question_entrys as $modality) {
                                if ($modality->entry_type_id == \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT) {
                                    $headers[] = $question->code . '-Content';
                                }
                            }
                            break;
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE:
                            $headers[] = $question->code;
                            break;
    
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED:
                            $meta = \Zend_Json::decode($question->meta);
                            if (! isset($meta['max']))
                                $meta['max'] = 0;
                            for ($i = 0; $i < $meta['max']; $i ++) {
                                $headers[] = $question->code . '-' . ($i + 1);
                            }
                            break;
                        case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                            $headers[]                               = $question->code;
                            break;
                    }
                }
            }
        }
    
        // View data
        return $headers;
    }
    
    public function getColorByModality($idModality=null) {
        
         if(!$idModality) {
             return '';
         }
        
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $selectQuery = $adapter->select();
        $selectQuery->from('statflow_question_entry',array('complement_data'=>'complement_data'))
                    ->where('statflow_question_entry.id = ?',  $idModality)
                    ->limit(1);
        $result =  $adapter->fetchOne($selectQuery);
        if ($result) {
            $complement_data = $result ? json_decode($result) : array ();
            return isset($complement_data->colourCode) ? $complement_data->colourCode : '';
        }
        else {
            return '';
        }   
    }
    
    public function getColorOpenQuestion($idQuestion) {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $selectQuery = $adapter->select();
        $selectQuery->from('statflow_question',array('meta'=>'meta'))
                    ->where('statflow_question.id = ?',  $idQuestion)
                    ->limit(1);
        $result =  $adapter->fetchOne($selectQuery);
    
        if ($result) {
            $meta = $result ? json_decode($result) : array ();
            return isset($meta->colourCode) ? $meta->colourCode : '';
        }
        else {
            return '';
        }
    }

    public function getStructureName($structure_belonging, $studyId) {
        
        $stuctService = $this->getStructureService();
        $structure =  $stuctService->getStructureName($structure_belonging, $studyId);
        return (isset($structure['name']))? $structure['name']:'-';
    }
    
    public function getRespondentHeadersIgnore($feedbackFormId) {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();

        $selectQuery = $adapter->select();
                   $selectQuery->from('statflow_respondent_field_hidden', array('respondent_field'=>'respondent_field'))
                               ->where('statflow_respondent_field_hidden.feedback_form_id = ?',  $feedbackFormId);
        $result =  $adapter->fetchAll($selectQuery);
    
        $data = array();
        foreach ($result as $field){
            $data[] = $field['respondent_field'];
        }
        
        return $data;
    }
    
   public function getQuestionIdByCode($feedbackFormId, $code){
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_question',array('id'=>'id'))
                           ->where('feedback_form_id = ?',$feedbackFormId)
                           ->where('code = ?',$code);
        $question_id = $adapter->fetchOne($select);
        return $question_id;
    }
    
    public function getTypeModality($id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $selectQuery = $adapter->select();
        $selectQuery->from('statflow_question_entry', array('type'=>'entry_type_id'))
        ->where('id = ?',  $id);
        $result =  $adapter->fetchOne($selectQuery);
        return $result;
    
    }
    
}

