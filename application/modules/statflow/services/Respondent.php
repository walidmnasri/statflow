<?php
namespace Statflow\Service;

class Respondent
{

    private $models = array();
    
    private $status;
    
    private $respondent_id ;
    
    private $from;
    
    private $to;
    
    private $structure;
    
    private $export;
    
    private $page;
    
    private $alert;
    
    /**
     * set $status
     */
    public function setStatus($val)
    {
        $this->status = $val;
    }
    
    /**
     * get $status
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * set $respondent_id
     */
    public function setRespondentId($val)
    {
        $this->respondent_id = $val;
    }
    
    /**
     * get $respondent_id
     */
    public function getRespondentId()
    {
        return $this->respondent_id;
    }
    
    /**
     * set $from
     */
    public function setFrom($val)
    {
        $this->from = $val;
    }
    
    /**
     * get $from
     */
    public function getFrom()
    {
        return $this->from;
    }
    
    /**
     * set $to
     */
    public function setTo($val)
    {
        $this->to =$val;
    }
    
    /**
     * get $to
     */
    public function getTo()
    {
        return $this->to;
    }
    
    /**
     * set $structure
     */
    public function setStructure($val)
    {
        $this->structure = $val;
    }
    
    /**
     * get $structure
     */
    public function getStructure()
    {
        return $this->structure;
    }
    
    /**
     * set $structure
     */
    public function setAlert($val)
    {
        $this->alert = $val;
    }
    
    /**
     * get $structure
     */
    public function getAlert()
    {
        return $this->alert;
    }
    
    /**
     * 
     * set $page
     */
    public function setPage($val)
    {
        $this->page = $val;
    }
    
    /**
     * get $page
     */
    public function getPage()
    {
        return $this->page;
    }
    
    /**
     *
     * set $export
     */
    public function setExport($val)
    {
        $this->export = $val;
    }
    
    /**
     * get $export
     */
    public function getExport()
    {
        return $this->export;
    }
    /**
     * __conctruct()
     */
    public function __construct()
    {
        $this->models['structure'] = \Centurion_Db::getSingleton('statflow/structure');
        $this->models['study'] = \Centurion_Db::getSingleton('statflow/study');
        $this->models['respondent'] = \Centurion_Db::getSingleton('statflow/respondent');
        $this->models['answer'] = \Centurion_Db::getSingleton('statflow/answer');
        $this->models['question'] = \Centurion_Db::getSingleton('statflow/question');
        $this->models['feedbackForm'] = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->models['feedbackFormPage'] = \Centurion_Db::getSingleton('statflow/feedbackFormPage');
    }
    
    /**
     * 
     * @param object $adapter
     * @param object $select
     * @param object $modalityQuestionsCodes
     * @param unknown $openQuestions
     * @param integer $feedback_form_id
     * @param integer $page_number
     * @param unknown $serverUrl
     */
    public function export($adapter, $select, $modalityQuestionsCodes, $openQuestions, $feedback_form_id, $serverUrl)
    {
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition:inline; filename="' . 'Export-' . time() . '.csv"');
        header("Pragma: ");
        header("Cache-Control: ");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $paginator = new \Zend_Paginator(new \Zend_Paginator_Adapter_DbSelect($select));
        $paginator->setCurrentPageNumber($this->getPage());
        $paginator->setItemCountPerPage(1);
        $headers = array();
        foreach ($paginator as $line)
            foreach ($line as $header => $unused)
                $headers[] = $header;
        
        echo implode('|', str_replace(array(
            '|',
            "\r\n",
            "\r",
            "\n",
            "<br>",
            "<br/>",
            "<br />"
        ), array(
            ';',
            ''
        ), $headers)) . "\r\n";
        $respondentCount = 0;
        $page = 1;
        $totalItemCount = $paginator->getTotalItemCount();
        unset($paginator);
        
        while ($respondentCount < $totalItemCount) {
            $tmpSelect = clone ($select);
            $tmpSelect->limit(1000, ($page > 1) ? $respondentCount : null);
            $respondentCount += 1000;
            $data = $adapter->fetchAll($tmpSelect);
            unset($tmpSelect);
            if (count($data) > 0) {
                foreach ($data as $line) {
                    $tmpArray = array();
                    foreach ($line as $code => $value) {
                        if ($code == 'status') {
                            switch ($value) {
                                case 0:
                                    $tmpArray[] = 'Imported';
                                    break;
                                case 1:
                                    $tmpArray[] = 'Exported';
                                    break;
                                case 10:
                                    $tmpArray[] = 'Pending';
                                    break;
                                case 20:
                                    $tmpArray[] = 'Answered';
                                    break;
                            }
                        } else 
                            if ($code == 'token') {
                                $tmpArray[] = sprintf($serverUrl . '/statflow/form/%s' . '/token/' . $value, $feedback_form_id);
                            } else 
                                if (isset($modalityQuestionsCodes[$code]) && ! in_array($code, $openQuestions)) {
                                    if (preg_match('#\|#', $value)) {
                                $tmpArray[] = $this->displayValue($modalityQuestionsCodes[$code], $value, array(), array(), 10000, true);
                            }
                            else
                                $tmpArray[] = $this->displayModalityLabel($modalityQuestionsCodes[$code], $value);
                        }
                        else $tmpArray[] = html_entity_decode(strip_tags($value));
                    }
                    echo implode('|', str_replace(array(
                        '|',
                        "\r\n",
                        "\r",
                        "\n",
                        "<br>",
                        "<br/>",
                        "<br />"
                    ), array(
                        ';',
                        ''
                    ), $tmpArray)) . "\r\n";
                }
            }
            gc_collect_cycles();
            $page ++;
        }
        die();
    }
    
    public function displayValue($code, $value, $modalityQuestions = array(), $openQuestions = array(), $limitCrop = 15, $export = false)
    {
        if(preg_match('#\|#', $value)){
            $values = explode('|', $value);
            $texts = array();
            foreach($values as $v){
                $modalityText = $this->displayModalityLabel($code, $v);
                if(strlen($modalityText) > $limitCrop) {
                    $texts[] =  '<a href="#" title="'. $modalityText  .'">'. strip_tags($this->simpleTextCrop($modalityText, $limitCrop)) .'</a>';
                }
                else
                    $texts[] = $modalityText;
            }
            if($export) {
                return implode(', ', $texts);
            }
            return implode('<br>', $texts);
    
        } else {
            if(in_array($code, $modalityQuestions)) {
                $modalityText = $this->displayModalityLabel($code, $value);
                if(strlen($modalityText) > $limitCrop) {
                    return '<a href="#" title="'. $modalityText  .'">'. strip_tags($this->simpleTextCrop($modalityText, $limitCrop)) .'</a>';
                }
                else
                    return $modalityText;
            }
            if(strlen($value) > $limitCrop) {
                return '<a href="#" title="'. $value  .'">'. strip_tags($this->simpleTextCrop($value, $limitCrop)) .'</a>';
            }
            return $value;
        }
    }
    
    public function displayModalityLabel($code, $modality_id)
    {
        $translator = \Zend_Registry::get('Zend_Translate');
        if($modality_id == null || $modality_id == '')
            return '';
        return $translator->translate('form__question_' . $code . '_modality-' . $modality_id);
    }
    
    public function simpleTextCrop($text, $limit)
    {
        $cleanText = html_entity_decode(strip_tags($text), null, 'UTF-8');
    
        $len = mb_strlen($cleanText);
    
        if ($len > $limit) {
            return mb_substr(strip_tags($text), 0, $limit, 'UTF-8') . '...';
        }
        return $cleanText;
    }
    
    public function displayMysqlDate($mysqldate)
    {
        if (empty($mysqldate) || $mysqldate == '0000-00-00 00:00:00')
            return '';
    
        $date = new \Zend_Date($mysqldate, 'YYYY-MM-dd HH:mm:ss');
        $formatedDate = $date->get(\Zend_Date::DATE_MEDIUM);
        unset($date);
        return $formatedDate;
    }
    
    
    /**
     * 
     * @param integer $feedback_form_id
     * @param string $serverUrl
     * @return multitype:unknown \Zend_Paginator multitype: multitype:string NULL  multitype:NULL  multitype:string NULL Ambigous <multitype:, multitype:mixed >
     */
    public function buildQuery($feedback_form_id, $serverUrl)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $feedbackForm = $this->models['feedbackForm']->get(array(
            'id' => $feedback_form_id
        ));
        
        // main request
        $select = $adapter->select();
        $select->from('statflow_respondent')
            ->reset(\Zend_Db_Select::COLUMNS)
            ->where('statflow_respondent.feedback_form_id = ?', $feedback_form_id);
        
        $status             = $this->getStatus();
        $respondent_id      = $this->getRespondentId();
        $from               = $this->getFrom();
        $to                 = $this->getTo();
        $struct             = $this->getStructure();
        $alert              = $this->getAlert();
        
        $postStructures = array();
        
        if ($struct != false) {
            foreach (explode(',', $struct) as $structure) {
                $structureParts     = explode(': ', $structure);
                $postStructures[]   = $structureParts[0];
            }
            if (count($postStructures) > 0) {
                $select->where('statflow_respondent.structure_belonging IN ("' . join('","', $postStructures) . '")');
            }
        }
        if ($alert == 1) 
            $select->where('statflow_respondent.alert IS NOT NULL');
        elseif($alert == 0)
            $select->where('statflow_respondent.alert  IS  NULL');
        //$this->view->associated_json_structures = $postStructures;
        
        if ($status !== false && $status !== '') {
            $select->where('statflow_respondent.status = ?', $status);
        }
        if ($respondent_id !== false && $respondent_id !== '') {
            $select->where('statflow_respondent.id = ?', $respondent_id);
        }
        
        $formatedDate = 'dd/MM/yyyy';
        if ($from != '' || $to != '') {
            if ($from != '' && $to != '') {
                $dateFrom = new \Zend_Date($from, $formatedDate);
                $dateTo = new \Zend_Date($to, $formatedDate);
                $select->where('statflow_respondent.answered_at >= ?', $dateFrom->get('yyyy-MM-dd') . ' 00:00:00');
                $select->where('statflow_respondent.answered_at <= ?', $dateTo->get('yyyy-MM-dd') . ' 23:59:59');
                unset($dateFrom);
                unset($dateTo);
            } else 
                if ($from != '') {
                    $dateFrom = new \Zend_Date($from, $formatedDate);
                    $select->where('statflow_respondent.answered_at >= ?', $dateFrom->get('yyyy-MM-dd') . ' 00:00:00');
                    unset($dateFrom);
                } else 
                    if ($to != '') {
                        $dateTo = new \Zend_Date($to, $formatedDate);
                        $select->where('statflow_respondent.status <= ?', $dateTo->get('yyyy-MM-dd') . ' 23:59:59');
                        unset($dateTo);
                    }
        }
        $selectExternal_id = $adapter->select();
        $selectExternal_id->from('statflow_feedback_form',array('external'=>'external'))
        ->where('statflow_feedback_form.id = ?',  $feedback_form_id)
        ->limit(1);
        //echo $selectExternal_id;die;
        $external_id = $adapter->fetchOne($selectExternal_id);
        $select->columns('statflow_respondent.id as respondent_id')
            ->columns('statflow_respondent.status as status')
            ->columns('statflow_respondent.alert as alert')
            ->columns('statflow_respondent.token as token')
            ->columns('statflow_respondent.email as email')
            ->columns('statflow_respondent.firstname as firstname')
            ->columns('statflow_respondent.lastname as lastname')
            ->columns('statflow_respondent.completion_duration as completion_time')
            ->columns('statflow_respondent.locale as language')
            ->columns('statflow_respondent.structure_belonging as structure_belonging')
            ->columns("DATE_FORMAT(statflow_respondent.created_at, '%Y-%m-%d')  as imported_at")
            ->columns("DATE_FORMAT(statflow_respondent.answered_at, '%Y-%m-%d')  as answer_date")
            ->columns('statflow_respondent.external_id as '.$external_id)
            ->order('statflow_respondent.created_at DESC')
            ->order('statflow_respondent.answered_at DESC');
        
        // TODO Fix Import Field List + External ID Key (from config file)
        $keysToIgnore = array(
            'email',
            'Name',
            'Surname',
            'language',
            'structure_belonging',
            'comment'
        );
        // $select->columns('statflow_respondent.external_id as ID_Club_Member');
        $externalFieldsSelect = $adapter->select()
            ->from('statflow_import_field', array(
            'id',
            'label'
        ))
            ->where('statflow_import_field.feedback_form_id = ?', $feedback_form_id)
            
        // Remove THAT
            ->where('statflow_import_field.label NOT IN ("' . join('","', $keysToIgnore) . '")');
        
        $fields = $adapter->fetchPairs($externalFieldsSelect);
        
        foreach ($fields as $id => $field) {
            $subSelect = $adapter->select();
            $subSelect->from('statflow_respondent_external_field', array(
                'statflow_respondent_external_field.value'
            ))
                ->joinInner('statflow_import_field', 'statflow_import_field.id = statflow_respondent_external_field.import_field_id', array())
                ->where('statflow_respondent_external_field.respondent_id = statflow_respondent.id')
                ->where('statflow_import_field.id = ?', $id);
            $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($field)));
        }
        $headers_init = array(
            'respondent_id',
            'status',
            'alert',
            'link',
            'email',
            'firstname',
            'lastname',
            'completion_time',
            'language',
            'structure_belonging',
            'import_date',
            'answer_date',
            $external_id
        );
        $headers = $headers_init;
        foreach ($fields as $field) {
            if(!in_array($field, $headers_init))
                $headers[] = $field;
        }
        
        $modalityQuestions      = array();
        $modalityQuestionsCodes = array();
        $openQuestions          = array();
        
        foreach ($feedbackForm->feedback_form_pages as $feedback_form_page) {
             foreach ($feedback_form_page->questions as $question) {
            if ($question->parent_id != null) {
                if(!$question->parent) {
                    continue;
                }
                $subSelect = $adapter->select();
                $subSelect->from('statflow_answer', array(
                    'value'
                ))
                    ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                    ->where('statflow_answer.respondent_id = statflow_respondent.id')
                    ->where('statflow_question.code = ?', $question->code)
                    ->limit(1);
                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                $headers[] = $question->code;
                $modalityQuestions[] = $question->parent->code;
                $modalityQuestionsCodes[$question->code] = $question->parent->code;
            } else {
                switch ($question->question_type_id) {
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY:
                        break;
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ: // Q1 Q7 Q8 Q12
                        $subSelect = $adapter->select();
                        $subSelect->from('statflow_answer_entry', array())
                            ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                            ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                            ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array(
                            'id'
                        ))
                            ->where('statflow_answer.respondent_id = statflow_respondent.id')
                            ->where('statflow_question.code = ?', $question->code)
                            ->limit(1);
                        $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                        $headers[] = $question->code;
                        $modalityQuestions[] = $question->code;
                        $modalityQuestionsCodes[$question->code] = $question->code;
                        foreach ($question->question_entrys as $modality) {
                            if ($modality->entry_type_id == \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT) {
                                $subSelect = $adapter->select();
                                $subSelect->from('statflow_answer_entry', array(
                                    'content'
                                ))
                                    ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                                    ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                                    ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array())
                                    ->where('statflow_answer.respondent_id = statflow_respondent.id')
                                    ->where('statflow_question.code = ?', $question->code)
                                    ->limit(1);
                                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code . '-Content')));
                                $headers[] = $question->code . '-Content';
                                $openQuestions[] = $question->code . '-Content';
                            }
                        }
                        break;
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE:
                        $subSelect = $adapter->select();
                        $subSelect->from('statflow_answer_entry', 'GROUP_CONCAT( statflow_answer_entry.question_entry_id SEPARATOR "|")')
                            ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                            ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                            ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array())
                            ->where('statflow_answer.respondent_id = statflow_respondent.id')
                            ->where('statflow_question.code = ?', $question->code);
                        $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                        
                        $headers[]                               = $question->code;
                        $modalityQuestions[]                     = $question->code;
                        $modalityQuestionsCodes[$question->code] = $question->code;
                        
                        foreach ($question->question_entrys as $modality) {
                            if ($modality->entry_type_id == \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT) {
                                $subSelect = $adapter->select();
                                $subSelect->from('statflow_answer_entry', array(
                                    'content'
                                ))
                                ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                                ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array())
                                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                                ->where('statflow_question.code = ?', $question->code)
                                ->limit(1);
                                $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code . '-Content')));
                                $headers[] = $question->code . '-Content';
                                
                            }
                        }
                        break;
                        
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED:
                        $meta = \Zend_Json::decode($question->meta);
                        if (! isset($meta['max']))
                            $meta['max'] = 0;
                        $modalityQuestions[] = $question->code;
                        for ($i = 0; $i < $meta['max']; $i ++) {
                            $subSelect = $adapter->select();
                            $subSelect->from('statflow_answer_entry', array())
                                ->joinInner('statflow_answer', 'statflow_answer.id = statflow_answer_entry.answer_id', array())
                                ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                                ->joinInner('statflow_question_entry', 'statflow_question_entry.id = statflow_answer_entry.question_entry_id', array(
                                'id'
                            ))
                                ->where('statflow_answer.respondent_id = statflow_respondent.id')
                                ->where('statflow_question.code = ?', $question->code)
                                ->limit(1, ($i > 0) ? $i : null);
                            $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code) . '-' . ($i + 1)));
                            $headers[] = $question->code . '-' . ($i + 1);
                            
                            $modalityQuestionsCodes[$question->code . '-' . ($i + 1)] = $question->code;
                        }
                        break;
                    case \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                        $subSelect = $adapter->select();
                        $subSelect->from('statflow_answer', array(
                            'value'
                        ))
                            ->joinInner('statflow_question', 'statflow_question.id = statflow_answer.question_id', array())
                            ->where('statflow_answer.respondent_id = statflow_respondent.id')
                            ->where('statflow_question.code = ?', $question->code)
                            ->limit(1);
                        $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $adapter->quote(trim($question->code)));
                        
                        $headers[]                               = $question->code;
                        $modalityQuestionsCodes[$question->code] = $question->code;
                        $openQuestions[]                         = $question->code;
                        break;
                }
            }
        }
    }
        $export = $this->getExport();
        $page = $this->getPage();
        
        if ($export) {
            $this->export($adapter, $select, $modalityQuestionsCodes, $openQuestions, $feedback_form_id, $serverUrl);
        } else {
            $paginator = new \Zend_Paginator(new \Zend_Paginator_Adapter_DbSelect($select));
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage(30);
            
            $modalityQuestions = array_unique($modalityQuestions);
            
            // View data
            return array(
                'headers'                   =>$headers,
                'paginator'                 =>$paginator,
                'modalityQuestions'         =>$modalityQuestions,
                'modalityQuestionsCodes'    =>$modalityQuestionsCodes,
                'openQuestions'             =>$openQuestions,
                'status'                    =>$status,
                'feedback_form_id'          =>$feedback_form_id,
                'from'                      =>$from,
                'to'                        =>$to,
                'page'                      =>$page,
                'feedback_form_id'          =>$feedback_form_id,
                'respondent_id'             =>$respondent_id
            );
        }
    }
    
    public function getAllStructure()
    {
        return $this->models['structure']->fetchAll();
    }
}

?>