<?php
namespace Statflow\Service;



class Translation
{
    private $transLang;
    private $translation;
    private $transTag;
    private $translationUid;
    private $question;
    private $translationTagUid;
    
    public function __construct()
    {
        $this->transLang = \Centurion_Db::getSingleton('translation/language');
        $this->translation = \Centurion_Db::getSingleton('translation/translation');
        $this->transTag = \Centurion_Db::getSingleton('translation/tag');
        $this->translationUid = \Centurion_Db::getSingleton('translation/uid');
        $this->question = \Centurion_Db::getSingleton('statflow/question');
        $this->translationTagUid = \Centurion_Db::getSingleton('translation/tagUid');
    }

    public function saveTag()
    {
        
    }
    
    //obtenir les infos Translations
    public function getTranslationId($libelle)
    {
        $translation_id = null;
        $translation = $this->getByTranslationUid($libelle);
        if($translation != null) {
            $translation_id = $translation->id;
        }
        
        return $translation_id;
    }
    
    //obtenir les infos Translations
    public function getTranslationUId($adapter, $libelle)
    {
        $select= $this->translationUid->select()
                      ->where('uid ='.$libelle);
        $data = $this->translationUid->fetchAll($select);
        $translation_id = null;
        foreach ($data as $uid)
            $translation_id = $uid['id'];
        return $translation_id;
    }
    
    // get translation name + language used
    public function getAllTranslationInfoLang($adapter, $uid_id)
    {
        $select  = $adapter->select();
        $select->from('translation_translation')
        ->join('translation_uid', 'uid_id=id')
        ->where('uid_id ='.$uid_id);
        $data = $adapter->fetchAll($select);
        return $data;
    }
    
    public function getLanguageInfo($language_id)
    {
        return $this->transLang->get(array(
            'id' => $language_id
        ));
    
    }
    
    public function getByTranslationUid($uid)
    {
        return $this->translationUid->get(array(
            'uid' => $uid
        ));
    }
    
    public function getByTranslationUid_Id($uid_id)
    {
        return $this->translation->get(array(
            'uid_id' => $uid_id
        ));
    }
    
    public function getLanguageId($language_used)
    {
        $language_id = null;
        $select= $this->transLang->select()
                      ->where('name = ?', $language_used);
        
        $language    = $this->transLang->fetchRow($select);
        if($language != null) {
        $language_id = $language->id;
        }
        
        return $language_id;
        
    }
    
    
    public function checkRecordTranslationExist($uId, $language_id)
    {
        $result =  $this->translation->select(true)
                        ->where('uid_id = ' . $uId. ' and language_id = '.$language_id)
                        ->fetchAll();
    
        return $result->count();
    }
    
    public function updateTraduction($uId, $language_id, $traduction) 
    {
        $sqlUpate = 'UPDATE translation_translation SET translation = "'.$traduction.'" WHERE uid_id = ' .$uId. ' AND language_id = '.$language_id;
       // die('sql' . $sqlUpate);
        \Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlUpate);
        
        //\Translation_Model_Manager::generate();
    } 

    
    public function saveTraduction($uId, $language_id, $traduction)
    {
        $translation = $this->translation->createRow(array('translation' => $traduction,
            'uid_id' => $uId, 'language_id' => $language_id));
        
        $translation->save();
        
        //\Translation_Model_Manager::generate();
    }
    
   
    public function duplicateTraductionQuestion($id,$codeCloned,$newFeedbackFormId=null)
    {
    
        $questionRow = $this->question->fetchRow($this->question->select()->where('id = ?',$id));
      
        $oldLibelleTitleQuestion= 'form__'.$questionRow->feedback_form_id.'__question_title-'.$questionRow->code;
        $oldLibelleHelpQuestion= 'form__'.$questionRow->feedback_form_id.'__question_help-'.$questionRow->code;
        if($newFeedbackFormId != null){
            $newLibelleTitleQuestion= 'form__'.$newFeedbackFormId.'__question_title-'.$codeCloned;
            $newLibelleHelpQuestion= 'form__'.$newFeedbackFormId.'__question_help-'.$codeCloned;
        }else{
            $newLibelleTitleQuestion= 'form__'.$questionRow->feedback_form_id.'__question_title-'.$codeCloned;
            $newLibelleHelpQuestion= 'form__'.$questionRow->feedback_form_id.'__question_help-'.$codeCloned;
        }
    
        $this->buildTranslationLibelle($oldLibelleTitleQuestion,$newLibelleTitleQuestion);
        $this->buildTranslationLibelle($oldLibelleHelpQuestion,$newLibelleHelpQuestion);
    
         
    }
    

    
    public function buildTranslationLibelle($oldLibelle, $newLibelle)
    {
       
        $translationUidQuestionRow = $this->translationUid->fetchRow($this->translationUid->select()->where('uid = ?',$oldLibelle));
         
        $data = $translationUidQuestionRow->toArray();
        $data['id']= null;
        $data['uid']= $newLibelle;
     
        $newQuestionTranslationUid = $this->translationUid->createRow($data);
        $newQuestionTranslationUid->save();
    
        $this->saveEntryTranslationTagUid($newQuestionTranslationUid->id,$translationUidQuestionRow->id);
        $this->duplicateTranslation ($translationUidQuestionRow->id, $newQuestionTranslationUid->id);
    
    }
    
    public function saveEntryTranslationTagUid($newUidId,$oldUidId)
    {
         
        $translationTagUidRow = $this->translationTagUid->fetchRow($this->translationTagUid->select()->where('uid_id = ?',$oldUidId));
        $data = $translationTagUidRow->toArray();
        $data['uid_id'] = $newUidId;
        $rowData= $this->translationTagUid->createRow($data);
        $rowData->save();
        return $rowData;
    }
    
    
    public function duplicateTranslation ($uid_id, $uid_id_new)
    {
       $translations = $this->translation->fetchAll($this->translation->select()->where('uid_id = ?',$uid_id));
 
        if (($translations ->count()) > 0){
            foreach ($translations as $translation) {
                $dataTranslation = $translation->toArray();
                $dataTranslation['uid_id']= $uid_id_new;
                $newTranslationRow = $this->translation->createRow($dataTranslation);
                $newTranslationRow->save();
            }
    
        }
    
    }
    
    public function insertRowTranslationUid ($new_uid_variable)
    {
        $data['id']= null;
        $data['uid']= $new_uid_variable;
        $newQuestionTranslationUid = $this->translationUid->createRow($data);
        $newQuestionTranslationUid->save();
        return $newQuestionTranslationUid;
    }
    
    public function insertRowTranslationTagUid ($new_uid_id, $tag_id)
    {
        $dataTranslationTagUid['uid_id'] = $new_uid_id;
        $dataTranslationTagUid['tag_id'] = $tag_id;
        $rowData= $this->translationTagUid->createRow($dataTranslationTagUid);
        $rowData->save();
        return $rowData;
    }
    

    public function getTranslationUidRow ($uid_variable)
    {
       return $this->translationUid->fetchRow($this->translationUid->select()->where('uid = ?',$uid_variable));
    
    }
    
    public function getTagId ($tagName)
    {
        $translationTagRow = $this->transTag->fetchRow($this->transTag->select()->where('tag = ?',$tagName));
        $tagId = $translationTagRow->id;
        return $tagId;
    
    }
    
    public function deleteTranslation ($feedback_form_id)
    {
        $tagName = "form_".$feedback_form_id;
        $translationTagRow = $this->transTag->fetchRow($this->transTag->select()->where('tag = ?',$tagName));
        
        if (!is_null($translationTagRow)){
            $tagId = $translationTagRow->id;
            
            $translationTagUidRows = $this->translationTagUid->fetchAll($this->translationTagUid->select()->where('tag_id = ?', $tagId));
            
            if(!is_null($translationTagUidRows)){
                foreach ($translationTagUidRows as $translationTagUidRow) {
                    $translationUid = $this->translationUid->deleteRow("id = ".$translationTagUidRow->uid_id);
                } 
            }
            $translationTag = $this->transTag->deleteRow("id = ".$tagId);       
        }
    
    }
    
    public function insertRowTag ($new_form_id)
    {
        $data['id']= null;
        $data['tag']= $new_form_id;
        $newTagRow = $this->transTag->createRow($data);
        $newTagRow->save();
        return $newTagRow->id;
    }
    
    public function generateTranslationFiles()
    {
       \Translation_Model_Manager::generate();
    }


}