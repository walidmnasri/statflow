<?php
namespace Statflow\Service;

class Auth
{

    private $user;

    private $validator;

    private $validatorPwd;
    
    private $mail;
    
    public function __construct($user = null, $mail=null)
    {
        $this->setModel($user);
        $this->setMail($mail);
    }
    
    public function setModel($user)
    {
        if (!$user) {
            $user = \Centurion_Db::getSingleton('auth/user');
        }
        $this->user = $user;
    }
    public function setMail($mail)
    {
        if (!$mail) {
            $mail = new \Zend_Mail('UTF-8');
        }
        $this->mail = $mail;
    }
    public function getMail()
    {
       return  $this->mail ;
    }
    
    public function getValidatoreReset($parameters)
    {
        if (! $this->validator) {
            
            $filters = array(
                'email' => array(
                    'StringTrim',
                    'StripTags'
                )
            );
            
            $validators = array(
                'email' => array(
                    'required' => true,
                    'NotEmpty',
                    'EmailAddress',
                    new \Zend_Validate_Db_RecordExists(array(
                        'table' => 'auth_user',
                        'field' => 'email'
                    ))
                )
            );
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator;
    }

    public function getValidatorePwd($parameters)
    {
        if (! $this->validatorPwd) {
            
            $filters = array(
                'email' => array(
                    'StringTrim',
                    'StripTags'
                )
            );
            $validators = array(
                'password1' => array(
                    'required' => true,
                    'NotEmpty'
                ),
                'password2' => array(
                    'required' => true,
                    'NotEmpty'
                )
            );
            $this->validatorPwd = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validatorPwd;
    }

    public function isValidPwd($parameters)
    {
        return $this->getValidatorePwd($parameters)->isValid();
    }

    public function isValidReset($parameters)
    {
        return $this->getValidatoreReset($parameters)->isValid();
    }

    public function getValidated()
    {
        $rowUser = $this->getUserByEmail($this->validator->email);
        $rowUser->token = sha1($rowUser->salt . $this->validator->email . time());
        $rowUser->save();
        return $rowUser;
    }

    public function getValidatedPwd($token)
    {
        $user = $this->getUserByToken($token);
        $user->setPassword($this->validatorPwd->password1);
        //$user->token = NULL;
        $user->is_active = 1;
        $user->save();
        return $user;
    }

    public function FormLogin()
    {
        return new \Auth_Form_Login(array(
            'dbAdapter' => \Zend_Db_Table_Abstract::getDefaultAdapter(),
            'tableName' => 'auth_user',
            'loginColumn' => 'username',
            'passwordColumn' => 'password',
            'authAdapter' => 'Centurion_Auth_Adapter_DbTable',
            'checkColumn' => 'is_active = 1'
        ));
    }

    public function getValidatedLogin()
    {
        $userRow = \Centurion_Auth::getInstance()->getIdentity();
        $userRow->last_login = date('Y-m-d h:i:s');
        $userRow->save();

        // used for lock screen
        $_SESSION['fullName'] = $userRow->first_name . ' ' . $userRow->last_name;
        $_SESSION['username'] = $userRow->username;
        $_SESSION['email'] =  $userRow->email;
        $_SESSION['user_id'] = $userRow->id;

        return $userRow;
    }

    public function redirectIfAuthenticated($hasparam = null, $getParam=null)
    {
        if (\Centurion_Auth::getInstance()->hasIdentity()) {
            $authenticatedUser = \Centurion_Auth::getInstance()->getIdentity();
            if (in_array($authenticatedUser->groups->current()->name, array(
                'Administrator'
            )))
                return 2;
            else{
                return 1;
            } 
        }
        return 0;
    }

    public function ClearSession()
    {
        \Zend_Auth::getInstance()->clearIdentity();
        \Zend_Session::destroy();
        return 'session clear';
    }

    public function lockedSession()
    {
        \Zend_Auth::getInstance()->clearIdentity();
        return 'session locked';
    }

    public function sendMail($email_to, $subject, $urlDoReset, $studyInfo, $groupName, $parameters)
    {
        // SMTP server configuration
        $smtpHost = 'pro1.mail.ovh.net';
        $smtpConf = array(
            'port' => '587',
            'ssl' => 'tls',
            'auth' => 'login',
            'username' => 'webmaster@it-prospect.com',
            'password' => 'itprospect@@',
            'timeout' => 60
        );
        $transport = new \Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
        $translator = \Zend_Registry::get('Zend_Translate');
        /*
        $email_from = "webmaster@it-prospect";
            $email_from_name = "StatFlow";
        $bodyHtml = "<html>
                    <p>Dear user,</p><br>
                    <p>To reset your password on the Statflow platform, please use the following link :<br>
                    <a href='$urlDoReset'>Password reinitialization</a></p><br>
                    <p>Sincerely yours,<br>The Statflow Support Team</p>
                    </html>";
        $bodyText = "Dear user,
                    To reset your password on the Statflow platform, please use the following link :'$urlDoReset'
                    Sincerely yours,
                    The Statflow Support Team";
        */
        $email_from_name = $parameters[0]['value'];
        $email_from      = $parameters[1]['value'];
        $bcc_email       = $parameters[2]['value'];
        $bodyText        = str_replace("urlDoReset",$urlDoReset, $translator->translate('param_email_renewal_body_text'));
        $bodyHtml        = str_replace("urlDoReset",$urlDoReset, $translator->translate('param_email_renewal_body_html'));
        $subject         = $translator->translate('param_email_renewal_subject');

        if($groupName == 'Client'){
            $translator = \Zend_Registry::get('Zend_Translate');
            foreach ($studyInfo as $key) :
                $study_id = $key['study_id'];
                $isActif  = $key['actif_renew'];
                $sender_email_renew = $key['sender_email_renew'];
                $sender_name_renew =  $key['sender_name_renew'];
            endforeach;
            if($isActif == 1) {
                $email_from = $sender_email_renew;
                $email_from_name = $sender_name_renew;
                $bodyText  = str_replace("urlDoReset",$urlDoReset, $translator->translate('email_renewal_body-text_study_'.$study_id));
                $bodyHtml = str_replace("urlDoReset",$urlDoReset, $translator->translate('email_renewal_body_study_'.$study_id));
                $subject   = $translator->translate('email_renewal_subject_study_'.$study_id);
            }
        }
     
        $mail = $this->getMail();
        $mail->setFrom($email_from, $email_from_name);
        $mail->addTo($email_to);
        //$mail->addBcc('mnasri.walid@gmail.com');
        $mail->addBcc($bcc_email);
        $mail->setSubject($subject);
        $mail->setBodyHtml($bodyHtml);
        $mail->setBodyText($bodyText);
        if($mail->send($transport))
            return true;
        
        return false;
    }

    public function getUserByEmail($email)
    {
        return $this->user->get(array(
            'email' => $email
        ));
    }

    public function getUserByToken($token)
    {
        return $this->user->get(array(
            'token' => $token
        ));
    }
    
}
