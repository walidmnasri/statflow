<?php
namespace Statflow\Service;

class NominativeFieldHidden
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
    
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/NominativeFieldHidden');
        }
        $this->model = $model;
    }
    
    public function getNominativeFieldHiddenByFeedbackFormId($feedbackFormId)
    {
        $select=  $this->model->select(true)
                       ->where('feedback_form_id ='.$feedbackFormId)
                       ->fetchAll()->toArray();
    
        $data = array();
        foreach ($select as $field){
            $data[] = str_replace(' ', '_', $field['nominative_field']);
        }
    
        return $data;
    }
        
    public function saveNominativeFieldHidden($feedbackFormId, $respondentField)
    {
        $hiddenFields = $this->model->createRow( array( 'feedback_form_id' => $feedbackFormId,
                                                        'nominative_field' => $respondentField
                                               ));
        $hiddenFields->save();
        return $hiddenFields;  
    }
    
    public function deleteNominativeFieldHiddenByFeedbackFormId($feedbackFormId)
    {
        return $this->model->deleteRow("feedback_form_id = ".$feedbackFormId);
    }
    
    

    
}

?>