<?php
namespace Statflow\Service;

class UserFeedbackForm
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
    
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/UserFeedbackForm');
        }
        $this->model = $model;
    }
    
    public function getAllFeedbackFormByUserId($userId)
    {
        return $this->model->select(true)
                            ->where('user_id = ' . $userId)
                            ->fetchAll();
    }
    
    public function saveUserFeedbackForm($userId, $feedbackFormId)
    {
        $userFeedbackForm = $this->model->createRow(array(
                                                        'user_id' => $userId,
                                                        'feedback_form_id' => $feedbackFormId
                                                    ));
    
        $userFeedbackForm->save();
        return $userFeedbackForm;
    
    }
    

    public function deleteByUserId($user_id)
    {
    
        return $this->model->deleteRow("user_id = ".$user_id);
    
    }
    
    public function deleteByFeedbackFormId($feedbackform_id)
    {
    
        return $this->model->deleteRow("feedback_form_id = ".$feedbackform_id);
    
    }
    
    // Delete a list of user id
    public function deleteByListUserId($listid)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        return $adapter->delete('statflow_user_feedback_form','user_id in ('.$listid.')');
    }
}

?>