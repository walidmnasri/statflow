<?php
namespace Statflow\Service;

class RespondentExternalField
{
    private $model;
    
    public function __construct($model = null)
    {    
         $this->setModel($model);
    }
   
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/RespondentExternalField');
        }
        $this->model = $model;
    }
       
    public function getValuesOfExternalField($import_field_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();

        $select = $adapter->select()
                          ->distinct()
                          ->from(array('ref' => 'statflow_respondent_external_field'), array('value'=>'ref.value'))
                          ->joinInner(array('res' => 'statflow_respondent'), 'res.id = ref.respondent_id',array())
                          ->where('import_field_id ='.$import_field_id)
                          ->where('res.status = 20')
                          ->order(array('value ASC'));                            
        
        $result =  $adapter->fetchAll($select);
        return $result;
    }    
    
    public function getRespondentDetails($respondent_id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
                          ->from(array('res' => 'statflow_respondent'))
                          ->where('res.id = ?', $respondent_id);
    
        $result =  $adapter->fetchAll($select);
        return $result;
    }
    
    public function getRespondentExternalField($respondent_id)
    {
        return $this->model->select(true)
                           ->where('respondent_id = ?', $respondent_id)
                           ->fetchAll();
    }
    
    
}
?>