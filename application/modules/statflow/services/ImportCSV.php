<?php

use Statflow\Service\FeedbackForm;
require_once __DIR__ . '/../services/FeedbackForm.php';

//use Statflow\Service\FeedbackForm;

/**
 * Class Statflow_Model_ImportCSV
 */
class Statflow_Service_ImportCSV
{
    /**
     * @var string
     */
    protected $_fileName;

    /**
     * @var array
     */
    protected $_config = array();

    /**
     * @var array
     */
    protected $_data = array();

    /**
     * @var array
     */
    protected $_headers = array();

    /**
     * @var bool
     */
    protected $_isValid = true;

    /**
     * @var array
     */
    protected $_validationErrors = array();

    /**
     * @var array
     */
    protected $_rawData = array();

    /**
     * @var array
     */
    protected $_duplicatedData = array();
    
    /**
     * @var array
     */
    protected $_ignoresFields;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Setter CSV (absolute) file name
     *
     * @param $fileName
     *
     * @return Statflow_Model_ImportCSV $this
     */
    public function setFileName($fileName)
    {
        $this->_fileName = $fileName;
        return $this;
    }

    /**
     * Setter config (services + fields)
     *
     * @param array $config
     *
     * @return Statflow_Model_ImportCSV $this
     */
    public function setConfig($config)
    {
        $this->_config = $config;
        return $this;
    }

    /**
     * Remove duplicates by email
     */
    public function removeDuplicates()
    {
        $emails = array();
        $cleanData = array();
        foreach ($this->_data as $entry) {
            if (!in_array($entry['email'], $emails)) {
                $cleanData[] = $entry;
                $emails[] = $entry['email'];
            }
            else {
                $this->_duplicatedData[] = $entry;
            }
        }
        $this->_data = $cleanData;
    }


    /**
     * Validation and Loading data from CSV
     *
     * @throws Zend_Exception
     * @return Statflow_Model_ImportCSV $this
     */
    public function checkAndLoadData($feedback_form_id)
    {
        if ($this->_fileName == null || realpath($this->_fileName) === false) {
            throw new Zend_Exception("Can't load file at " . $this->_fileName);
        }
        
        /*
         * Get info from feedbackform -> par questionnaire
         */
        $feedbackFormService = new FeedbackForm();
        $infoFeedbackForm = $feedbackFormService->getById($feedback_form_id);
        
        
        $column_separator = $infoFeedbackForm->column_separator;
        $rule_numdays     = $infoFeedbackForm->rule_numdays;
        $label_info       = $infoFeedbackForm->label_info;
       
        if($column_separator == '')
        {
            $column_separator = "|";
            \Sayyes_Tools_Console::printLine("<br> <br>". 'No separator set, default separator | will be used <br>');
        }else{
            \Sayyes_Tools_Console::printLine("<br> <br>". 'CSV separator set for this survey : ' . $column_separator. '<br>');
        }
            
        
        
        /**
         * Read Loaded File Data
         */
        $file = new SplFileObject($this->_fileName);
        $file->setFlags(SplFileObject::READ_CSV);
        $file->setCsvControl($column_separator);
        /**
         * Checking the number of loaded rows
         * at least 2 rows ( rows1 => header | rows 2 to rows n => datas )
         */
        $count = 0;
        foreach ($file as $row) {
            $column_header = $row ;break;
        }
        foreach ($file as $row) {
            $count++;
        }
        
        if ($count < 2) {
            throw new Zend_Exception("Data missing in loaded file " . $file->getBasename());
        }
	
	if(count($column_header) < 2){
		\Sayyes_Tools_Console::printLine("<br> Only one column of data found, is the CSV separator correct ? <br> <br>");
	}

        /**
         * Checking file formatting
         * Each row should have the same number of items
         */
        $i = 0;
        $nItem = 0;
        foreach ($file as $row) {
            if ($i == 0) {
                $bom = pack('H*', 'EFBBBF');
                $row[0] = preg_replace("/^$bom/", '', $row[0]);
                //$line = explode(',', $row[0]);
                $nItem = count($row);
            } else {
                if (count($row) != $nItem && $row[0] != null) {
                    throw new Zend_Exception("Bad formatting data in loaded file " . $file->getBasename() .
                    ' line ' . ($i + 1));

                }
            }
            $i++;
        }
        /**
         * Building headers and data
         */

        $i = 0;
        $emptyCol = 1;
        $headers = array();
        $data = array();
        foreach ($file as $row) {
            $bom = pack('H*', 'EFBBBF');
            $row[0] = preg_replace("/^$bom/", '', $row[0]);
            //$line = explode(',', $row[0]);
            if ($i == 0) {
                /**
                 * Building headers
                 */
                foreach ($row as $key => $header) {
                    if (!empty($header)) {
                        $headers[$key] = $header;
                    } else {
                        $headers[$key] = 'empty_col__' . $emptyCol;
                        $emptyCol++;
                    }
                }
            } else {
                /**
                 * Loading data into array
                 */
                $entryData = array();
                if (count($row) > 1) {
                    foreach ($row as $key => $entry) {
                        $entryData[$headers[$key]] = $entry;
                    }
                    $data[] = $entryData;
                }
            }
            $i++;
        }
        $this->_headers = $headers;
        $this->_data = $data;
        $this->_rawData = $data;
        /**
         * Checking the existence of config fields in header
         */
        $ignoredFields = array();
        foreach (array_keys($this->_config['respondent']) as $key) {
            $ignoredFields [] = $this->_config['respondent'][$key]['csv_column_name'];
            if (!in_array($this->_config['respondent'][$key]['csv_column_name'], $this->_headers)) {
                throw new Zend_Exception('the loaded file ' . $file->getBasename()
                . ' doesn\'t  contain the config field ' . $this->_config['respondent'][$key]['csv_column_name']);
            }
        }
        
        /**
         * validation of header's external fields
         */
        $externalFilters = array(
            'external_header' => array(
                new Zend_Filter_StringTrim(),
                new Zend_Filter_StripTags()
            )
        );
        $externalvalidators = array(
            'external_header' => array(
                new Zend_Validate_NotEmpty(),
                new Zend_Validate_StringLength(array('min' => 1, 'max' => 45)),
                'messages' => array(
                    'this value can not be empty',
                    array(
                        Zend_Validate_StringLength::TOO_SHORT =>
                        'The \'%value%\' must have between \'%min%\' and \'%max%\' characters',
                        Zend_Validate_StringLength::TOO_LONG  =>
                        'The \'%value%\' must have between \'%min%\' and \'%max%\' characters'
                    )
                )
            )
        );
        $externalInput = new Zend_Filter_Input($externalFilters, $externalvalidators);
        foreach ($this->_headers as $header) {
            if (!in_array($header, $ignoredFields) && strpos($header, 'empty_col__') === false) {
                $externalfield = array('external_header' => $header);
                $externalInput->setData($externalfield);
                if (!$externalInput->isValid('external_header')) {
                    $exceptionMessage = 'the loaded data "' . $header . '" is not valid , Validation headers error(s) ';
                    foreach ($externalInput->getMessages() as $messageId => $message) {
                        foreach ($message as $msg) {
                            $exceptionMessage .= ' ' . $messageId . ' : ' . $msg . '. <br />';
                        }
                    }
                    throw new Zend_Exception($exceptionMessage);
                }
            }
        }
        $this->_ignoresFields = $ignoredFields;
        /**
         * Load filters and validators from config
         */
        foreach ($this->_config['respondent'] as $field) {
            if (isset ($field['filters']) && is_array($field['filters']) && isset ($field['validators'])
                && is_array(
                    $field['validators']
                )
            ) {
                $inputFilters [$field['csv_column_name']]
                    = new Zend_Filter_Input($field['filters'], $field['validators']);
            }
        }
        
        //echo "label info = ".$label_info;
        
        /**
         * Validation of loaded Data
         */
        foreach ($this->_data as $lineId => $data) {
            $email = $data['email'];
            foreach ($data as $key => $field) {
                //var_dump($key);
                if (isset($inputFilters[$key])) {
                    //var_dump($field);
                    $datafield = array($key => $field);
                    $inputFilters [$key]->setData($datafield);
                    if (!$inputFilters [$key]->isValid($key)) {
                        $exceptionMessage = 'the loaded data "' . $field . '" is not valid for the field "' . $key
                            . '", Validation data error(s)';
                        foreach ($inputFilters[$key]->getMessages() as $messageId => $message) {
                            foreach ($message as $msg) {
                                $exceptionMessage .= ' ' . $messageId . ' : ' . $msg . '. <br />';
                            }
                        }
                        if (!isset($this->_validationErrors[$lineId])) {
                            $this->_validationErrors[$lineId] = array();
                        }
                        $this->_validationErrors[$lineId][] = $exceptionMessage;
                        $this->_isValid = false;

                        unset($this->_data[$lineId]);
                        //throw new Zend_Exception( $exceptionMessage );
                    }
                }
                elseif ($label_info != '' && $key == $label_info) {
                    $existingRespondents = Centurion_Db::getSingleton('statflow/respondent')
                        ->select(true)
                        ->filter( array(
                            'email' => $email,
                            'feedback_form_id' => $feedback_form_id
                        ))
                        ->fetchAll();
                    foreach($existingRespondents as $respondent) {
                        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
                        $select = $adapter
                            ->select()
                            ->from('statflow_respondent_external_field')
                            ->where('respondent_id = ?', $respondent->id)
                            ->where('label = ?', $label_info);

                        $row =$adapter->fetchRow($select);
                        $date = new Zend_Date($row['value'], 'YYYY-MM-dd HH:mm:ss');
                        $now = Zend_Date::now();
                        
                        if($rule_numdays != '')
                        {
                            if($now->subDay($rule_numdays)->isEarlier($date)) {
                                $this->_isValid = false;
                                $this->_validationErrors[$lineId][] = 'Last order is less than '.$rule_numdays.' days ago';
                                unset($this->_data[$lineId]);
                            }
                        }
                        
                    }
                }
            }
        }
        return $this;
    }

    /**
     * Saving data from CSV to previously configured models
     *
     * @void
     *
     * @param int $study_id
     * @param int $feedback_form_id
     * @param string $logfile_path
     * @throws Zend_Exception
     */
    public function saveData($study_id = 1, $feedback_form_id = 1, $logfile_path)
    {
        /*
         * Get info from feedbackform -> par questionnaire
         */
        $feedbackFormService = new FeedbackForm();
        $infoFeedbackForm = $feedbackFormService->getById($feedback_form_id);

      //  var_dump($infoFeedbackForm);
        $columnDate = $infoFeedbackForm->field_label;
        $formatDate = $infoFeedbackForm->field_format; 
        $startTime = time();
        $filePath = APPLICATION_PATH . '/../public/ftp/' . $study_id . '/' . $feedback_form_id . '/logs/sql/export-'.$startTime.'.sql';
        $handle = fopen($filePath, 'w');
        $logfile = fopen($logfile_path, "a");
        fwrite($logfile, Zend_Debug::dump(count($this->_data), 'count line'));
        fwrite($logfile,  Zend_Debug::dump($this->_validationErrors, 'validation errors'));
        try {
            $models = array();
            /**
             * Building models array with csv to database field's mapping
             */
            foreach (array_keys($this->_config['respondent']) as $key) {
                $model__field__toString = explode('__', $key);
                if (!isset($models[$model__field__toString[0]])) {
                    $models[$model__field__toString[0]] = array('model'  => Centurion_Db::getSingleton($model__field__toString[0]),
                                                                'fields' => array()
                                                          );
                }
                $models[$model__field__toString[0]]['fields'][$model__field__toString[1]]
                    = $this->_config['respondent'][$key]['csv_column_name'];
            }
            /**
             * Ignored fields for import fields
             */
            $ignoredFields = $this->_config['ignoredFields'];
            foreach ($models as $model) {
                $ignoredFields = array_merge($ignoredFields, $model['fields']);
            }
            $parsedData = array();
            $parsedData[$study_id] = $this->_data;
            /**
             * Create External fields for import
             */
            $modelImport = Centurion_Db::getSingleton('statflow/import');
            $importLastRow = $modelImport->fetchRow('id > 0','id desc');
            // TODO requete pour chopper valeur
            $import = $importLastRow['id'] + 1;
            $lastImportId = $modelImport
                ->select()
                ->from(
                    $modelImport->info('name'),
                    array(new Zend_Db_Expr('CASE WHEN (MAX(number) IS NULL) THEN 0  ELSE MAX(number) END as numberMax'))
                )
                ->filter(
                    array(
                         'study_id'         => $study_id,
                         'feedback_form_id' => $feedback_form_id,
                    )
                )
                ->fetchRow()->numberMax;
            $importRow = $modelImport->createRow(
                array(
                     'study_id'         => $study_id,
                     'feedback_form_id' => $feedback_form_id,
                     'number'           => ++$lastImportId
                )
            );
            $importRow->id = $import;
            $importRow->created_at = Zend_Date::now()->get('yyyy-MM-dd HH:mm:ss');
            $sql[] = $importRow->getInsertQuery();
            $parsedDataFormImportIds[$key] = $importRow->id;

            $externalFields = Centurion_Db::getSingleton('statflow/import_field')->
                getAdapter()
                ->fetchPairs(
                    Centurion_Db::getSingleton('statflow/import_field')
                        ->select()
                        ->from('statflow_import_field', array('label', 'id'))
                        ->where('study_id = ?', $study_id)
                        ->where('feedback_form_id = ?', $feedback_form_id)
                );
            $externalFieldsLastRow = Centurion_Db::getSingleton('statflow/import_field')->fetchRow('id > 0','id desc');
            if($externalFieldsLastRow == null || (is_array($externalFieldsLastRow) && count($externalFieldsLastRow) == 0)) {
                $externalFieldsLastRowId = 1;
            }
            else
                $externalFieldsLastRowId = $externalFieldsLastRow['id'];

            foreach($this->_headers as $header) {
                if(!isset($externalFields[$header]) && !Sayyes_Tools_String::startWith('empty_col__', $header) && !in_array($header, $this->_ignoresFields) ) {
                    $id = $externalFieldsLastRowId + 1;
                    $externalFieldsLastRowId++;
                    echo $header;
                    $importField = Centurion_Db::getSingleton('statflow/importField')->createRow(
                        array(
                            'id'                => $id,
                            'study_id'         => $study_id,
                            'feedback_form_id' => $feedback_form_id,
                            'import_id'        => $importRow->id,
                            //TODO replace label with something from a config file ??????? le label définit dans la config est pour le respondent ? et ici on ne stocke pas les label du respondent
                            'label'            => $header,
                            'external_id'      => $header,
                            'created_at'       => Zend_Date::now()->get('YYYY-MM-dd HH:mm:ss')
                        )
                    );
                    $sql[] = $importField->getInsertQuery();
                    $externalFields[$header] = $id;
                }
            }
            $modelObject = Centurion_Db::getSingleton('statflow/respondent');
            $modelExternal = Centurion_Db::getSingleton('statflow/respondentExternalField');
            $respondentLastRow = $modelObject->fetchRow('id > 0','id desc');
            $respondentExternalLastRow = $modelExternal->fetchRow('id > 0','id desc');
            $a = $respondentLastRow['id'] + 1 ;
            $b = $respondentExternalLastRow['id'] + 1;
            $respondentSql = array();
            $respondentExternalSql = array();
            /**
             * Saving data into models
             */
            foreach ($models as $wrapperData) {
                foreach ($this->_data as $entry) {
                    $row = array(
                        'study_id'=> (int) $study_id,
                        'feedback_form_id' => $feedback_form_id

                    );
                    foreach ($wrapperData['fields'] as $fieldDb => $fieldCSV) {
                        $row[$fieldDb] = $entry[$fieldCSV];
                    }
                    $row['id'] = $a;
                    $row['status'] = 0;
                    $row['token'] = sha1($row['email'] . '__' . $study_id . '__' . $row['id']);
                    $row['created_at'] = Zend_Date::now()->get('yyyy-MM-dd HH:mm:ss');
                    $respondentSql[] = '('.implode(',', array_map(function($v){return is_int($v) ? $v : (Zend_Db_Table_Abstract::getDefaultAdapter()->quote($v));},array_values($row))).')'; //$row->getInsertQuery();
                    foreach ($entry as $externalFieldLabel => $externalFieldValue) {
                        if ($externalFieldLabel == $columnDate) {
                           $periode =  DateTime::createFromFormat($formatDate, $externalFieldValue);
                           $value = $periode->format('Y-m-d');
                        }
                        else {
                            $value = $externalFieldValue;
                        }
                        if (!in_array($externalFieldLabel, array_values($wrapperData['fields']))
                            && strpos($externalFieldLabel, 'empty_col__') === false
                        ) {
                            $externalRow = array(
                                'respondent_id' => $row['id'],
                                'import_field_id' => $externalFields[$externalFieldLabel],
                                'label'           => $externalFieldLabel,
                                'value'           => trim(html_entity_decode($value, null, 'UTF-8')),
                            );
                            $externalRow['id'] = $b;
                            $externalRow['created_at'] = Zend_Date::now()->get('yyyy-MM-dd HH:mm:ss');
                            $respondentExternalSql[] = '('. implode(',', array_map(function($v){return is_int($v) ? $v : (Zend_Db_Table_Abstract::getDefaultAdapter()->quote($v));},array_values($externalRow))) .')';
                            $b++;
                        }
                    }
                    $a++;
                    gc_collect_cycles();
                }
            }
        } catch (Zend_Exception $e) {
            throw $e;
        }
        foreach($sql as $query) {
            $modelObject->getAdapter()->getConnection()->exec($query);
            fwrite($handle, $query.PHP_EOL);
        }
        while(count($respondentSql) > 0){
            $respondentQuery = 'INSERT INTO `statflow_respondent` (`'.implode('`,`', array_keys($row)).'`) VALUES '
                . PHP_EOL .  implode(',' . PHP_EOL, array_splice($respondentSql,0,1000)). ';' . PHP_EOL.PHP_EOL;
            fwrite($handle, $respondentQuery);
            $modelObject->getAdapter()->getConnection()->exec($respondentQuery);
            gc_collect_cycles();
        }
        while(count($respondentExternalSql) > 0){
            $externalFieldsQuery = 'INSERT INTO `statflow_respondent_external_field` (`'.implode('`,`', array_keys($externalRow)).'`) VALUES '
                . PHP_EOL . implode(',' . PHP_EOL, array_splice($respondentExternalSql,0,1000)). ';' . PHP_EOL.PHP_EOL;
            fwrite($handle, $externalFieldsQuery);
            $modelExternal->getAdapter()->getConnection()->exec($externalFieldsQuery);
            gc_collect_cycles();
        }
        fclose($handle);
        fwrite($logfile, Zend_Debug::dump($a, 'Last RespondentId'));
        fwrite($logfile, Zend_Debug::dump($b, 'Last ExternalFieldId'));
        fclose($logfile);
    }

    /**
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->_validationErrors;
    }

    public function isValid()
    {
        return $this->_isValid;
    }

    public function getData()
    {
        return $this->_data;
    }
}
