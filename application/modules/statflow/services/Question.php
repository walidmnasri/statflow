<?php
namespace Statflow\Service;

class Question
{

    private $validator;

    private $model;

    private $feedbackFormPageModel;
    
    private $translationUid;
    
    private $translationTagUid;
    
    private $translationTag;
    
    private $translation;
    
    private $translationService;

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/question');
        $this->feedbackFormPageModel = \Centurion_Db::getSingleton('statflow/feedbackFormPage');
        $this->translationUid = \Centurion_Db::getSingleton('translation/uid');
        $this->translationTagUid = \Centurion_Db::getSingleton('translation/tagUid');
        $this->translationTag = \Centurion_Db::getSingleton('translation/tag');
        $this->translation = \Centurion_Db::getSingleton('translation/translation');
    }

    /**
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (!$this->translationService){
            $this->translationService = new Translation();
        }
        return $this->translationService;
    }
    /**
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }
    
    
    public function addImageForm()
    {
        $this->imageForm = new \Centurion_Form();
        $fileInput = new \Zend_Form_Element_File('image');
        $fileInput->addValidator('Count', false, 1);
        $fileInput->addValidator('Extension', false, array(
            'jpg',
            'jpeg',
            'png'
        ));
        $fileInput->addValidator('Size', array(
            'min' => 0
        ));
        $fileInput->setRequired(true);
        return $this->imageForm->addElement($fileInput);
    }

    public function getValidator($parameters)
    {
        if (! $this->validator) {
            
            $filters = array(
                '*' => array(
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines()
                )
            );
            $validators = array(
                'question_type_id' => array(
                    'presence' => 'required',
                    new \Zend_Validate_Int()
                ),
                'feedback_form_page_id' => array(
                    'presence' => 'required',
                    new \Zend_Validate_Int()
                ),
                'code' => array(
                    'presence' => 'required',
                    new \Zend_Validate_NotEmpty(),
                    new \Zend_Validate_StringLength(array(
                        'max' => 45
                    ))
                )
            );
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters, array(
                'allowEmpty' => true
            ));
        }
        
        return $this->validator;
    }

    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }

    public function getValidated($id = null, $feedback_form_id, $meta)
    {
        if ($id) {
            $question = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $question = $this->model->createRow();
            $question->order = $this->getNextOrderQuestionByPage($this->validator->feedback_form_page_id);
        }
        
        $question->question_type_id = $this->validator->question_type_id;
        $question->feedback_form_page_id = $this->validator->feedback_form_page_id;
        $question->feedback_form_id = $feedback_form_id;
        $question->code = $this->validator->code;
        $question->meta = $meta;
       
        return $question;
    }
    
    public function duplicateQuestion($id, $orderNextQuestion=null,$newPageId=null,$newFeedbackFormId=null)
    {
   
        $translationService = $this->getTranslationService();
        $questionRow = $this->model->fetchRow($this->model->select()->where('id = ?',$id));
        $data = $questionRow->toArray();
        $data['id']= null;
        $data['created_at']= null;
        $data['updated_at']= null;
        $newQuestion = $this->model->createRow($data);
        if($orderNextQuestion != null){
            $newQuestion->order = $orderNextQuestion;
        }
        if($newPageId != null){
            $newQuestion->feedback_form_page_id = $newPageId;
        }
        
        if($newFeedbackFormId != null){
            $newQuestion->feedback_form_id = $newFeedbackFormId;
        }
     
        $newQuestion->code  = $questionRow->code.'cp'.$data['duplicated'];
     
        $newQuestion->save();
        
        $questionRow->duplicated = $data['duplicated']+1;
        $questionRow->save();
        
        //duplicate translations for question title
        $newQuestionTranslationUid = $translationService->insertRowTranslationUid('form__'.$newQuestion->feedback_form_id.'__question_title-'.$newQuestion->code);
        $tagId= $translationService->getTagId('form_'.$newQuestion->feedback_form_id);
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newQuestionTranslationUid->id,$tagId);
        $oldTranslationUidRow= $translationService->getTranslationUidRow('form__'.$questionRow->feedback_form_id.'__question_title-'.$questionRow->code);
        if($oldTranslationUidRow){
            $translationService->duplicateTranslation ($oldTranslationUidRow->id, $newQuestionTranslationUid->id);
        }
        //duplicate translations for question help
        $newQuestionTranslationUid = $translationService->insertRowTranslationUid('form__'.$newQuestion->feedback_form_id.'__question_help-'.$newQuestion->code);
        $tagId= $translationService->getTagId('form_'.$newQuestion->feedback_form_id);
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newQuestionTranslationUid->id,$tagId);
        $oldTranslationUidRow= $translationService->getTranslationUidRow('form__'.$questionRow->feedback_form_id.'__question_help-'.$questionRow->code);
        if($oldTranslationUidRow){
            $translationService->duplicateTranslation ($oldTranslationUidRow->id, $newQuestionTranslationUid->id);
        }
       
        return $newQuestion;
    }
    
 

    public function duplicateSousQuestions($idQuestion, $idQuestionCloned, $codeQuestionOrig, $codeQuestionCloned, $orderNextQuestion=null ,$newPageId=null,$newFeedbackFormId=null)
    {
        $translationService = $this->getTranslationService();
        
        $sousQuestions = $this->model->fetchAll($this->model->select()->where('parent_id = ?',$idQuestion));
      
        
        if (($sousQuestions ->count()) > 0){
            foreach ($sousQuestions as $sousQuestion) {
                $dataSousQuestion = $sousQuestion->toArray();
                $dataSousQuestion['id']= null;
                $dataSousQuestion['created_at']= null;
                $dataSousQuestion['updated_at']= null;
                $dataSousQuestion['parent_id']= $idQuestionCloned;
                $newSousQuestion = $this->model->createRow($dataSousQuestion);
                if($orderNextQuestion != null){
                    $newSousQuestion->order = $orderNextQuestion;
                }
                if($newPageId != null){
                    $newSousQuestion->feedback_form_page_id = $newPageId;
                }
                
                if($newFeedbackFormId != null){
                    $newSousQuestion->feedback_form_id = $newFeedbackFormId;
                }
              
                //$newSousQuestion->code  = $sousQuestion->code.'+cp'.$dataSousQuestion['duplicated'];
                $newSousQuestion->code  = str_replace ($codeQuestionOrig,$codeQuestionCloned,$sousQuestion->code);
                              
                $newSousQuestion->save();
               // $sousQuestion->duplicated = $dataSousQuestion['duplicated']+1;
               // $sousQuestion->save();
                
              
                
                //duplicate translations
                $newSousQuestionTranslationUid = $translationService->insertRowTranslationUid('form__'.$newSousQuestion->feedback_form_id.'__question_title-'.$newSousQuestion->code);

                $tagId= $translationService->getTagId('form_'.$newSousQuestion->feedback_form_id);

                $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newSousQuestionTranslationUid->id,$tagId);
                
                $oldTranslationUidRow= $translationService->getTranslationUidRow('form__'.$sousQuestion->feedback_form_id.'__question_title-'.$sousQuestion->code);
                if($oldTranslationUidRow){
                    $translationService->duplicateTranslation ($oldTranslationUidRow->id, $newSousQuestionTranslationUid->id);
                }
            }
            
        }

    }
   

    public function getSousQuestion($question_type_id, $feedback_form_page_id, $feedback_form_id, $parent_id)
    {
        $sousquestion = $this->model->createRow();
        
        $sousquestion->question_type_id = $question_type_id;
        $sousquestion->feedback_form_page_id = $feedback_form_page_id;
        $sousquestion->feedback_form_id = $feedback_form_id;
        $sousquestion->parent_id = $parent_id;
        
        return $sousquestion;
    }

    public function getEditSousQuestion($id, $code, $meta)
    {
        $sousquestion = $this->model->get(array(
            'id' => $id
        ));
        $sousquestion->code = $code;
        $sousquestion->meta = $meta;
        
        return $sousquestion;
    }

    public function getArrayQuestionTypes()
    {
        return array(
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ => 'QUESTION_TYPE_MCQ',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE => 'QUESTION_TYPE_MULTIPLE_CHOICE',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL => 'QUESTION_TYPE_INTERVAL',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED => 'QUESTION_TYPE_ORDERED',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN => 'QUESTION_TYPE_OPEN',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY => 'QUESTION_TYPE_ARRAY',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_IMAGE => 'QUESTION_TYPE_IMAGE',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_IMAGES => 'QUESTION_TYPE_MULTIPLE_IMAGES'
        );
    }

    public function getAllQuestion()
    {
        return $this->model->fetchAll();
    }

    public function getQuestionById($id)
    {
        return $this->model->get(array(
            'id' => $id
        ));
    }
    
    public function getAllSousQuestionByQuestion($questionId){

        return $this->model->select(true)->where('parent_id = '. $questionId)->fetchAll() ;
         
    }

    public function getAllFeedbackFormPage()
    {
        return $this->feedbackFormPageModel->fetchAll();
    }

    public function getFeedbackFormPageById($id)
    {
        return $this->feedbackFormPageModel->get(array(
            'id' => $id
        ));
    }

    public function getFeedbackFormId()
    {
        return $this->feedbackFormPageModel->get(array(
            'id' => $this->validator->feedback_form_page_id
        ))->feedback_form->id;
    }

    public function validateMeta($meta, $post)
    {
        if (isset($post['min']))
            $meta['min'] = (int) $post['min'];
        if (isset($post['max']))
            $meta['max'] = (int) $post['max'];
        if (isset($post['dontKnow']))
            $meta['dontKnow'] = (bool) $post['dontKnow'];
        if (isset($post['random']))
            $meta['random'] = (bool) $post['random'];
        if (isset($post['cols']))
            $meta['cols'] = (int) $post['cols'];
        if (isset($post['rows']))
            $meta['rows'] = (int) $post['rows'];
        if (isset($post['inline']))
            $meta['inline'] = $post['inline'] == '1';
        if (isset($post['condition'])) {
            $meta['condition'] = $post['condition'];
        }
        if (isset($post['dataFormat'])) {
            $meta['dataFormat'] = $post['dataFormat'];
        }
        if (isset($post['colourCode'])) {
            $meta['colourCode'] = $post['colourCode'];
        }    
        if (isset($post['nbModaliteMax'])) {
            $meta['nbModaliteMax'] = (int) $post['nbModaliteMax'];
        }
        if (isset($post['strict']) && isset($post['nbModaliteMax']))
            $meta['strict'] = $post['strict'] == '1';
        return $meta;
    }
    
    public function getNextOrderQuestionByPage($pageID)
    {
       return $this->model->select(true)
                              ->where('parent_id is null')
                              ->where('feedback_form_page_id = ' . $pageID)
                               ->fetchAll()->count();
    
    }
    
    public function getSousQuestionParentOrder($parentID)
    {
        return $this->model->select(true)
        ->where('id = ' . $parentID)
        ->fetchAll()->current()->order;
    }
    
    public function getAllQuestionsByPage($pageId){
    
        return $this->model->select(true)
                            ->where('parent_id is null')
                            ->where('feedback_form_page_id = ' . $pageId)
                            ->fetchAll();
         
    }
    
    public function getQuestionCodeTypeMCQ($feedbackFormId)
    {    
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_question',array('code'))
                           ->where('feedback_form_id = ' . $feedbackFormId)
                           ->where('question_type_id = ' . \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ);
        $results = $adapter->fetchAll($select);
        
        $questionMCQ = array();
        foreach ($results as $result){       
            $questionMCQ[] = $result['code'];
        }

        return $questionMCQ;
    } 
    
    public function getQuestionIdByCode($feedbackFormId, $code){
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
                           ->from('statflow_question',array('id'=>'id'))
                           ->where('feedback_form_id = ?',$feedbackFormId)
                           ->where('code = ?',$code);
        $question_id = $adapter->fetchOne($select);
        return $question_id;
    }
}