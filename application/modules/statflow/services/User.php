<?php

namespace Statflow\Service;

use Statflow\Service\StructureUser;
use Statflow\Service\Parameter;
use Statflow\Service\Study;
use Statflow\Service\UserFeedbackForm;

class User
{
    private $model;
    private $studyUserModel;
    private $profileModel;
    private $authBelongModel;
    private $studyModel;
    private $feedBackFormModel;
    private $structureModel;
    private $validator;
    private $mail;
    private $export;
    
    // Trace suite à import 0: insert, 1:update, 2:error
    private $import_status;
    
    public function __construct($model = null,$profile = null, $authBelongModel = null,$mail=null, $studyModel=null, $feedBackFormModel=null, $structureModel=null)
    {
        $this->setModel($model);
        $this->setProfile($profile);
        $this->setAuthBelong($authBelongModel);
        $this->setStudyModel($studyModel);
        $this->setStructureModel($structureModel);
        $this->setFeedBackFormModel($feedBackFormModel);
        $this->setMail($mail);
    
    }
    
 
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('auth/user');
        }
        $this->model = $model;
    }

        
    public function setProfile($profile)
    {
        if (!$profile) {
            $profile = \Centurion_Db::getSingleton('user/profile');
        }
        $this->profileModel = $profile;
    }

    
    public function setMail($mail)
    {
        if (!$mail) {
            $mail = new \Zend_Mail('UTF-8');
        }
        $this->mail = $mail;
    }

    /**
     * @return \Zend_Mail
     */
    public function getMai()
    {
        return $this->mail;
    }
    
    public function setAuthBelong($authBelongModel)
    {
        if (!$authBelongModel) {
            $authBelongModel = \Centurion_Db::getSingleton('auth/belong');
        }
        $this->authBelongModel = $authBelongModel;
    }
    
    /**
     *
     * set $export
     */
    public function setExport($val)
    {
        $this->export = $val;
    }
    
    /**
     * @return the $structureModel
     */
    public function getStructureModel()
    {
        return $this->structureModel;
    }
    
    /**
     * @param field_type $structureModel
     */
    public function setStructureModel($structureModel)
    {
        if (!$structureModel) {
            $structureModel = \Centurion_Db::getSingleton('statflow/structure');
        }
        $this->structureModel = $structureModel;
    }
    
    
    /**
     * @return the $feedBackFormModel
     */
    public function getFeedBackFormModel()
    {
        return $this->feedBackFormModel;
    }
    
    /**
     * @param field_type $feedBackFormModel
     */
    public function setFeedBackFormModel($feedBackFormModel)
    {
        if (!$feedBackFormModel) {
            $feedBackFormModel = \Centurion_Db::getSingleton('statflow/feedbackForm');
        }
        $this->feedBackFormModel = $feedBackFormModel;
    }
    
    /**
     * @param field_type $studyModel
     */
    public function setStudyModel($studyModel)
    {
        if (!$studyModel) {
            $studyModel = \Centurion_Db::getSingleton('statflow/study');
        }
        $this->studyModel = $studyModel;
    }
    
    
    /**
     * @return the $studyModel
     */
    public function getStudyModel()
    {
        return $this->studyModel;
    }
    
    /**
     * get $export
     */
    public function getExport()
    {
        return $this->export;
    }
    
    public function getValidator($parameters, $action)
    {
        if (! $this->validator) {
            $filters = array(
                'user_add' => array(
                    '*' => array(
                        new \Zend_Filter_StringTrim(),
                        new \Zend_Filter_StripTags(),
                        new \Zend_Filter_StripNewlines(),
                    ),
                    'password' => array(
                     //   'allowEmpty' => true,
                        //new \Zend_Filter_StringLength(array('max' => 8))
                    ),
                ),
                'user_edit' => array(
                    '*' => array(
                        new \Zend_Filter_StringTrim(),
                        new \Zend_Filter_StripTags(),
                        new \Zend_Filter_StripNewlines(),
                    ),
                    //'password' => array(
                     //   'allowEmpty' => true,
                    //),
                    'phone' => array(
                        'allowEmpty' => true,
                    ),
                    'mobile' => array(
                        'allowEmpty' => true,
                    ),
                    'id' => new \Zend_Filter_Digits()
                ),
            );
            
            $validators = array(
                'user_add' => array(
                    'username' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 255)),
                    ),
                    'email' => array(
                        'presence' => 'required',
                        new \Zend_Validate_EmailAddress(),
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 354)),
                    ),
                    'password' => array(
                     //   'allowEmpty' => true,
                    //    'presence' => 'required',
                    //    new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 128)),
                    ),
                    'first_name' => array(
                        //new Zend_Validate_Alnum(),
                        new \Zend_Validate_StringLength(array('max' => 30)),
                    ),
                    'last_name' => array(
                        //new Zend_Validate_Alnum(),
                        new \Zend_Validate_StringLength(array('max' => 30)),
                    ),
            
                    'nickname' => array(
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 50)),
                    ),
                    'about' => array(
                        //new Zend_Validate_NotEmpty(),
                    ),
                    'website' => array(
                        new \Zend_Validate_StringLength(array('max' => 150)),
                    ),
                    'phone' => array(
                        new \Zend_Validate_Digits(),
                        new \Zend_Validate_StringLength(array('max' => 20)),
                    ),
                    'mobile' => array(
                        new \Zend_Validate_Digits(),
                        new \Zend_Validate_StringLength(array('max' => 20)),
                    ),
                ),
                'user_edit' => array(
                    'id' => array(
                        'presence' => 'required',
                        new \Zend_Validate_Int(),
                    ),
                    'username' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 255)),
                    ),
                    'email' => array(
                        'presence' => 'required',
                        new \Zend_Validate_EmailAddress(),
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 354)),
                    ),
                    //'password' => array(
                        //'allowEmpty' => true,
                    //    new \Zend_Validate_NotEmpty(),
                     //  new \Zend_Validate_StringLength(array('max' => 128)),
                    //),
                    'first_name' => array(
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 30)),
                    ),
                    'last_name' => array(
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 30)),
                    ),
            
                    'nickname' => array(
                        new \Zend_Validate_StringLength(array('max' => 50)),
                    ),
                    'about' => array(
                        //new Zend_Validate_Alnum(array('allowWhiteSpace' => true)),
                    ),
                    'website' => array(
                        new \Zend_Validate_StringLength(array('max' => 150)),
                    ),
                    'phone' => array(
                        new \Zend_Validate_Digits(),
                        new \Zend_Validate_StringLength(array('max' => 20)),
                    ),
                    'mobile' => array(
                        new \Zend_Validate_Digits(),
                        new \Zend_Validate_StringLength(array('max' => 20)),
                    )
                )
            
            );
            $this->validator = new \Zend_Filter_Input($filters[$action], $validators[$action], $parameters, array('allowEmpty' => true));
        }

        return $this->validator;
    }
    
    
    public function isValid($parameters, $action)
    {
        return $this->getValidator($parameters, $action)->isValid();
    }
    
    public function getValidated($mdp, $can_be_deleted, $is_active, $is_super_admin,$is_staff,$id = null)
    {          
        if ($id) {
            $user = $this->model->get(array('id' => $this->validator->id));
        } else {
            $user = $this->model->createRow();
            //$user->password = $this->validator->password;
            if($mdp != "") {
                $user->password = $mdp;
            }
        }
            $user->username = $this->validator->username;
            $user->email = $this->validator->email;

            $user->first_name = $this->validator->first_name;
            $user->last_name = $this->validator->last_name;
            
            $user->can_be_deleted = $can_be_deleted;
            $user->is_active = $is_active;
            $user->is_super_admin = $is_super_admin;
            $user->is_staff = $is_staff;
            
            $user->save();
            return $user;
          
 
    }
    
    public function deleteUser($id)
    {
       return $this->model->deleteRow("id = ".$id);
 
    }
    
    public function saveProfile($userId =null)
    {
   
         if($userId){ //add profile
            $profile = $this->profileModel->createRow(array( 'user_id' => $userId));
         }else { //update
            $profile = $this->profileModel->get(array('user_id' => $this->validator->id));
         }
         $profile->nickname = $this->validator->nickname;
         $profile->about = $this->validator->about;
         $profile->website = $this->validator->website;
         $profile->phone = $this->validator->phone;
         $profile->mobile = $this->validator->mobile;
    
         $profile->save();
         return $profile;
        
    }

    public function saveProfilImport($userId)
    {
        if($userId){ //add profile
            $profile = $this->profileModel->createRow(array( 'user_id' => $userId));
        }
        
        $profile->save();
    }
    
    public function saveUserBelong($id, $groupId)
    {
        $group=$this->authBelongModel->createRow(array(
            'user_id' => $id,
            'group_id' => $groupId
        ));
    
        $group->save();
        return $group;
    }

    

    public function sendMail($email_to, $subject, $urlLink, $user_id, $group_id, $studyInfo, $parameters)
    {
        $smtpHost = 'pro1.mail.ovh.net';
        $smtpConf = array(
            'port' => '587',
            'ssl' => 'tls',
            'auth' => 'login',
            'username' => 'webmaster@it-prospect.com',
            'password' => 'itprospect@@',
            'timeout' => 60
        );
        $transport = new \Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
        $translator = \Zend_Registry::get('Zend_Translate');

        /*
        // Donnees par defauts
        $email_from = "mnasri.walid@gmail.com";
        $email_from_name = "Statflow";
        $bodyHtml   = "<html><p>Dear user,</p><br>
        <p>To set your password on the Statflow platform, please use the following link :<br><a href='$urlLink'>Password initialization</a></p><br>
        <p>Your login id is your email address.</p><br>
        <p>Sincerely yours,<br>The Statflow Support Team</p></html>";
        $bodyText   = "Dear user,
        To set your password on the Statflow platform, please use the following link :'$urlLink'
        Your login id is your email address.
        Sincerely yours,
        The Statflow Support Team";*/

        $email_from_name = $parameters[0]['value'];
        $email_from      = $parameters[1]['value'];
        $bcc_email       = $parameters[2]['value'];
        $bodyText        = str_replace("urlLink",$urlLink, $translator->translate('param_email_connexion_body_text'));
        $bodyHtml        = str_replace("urlLink",$urlLink, $translator->translate('param_email_connexion_body_html'));
        $subject         = $translator->translate('param_email_connexion_subject');
        if($group_id == '2' && !is_null($studyInfo)){  
            $translator = \Zend_Registry::get('Zend_Translate');    
            $isActif = '';
            foreach ($studyInfo as $key) :
                $study_id = $key['study_id'];
                $isActif  = $key['actif_connex'];
                $sender_email_connex = $key['sender_email_connex'];
                $sender_name_connex =  $key['sender_name_connex'];
            endforeach;
            if($isActif == 1) {
                $email_from = $sender_email_connex;
                $email_from_name = $sender_name_connex;
                $bodyText  = str_replace("urlLink",$urlLink, $translator->translate('email_connexion_body-text_study_'.$study_id));
                $bodyHtml = str_replace("urlLink",$urlLink, $translator->translate('email_connexion_body_study_'.$study_id));
                $subject   = $translator->translate('email_connexion_subject_study_'.$study_id);
            }
        }
 
        $mail = $this->getMai();

        $mail->setFrom($email_from, $email_from_name);
        $mail->addTo($email_to);
        //$mail->addBcc('mnasri.walid@gmail.com');
        $mail->addBcc($bcc_email);
        $mail->setSubject($subject);
        $mail->setBodyHtml($bodyHtml);
        $mail->setBodyText($bodyText);
        $mail->send($transport);
        $mail->clearFrom();
        $mail->clearSubject();
        $mail->clearRecipients();
        $mail->clearMessageId();
    }
    
 
    public function getById($id)
    {
        return $this->model->get(array('id' => $id ));
    
    }
    
    public function getAll()
    {
      return $this->model->select(true)->order('updated_at DESC')->fetchAll();
    }
    
    
    public function getAllAuthBelong($userId)
    {
        return $this->authBelongModel->select(true)->where('user_id = '. $userId)->fetchAll() ;
                                                    
    }
    
    public function exportUser()
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select();
        $select->from('auth_user')
        ->join('auth_belong', 'auth_user.id=auth_belong.user_id');
    
        $headers = array(
            'ID',
            'Username',
            'First name',
            'Last name',
            'Email',
            'Group',
            'Created at',
            'Last login',
            'Study Id',
            'FeedbackForm Id',
            'Structure Id',
            'Item to hide'
        );
    
        $export = $this->getExport();
    
        if ($export) {
            $this->exportCSV($adapter, $select, $headers);
        }
    }
    
    
    
    
    public function exportCSV($adapter, $select, $headers)
    {
        $structureUser = new Structure();
        $userFrontItem = new UserFrontItem();
        $studyUser = new StudyUser();
        $userfeedBackForm = new UserFeedbackForm();
        
        $currentDateTime = date("dmYHis");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition:inline; filename="' . 'Export-User_' . $currentDateTime . '.csv"');
        header("Pragma: ");
        header("Cache-Control: ");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
    
        echo implode(',', str_replace(array(
            '|',
            "\r\n",
            "\r",
            "\n",
            "<br>",
            "<br/>",
            "<br />"
        ), array(
            ';',
            ''
        ), $headers)) . "\r\n";
        
        /*
         *  'ID',              [0]      
            'Username',        [1] 
            'First name',      [2]
            'Last name',       [3]
            'Email',           [4]
            'Group',           [5] 
            'Created at',      [6]
            'Last login',      [7]
            'Study Id',        [8]
            'FeedbackForm Id', [9]
            'Structure Id',    [10]
            'Item to hide'     [11]
         */
        
        $dataUser = $adapter->fetchAll($select);
        foreach ($dataUser as $line) {
            $tmpArray = array();
            $tmpArray[] = $line['id'];
            $tmpArray[] = html_entity_decode(strip_tags($line['username']));
            $tmpArray[] = html_entity_decode(strip_tags($line['first_name']));
            $tmpArray[] = html_entity_decode(strip_tags($line['last_name']));
            $tmpArray[] = $line['email'];
            $tmpArray[] = ($line['group_id'] == 1) ? 'Administrator': 'Client';
            $tmpArray[] = $line['created_at'];
            $tmpArray[] = $line['last_login'];
    
            // get study id
            $allStudyId = $studyUser->getAllStudyByUserId($line['id']);
            $strStudyId = "";
            foreach ($allStudyId as $studyId){
                $strStudyId.= $studyId['study_id']."-";
            }
            
            $strStudyId = substr($strStudyId, 0, -1);
            $tmpArray[]   = html_entity_decode(strip_tags($strStudyId));
            
            
            // get feedback form id if group_id != 1 (i.e if Client)
            $allUserFeedBackFormId = $userfeedBackForm->getAllFeedbackFormByUserId($line['id']);
            $strUserFeedBackFormId = "";
            foreach ($allUserFeedBackFormId as $userFeedbackFormId) {
                $strUserFeedBackFormId.= $userFeedbackFormId['feedback_form_id']."-";
            }
            
            $strUserFeedBackFormId = substr($strUserFeedBackFormId, 0, -1);
            $tmpArray[]   = ($line['group_id'] == 1) ? '': html_entity_decode(strip_tags($strUserFeedBackFormId));   
            
            
            //get Structure attached
            $allStructureAttached = $structureUser->getAllStructureByUser($line['id']);
            $strStructure = "";
            foreach ($allStructureAttached as $structure) {
                $strStructure.= $structure['id']."-";
            }
            
            $strStructure = substr($strStructure, 0, -1);
            $tmpArray[]   = html_entity_decode(strip_tags($strStructure));

            //get items to hide in the front office for user
            $frontItemsToHide = $userFrontItem->getFrontItemIDByUserId($line['id']);
            $frontItem = "";
            foreach ($frontItemsToHide as $item){
                $frontItem .= $item."-";
            }
            
            $frontItem = substr($frontItem, 0, -1);
            $tmpArray[]   = html_entity_decode(strip_tags($frontItem));

            echo implode(',', str_replace(array(
                '|',
                "\r\n",
                "\r",
                "\n",
                "<br>",
                "<br/>",
                "<br />"
            ), array(
                ';',
                ''
            ), $tmpArray)) . "\r\n";
        }
        die();
    }
    
    
    
    
    
    /*
     * IMPORT des fichiers csv des utilisateurs
     */
    
    public function getImportStatus()
    {
        return $this->import_status;
    }
    
    
    function check_email_address($email) {
        if (!strpos($email,'@')) {
            return false;
        }
        
        if (!strpos($email,'.')) {
            return false;
        }
        
        return true;

    }
    
     
    // Fonction d'import avec statut 0: insert, 1:update, 2:error
    public function importCsvUtilisateur($csv_utilisateur)
    {
        $study = new Study();
        $this->import_status = 0; // insert new user
       
        
        /*
         *  'ID',              [0]
         'Username',        [1]
         'First name',      [2]
         'Last name',       [3]
         'Email',           [4]
         'Group',           [5]
         'Created at',      [6]
         'Last login',      [7]
         'Study Id',        [8]  - to be saved
         'FeedbackForm Id', [9]  - to be saved
         'Structure Id',    [10] - to be saved
         'Item to hide'     [11]
         */
        
        $id = $csv_utilisateur[0];
        // si username ou email ou group sont a vide => log une erreur
        if($csv_utilisateur[1] == '' || $csv_utilisateur[4] == '') {
            $this->import_status = 2;
            return NULL;
        }
        
        
        if (!$this->check_email_address($csv_utilisateur[4])) {
             $this->import_status = 2;
             return NULL;
         }
        
        
        if($csv_utilisateur[5] == 'Administrator' || $csv_utilisateur[5] == 'Client') {
            
            $strStudyId = $csv_utilisateur[8];
            $arrStudyId = explode("-", $strStudyId);
            $countArrStudyId = count($arrStudyId);
            
            
            if($csv_utilisateur[5] == 'Client' && $countArrStudyId > 1) {
                $this->import_status = 2;
                return NULL;
            }
            
            // Si Admin, et présence de questionnaire id, rejeter la ligne
            if($csv_utilisateur[5] == 'Administrator' && $csv_utilisateur[9] != '') {
                $this->import_status = 2;
                return NULL; // 
            }
            
            
            // verifier si id study existe dans la base
            if($strStudyId != "") {
                if(!$this->isValidStudyId($strStudyId, $arrStudyId)) 
                    return NULL;
            }

            
            // verifier si id questionnaire existe dans la base
            $strFeedBackFormId = $csv_utilisateur[9];
            $listStudyId = implode(",", $arrStudyId);
            
            if($strFeedBackFormId != "" && $strStudyId != "") {
                if(!$this->isValidFeedBackFormId($strFeedBackFormId, $listStudyId)) 
                    return NULL;
            }
            
            
            // verifier si structure existe en base ou pas
            $strStructureId = $csv_utilisateur[10];
            if($strStructureId != "") {
                if(!$this->isValidStructureId($strStructureId))
                    return NULL;
            }
            
            if ($id) {
                if ($this->model->select(true)->where('id = ' . $id)->fetchAll()->count() == 0){
                    //ligne en échec, car id introuvable dans la base
                    $this->import_status = 2;
                    return NULL;
                }else{ //update
                    
                    // TO DO -> Method to update or insert
                    
                    $user = $this->model->get(array('id' => $id));
                    $this->import_status = 1;
                }
            } else {// insert new user
                $user = $this->model->createRow();
                
            }
        }else { // si group n'est pas Administrateur ou Client ou a vide => log une erreur
            $this->import_status = 2;
            return NULL;
        }
    
        $user->username = $csv_utilisateur[1];
        $user->first_name = $csv_utilisateur[2];
        $user->last_name = $csv_utilisateur[3];
        $user->email = $csv_utilisateur[4];
        
        // save group with $csv_utilisateur[5]
        
        $user->created_at = $csv_utilisateur[6];
        $user->last_login = $csv_utilisateur[7];
    
        return $user;
    }
    

    public function isValidStudyId($strStudyId, $arrStudyId){
        // verifier si id study existe dans la base
        
            foreach ($arrStudyId as $key=>$value){
                if ($this->studyModel->select(true)->where('id = ' . $value)->fetchAll()->count() == 0){
                    //ligne en échec, car id introuvable dans la base
                    $this->import_status = 2;
                    return false;
                }
            }
        
        return true;
    }
    
    public function isValidFeedBackFormId($strFeedBackFormId, $listStudyId){
        // verifier si id questionnaire existe dans la base
        
            $arrFeedBackFormId = explode("-", $strFeedBackFormId);
            foreach ($arrFeedBackFormId as $keyFBId=>$valueFBId) {
                if ($this->feedBackFormModel->select(true)
                    ->where('id = ' . $valueFBId)
                    ->where('study_id IN ('.$listStudyId.')')
                    ->fetchAll()->count() == 0){
                    //ligne en échec, car id introuvable dans la base
                    $this->import_status = 2;
                    return false;
                }//else
                    //   echo "<br>FBexist=".$valueFBId;
            }
         
            return true;
    }
    
    public function isValidStructureId($strStructureId){
        // verifier si structure existe en base ou pas
        
            $arrStructureId = explode("-", $strStructureId);
            foreach ($arrStructureId as $key=>$value){
                if ($this->structureModel->select(true)->where('id = ' . $value)->fetchAll()->count() == 0){
                    //ligne en échec, car id introuvable dans la base
                    $this->import_status = 2;
                    return false;
                }
            }
            return true;
        
    }
     
    //Fonction de lecture du fichier utilisateur csv
    public function readCSV($csv_file){
        //vérif nombre de colonne
        $column_count = 12;
        //vérif nombre de ligne
        $line_count=2;
        //vérif du header et ne pas le garder
        $row_count=1;
    
        $file_handle = fopen($csv_file, "r");
        while (($single_line = fgetcsv($file_handle, 1024)) !== FALSE) {
            // vérif format du fichier
            if(count($single_line) != $column_count){
                $line_of_text=NULL;
                break;
            }
            // sauvegarde données csv dans array
            if($row_count > 1) $line_of_text[] = $single_line;
    
            $row_count++;
        }
        fclose($file_handle);
    
        return $line_of_text;
    }
    
    //Fonction de lecture du fichier log suite à l'import
    public function getImportResult() {
        $result_file = "/tmp/import_user_log.txt";
        if (file_exists($result_file)){
            //die();
            $file_handle = fopen($result_file, "r");
            while (($result_line = fgetcsv($file_handle, 1024)) !== FALSE) {
                $result[] = $result_line;
            }
            fclose($file_handle);
            unlink($result_file);
            return $result;
        }
        return NULL;
    }
    

    // Delete a list of user id
    public function deleteByListUserId($listid)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        return $adapter->delete('auth_user','id in ('.$listid.')');
        
        //return $adapter->delete('auth_user','is_super_admin=0 and id in ('.$listid.')');
    }
    
    public function studyIdExist($studyId){
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        return $adapter->select('statflow_study', 'id ='.$studyId);
    }
  
    /*
    public function setStudyUser($studyUserModel)
    {
        if (!$studyUserModel) {
            $studyUserModel = \Centurion_Db::getSingleton('statflow/studyUser');
        }
        $this->studyUserModel = $studyUserModel;
    }
    public function getAllStudyByUserId($userId)
    {
        return $this->studyUserModel->select(true)
        ->where('user_id = ' . $userId)
        ->fetchAll();
    }
    */
}