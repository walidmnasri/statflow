<?php

namespace Statflow\Service;

class Import
{
    private $_config;
    private $model;
    private $adapter;
    
    public function __construct($model=null, $adapter = null)
    {        
        $this->setModel($model);
        $this->setAdapter($adapter);
        $this->_config = array();
    }
    
    public function setModel($model)
    {
        if (!$model) {
            $model = new \Statflow_Service_ImportCSV();
        }
        $this->model = $model;
    }
    
    public function getModel()
    {
        return $this->model ;
    }
    
    
    public function setAdapter($adapter)
    {
        if (!$adapter) {
            $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        }
        $this->adapter = $adapter;
    }
    
    public function getAdapter()
    {
        return $this->adapter ;
    }
    /**
     * @param $file_path
     * @param $study_id
     * @param $feedback_form_id
     * @void
     */
    public function importCSV($file_path, $study_id, $feedback_form_id,$error)
    {
        $logfile_path = APPLICATION_PATH . '/../' . 'public/ftp/' . $study_id . '/' . $feedback_form_id . '/logs' .
            DIRECTORY_SEPARATOR . 'export-' . time() . '.log';
        $model = $this->getModel();
        $model->setFileName($file_path);
        $model->setConfig($this->_config);

        $hasErrors = false;
        try {
            $model->checkAndLoadData($feedback_form_id);
            if (!$model->isValid()) {
                $lf = fopen($logfile_path, "w");
                fwrite($lf, \Zend_Debug::dump($model->getValidationErrors(), $error, false));
                fclose($lf);
                $hasErrors = true;
            }
        } catch (\Zend_Exception $e) {
            $lf = fopen($logfile_path, "w");
            fwrite($lf, \Zend_Debug::dump($e->getMessage(), $error, false));
            fclose($lf);
            $hasErrors = true;
            return 'Error to open logs';
        }
    
        try {
            $model->removeDuplicates();
            $model->saveData($study_id, $feedback_form_id, $logfile_path);
    
        } catch (\Zend_Exception $e) {
            $lf = fopen($logfile_path, "w");
            fwrite($lf, \Zend_Debug::dump($e->getMessage(), $error, false));
            fclose($lf);
            $hasErrors = true;
            return 'Error to open logs';
        }
       // if ($hasErrors == false) {
            $importDirectory = dirname($file_path);
	        exec("find " . $importDirectory . " -type d -exec chmod -R 777 {} +");
            exec("find " . $importDirectory . DIRECTORY_SEPARATOR . 'archives' . " -type d -exec chmod -R 777 {} +");
            copy($file_path, $importDirectory . DIRECTORY_SEPARATOR . 'archives' . DIRECTORY_SEPARATOR . basename($file_path));
            unlink($file_path);
       // }
    }

    /**
     * @void
     */
    public function export($study_id, $feedback_form_id, $serverUrl, $file)
    {
        $adapter = $this->getAdapter();
        //main request
        $select = $adapter->select();
        $select
        ->from('statflow_respondent')
        ->reset(\Zend_Db_Select::COLUMNS)
        ->where('statflow_respondent.status = 0')
        ->where('statflow_respondent.feedback_form_id = ?', $feedback_form_id);
    
        $select
        ->columns('statflow_respondent.id as respondent_id')
        ->columns('statflow_respondent.token as token')
        ->columns('statflow_respondent.email as email')
        ->columns('statflow_respondent.locale as language');
        
        $respondentFields = array();
        if (isset($this->_config['exportedRespondentFields'])) {
            $respondentFields = $this->_config['exportedRespondentFields'];
            foreach ($respondentFields as $field) {
                $select->columns('statflow_respondent.' . $field);
            }
        }
        
        //External fields
        $fields = $this->_config['exportedFields'];
        foreach ($fields as $field) {
            $subSelect = $adapter->select();
            $subSelect
            ->from('statflow_respondent_external_field', array('statflow_respondent_external_field.value'))
            ->joinInner(
                'statflow_import_field',
                'statflow_import_field.id = statflow_respondent_external_field.import_field_id', array()
            )
            ->where('statflow_respondent_external_field.respondent_id = statflow_respondent.id')
            ->where('statflow_import_field.label = ?', $field);
            $select->columns('(' . new \Zend_Db_Expr($subSelect->assemble()) . ') as ' . $field);
        }
        $fields = array_merge($fields, $respondentFields);
        $respondents = $adapter->fetchAll($select);
        $parsedRespondent = array();
        
        $ids = array();
        for ($i = 0; $i < count($respondents); $i++) {
            $ids[] = $respondents[$i]['respondent_id'];
            $respondents[$i] = array_merge(
                array(
                    'link' => sprintf(
                        $serverUrl . '/statflow/questionnaire/form/id/%s'
                        . '/token/' . $respondents[$i]['token']
                        , $feedback_form_id
                    )
                ),
                $respondents[$i]
            );
            $parsedRespondentData = array(
                'link' => $respondents[$i]['link'],
                'email' => $respondents[$i]['email'],
                'language' => $respondents[$i]['language']
            );
            foreach ($fields as $field) {
                $parsedRespondentData[$field] = $respondents[$i][$field];
            }
            $parsedRespondent[] = $parsedRespondentData;
    
            unset($respondentRow);
            if ($i % 100 == 0) gc_collect_cycles();
        }
        if(count($ids) > 0) {
            \Zend_Db_Table_Abstract::getDefaultAdapter()->query('UPDATE statflow_respondent SET status = 1 WHERE id IN (' . join(',', $ids) . ')');
        }
        if(count($parsedRespondent) > 0) {
            $fileName = substr($file, 0, -4) . '__' . $study_id . '__' . $feedback_form_id . '__export_statflow.csv';
            $file = fopen(APPLICATION_PATH . '/../public/ftp/' . $study_id . '/' . $feedback_form_id . '/export/'.  $fileName, 'w');
            fwrite($file, $this->arrayToCSV($parsedRespondent));
            fclose($file);
            return 'Exported to -> ' . $fileName;
        }
        else
            return 'Nothing to export';
    
        //echo 'ok';
    }
    
    /**
     * Generatting CSV formatted string from an array.
     * By Sergey Gurevich.
     */
    public function arrayToCSV($array, $header_row = true, $col_sep = ";", $row_sep = "\n", $qut = '')
    {
        if (!is_array($array) or !is_array($array[0])) {
            return false;
        }
        $output = '';
        //Header row.
        if ($header_row) {
            foreach ($array[0] as $key => $val) {
                //Escaping quotes.
                $key = str_replace($qut, "$qut$qut", $key);
                $output .= "$col_sep$qut$key$qut";
            }
            $output = substr($output, 1) . "\n";
        }
        //Data rows.
        foreach ($array as $key => $val) {
            $tmp = '';
            foreach ($val as $cell_key => $cell_val) {
                //Escaping quotes.
                $cell_val = str_replace($qut, "$qut$qut", $cell_val);
                $tmp .= "$col_sep$qut$cell_val$qut";
            }
            $output .= substr($tmp, 1) . $row_sep;
        }
        return $output;
    }
    
    public function cronImportExport($study_id, $feedback_form_id, $error, $serverUrl)
    {
        $configFilePath = APPLICATION_PATH . '/modules/statflow/configs/streams/' . $study_id . '/' . $feedback_form_id . '/config.php';
        if (\Sayyes_Tools_Filesystem::fileExist($configFilePath)) {
            include_once ($configFilePath);
            $this->_config = ${'config__'.$study_id.'__'.$feedback_form_id};
        }
        else {
            //throw new \Zend_Exception('mandatory config file is missing -> ' . $configFilePath);
            return 'No file config.php in directory';
        }
        $baseDir = APPLICATION_PATH . '/../public/ftp/' . $study_id . '/' . $feedback_form_id . '/import';
        $files = \Sayyes_Tools_Filesystem::getFiles($baseDir);
        \Sayyes_Tools_Console::printLine('Files to import');
        \Zend_Debug::dump($files);
        if(count($files) > 0) {
            foreach ($files as $file) {
                \Sayyes_Tools_Console::printLine('Importing -> ' . $file);
                $this->importCSV($baseDir . DIRECTORY_SEPARATOR . $file, $study_id, $feedback_form_id,$error);
                \Sayyes_Tools_Console::printLine('Imported -> ' . $file);
                gc_collect_cycles();
            }
            \Sayyes_Tools_Console::printLine('Exporting');
            $this->export($study_id, $feedback_form_id, $serverUrl, $file);
            return 'Imported -> ' . $file;
        }
        else {
            \Sayyes_Tools_Console::printLine('Nothing to import');
            return 'Nothing to import';
        }
    }
    
    public function setConfig($config){
        $this->_config = $config;
    }
    
    public function getConfig(){
        return $this->_config ;
    }

    /**
     * Import Flux
     * @param $csv_file
     * @array
     */    
    public function readCSV($csv_file){
        //vérif nombre de colonne
        $column_count = 3;
        //vérif nombre de ligne
        $line_count=2;
        //vérif du header et ne pas le garder
        $row_count=1;
        $file_handle = fopen($csv_file, "r");
        while (($single_line = fgetcsv($file_handle, 1024)) !== FALSE) {
            // vérif format du fichier
            if(count($single_line) != $column_count){
                $line_of_text=NULL;
                break;
            }
            // sauvegarde données csv dans array
            if($row_count > 1) $line_of_text[] = $single_line;
            $row_count++;
        }
        fclose($file_handle);
        return $line_of_text;
    }
    /**
     * Import Flux
     * @param $questionnaireId
     * @study_id
     */
    public function getStudyIdByQuestionnaireId($questionnaireId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from(array('ff' => 'statflow_feedback_form'), array('study_id'))
        ->where('ff.id = ' . $questionnaireId);
        return $adapter->fetchOne($select);
    }
    /**
     * Import Flux
     * @param $respondentId
     * @param $feedbackFormId
     * @param $studyId
     * @true/false
     */
    public function isValidResondentId($respondentId, $feedbackFormId, $studyId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from(array('cr' => 'statflow_respondent'), array("cnt"=>"count(cr.id)"))
        ->where('cr.id = ' . $respondentId)
        ->where('cr.study_id = ' . $studyId)
        ->where('cr.feedback_form_id = ' . $feedbackFormId);
        return ($adapter->fetchOne($select) == 1);
    }    
    /**
     * Import Flux
     * @param $file_path
     * @param $study_id
     * @param $questionnaire_id
     * @import result string
     */
    public function importFluxCSV($file_path, $study_id, $questionnaire_id)
    {
        $count_update=0;
        $count_reject=0;
        $line_error="";
        $line_index=0;
        $count_non_modified=0;
	$line_non_modified="";
        // Read CSV file
        $arrayFlux = $this->readCSV($file_path);
        // Verify whether respondent id exist
        if($arrayFlux){
            foreach($arrayFlux as $ligneFlux){
                $line_index++;
                $Respondent_id = $ligneFlux[0];
                $Deliverability = $ligneFlux[1];
                $Deliverability_status_date = $ligneFlux[2];
                if($Deliverability_status_date == "" || !(\Zend_Date::isDate($Deliverability_status_date,'Y-m-d H:i:s'))){ 
                    $count_reject++;     
                    $line_error .= $line_index." : Date vide ou format non reconnu <br>";
                }else {
                    try{
                        $date = new \DateTime($Deliverability_status_date);
                    }
                    catch(\Zend_Exception $e){
                        $count_reject++;
                        $line_error .= $line_index." : Impossible de convertir la date pour injection en base <br>";
                    }
                    if($date && $this->isValidResondentId($Respondent_id, $questionnaire_id, $study_id) && is_numeric($Deliverability)){   
                        //update deliverability
                        $sqlUpate = "UPDATE statflow_respondent SET deliverability = ".$Deliverability;
                        $sqlUpate = $sqlUpate." ,deliverability_status_date = '".$date->format('Y-m-d H:i:s')."'";
                        $sqlUpate =  $sqlUpate." WHERE id= $Respondent_id";
                        $sqlUpate =  $sqlUpate." AND feedback_form_id= $questionnaire_id";
                        $sqlUpate =  $sqlUpate." AND study_id= $study_id";
                        if ( \Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlUpate)->rowCount() == 0){
                            $count_non_modified++;
                            $line_non_modified .= $line_index." , ";
                        }else{
                            $count_update++;
                        }
                    }else{
                        //reject
			if(!$this->isValidResondentId($Respondent_id, $questionnaire_id, $study_id)){
                        $count_reject++;
                        	$line_error .= $line_index." : R&eacute;pondant non trouv&eacute; pour ce questionnaire <br>";
			}
			if(!is_numeric($Deliverability)){
				$count_reject++;
                        	$line_error .= $line_index." : Le champ delivrability doit etre un entier <br>";
			}
                    }
                }
            }
            \Sayyes_Tools_Console::printLine("<br>Nombre de lignes mises &agrave; jour: ".$count_update);
            \Sayyes_Tools_Console::printLine("<br>Nombre de lignes non modifi&eacute;es car identique: ".$count_non_modified);
\Sayyes_Tools_Console::printLine("<br>Num&eacute;ro des lignes non modifi&eacute;: ".$line_non_modified."<br>");
            \Sayyes_Tools_Console::printLine("<br>Nombre de lignes reject&eacute;es: ".$count_reject);
            \Sayyes_Tools_Console::printLine("<br><br>Log des lignes en erreur: <br><br>".$line_error."<br>");
            return true;
        }       
        return false;
    }    
    /**
     * Import Flux
     * @param $questionnaireId
     * @param $nom_flux
     * @param $error
     * @param $serverUrl
     * @study_id
     */    
    public function importFlux($questionnaire_id, $nom_flux)
    {
        //Get study_id from questionnaire_id to build up the path to the flux file
        $study_id = $this->getStudyIdByQuestionnaireId($questionnaire_id);
        if($study_id){
            //Verfiy if the flux file exists
            $fileFlux = APPLICATION_PATH . '/../public/ftp/' . $study_id . '/' . $questionnaire_id . '/flux/'.$nom_flux;            
            if (\Sayyes_Tools_Filesystem::fileExist($fileFlux)) {
                \Sayyes_Tools_Console::printLine('Importing flux -> ' . $nom_flux."<br>");
                if($this->importFluxCSV($fileFlux, $study_id, $questionnaire_id)){
                    $fileFlux_new = APPLICATION_PATH . '/../public/ftp/' . $study_id . '/' . $questionnaire_id . '/flux/imported_'.$nom_flux;
                    rename($fileFlux, $fileFlux_new);
                    \Sayyes_Tools_Console::printLine("<br>Import r&eacute;ussi");
                    return "Import réussi";
                }   else{
                    \Sayyes_Tools_Console::printLine("<br>Import &eacute;chou&eacute;");
                    return "Import échoué";
                }                
            }
            else {
                \Sayyes_Tools_Console::printLine('Le fichier '.$nom_flux." n'existe pas");
                return 'Le fichier '.$nom_flux." n'existe pas";
            }            
        }else{
            \Sayyes_Tools_Console::printLine("Aucune &eacute;tude trouv&eacute;e avec ce questionnaire id ".$questionnaire_id);
            return "Aucune étude trouvée avec ce questionnaire id ".$questionnaire_id;            
        }        
    }    
}

