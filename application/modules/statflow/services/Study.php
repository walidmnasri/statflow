<?php
namespace Statflow\Service;

require_once __DIR__ . '/../services/Questionnaire.php';
require_once __DIR__ . '/../services/Translation.php';

use Statflow\Service\Questionnaire;
use Statflow\Service\Translation;

class Study
{

    private $model;

    private $questionModel;

    private $feedbackFormModel;
    
    private $studyUserModel;

    private $validator;
    private $translationUid;
    private $translation;
    private $questionnaireService;
    private $translationService;

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/study');
        $this->questionModel = \Centurion_Db::getSingleton('statflow/question');
        $this->feedbackFormModel = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->studyUserModel = \Centurion_Db::getSingleton('statflow/studyUser');
        $this->translationUid = \Centurion_Db::getSingleton('translation/uid');
        $this->translation = \Centurion_Db::getSingleton('translation/translation');
        $this->transLang = \Centurion_Db::getSingleton('translation/language');
    }

    public function getValidator($parameters, $action)
    {
        if (! $this->validator) {
            
            $filters = array(
                '*' => array(
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines()
                )
            );
            
            $validators = array(
                'manage_study' => array(
                'name' => array(
                    'presence' => 'required',
                    new \Zend_Validate_NotEmpty(),
                    new \Zend_Validate_StringLength(array(
                        'max' => 255
                    ))
                ),
                'external_id' => array(
                    new \Zend_Validate_NotEmpty(),
                    new \Zend_Validate_StringLength(array(
                        'max' => 255
                    )),
                    'allowEmpty' => true
                )
                 ),
                'manage_email_connex' => array(
                    'sender_name_connex' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 255
                        ))
                    ),
                    'sender_email_connex' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 354)),
                    )
                ),
                'manage_email_renew' => array(
                    'sender_name_renew' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 255
                        ))
                    ),
                    'sender_email_renew' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array('max' => 354)),
                    )
                )
            );
            $this->validator = new \Zend_Filter_Input($filters, $validators[$action], $parameters);
        }
        
        return $this->validator;
    }

    // Returns the css meta of the first studyid of a user 
    public function getFirstMetaByUserId($userId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
        ->from(array('s' => 'statflow_study'), array('meta'))
        ->join(array('su' => 'statflow_study_user'), 's.id = su.study_id', array())
        ->where('su.user_id = ' . $userId);
        return $adapter->fetchOne($select);
    }
    public function isValid($parameters, $action)
    {
        return $this->getValidator($parameters, $action)->isValid();
    }

    public function getValidated($structure_id, $is_published, $id = null, $meta, $global, $logo)
    {
        if ($id) {
            $study = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $study = $this->model->createRow();
        }
        
        $study->name = $this->validator->name;
        $study->is_published = $is_published;
        $study->structure_id = $structure_id;
        $study->meta = $meta;
        $study->ecran_global = $global;
        $study->image = $logo;
        return $study;
    }

    public function getById($id)
    {
        return $this->model->get(array(
            'id' => $id
        ));
    }

    public function getAll()
    {
        return $this->model->fetchAll();
    }
    
    public function getAllStudyByUser($user_id)
    {
       $select= $this->model->select()
                            ->setIntegrityCheck(false)
                            ->from(array('Study' => 'statflow_study'))
                            ->join(array('Study_User' => 'statflow_study_user'), 'id=study_id')
                            ->where('user_id ='.$user_id);
        return  $this->model->fetchAll($select);
    }
    
    public function getAllFeedbackForm()
    {
        return $this->feedbackFormModel->fetchAll();
    }

    public function getAllQuestion()
    {
        return $this->questionModel->fetchAll();
    }

    public function getArrayQuestionTypes()
    {
        return array(
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ => 'QUESTION_TYPE_MCQ',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE => 'QUESTION_TYPE_MULTIPLE_CHOICE',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL => 'QUESTION_TYPE_INTERVAL',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED => 'QUESTION_TYPE_ORDERED',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN => 'QUESTION_TYPE_OPEN',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY => 'QUESTION_TYPE_ARRAY',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_IMAGE => 'QUESTION_TYPE_IMAGE',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_IMAGES => 'QUESTION_TYPE_MULTIPLE_IMAGES'
        );
    }

    public function getArrayQuestionTypesIcons()
    {
        return array(
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MCQ => 'fa fa-check-circle-o',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_CHOICE => 'fa fa-check-square-o',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL => 'fa fa-arrows-h',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ORDERED => 'fa fa-list-ol',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN => 'fa fa-commenting-o',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY => 'fa fa-table',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_IMAGE => 'fa fa-photo',
            \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_MULTIPLE_IMAGES => 'fa fa-photo'
        );
    }

    public function getArrayModalities()
    {
        return array(
            'QUESTION_TYPE_MCQ' => array(
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_EXCLUSIVE => 'MCQ_MODALITY_EXCLUSIVE',
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT => 'MCQ_MODALITY_COMPLEMENT'
            ),
            'QUESTION_TYPE_MULTIPLE_CHOICE' => array(
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_SIMPLE => 'MCQ_MODALITY_SIMPLE',
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_EXCLUSIVE => 'MCQ_MODALITY_EXCLUSIVE',
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT => 'MCQ_MODALITY_COMPLEMENT'
            ),
            'QUESTION_TYPE_MULTIPLE_IMAGES' => array(
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_SIMPLE_IMAGE => 'MCQ_MODALITY_SIMPLE_IMAGE',
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_EXCLUSIVE_IMAGE => 'MCQ_MODALITY_EXCLUSIVE_IMAGE'
            ),
            'QUESTION_TYPE_ORDERED' => array(
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_SIMPLE => 'MCQ_MODALITY_SIMPLE',
                \Statflow_Model_DbTable_Row_QuestionEntry::MCQ_MODALITY_COMPLEMENT => 'MCQ_MODALITY_COMPLEMENT'
            )
        );
    }

    public function setDesactivation($action)
    {
        $_entry->actif_connex = 0;
        $_entry->sender_name_connex = "";
        $_entry->sender_email_connex = "";
        if($action == 'connex'){
            $study = array($study->actif_connex => 0, $study->sender_name_connex => '', $study->sender_email_connex => '');
        }
        return $study;
    }
    /**
     *
     * set $export
     */
    public function setExport($val)
    {
        $this->export = $val;
    }
    /**
     * get $export
     */
    public function getExport()
    {
        return $this->export;
    }
    
    public function exportTraductions($formId)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select();
        
        /*
        - Type d'element (Questionnaire / Page / Question / Modalité)
        - Ordre (a gérer dans le cas des Pages / Questions)
        - le numéro de l’uid `translation_uid`.`id`
        - le libellé de l’uid `translation_uid`.`uid`
        - le nom du langage `translation_language`.`name`
         */
        $headers = array(
            'Type d\'element',
            'Ordre',
            'Uid',
            'libellé', 
            'Langue',
            'Nom du language'
        );
        
        
        $export = $this->getExport();
        if ($export) {
            //die('form ' .$formId);
            $this->exportCSV($adapter,$headers, $formId);
        }
    }
    
    
    public function exportCSV($adapter, $headers, $formId)
    {
        $currentDateTime = date("dmYHis");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition:inline; filename="' . 'Export-Traduction_Questionnaire_' . $currentDateTime . '.csv"');
        header("Pragma: ");
        header("Cache-Control: ");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        echo implode('|', str_replace(array(
            ",",
            "\r\n",
            "\r",
            "\n",
            "<br>",
            "<br/>",
            "<br />"
        ), array(
            
            ''
        ), $headers)) . "\r\n";
        
        // get questionnaire info
        $this->getTypeElementInfo($adapter, 'Questionnaire', $formId, null);
        //
        // get pages info(obtenir les questions/help et sous questions
        $this->getTypeElementInfo($adapter, 'Pages', $formId, null);
        die();
    }
    
    
    
    public function getTypeElementInfo($adapter, $typeEl, $formId, $criteria)
    {
        switch ($typeEl){
            case 'Questionnaire':
                $libelleIntro = 'form__'.$formId.'__introduction-text';
                $this->buildTypeElement($adapter, 'Questionnaire', $libelleIntro, null);
                
                $nextQuestionText = 'form__'.$formId.'__next-question';
                $this->buildTypeElement($adapter, 'Questionnaire', $nextQuestionText, null);
                
                $previousQuestionText = 'form__'.$formId.'__previous-question';
                $this->buildTypeElement($adapter, 'Questionnaire', $previousQuestionText, null);
                
                $validateFormText = 'form__'.$formId.'__validate-form';
                $this->buildTypeElement($adapter, 'Questionnaire', $validateFormText, null);
                
                $invalidQuestionText = 'form__'.$formId.'__invalid-question';
                $this->buildTypeElement($adapter, 'Questionnaire', $invalidQuestionText, null);
                $invalidOpenQuestionText = 'form__'.$formId.'__invalid-openquestion';
                $this->buildTypeElement($adapter, 'Questionnaire', $invalidOpenQuestionText, null);
                
                $invalidMcQuestionText = 'form__'.$formId.'__invalid-mcquestion';
                $this->buildTypeElement($adapter, 'Questionnaire', $invalidMcQuestionText, null);
                
            break;
            case 'Pages':
                $questionnaire = $this->getQuestionnaireService();
                $form   = $questionnaire->getFormById($formId);
                $pages = $questionnaire->getAllPageByForm($form);
                foreach ($pages as $page) {
                    $libellePage = 'form__page_name-'.$page->id;
                    $this->buildTypeElement($adapter, 'Page', $libellePage, $page);
                    
                    // get Question info
                    $this->getTypeElementInfo($adapter, 'Question', $formId, $page);
                }
            break;
            case 'Question' :
                $questionnaire = $this->getQuestionnaireService();
                $questions = $questionnaire->getAllPageByQuestion($criteria);
                foreach($questions as $question){
                    /*
                     * Les Questions
                     */
                    $libelleQuestion = 'form__'.$formId.'__question_title-'.$question->code;
                    $this->buildTypeElement($adapter, 'Question', $libelleQuestion, $question);
                    
                    
                    /*
                     * Les Sous Questions
                     */
                    if($question->question_type_id == \Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY){
                        $subquestions = $questionnaire->getAllSubQuestion($question);
                        foreach($subquestions as $sq){
                            $libelleSousQuestion = $sq->code ? 'form__'.$formId.'__question_title-'.$sq->code : 'form__question_title-missing-code';
                            $this->buildTypeElement($adapter, 'Question', $libelleSousQuestion, $question);
                            
                        }
                    }
                    
                    
                    /*
                     * Les HelpText
                     */
                    $helpText = 'form__'.$formId.'__question_help-'.$question->code;
                    $this->buildTypeElement($adapter, 'Question', $helpText, $question);
                    // get Modality info
                    $this->getTypeElementInfo($adapter, 'Modality', $formId, $question);
                }
            break;
            case 'Modality':
                $questionnaire = $this->getQuestionnaireService();
                $entries = $questionnaire->getAllQuestionEntry($criteria);
                foreach($entries as $entry){
                    /**
                     * Evolution GLAF #1785
                     */
                    if($entry->id == 175) {
                        continue;
                    }
                    $libelleModality = $entry instanceof \Statflow_Model_DbTable_Row_Question ?  'form__'.$formId.'__question_title-'.$entry->code : 'form__question_'.$criteria->code.'_modality-'.$entry->id;
                    $this->buildTypeElement($adapter, 'Modality', $libelleModality, null);
                }
            break;
        }
    }
    
    public function buildTypeElement($adapter, $typeEl, $libelle, $objOrder)
    {
        $translationService = $this->getTranslationService();
         
        $uid       = $translationService->getTranslationId($libelle);
        $dataIntro = $translationService->getAllTranslationInfoLang($adapter, $uid);
        if (count($dataIntro) > 0) {
            foreach ($dataIntro as $dataQues) {
                $tmpAr = array();
                $tmpAr[] = $typeEl;
                $tmpAr[] = $typeEl == 'Questionnaire' || $typeEl == 'Modality' ? '' : $objOrder->order;
                $tmpAr[] = $uid;
                $tmpAr[] = $libelle;
                
                $langInfo = $translationService->getLanguageInfo($dataQues['language_id']);
                $tmpAr[] = $langInfo->name;
                
                $tmpAr[] = html_entity_decode($dataQues['translation']);
                
                $this->implodeArray($tmpAr);
             }
         }
         else {
            $tmpArray = array();
            $tmpArray[] = $typeEl;
            $tmpArray[] = $typeEl == 'Questionnaire' || $typeEl == 'Modality' ? '' : $objOrder->order;
            $tmpArray[] = $uid;
            $tmpArray[] = $libelle;
            $tmpArray[] = '';
            $tmpArray[] = '';
            $this->implodeArray($tmpArray);
         }  
    }
    
    public function implodeArray($array)
    {
        echo implode('|', str_replace(array(
            "\r\n",
            "\r",
            "\n"
        ), array(
            ''
        ), $array)) . "\r\n";
    }
   
    /*
    //obtenir les infos Translations
    public function getTranslationId($libelle)
    {
        $translation = $this->getByTranslationUid($libelle);
        $translation_id = $translation->id;
        return $translation_id;
    }
    
    //obtenir les infos Translations
    public function getTranslationUId($adapter, $libelle)
    {
        $select  = $adapter->select();
        $select->from('translation_uid')
        ->where('uid ='.$libelle);
        $data = $adapter->fetchAll($select);
        $translation_id = null;
        foreach ($data as $uid)
            $translation_id = $uid['id'];
        return $translation_id;
    }
    
    // get translation name + language used
    public function getAllTranslationInfoLang($adapter, $uid_id)
    { 
        $select  = $adapter->select();
        $select->from('translation_translation')
               ->join('translation_uid', 'uid_id=id')
               ->where('uid_id ='.$uid_id);
        $data = $adapter->fetchAll($select);
        return $data;
    }
    
    public function getLanguageInfo($language_id)
    {
        return $this->transLang->get(array(
            'id' => $language_id
        ));
        
    }
    
    public function getByTranslationUid($uid)
    {
          return $this->translationUid->get(array(
            'uid' => $uid
        ));
    }
    public function getByTranslationUid_Id($uid_id)
    {
        return $this->translation->get(array(
            'uid_id' => $uid_id
        ));
    }
    */
    
    /**
     * @return \Statflow\Service\Questionnaire
     */
    public function getQuestionnaireService()
    {
        if (!$this->questionnaireService){
            $this->questionnaireService = new Questionnaire();
        }
        return $this->questionnaireService;
    }
    /**
     * @param \Statflow\Service\Questionnaire $questionnaireService
     */
    public function setQuestionnaireService($questionnaireService)
    {
        $this->questionnaireService = $questionnaireService;
    }
    
    
    /**
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (!$this->translationService){
            $this->translationService = new Translation();
        }
        return $this->translationService;
    }
    /**
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }
    
    /*
     * Import des traductions
     */
    
    public function getImportStatus()
    {
        return $this->import_status;
    }
     
    // Fonction d'import avec statut 0: insert, 1:update, 2:error
    public function importCsvTraduction($csv_traduction)
    {
        $this->import_status = 1;
         
        $translationService = $this->getTranslationService();
        
        $order      = $csv_traduction[1]; // order
        $uId        = $csv_traduction[2];
        $variable   = $csv_traduction[3]; // libelle(variable) => uid from translation_uid
        $language_used = $csv_traduction[4]; // langue utilisee
        $traduction = str_replace(';', '', $csv_traduction[5]); // nom du language(traduction de la variable)

        //die('lib '.$traduction);
        $translation = null;
       
        if ($variable && $uId) {
            
            if($this->translationUid->select(true)->where('uid ='."'".$variable."'".' AND id='.$uId)->fetchAll()->count() == 0) {
                
                //insert
                //$translation = $this->translationUid->createRow();
                $this->import_status = 2;
                return NULL;
                
                
            }else{ // update
                
                
                // check uid / delete in translation_translation => then insert again with language id and uid and translation
                if($language_used && $language_used != ';') {
                    // obtain id of language used
                    $language_id = $translationService->getLanguageId($language_used);
                    
                    if($language_id != null) {
                    if($translationService->checkRecordTranslationExist($uId, $language_id) > 0) { // exists in table translation_translation
                        $this->import_status = 1;
                        
                        // update table translation_translation with $traduction
                        $translationService->updateTraduction($uId, $language_id, $traduction);
                        
                        
                    }else { // insert
                        $this->import_status = 0;
                        
                        // insert into translation_translation with uid, language_id and $traduction
                        $translationService->saveTraduction($uId, $language_id, $traduction);
                        }
                    }else {
                        $this->import_status = 2;
                        return NULL;
                    }
                }else{ // le champ language est a vide et traduction non vide
                    if($traduction != '') {
                        $this->import_status = 2;
                    }
                }
            }
           
        } else { // ne peut etre vide - error
            $this->import_status = 2;
            return NULL;
    
        }
        
        //$translation->uid = $variable;
        
        return $translation;
        
        
    }
    
    //Fonction de lecture du fichier csv traduction
    public function readCSV($csv_file){
        //vérif nombre de colonne
        $column_count = 6;
        //vérif nombre de ligne
        $line_count=2;
        //vérif du header et ne pas le garder
        $row_count=1;
    
        $file_handle = fopen($csv_file, "r");
        while (($single_line = fgetcsv($file_handle, 1024, "|")) !== FALSE) {
            // vérif format du fichier
            //echo '<br>row count ' .$row_count.' count '.count($single_line).' '.$column_count;
            if(count($single_line) != $column_count){
                $line_of_text=NULL;
                break;
            }
            // sauvegarde données csv dans array
            if($row_count > 1) $line_of_text[] = $single_line;
    
            $row_count++;
        }
        fclose($file_handle);
    
        return $line_of_text;
    }
    
    //Fonction de lecture du fichier log suite à l'import
    public function getImportResult() {
        $result_file = "/tmp/import_questionnaire_log.txt";
        if (file_exists($result_file)){
            //die();
            $file_handle = fopen($result_file, "r");
            while (($result_line = fgetcsv($file_handle, 1024)) !== FALSE) {
                $result[] = $result_line;
            }
            fclose($file_handle);
            unlink($result_file);
            return $result;
        }
        return NULL;
    }

 
}