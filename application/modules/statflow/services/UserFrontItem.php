<?php
namespace Statflow\Service;

class UserFrontItem
{
    private $model;
   
    public function getModel()
    {
        if (!$this->model) {
            $this->model = \Centurion_Db::getSingleton('statflow/UserFrontItem');
        }
        return $this->model;
    }
        
    public function getFrontItemIDByUserId($user_id)
    {
        $model = $this->getModel();
        $select=  $model->select(true)
                        ->where('user_id ='.$user_id)
                        ->fetchAll()->toArray();
    
        $data = array();
        foreach ($select as $item){
            $data[] = $item['front_item_id'];
        }
    
        return $data;
    }
    
    public function getFrontItemCodeByUserId($user_id)
    {
        $model = $this->getModel();
        $select= $model->select()
                       ->setIntegrityCheck(false)
                       ->from(array('cfi' => 'statflow_front_item'))
                       ->join(array('cuf' => 'statflow_user_front_item'), 'cfi.id=cuf.front_item_id')
                       ->where('cuf.user_id ='.$user_id)
                       ->fetchAll()->toArray();
    
        $data = array();
        foreach ($select as $item){
            $data[] = $item['code'];
        }
    
        return $data; 
    }
           
    public function deleteFrontItemByUserId($user_id)
    {
        $model = $this->getModel();
        return $model->deleteRow("user_id = ".$user_id);
    }

    public function saveUserFrontItems($user_id, $front_item_id)
    {
        $model = $this->getModel();
        $query = $model->createRow(['user_id' => $user_id,
                                    'front_item_id' => $front_item_id]);
    
        $query->save();
        return $query;
    }
}

?>