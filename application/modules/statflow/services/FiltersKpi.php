<?php
namespace Statflow\Service;

class FiltersKpi
{
    private $model;
    private $importFieldModel;
        
    public function getModel()
    {
        if (!$this->model) {
            $this->model = \Centurion_Db::getSingleton('statflow/FiltersKpi');
        }
        return $this->model;
    }
    
    public function getImportField()
    {
        if (!$this->importFieldModel) {
            $this->importFieldModel = \Centurion_Db::getSingleton('statflow/ImportField');
        }
        return $this->importFieldModel;
    }
       
    public function getAllExternalFieldByFeedbackFormId($feedback_form_id)
    {
        $importFieldModel = $this->getImportField();
        
        if (is_array($feedback_form_id)) {
           $where =  'feedback_form_id IN (' . implode(', ', $feedback_form_id) . ')';      
        }
        else {
            $where =  'feedback_form_id ='.$feedback_form_id;
        }
        return $importFieldModel->select(true)
                                ->where($where)
                                ->fetchAll();   
    }
    
    public function getFiltersByFeedbackFormId($feedback_form_id)
    {
        $model = $this->getModel();
        $select=  $model->select(true)
                        ->where('feedback_form_id ='.$feedback_form_id)
                        ->fetchAll()->toArray();
        
        $data = array();
        foreach ($select as $filter){
            $data[] = $filter['import_field_id'];
        }
        
        return $data;
    }


    public function getAllFiltersByFeedbackFormId($feedback_form_id)
    {
        $model = $this->getModel();
        $select= $model->select()
                             ->setIntegrityCheck(false)
                             ->from(array('cfk' => 'statflow_filters_kpi'))
                             ->join(array('cif' => 'statflow_import_field'), 'cfk.import_field_id=cif.id')
                             ->where('cfk.feedback_form_id ='.$feedback_form_id);
        
        return  $model->fetchAll($select);  
    }
       
    public function saveFilters($feedback_form_id, $import_field_id)
    {
        $model = $this->getModel();
        $filters = $model->createRow([
                    'feedback_form_id' => $feedback_form_id,
                    'import_field_id' => $import_field_id
                    ]);
    
        $filters->save();
        return $filters;
    
    }
    
    public function getDetailsFilterSelected($filterSelected) 
    {
        $model = $this->getModel();
        $select= $model->select()
                     ->setIntegrityCheck(false)
                     ->from('statflow_import_field')
                     ->where('id IN (' . implode(', ', $filterSelected) . ')');
        
        return  $model->fetchAll($select);
    }

    public function deleteFiltersByImportId($import_field_id)
    {
        $model = $this->getModel();
        return $model->deleteRow("import_field_id = ".$import_field_id);
    
    }
    
    public function deleteFiltersByFeedbackFormId($feedbackform_id)
    {
        $model = $this->getModel();
        return $model->deleteRow("feedback_form_id = ".$feedbackform_id);
    
    }

}

?>