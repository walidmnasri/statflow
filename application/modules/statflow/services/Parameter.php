<?php
namespace Statflow\Service;

class Parameter
{

    private $model;

    private $validator;

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/parameter');
    }

    public function getValidator($parameters, $action)
    {
        if (! $this->validator) {
            
            $filters = array(
                'manage_email_connex' => array(
                    '*' => array(
                        new \Zend_Filter_StringTrim(),
                        new \Zend_Filter_StripTags(),
                        new \Zend_Filter_StripNewlines(),
                    )
                ),
                'manage_email_renew' => array(
                    '*' => array(
                        new \Zend_Filter_StringTrim(),
                        new \Zend_Filter_StripTags(),
                        new \Zend_Filter_StripNewlines(),
                    )
                )
            );
            
            $validators = array(
                'manage_email_connex' => array(
                    'sender_name_connex' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 50
                        ))
                    ),
                    'sender_email_connex' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_EmailAddress(),
                        new \Zend_Validate_StringLength(array('max' => 50)),
                    ),
                    'bcc_email_connex' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_EmailAddress(),
                        new \Zend_Validate_StringLength(array('max' => 50)),
                    )
                ),
                'manage_email_renew' => array(
                    'sender_name_renew' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_StringLength(array(
                            'max' => 50
                        ))
                    ),
                    'sender_email_renew' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_EmailAddress(),
                        new \Zend_Validate_StringLength(array('max' => 50)),
                    ),
                    'bcc_email_renew' => array(
                        'presence' => 'required',
                        new \Zend_Validate_NotEmpty(),
                        new \Zend_Validate_EmailAddress(),
                        new \Zend_Validate_StringLength(array('max' => 50)),
                    )
                
                )
            );
            $this->validator = new \Zend_Filter_Input($filters[$action], $validators[$action], $parameters);
        }
        
        return $this->validator;
    }

    public function isValid($parameters, $action)
    {
        return $this->getValidator($parameters, $action)->isValid();
    }

    public function getValidated($structure_id, $is_published, $id = null)
    {
        return true;
    }
    
    public function getEmailParameters()
    {
        $select= $this->model->select()
        ->where("label IN ('sender_name_connex', 'sender_email_connex','bcc_email_connex','sender_name_renew','sender_email_renew','bcc_email_renew')");
        return  $this->model->fetchAll($select);
    
    }
    
    public function updateEmailParameters($criteria, $value)
    {
        if(count($criteria) > 0)
        {
            for($i=0; $i < count($criteria); $i++ )
            {
                $sqlUpate = "UPDATE statflow_parameter SET value = '$value[$i]' WHERE label = '$criteria[$i]'";
                \Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlUpate);
            }
        }
    }
    public function getAllLanguages()
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
        ->from(array('lang' => 'translation_language'));
    
        $result =  $adapter->fetchAll($select);
        return $result;
    }
    
    public function getLanguagesDirectionR2L()
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $select = $adapter->select()
        ->from(array('lang' => 'translation_language'))
        ->where('lang.direction = 1');
    
        $result =  $adapter->fetchAll($select);
    
        $data = array();
        foreach ($result as $field){
            $data[] = str_replace(' ', '_', $field['id']);
        }
    
        return $data;
    
    }
    
    public function updateAllLanguagesDirectionL2R()
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $data = array( 'direction' => '0');
    
        $adapter->update('translation_language', $data);
    }
    
    public function updateLanguagesDirectionR2L($id)
    {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $data = array( 'direction' => '1');
    
        $adapter->update('translation_language', $data, 'id = '.$id);
    }    
}