<?php
namespace Statflow\service;

class FeedbackFormPage
{

    private $model;
    private $feedbackFormModel;
    private $validator;

    private $translationService;

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('statflow/feedbackFormPage');
        $this->feedbackFormModel = \Centurion_Db::getSingleton('statflow/feedbackForm');
    }
    
    /**
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (!$this->translationService){
            $this->translationService = new Translation();
        }
        return $this->translationService;
    }
    /**
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }

    private function getValidator($parameters)
    {
        if (! $this->validator) {
            
            $filters = array(
                '*' => array(
                    new \Zend_Filter_Alnum(array(
                        'allowwhitespace' => true
                    )),
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines()
                )
            )
            ;
            $validators = array(
                'feedback_form_id' => array(
                    'presence' => 'required',
                    new \Zend_Validate_Int()
                )
            );
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator;
    }

    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }

    public function getValidated($title_is_visible,$meta,$order, $id = null)
    {
        if ($id) {
            $structure = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $structure = $this->model->createRow();
        }
        
        $structure->feedback_form_id =  $this->validator->feedback_form_id;
        $structure->title_is_visible =  $title_is_visible;
        $structure->meta             =  $meta;
        $structure->order            =  $order;
        
        return $structure;
    }

    public function getById($id)
    {
        return $this->model->get(array(
            'id' => $id
        ));
    }

    public function getAll()
    {
        return $this->model->fetchAll();
    }
   
    public function getAllFeedbackForm()
    {
        return $this->feedbackFormModel->fetchAll();
    }
    
    public function getByIdFeedbackForm($id)
    {
        return $this->feedbackFormModel->get(array(
            'id' => $id
        ));
    }
    
    public function duplicatePage($id, $orderNextPage=null,$newFeedbackFormId=null)
    {
        $translationService = $this->getTranslationService();
        $pageRow = $this->model->fetchRow($this->model->select()->where('id = ?',$id));
        $data = $pageRow->toArray();
        $data['id']= null;
        $data['created_at']= null;
        $data['updated_at']= null;
        if($orderNextPage != null){
            $data['order']= $orderNextPage;
        }
        if($newFeedbackFormId != null){
            $data['feedback_form_id']= $newFeedbackFormId;
        }
        $newPage = $this->model->createRow($data);
        $newPage->save();
        
        //duplicate translations
        $newPageTranslationUid = $translationService->insertRowTranslationUid('form__page_name-'.$newPage->id);
        
        $tagId= $translationService->getTagId('form_'.$newPage->feedback_form_id);
        
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newPageTranslationUid->id,$tagId);
        
        $oldTranslationUidRow= $translationService->getTranslationUidRow('form__page_name-'.$pageRow->id);
        
        $translationService->duplicateTranslation ($oldTranslationUidRow->id, $newPageTranslationUid->id);
        
        return $newPage;
    }
    
    

    public function getNextOrderPageByFeedbackForm($feedbackFormID)
    {
        return $this->model->select(true)
        ->where('feedback_form_id = ' . $feedbackFormID)
        ->fetchAll()->count();
    
    }
    
   
    public function getAllPagesByFeedbackForm($feedbackFormId){
    
        return $this->model->select(true)
        ->where('feedback_form_id = ' . $feedbackFormId)
        ->fetchAll();
         
    }
    
}

?>