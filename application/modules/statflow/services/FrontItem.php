<?php
namespace Statflow\Service;

class FrontItem
{
    private $model;
    
    public function __construct($model = null)
    {
        $this->setModel($model);
    }
    
    public function setModel($model)
    {
        if (!$model) {
            $model = \Centurion_Db::getSingleton('statflow/FrontItem');
        }
        $this->model = $model;
    }    
    
    public function getAll()
    {
        return $this->model->fetchAll();
    }
}

?>