<?php
namespace Statflow\Service;

class Permission
{

    private $model;

    private $groupModel;

    private $groupPermissionModel;

    private $userModel;

    private $userPermissionModel;

    public $validator;

    public function __construct()
    {
        $this->model = \Centurion_Db::getSingleton('auth/permission');
        $this->groupModel = \Centurion_Db::getSingleton('auth/group');
        $this->groupPermissionModel = \Centurion_Db::getSingleton('auth/groupPermission');
        $this->userModel = \Centurion_Db::getSingleton('auth/user');
        $this->userPermissionModel = \Centurion_Db::getSingleton('auth/userPermission');
    }

    public function getValidator($parameters)
    {
        if (! $this->validator) {
            
            $filters = array(
                'name' => array(
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines()
                ),
                'description' => array(
                    new \Zend_Filter_StringTrim(),
                    new \Zend_Filter_StripTags(),
                    new \Zend_Filter_StripNewlines()
                )
            );
            
            $validators = array(
                'name' => array(
                    'presence' => 'required',
                    new \Zend_Validate_NotEmpty(),
                    new \Zend_Validate_StringLength(array(
                        'max' => 255
                    ))
                ),
                'description' => array(
                    'presence' => 'required',
                    new \Zend_Validate_NotEmpty(),
                    new \Zend_Validate_StringLength(array(
                        'max' => 255
                    ))
                )
            );
            
            $this->validator = new \Zend_Filter_Input($filters, $validators, $parameters);
        }
        
        return $this->validator;
    }

    public function isValid($parameters)
    {
        return $this->getValidator($parameters)->isValid();
    }

    public function getValidated($id = null)
    {
        if ($id) {
            $permission = $this->model->get(array(
                'id' => $id
            ));
        } else {
            $permission = $this->model->createRow();
        }
        
        
        $permission->name = $this->validator->name;
        $permission->description = $this->validator->description;
        $permission->save();
        return $permission; 
    }

    public function getValidatedGroupPermission($group_id, $permission_id)
    {
        $permission = $this->groupPermissionModel->createRow();
        
        $permission->group_id = $group_id;
        $permission->permission_id = $permission_id;
        $permission->save();
        return $permission;
    }

    public function getValidatedUserPermission($user_id, $permission_id)
    {
        $permission = $this->userPermissionModel->createRow();
    
        $permission->user_id = $user_id;
        $permission->permission_id = $permission_id;
        $permission->save();
        return $permission;
    }
    
    public function getAllPermission()
    {
        return $this->model->fetchAll();
    }

    public function getPermissionById($id)
    {
        return $this->model->get(array(
            'id' => $id
        ));
    }

    public function getAllGroup()
    {
        return $this->groupModel->fetchAll();
    }

    public function getAllGroupPermission()
    {
        return $this->groupPermissionModel->fetchAll();
    }

    public function getAllUserPermission()
    {
        return $this->userPermissionModel->fetchAll();
    }

    public function getUserById($id)
    {
        return $this->userModel->get(array('id' => $id));
    }

    public function getGroupById($id)
    {
        return $this->groupModel->get(array(
            'id' => $id
        ));
    }
    
    public function getGroupPermissionById($perm, $group)
    {
        return $this->groupPermissionModel->select(true)
                                          ->where("permission_id = $perm AND group_id = $group")->get();
    }
    
    public function getUserPermissionById($perm, $user)
    {
        return $this->userPermissionModel->select(true)
                                         ->where("permission_id = $perm AND user_id = $user")->get();
    }    
    /**
     * Build the permissions tree
     * 
     * @param $_permissions array            
     * @return array
     */
    public function permissionTree($_permissions)
    {
        $tree = array();
        foreach ($_permissions as $permission) :
            if ($permission->name != 'all') {
                $mvc = explode('_', $permission->name);
                
                $module = array_shift($mvc);
                if (! array_key_exists($module, $tree)) {
                    $tree[$module] = array();
                }
                
                $controller = array_shift($mvc);
                $tree[$module][$controller][] = $permission;
            }
        endforeach;
        return $tree;
    }
    
    public function deletePermission($id)
    {
       return $this->model->delete("id=".$id);
    }
    
    public function deleteUserPermission($user_id, $permission_id)
    {
        return $this->userPermissionModel->delete("user_id=".$user_id. " and permission_id=".$permission_id);
    }   

    public function deleteGroupPermission($group_id, $permission_id)
    {
        return $this->groupPermissionModel->delete("group_id=".$group_id. " and permission_id=".$permission_id);
    }    
}