<?php

class Statflow_Bootstrap extends Centurion_Application_Module_Bootstrap
{

    protected function _initSolarium(){
        require_once(APPLICATION_PATH . '/../library/Solarium/Autoloader.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcherInterface.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/Event.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcher.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/ImmutableEventDispatcher.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/GenericEvent.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventSubscriberInterface.php');
        require_once(APPLICATION_PATH
            . '/../library/Symfony/Component/EventDispatcher/ContainerAwareEventDispatcher.php');


        $solariumOptions = $this->getOption('solarium');
        if(is_array($solariumOptions)) Statflow_Model_Solr::setDefaultConfig($solariumOptions);

    }
}

