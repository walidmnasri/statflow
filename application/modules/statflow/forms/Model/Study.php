<?php

class Statflow_Form_Model_Study extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_Study';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'slug', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'language_id'              => $this->_translate('language'),
        'name'              => $this->_translate('name'),
        'is_published'              => $this->_translate('is_published'),
        );
        
        parent::__construct($options, $instance);
    }


}

