<?php

class Statflow_Form_Model_AnswerEntry extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_AnswerEntry';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'question_entry_id'              => $this->_translate('question_entry'),
        'answer_id'              => $this->_translate('answer'),
        'entry_type_id'              => $this->_translate('entry_type'),
        'content'              => $this->_translate('content'),
        );
        
        parent::__construct($options, $instance);
    }


}

