<?php

class Statflow_Form_Model_Import extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_Import';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');

        $this->_elementLabels = array(
            'study_id'              => $this->_translate('study'),
            'feedback_form_id'              => $this->_translate('feedback_form'),
            'number'              => $this->_translate('number'),
        );

        parent::__construct($options, $instance);
    }

    public function init()
    {

        $this->setMethod('post');

        $this->setAttrib('enctype', 'multipart/form-data');

        $element = new Zend_Form_Element_File('csv');
        $element->setLabel('csv');
        $element->addValidator('Count', false, 1);
        $element->addValidator('Extension', false, array('csv', 'xlsx'));
        $element->addValidator('Size', array('min' => 0));
        $element->setRequired(true);

        $this->addElement($element);

        $this->addElement('submit', 'submit', array(
            'label' => 'Submit'
        ));

    }


}

