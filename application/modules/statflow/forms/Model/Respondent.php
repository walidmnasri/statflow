<?php

class Statflow_Form_Model_Respondent extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_Respondent';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'study_id'              => $this->_translate('study'),
        'feedback_form_id'              => $this->_translate('feedback_form'),
        'firstname'              => $this->_translate('firstname'),
        'lastname'              => $this->_translate('lastname'),
        'external_id'              => $this->_translate('external'),
        'email'              => $this->_translate('email'),
        'is_alert'              => $this->_translate('is_alert'),
        );
        
        parent::__construct($options, $instance);
    }


}

