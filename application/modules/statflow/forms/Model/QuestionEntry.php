<?php

class Statflow_Form_Model_QuestionEntry extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_QuestionEntry';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'question_id'              => $this->_translate('question'),
        'entry_type_id'              => $this->_translate('entry_type'),
        'content'              => $this->_translate('content'),
        'complement_data'              => $this->_translate('complement_data'),
        );
        
        parent::__construct($options, $instance);
    }


}

