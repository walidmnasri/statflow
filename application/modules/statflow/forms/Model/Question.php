<?php

class Statflow_Form_Model_Question extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_Question';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'section_id'              => $this->_translate('section'),
        'question_type_id'              => $this->_translate('question_type'),
        'feedback_form_page_id'              => $this->_translate('feedback_form_page'),
        'content'              => $this->_translate('content'),
        'code'              => $this->_translate('code'),
        );
        
        parent::__construct($options, $instance);
    }


}

