<?php

class Statflow_Form_Model_RespondentExternalField extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_RespondentExternalField';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'respondent_id'              => $this->_translate('respondent'),
        'import_field_id'              => $this->_translate('import_field'),
        'label'              => $this->_translate('label'),
        'value'              => $this->_translate('value'),
        );
        
        parent::__construct($options, $instance);
    }


}

