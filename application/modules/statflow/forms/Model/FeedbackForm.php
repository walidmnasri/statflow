<?php

class Statflow_Form_Model_FeedbackForm extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_FeedbackForm';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'slug', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'study_id'              => $this->_translate('study'),
        'language_id'              => $this->_translate('language'),
        'feedback_form_template_id'              => $this->_translate('feedback_form_template'),
        'name'              => $this->_translate('name'),
        'is_published'              => $this->_translate('is_published'),
        'introduction_text'              => $this->_translate('introduction_text'),
        'introduction_file_id'              => $this->_translate('introduction_file'),
        'introduction_is_visible'              => $this->_translate('introduction_is_visible'),
        'publication_start'              => $this->_translate('publication_start'),
        'publication_end'              => $this->_translate('publication_end'),
        );
        
        parent::__construct($options, $instance);
    }


}

