<?php

class Statflow_Form_Model_FeedbackFormTemplate extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_FeedbackFormTemplate';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'name'              => $this->_translate('name'),
        'file'              => $this->_translate('file'),
        );
        
        parent::__construct($options, $instance);
    }


}

