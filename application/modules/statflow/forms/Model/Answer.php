<?php

class Statflow_Form_Model_Answer extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_Answer';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'respondent_id'              => $this->_translate('respondent'),
        'feedback_form_id'              => $this->_translate('feedback_form'),
        'question_id'              => $this->_translate('question'),
        'value'              => $this->_translate('value'),
        );
        
        parent::__construct($options, $instance);
    }


}

