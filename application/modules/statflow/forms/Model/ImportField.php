<?php

class Statflow_Form_Model_ImportField extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_ImportField';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'import_id'              => $this->_translate('import'),
        'study_id'              => $this->_translate('study'),
        'feedback_form_id'              => $this->_translate('feedback_form'),
        'label'              => $this->_translate('label'),
        'external_id'              => $this->_translate('external'),
        );
        
        parent::__construct($options, $instance);
    }


}

