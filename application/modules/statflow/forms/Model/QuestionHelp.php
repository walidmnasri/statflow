<?php

class Statflow_Form_Model_QuestionHelp extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_QuestionHelp';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'question_id'              => $this->_translate('question'),
        'content'              => $this->_translate('content'),
        );
        
        parent::__construct($options, $instance);
    }


}

