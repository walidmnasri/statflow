<?php

class Statflow_Form_Model_Structure extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_Structure';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'slug', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'parent_id'              => $this->_translate('parent'),
        'name'              => $this->_translate('name'),
        'external_id'              => $this->_translate('external'),
        );
        
        parent::__construct($options, $instance);
    }


}

