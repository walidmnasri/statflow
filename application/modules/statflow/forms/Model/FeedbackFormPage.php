<?php

class Statflow_Form_Model_FeedbackFormPage extends Centurion_Form_Model_Abstract
{

    protected $_modelClassName = 'Statflow_Model_DbTable_FeedbackFormPage';

    public function __construct($options = array (), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_exclude = array('id', 'created_at', 'updated_at');
        
        $this->_elementLabels = array(
        'feedback_form_id'              => $this->_translate('feedback_form'),
        'name'              => $this->_translate('name'),
        'title_is_visible'              => $this->_translate('title_is_visible'),
        'order'              => $this->_translate('order'),
        );
        
        parent::__construct($options, $instance);
    }


}

