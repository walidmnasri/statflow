<?php

class Statflow_Model_DbTable_FrontItem extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_front_item';
    
    const FIELD_ID = 'id';

    const FIELD_CODE = 'code';

    const FIELD_NAME = 'name';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_front_item';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_FrontItem';

}

