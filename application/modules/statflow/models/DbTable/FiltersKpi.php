<?php

class Statflow_Model_DbTable_FiltersKpi extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_filters_kpi';
    
    const FIELD_STATFLOW_FEEDBACK_FORM_ID = 'statflow_feedback_form_id';
    
    const FIELD_STATFLOW_IMPORT_FIELD_ID = 'statflow_import_field_id';
    
    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_filters_kpi';
    
    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array(
        'feedback_form_id',
        'import_field_id'
    );
    
    /**
     * Classname for row
     *
     * @var string
    */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_FiltersKpi';
    
    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap = array(
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
        ),
        'import_field' => array(
            'columns' => 'import_field_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_ImportField'
        )
    );

}

