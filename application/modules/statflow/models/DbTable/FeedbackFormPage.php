<?php

class Statflow_Model_DbTable_FeedbackFormPage extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_feedback_form_page';

    const FIELD_ID = 'id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';

    const FIELD_TITLE_IS_VISIBLE = 'title_is_visible';

    const FIELD_ORDER = 'order';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';
    
    const FIELD_META = 'meta';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_feedback_form_page';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_FeedbackFormPage';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array('questions' => 'Statflow_Model_DbTable_Question');

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array('feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            ));


}

