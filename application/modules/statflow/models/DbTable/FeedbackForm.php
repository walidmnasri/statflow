<?php

class Statflow_Model_DbTable_FeedbackForm extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_feedback_form';

    const FIELD_ID = 'id';

    const FIELD_STUDY_ID = 'study_id';

    const FIELD_NAME = 'name';

    const FIELD_SLUG = 'slug';

    const FIELD_IS_PUBLISHED = 'is_published';

    const FIELD_IS_OPEN= 'is_open';

    const FIELD_INTRODUCTION_FILE_ID = 'introduction_file_id';

    const FIELD_INTRODUCTION_IS_VISIBLE = 'introduction_is_visible';

    const FIELD_PUBLICATION_START = 'publication_start';

    const FIELD_PUBLICATION_END = 'publication_end';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    const FIELD_META = 'meta';

    const FIELD_IMAGE = 'image';

    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_feedback_form';

    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     *
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_FeedbackForm';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     *
     * @var array
     */
    protected $_dependentTables
        = array(
            'answers'                 => 'Statflow_Model_DbTable_Answer',
            'feedback_form_pages'     => 'Statflow_Model_DbTable_FeedbackFormPage',
            'imports'                 => 'Statflow_Model_DbTable_Import',
            'import_fields'           => 'Statflow_Model_DbTable_ImportField',
            'respondents'             => 'Statflow_Model_DbTable_Respondent',
            'questions'               => 'Statflow_Model_DbTable_Question',
        );

    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap
        = array(
            'study'                  => array(
                'columns'       => 'study_id',
                'refColumns'    => 'id',
                'refTableClass' => 'Statflow_Model_DbTable_Study'
            )
        );

}

