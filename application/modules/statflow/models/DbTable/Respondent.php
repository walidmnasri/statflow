<?php

class Statflow_Model_DbTable_Respondent extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_respondent';

    const FIELD_ID = 'id';

    const FIELD_STUDY_ID = 'study_id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';

    const FIELD_FIRSTNAME = 'firstname';

    const FIELD_LASTNAME = 'lastname';

    const FIELD_EXTERNAL_ID = 'external_id';

    const FIELD_EMAIL = 'email';

    const FIELD_IS_ALERT = 'is_alert';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    const FIELD_RESPONDENT_IP = 'respondent_ip';
    
    const FIELD_RESPONDENT_HOST = 'respondent_host';
    
    const FIELD_USER_AGENT = 'user_agent';
    
    const FIELD_COUNTRY_CODE = 'country_code';
    
    const FIELD_TERMINAL = 'terminal';
    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_respondent';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Respondent';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array(
        'answers' => 'Statflow_Model_DbTable_Answer',
        'respondent_external_fields' => 'Statflow_Model_DbTable_RespondentExternalField'
        );

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            ),
        'study' => array(
            'columns' => 'study_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Study'
            )
        );


}

