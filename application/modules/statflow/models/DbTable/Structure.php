<?php

class Statflow_Model_DbTable_Structure extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_structure';

    const FIELD_ID = 'id';

    const FIELD_PARENT_ID = 'parent_id';

    const FIELD_NAME = 'name';

    const FIELD_SLUG = 'slug';

    const FIELD_EXTERNAL_ID = 'external_id';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_structure';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Structure';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array(
        'studies' => 'Statflow_Model_DbTable_Study',
        'structures' => 'Statflow_Model_DbTable_Structure',
        'structure_users' => 'Statflow_Model_DbTable_StructureUser'
        );

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array('parent' => array(
            'columns' => 'parent_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Structure'
            ));

    /**
     * @var array
     */
    protected $_manyDependentTables = array('users' => array(
            'refTableClass' => 'Auth_Model_DbTable_User',
            'intersectionTable' => 'Statflow_Model_DbTable_StructureUser',
            'columns' => array(
                'local' => 'structure_id',
                'foreign' => 'user_id'
                )
            ));

    public static function getStoreByExternalId($external_id) {
        return Centurion_Db::getSingleton('statflow/structure')
            ->get(array('external_id' => $external_id));
    }


}

