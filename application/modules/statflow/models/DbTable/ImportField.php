<?php

class Statflow_Model_DbTable_ImportField extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_import_field';

    const FIELD_ID = 'id';

    const FIELD_IMPORT_ID = 'import_id';

    const FIELD_STUDY_ID = 'study_id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';

    const FIELD_LABEL = 'label';

    const FIELD_EXTERNAL_ID = 'external_id';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_import_field';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_ImportField';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array('respondent_external_fields' => 'Statflow_Model_DbTable_RespondentExternalField');

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'import' => array(
            'columns' => 'import_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Import'
            ),
        'study' => array(
            'columns' => 'study_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Study'
            ),
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            )
        );


}

