<?php

class Statflow_Model_DbTable_RecoveryEntry extends Centurion_Db_Table_Abstract
{

    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_recovery_entry';

    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     *
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_RecoveryEntry';


    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap
        = array(
            'recovery' => array(
                'columns'       => 'recovery_id',
                'refColumns'    => 'id',
                'refTableClass' => 'Statflow_Model_DbTable_Recovery'
            )
        );


}

