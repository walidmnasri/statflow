<?php

class Statflow_Model_DbTable_RespondentAction extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_respondent_action';
    
    const FIELD_ID = 'id';

    const FIELD_RESPONDENT_ID = 'respondent_id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';
    
    const FIELD_ACTION = 'action';
    
    const FIELD_COMMENT = 'comment';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_respondent_action';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_RespondentAction';

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'respondent' => array(
            'columns' => 'respondent_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Respondent'
            ),
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            )
        );
}

