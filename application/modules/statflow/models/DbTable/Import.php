<?php

class Statflow_Model_DbTable_Import extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_import';

    const FIELD_ID = 'id';

    const FIELD_STUDY_ID = 'study_id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';

    const FIELD_NUMBER = 'number';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_import';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Import';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array('import_fields' => 'Statflow_Model_DbTable_ImportField');

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'study' => array(
            'columns' => 'study_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Study'
            ),
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            )
        );


}

