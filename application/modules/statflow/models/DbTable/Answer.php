<?php

class Statflow_Model_DbTable_Answer extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_answer';

    const FIELD_ID = 'id';

    const FIELD_RESPONDENT_ID = 'respondent_id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';

    const FIELD_QUESTION_ID = 'question_id';

    const FIELD_VALUE = 'value';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_answer';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Answer';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array('answer_entrys' => 'Statflow_Model_DbTable_AnswerEntry');

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'question' => array(
            'columns' => 'question_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Question'
            ),
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            ),
        'respondent' => array(
            'columns' => 'respondent_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Respondent'
            )
        );


}

