<?php

class Statflow_Model_DbTable_Study extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_study';

    const FIELD_ID = 'id';

    const FIELD_NAME = 'name';

    const FIELD_SLUG = 'slug';

    const FIELD_IS_PUBLISHED = 'is_published';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_study';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Study';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array(
        'feedback_forms' => 'Statflow_Model_DbTable_FeedbackForm',
        'imports' => 'Statflow_Model_DbTable_Import',
        'import_fields' => 'Statflow_Model_DbTable_ImportField',
        'respondents' => 'Statflow_Model_DbTable_Respondent',
        'study_users' => 'Statflow_Model_DbTable_StudyUser'
        );

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'structure' => array(
            'columns' => 'structure_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Structure'
        ),
    );


}

