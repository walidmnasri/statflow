<?php

class Statflow_Model_DbTable_UserFrontItem extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_user_front_item';
    
    const FIELD_USER_ID = 'auth_user_id';
    
    const FIELD_FRONT_ITEM_ID = 'statflow_front_item_id';
    
    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_user_front_item';
    
    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array(
        'user_id',
        'front_item_id'
    );
    
    /**
     * Classname for row
     *
     * @var string
    */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_UserFrontItem';
    
    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap = array(
        'user' => array(
            'columns' => 'user_id',
            'refColumns' => 'id',
            'refTableClass' => 'Auth_Model_DbTable_User'
        ),
        'front_item' => array(
            'columns' => 'front_item_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FrontItem'
        )
    );

}

