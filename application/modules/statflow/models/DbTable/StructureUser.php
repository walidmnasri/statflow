<?php

 class Statflow_Model_DbTable_StructureUser extends Centurion_Db_Table_Abstract
 {

    const TABLE = 'statflow_structure_user';

    const FIELD_AUTH_USER_ID = 'auth_user_id';

    const FIELD_STATFLOW_STRUCTURE_ID = 'statflow_structure_id';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_structure_user';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array(
        'user_id',
        'structure_id'
        );

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_StructureUser';

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'user' => array(
            'columns' => 'user_id',
            'refColumns' => 'id',
            'refTableClass' => 'Auth_Model_DbTable_User'
            ),
        'structure' => array(
            'columns' => 'structure_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Structure'
            )
        );


 }

