<?php

class Statflow_Model_DbTable_Question extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_question';

    const FIELD_ID = 'id';

    const FIELD_QUESTION_TYPE_ID = 'question_type_id';

    const FIELD_FEEDBACK_FORM_PAGE_ID = 'feedback_form_page_id';

    const FIELD_CONTENT = 'content';

    const FIELD_CODE = 'code';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    const FIELD_META = 'meta';
    
    const FIELD_PARENT_ID = 'parent_id';
    
    const FIELD_ORDER = 'order';
    

    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_question';

    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     *
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Question';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     *
     * @var array
     */
    protected $_dependentTables
        = array(
            'answers'                 => 'Statflow_Model_DbTable_Answer',
            'question_entrys'         => 'Statflow_Model_DbTable_QuestionEntry',
            'children'                => 'Statflow_Model_DbTable_Question'
        );

    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap
        = array(
            'feedback_form'      => array(
                'columns'       => 'feedback_form_id',
                'refColumns'    => 'id',
                'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            ),
            'feedback_form_page' => array(
                'columns'       => 'feedback_form_page_id',
                'refColumns'    => 'id',
                'refTableClass' => 'Statflow_Model_DbTable_FeedbackFormPage'
            ),
            'parent'            => array( // for questions arrays
                'columns'       => 'parent_id',
                'refColumns'    => 'id',
                'refTableClass' => 'Statflow_Model_DbTable_Question'
            )
        );
}

