<?php

class Statflow_Model_DbTable_RespondentFieldHidden extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_respondent_field_hidden';
    
    const FIELD_ID = 'id';

    const FIELD_FEEDBACK_FORM_ID = 'feedback_form_id';
    
    const FIELD_RESPONDENT_FIELD = 'respondent_field';
    

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_respondent_field_hidden';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_RespondentFieldHidden';

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
            )
        );
}

