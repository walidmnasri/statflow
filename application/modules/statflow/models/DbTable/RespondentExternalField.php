<?php

class Statflow_Model_DbTable_RespondentExternalField extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_respondent_external_field';

    const FIELD_ID = 'id';

    const FIELD_RESPONDENT_ID = 'respondent_id';

    const FIELD_IMPORT_FIELD_ID = 'import_field_id';

    const FIELD_LABEL = 'label';

    const FIELD_VALUE = 'value';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_respondent_external_field';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_RespondentExternalField';

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'respondent' => array(
            'columns' => 'respondent_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Respondent'
            ),
        'import_field' => array(
            'columns' => 'import_field_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_ImportField'
            )
        );


}

