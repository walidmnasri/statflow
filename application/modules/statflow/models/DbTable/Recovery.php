<?php

class Statflow_Model_DbTable_Recovery extends Centurion_Db_Table_Abstract
{

    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_recovery';

    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     *
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_Recovery';

    protected $_dependentTables = array('recovery_entries' => 'Statflow_Model_DbTable_RecoveryEntry');


    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap
        = array(
            'respondent' => array(
                'columns'       => 'respondent_id',
                'refColumns'    => 'id',
                'refTableClass' => 'Statflow_Model_DbTable_Respondent'
            )
        );


}

