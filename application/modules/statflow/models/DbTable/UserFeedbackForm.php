<?php

class Statflow_Model_DbTable_UserFeedbackForm extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_user_feedback_form';
    
    const FIELD_AUTH_USER_ID = 'auth_user_id';
    
    const FIELD_STATFLOW_FEEDBACK_FORM_ID = 'statflow_feedback_form_id';
    
    /**
     * The table name
     *
     * @var string
     */
    protected $_name = 'statflow_user_feedback_form';
    
    /**
     * The primary key column or columns
     *
     * @var mixed
     */
    protected $_primary = array(
        'user_id',
        'feedback_form_id'
    );
    
    /**
     * Classname for row
     *
     * @var string
    */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_UserFeedbackForm';
    
    /**
     * Associative array map of declarative referential integrity rules.
     *
     * @var array
     */
    protected $_referenceMap = array(
        'user' => array(
            'columns' => 'user_id',
            'refColumns' => 'id',
            'refTableClass' => 'Auth_Model_DbTable_User'
        ),
        'feedback_form' => array(
            'columns' => 'feedback_form_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_FeedbackForm'
        )
    );

}

