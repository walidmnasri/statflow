<?php

class Statflow_Model_DbTable_QuestionEntry extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_question_entry';

    const FIELD_ID = 'id';

    const FIELD_QUESTION_ID = 'question_id';

    const FIELD_ENTRY_TYPE_ID = 'entry_type_id';

    const FIELD_CONTENT = 'content';

    const FIELD_COMPLEMENT_DATA = 'complement_data';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_question_entry';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_QuestionEntry';

    /**
     * Simple array of class names of tables that are "children" of the current table.
     * 
     * @var array
     */
    protected $_dependentTables = array('answer_entrys' => 'Statflow_Model_DbTable_AnswerEntry');

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array('question' => array(
            'columns' => 'question_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Question'
            ));


}

