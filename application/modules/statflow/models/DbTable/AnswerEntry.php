<?php

class Statflow_Model_DbTable_AnswerEntry extends Centurion_Db_Table_Abstract
{

    const TABLE = 'statflow_answer_entry';

    const FIELD_ID = 'id';

    const FIELD_QUESTION_ENTRY_ID = 'question_entry_id';

    const FIELD_ANSWER_ID = 'answer_id';

    const FIELD_ENTRY_TYPE_ID = 'entry_type_id';

    const FIELD_CONTENT = 'content';

    const FIELD_CREATED_AT = 'created_at';

    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * The table name
     * 
     * @var string
     */
    protected $_name = 'statflow_answer_entry';

    /**
     * The primary key column or columns
     * 
     * @var mixed
     */
    protected $_primary = array('id');

    /**
     * Classname for row
     * 
     * @var string
     */
    protected $_rowClass = 'Statflow_Model_DbTable_Row_AnswerEntry';

    /**
     * Associative array map of declarative referential integrity rules.
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'answer' => array(
            'columns' => 'answer_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_Answer'
            ),
        'question_entry' => array(
            'columns' => 'question_entry_id',
            'refColumns' => 'id',
            'refTableClass' => 'Statflow_Model_DbTable_QuestionEntry'
            )
        );


}

