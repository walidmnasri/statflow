<?php

class Statflow_Model_DbTable_Row_ImportField extends Centurion_Db_Table_Row
{

    public function getInsertQuery()
    {
        return 'INSERT INTO `statflow_import_field` (`id`, `import_id`, `study_id`, `feedback_form_id`, `label`, `external_id`, `created_at`) VALUES ('
        . $this->id . ', '
        . $this->import_id . ', '
        . $this->study_id . ', '
        . $this->feedback_form_id . ', '
        . $this->getTable()->getAdapter()->quote($this->label) . ', '
        . $this->getTable()->getAdapter()->quote(Centurion_Inflector::slugify($this->external_id)) . ', '
        . $this->getTable()->getAdapter()->quote($this->created_at)
        . ');';
    }

}

