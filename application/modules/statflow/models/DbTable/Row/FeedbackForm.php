<?php

class Statflow_Model_DbTable_Row_FeedbackForm extends Centurion_Db_Table_Row implements Core_Traits_Slug_Model_DbTable_Row_Interface
{

    public function getSlugifyName()
    {
        return 'name';
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __construct(array $config)
    {
        parent::__construct($config);

        $this->_specialGets['feedback_form_pages'] = '_getFeedbackFormPages';
        $this->_specialGets['questions'] = '_getQuestions';
    }

    protected function _getFeedbackFormPages()
    {
        $dependentTables = $this->_getTable()->info('dependentTables');
        return $this->findDependentRowset(
            $dependentTables['feedback_form_pages'],
            null,
            Centurion_Db::getSingleton('statflow/feedbackFormPage')->select(true)->order('order ASC')
        );
    }
     
    protected function _getQuestions()
    {
        $dependentTables = $this->_getTable()->info('dependentTables');
        return $this->findDependentRowset(
            $dependentTables['questions'],
            null,
            Centurion_Db::getSingleton('statflow/question')->select(true)->order('order ASC')
        );
    } 

}

