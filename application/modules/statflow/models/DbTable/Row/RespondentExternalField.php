<?php

class Statflow_Model_DbTable_Row_RespondentExternalField extends Centurion_Db_Table_Row
{

    public function getInsertQuery()
    {
        return 'INSERT INTO `statflow_respondent_external_field` (`id`, `respondent_id`, `import_field_id`, `label`, `value`, `created_at`) VALUES ('
        . $this->id . ', '
        . $this->respondent_id . ', '
        . $this->import_field_id . ', '
        . $this->getTable()->getAdapter()->quote($this->label) . ', '
        . $this->getTable()->getAdapter()->quote($this->value) . ', '
        . $this->getTable()->getAdapter()->quote($this->created_at)
        . ');';
    }

}

