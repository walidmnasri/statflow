<?php

class Statflow_Model_DbTable_Row_Recovery extends Centurion_Db_Table_Row
{

    public function getLastEntryField($field)
    {
        $lastEntry = $this->getLastEntry();
        if($lastEntry)
            return $lastEntry->{$field};
        else
            return null;
    }

    public function getLastEntry()
    {
        return ($this->recovery_entries->count() > 0)
            ?
            $this->recovery_entries->getRow($this->recovery_entries->count() - 1)
            :
            null;
    }


}

