<?php

class Statflow_Model_DbTable_Row_Question extends Centurion_Db_Table_Row
{
    const QUESTION_TYPE_MCQ = 1;

    const QUESTION_TYPE_INTERVAL = 2;

    const QUESTION_TYPE_ORDERED = 3;

    const QUESTION_TYPE_OPEN = 4;

    const QUESTION_TYPE_ARRAY = 5;

    const QUESTION_TYPE_MULTIPLE_CHOICE = 6;

    const QUESTION_TYPE_IMAGE = 7;

    const QUESTION_TYPE_MULTIPLE_IMAGES = 8;
}

