<?php

class Statflow_Model_DbTable_Row_Study extends Centurion_Db_Table_Row implements Core_Traits_Slug_Model_DbTable_Row_Interface
{

    public function getSlugifyName()
    {
        return 'name';
    }

    public function __toString()
    {
        return $this->name;
    }
}

