<?php

class Statflow_Model_DbTable_Row_FrontItem extends Centurion_Db_Table_Row
{
    const FRONT_ITEM_KPI = 1;
    
    const FRONT_ITEM_TDR = 2;
    
    const FRONT_ITEM_EXP = 3;
    
    const FRONT_ITEM_GLO = 4; 

}

