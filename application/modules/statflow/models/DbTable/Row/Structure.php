<?php

class Statflow_Model_DbTable_Row_Structure extends Centurion_Db_Table_Row implements Core_Traits_Slug_Model_DbTable_Row_Interface
{

    public function getSlugifyName()
    {
        return 'external_id';
    }
}

