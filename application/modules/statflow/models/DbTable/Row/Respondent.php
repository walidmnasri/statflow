<?php

class Statflow_Model_DbTable_Row_Respondent extends Centurion_Db_Table_Row
{

    public function getInsertQuery()
    {
        return 'INSERT INTO `statflow_respondent` (`id`, `study_id`, `feedback_form_id`, `status`, `firstname`, `lastname`, `external_id`, `email`, `token`, `created_at`) VALUES ('
        . $this->id . ', '
        . $this->study_id . ', '
        . $this->feedback_form_id . ', '
        . $this->status . ', '
        . $this->getTable()->getAdapter()->quote($this->firstname) . ', '
        . $this->getTable()->getAdapter()->quote($this->lastname) . ', '
        . $this->getTable()->getAdapter()->quote($this->external_id) . ', '
        . $this->getTable()->getAdapter()->quote($this->email) . ', '
        . $this->getTable()->getAdapter()->quote($this->token) . ', '
        . $this->getTable()->getAdapter()->quote($this->created_at)
        . ');';
    }

}

