<?php

class Statflow_Model_DbTable_Row_FeedbackFormPage extends Centurion_Db_Table_Row
{

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->_specialGets['questions'] = '_getQuestionFeedbackByPages';
    }
    protected function _getQuestionFeedbackByPages()
    {
        $dependentTables = $this->_getTable()->info('dependentTables');
        return $this->findDependentRowset(
            $dependentTables['questions'],
            null,
            Centurion_Db::getSingleton('statflow/question')->select(true)->order('order ASC')
        );
    }
}

