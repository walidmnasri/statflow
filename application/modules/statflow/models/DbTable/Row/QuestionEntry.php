<?php

class Statflow_Model_DbTable_Row_QuestionEntry extends Centurion_Db_Table_Row
{

    const MCQ_MODALITY_SIMPLE = 1;
    const MCQ_MODALITY_EXCLUSIVE = 2;
    const MCQ_MODALITY_COMPLEMENT = 3;

    const ORDERED_MODALITY = 4;
    const INTERVAL_MODALITY = 5;
    const ARRAY_MODALITY = 6;


    const MCQ_MODALITY_SIMPLE_IMAGE = 7;
    const MCQ_MODALITY_EXCLUSIVE_IMAGE = 8;
}

