<?php

class Statflow_Model_DbTable_Row_Import extends Centurion_Db_Table_Row
{

    public function getInsertQuery()
    {
        return 'INSERT INTO `statflow_import` (`id`,`study_id`,`feedback_form_id`, `number`, `created_at`) VALUES ('
            . $this->id . ','
            . $this->study_id . ','
            . $this->feedback_form_id . ','
            . $this->number . ','
            . $this->getTable()->getAdapter()->quote($this->created_at)
            .');';
    }


}

