<?php

/**
 * Class Statflow_Model_Solr
 */
class Statflow_Model_Solr
{

    /**
     * @var Statflow_Model_Solr
     */
    protected static $_instance = null;

    protected static $_defaultConfig =  array(
        'endpoint' => array(
            'localhost' => array(
                'host' => 'java',
                'port' => 8986,
                'path' => '/solr/',
                'core' => '1',
            )
        )
    );

    /**
     * @var array
     */
    protected $_config = array();

    /**
     * @var array
     */
    protected $_handler = array();

    /**
     * @var int
     */
    protected $_start = 0;

    /**
     * @var int
     */
    protected $_limit = 10000;

    protected $_rowCount = 0;

    protected $_facetsData = array();

    /**
     * @return int
     */
    public function getRowCount()
    {
        return $this->_rowCount;
    }



    /**
     * Singleton
     *
     * @return Statflow_Model_Solr
     */
    public static function getInstance()
    {
        if (self::$_instance == null) {

            self::$_instance = new Statflow_Model_Solr();
            self::$_instance->_config = self::$_defaultConfig;
            Solarium\Autoloader::register();

        }
        return self::$_instance;
    }

    public static function setDefaultConfig($config){
        self::$_defaultConfig = $config;
    }

    public static function getDefaultConfig(){
        return self::$_defaultConfig;
    }

    /**
     * @param $limit
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }

    /**
     * @return array
     */
    public function getHandler()
    {
        return $this->_handler;
    }

    /**
     * @param $handler
     */
    public function setHandler($handler)
    {
        $this->_handler = $handler;
    }

    /**
     * @param $core
     */
    public function setCore($core)
    {
        $this->_config['endpoint']['localhost']['core'] = $core;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        require_once(APPLICATION_PATH . '/../library/Solarium/Autoloader.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcherInterface.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/Event.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcher.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/ImmutableEventDispatcher.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/GenericEvent.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventSubscriberInterface.php');
        require_once(APPLICATION_PATH
            . '/../library/Symfony/Component/EventDispatcher/ContainerAwareEventDispatcher.php');
    }

    /**
     * @param integer $respondent_id, $recovery_status
     * @param integer $recovery_status
     *
     * @return
     * @throws Zend_Exception
     */
    public function updateRecoveryStatus($respondent_id, $recovery_status)
    {
        if ($this->_config['endpoint']['localhost']['core'] == null) {
            throw new Zend_Exception('Solr Core is not specified !');
        }

        $client = new Solarium\Client($this->_config);
        $client->setAdapter('Solarium\Core\Client\Adapter\ZendHttp');


        // get an update query instance
        $update = $client->createUpdate();

        // create a new document for the data
        $doc = $update->createDocument();
        $doc->setKey('respondent_id', $respondent_id);
        $doc->setFieldModifier('recovery_status', Solarium\QueryType\Update\Query\Document\Document::MODIFIER_SET);
        $doc->setField('recovery_status', $recovery_status);

        $update->addDocument($doc);
        $update->addCommit();

        try {
            // this executes the query and returns the result
            $result = $client->update($update);
            return $result;
        } catch (Exception $e) {
            //Zend_Debug::dump($e->getTrace(), $e->getMessage());
            return 'Error: updateRecoveryStatus';
        }
    }

    /**
     * @return \Solarium\Client
     * @throws Zend_Exception
     */
    public function getClient()
    {
        if ($this->_config['endpoint']['localhost']['core'] == null) {
            throw new Zend_Exception('Solr Core is not specified !');
        }

        $client = new Solarium\Client($this->_config);
        $client->setAdapter('Solarium\Core\Client\Adapter\ZendHttp');

        return $client;
    }

    /**
     * @param array $params
     * @param array $sort
     *
     * @return array
     * @throws Zend_Exception
     */
    public function query($params = array(), $sort = array(), $start = 0, $limit = 30, $facets = array())
    {
        if ($this->_config['endpoint']['localhost']['core'] == null) {
            throw new Zend_Exception('Solr Core is not specified !');
        }

        $this->_start = $start;
        $this->_limit = $limit;

        $client = new Solarium\Client($this->_config);
        $client->setAdapter('Solarium\Core\Client\Adapter\ZendHttp');
        // get a select query instance
        $query = $client->createQuery($client::QUERY_SELECT);
        $query->setHandler($this->_handler);
        $query->setStart($this->_start);
        $query->setRows($this->_limit);

        //Query params
        if (count($params) > 0) {
            $queryString = implode('+', $params);
        } else {
            $queryString = '*:*';
        }
        $query->setQuery($queryString);

        //Sort params
        if (count($sort) > 0) {
            $sortString = implode(', ', $sort);
            $query->addParam('sort', $sortString);
        }

        if (count($facets)) {
            // get the facetset component
            $facetSet = $query->getFacetSet();
            foreach ($facets as $facetName => $facetQueries) {
                $facetMultiQuery = $facetSet->createFacetMultiQuery($facetName);
                foreach ($facetQueries as $facetQueryName => $facetQuery) {
                    $facetMultiQuery->createQuery($facetQueryName, $facetQuery);
                }
            }
        }

        //Execute query
        try {
            // this executes the query and returns the result
            $resultset = $client->execute($query);
            $data = $resultset->getData();
            $this->_rowCount = $resultset->getNumFound();
            $this->_facetsData = $resultset->getFacetSet();
            return $data['response']['docs'];
        } catch (Exception $e) {
            //Zend_Debug::dump($this->_getAllParams());
            //Zend_Debug::dump(func_get_args(), $e->getMessage());
            //Zend_Debug::dump($e->getTrace(), $e->getMessage());
            //die();
            return 'Error: query';
        }
        return array();

    }

    /**
     * @param array $params
     * @param array $sort
     * @param array $facets
     *
     * @return array
     * @throws Zend_Exception
     */
    public function queryWithFacet($params = array(), $sort = array(), $facets = array())
    {
        if ($this->_config['endpoint']['localhost']['core'] == null) {
            throw new Zend_Exception('Solr Core is not specified !');
        }

        $client = new Solarium\Client($this->_config);
        $client->setAdapter('Solarium\Core\Client\Adapter\ZendHttp');
        // get a select query instance
        $query = $client->createQuery($client::QUERY_SELECT);
        $query->setHandler($this->_handler);
        $query->setRows($this->_limit);


        //Query params
        if (count($params) > 0) {
            $queryString = implode('+', $params);
        } else {
            $queryString = '*:*';
        }

        $query->setQuery($queryString);


        //Sort params
        if (count($sort) > 0) {
            $sortString = implode(', ', $sort);
            $query->addParam('sort', $sortString);
        }



        if (count($facets)) {
            // get the facetset component
            $facetSet = $query->getFacetSet();
            foreach ($facets as $facetName => $facetQueries) {
                $facetMultiQuery = $facetSet->createFacetMultiQuery($facetName);
                foreach ($facetQueries as $facetQueryName => $facetQuery) {
                    $facetMultiQuery->createQuery($facetQueryName, $facetQuery);
                }
            }
        }

        //Execute query
        try {
            // this executes the query and returns the result
            $resultset = $client->execute($query);
            $data = $resultset->getData();
            return array(
                'data' => $data['response']['docs'],
                'facetset' => $resultset->getFacetSet()
            );
        } catch (Exception $e) {
            //Zend_Debug::dump($this->_getAllParams());
            //Zend_Debug::dump(func_get_args(), $e->getMessage());
            //Zend_Debug::dump($e->getTrace(), $e->getMessage());
            //die();
            return 'Error: queryWithFacet';
        }
        return array();

    }

    public function getFacetsData()
    {
        return $this->_facetsData;
    }

}