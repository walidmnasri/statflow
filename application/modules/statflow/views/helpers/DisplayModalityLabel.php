<?php

class Statflow_View_Helper_DisplayModalityLabel extends Zend_View_Helper_Abstract
{
    public function displayModalityLabel($code, $modality_id)
    {
        if($modality_id == null || $modality_id == '')
            return '';
        return $this->view->translate('form__question_' . $code . '_modality-' . $modality_id);
    }
}