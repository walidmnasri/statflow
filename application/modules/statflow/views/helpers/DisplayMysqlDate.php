<?php

class Statflow_View_Helper_DisplayMysqlDate extends Zend_View_Helper_Abstract
{
    public function displayMysqlDate($mysqldate)
    {
        if (empty($mysqldate) || $mysqldate == '0000-00-00 00:00:00')
            return '';

        $date = new Zend_Date($mysqldate, 'YYYY-MM-dd HH:mm:ss');
        $formatedDate = $date->get(Zend_Date::DATE_MEDIUM);
        unset($date);
        return $formatedDate;
    }
}