<?php

class Statflow_View_Helper_SimpleTextCrop extends Zend_View_Helper_Abstract
{
    public function simpleTextCrop($text, $limit)
    {
        $cleanText = html_entity_decode(strip_tags($text), null, 'UTF-8');

        $len = mb_strlen($cleanText);

        if ($len > $limit) {
            return mb_substr(strip_tags($text), 0, $limit, 'UTF-8') . '...';
        }
        return $cleanText;
    }
}