<?php

class Statflow_View_Helper_DisplayDateInLocale extends Zend_View_Helper_Abstract
{
    public function displayDateInLocale($mysqldate, $locale)
    {
        if (empty($mysqldate) || $mysqldate == '0000-00-00 00:00:00')
            return '';

        $date = new Zend_Date($mysqldate, 'yyyy-MM-dd HH:mm:ss');
        switch($locale) {
            case 'fr':
                $formatedDate = $date->get('dd/MM/yyyy');
                break;
            case 'en':
            default:
                $formatedDate = $date->get('MM/dd/yyyy');
                break;
        }
        unset($date);
        return $formatedDate;
    }
}