<?php

class Statflow_View_Helper_DisplayValue extends Zend_View_Helper_Abstract
{
    public function displayValue($code, $value, $modalityQuestions = array(), $openQuestions = array(), $limitCrop = 15, $export = false)
    {
        if(preg_match('#\|#', $value)){
            $values = explode('|', $value);
            $texts = array();
            foreach($values as $v){
                $modalityText = $this->view->displayModalityLabel($code, $v);
                if(strlen($modalityText) > $limitCrop) {
                    $texts[] =  '<a href="#" title="'. $modalityText  .'">'. strip_tags($this->view->simpleTextCrop($modalityText, $limitCrop)) .'</a>';
                }
                else
                $texts[] = $modalityText;
            }
            if($export) {
                return implode(', ', $texts);
            }
            return implode('<br>', $texts);

        } else {
            if(in_array($code, $modalityQuestions)) {
                $modalityText = $this->view->displayModalityLabel($code, $value);
                if(strlen($modalityText) > $limitCrop) {
                    return '<a href="#" title="'. $modalityText  .'">'. strip_tags($this->view->simpleTextCrop($modalityText, $limitCrop)) .'</a>';
                }
                else
                    return $modalityText;
            }
            if(strlen($value) > $limitCrop) {
                return '<a href="#" title="'. $value  .'">'. strip_tags($this->view->simpleTextCrop($value, $limitCrop)) .'</a>';
            }
            return $value;
        }
    }
}