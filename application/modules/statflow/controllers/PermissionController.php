<?php
require_once __DIR__ . '/../services/Permission.php';

use Statflow\Service\Permission;

class Statflow_PermissionController extends Centurion_Controller_Action
{

    /**
     *
     * @var Permission
     */
    private $permissionService;

    /**
     *
     * @return \Statflow\Service\Permission
     */
    public function getPermissionService()
    {
        if (! $this->permissionService) {
            $this->permissionService = new Permission();
        }
        
        return $this->permissionService;
    }

    /**
     *
     * @param \Statflow\Service\Permission $permissionService
     */
    public function setPermissionService($permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Initialize the User controller
     */
    public function init()
    {       
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * Grant all permission for a couple group / action
     */
    public function grantallAction()
    {
        $permissionService = $this->getPermissionService();
        
        $_success = true;
        $_permissions = $permissionService->permissionTree($permissionService->getAllPermission());
        
        if ($this->_getParam('group_id')) {
            $_group = $permissionService->getGroupById($this->_getParam('group_id'));
            if ($this->_getParam('action_name')) {
                $_perms = $_permissions[$this->_getParam('controller_name')][$this->_getParam('action_name')];
                foreach ($_perms as $_perm) :
                    $_entry = $permissionService->getGroupPermissionById($_perm->id, $_group->id);
                    if (is_null($_entry)) {
                        
                        try {
                            $_entry = $permissionService->getValidatedGroupPermission($_group->id, $_perm->id);
                        } catch (Exception $e) {
                            $_success = false;
                        }
                    }
                endforeach;
            } else {
                $_actions = $_permissions[$this->_getParam('controller_name')];
                foreach ($_actions as $_action => $_perms) :
                    foreach ($_perms as $_perm) :
                        $_entry = $permissionService->getGroupPermissionById($_perm->id, $_group->id);
                        if (is_null($_entry)) {
                            
                            try {
                                $_entry = $permissionService->getValidatedGroupPermission($_group->id, $_perm->id);
                            } catch (Exception $e) {
                                $_success = false;
                            }
                        }
                    endforeach;
                endforeach;
            }
            if ($_success)
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_grantallPerm_flashMessenger_success@backoffice'));
            else
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_grantallPerm_flashMessenger_error@backoffice'));
            
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'permission',
                'action' => 'index'
            ), null, true));
        } elseif ($this->_getParam('user_id')) {
            $_user = $permissionService->getUserById($this->_getParam('user_id'));
            if ($this->_getParam('action_name')) {
                $_perms = $_permissions[$this->_getParam('controller_name')][$this->_getParam('action_name')];
                foreach ($_perms as $_perm) :
                    $_entry = $permissionService->getUserPermissionById($_perm->id, $_user->id);
                    if (is_null($_entry)) {
                        
                        try {
                            $_entry = $permissionService->getValidatedUserPermission($_user->id, $_perm->id);
                        } catch (Exception $e) {
                            $_success = false;
                        }
                    }
                endforeach;
            } else {
                $_actions = $_permissions[$this->_getParam('controller_name')];
                foreach ($_actions as $_action => $_perms) :
                    foreach ($_perms as $_perm) :
                        $_entry = $permissionService->getUserPermissionById($_perm->id, $_user->id);
                        if (is_null($_entry)) {
                            
                            try {
                                $_entry = $permissionService->getValidatedUserPermission($_user->id, $_perm->id);
                            } catch (Exception $e) {
                                $_success = false;
                            }
                        }
                    endforeach;
                endforeach;
            }
            if ($_success)
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_denyallPerm_flashMessenger_success@backoffice'));
            else
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_denyallPerm_flashMessenger_error@backoffice'));
            
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'permission',
                'action' => 'user',
                'user_id' => $_user->id
            ), null, true));
        }
    }

    /**
     * Deny all permission for a couple group / action
     */
    public function denyallAction()
    {
        $permissionService = $this->getPermissionService();
        
        $_success = true;
        $_permissions = $permissionService->permissionTree($permissionService->getAllPermission());
        
        if ($this->_getParam('group_id')) {
            $_group = $permissionService->getGroupById($this->_getParam('group_id'));
            if ($this->_getParam('action_name')) {
                $_perms = $_permissions[$this->_getParam('controller_name')][$this->_getParam('action_name')];
                foreach ($_perms as $_perm) :
                    $_entry = $permissionService->getGroupPermissionById($_perm->id, $_group->id);
                    if (! is_null($_entry)) {
                        try {
                            $_entry->delete();
                        } catch (Exception $e) {
                            $_success = false;
                        }
                    }
                endforeach;
            } else {
                $_actions = $_permissions[$this->_getParam('controller_name')];
                foreach ($_actions as $_action => $_perms) :
                    foreach ($_perms as $_perm) :
                        $_entry = $permissionService->getGroupPermissionById($_perm->id, $_group->id);
                        if (! is_null($_entry)) {
                            try {
                                $_entry->delete();
                            } catch (Exception $e) {
                                $_success = false;
                            }
                        }
                    endforeach;
                endforeach;
            }
            if ($_success)
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_denyallPerm_flashMessenger_success@backoffice'));
            else
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_denyallPerm_flashMessenger_error@backoffice'));
            
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'permission',
                'action' => 'index'
            ), null, true));
        } elseif ($this->_getParam('user_id')) {
            $_user = $permissionService->getUserById($this->_getParam('user_id'));
            if ($this->_getParam('action_name')) {
                $_perms = $_permissions[$this->_getParam('controller_name')][$this->_getParam('action_name')];
                foreach ($_perms as $_perm) :
                    $_entry = $permissionService->getUserPermissionById($_perm->id, $_user->id);
                    if (! is_null($_entry)) {
                        try {
                            $_entry->delete();
                        } catch (Exception $e) {
                            $_success = false;
                        }
                    }
                endforeach;
            } else {
                $_actions = $_permissions[$this->_getParam('controller_name')];
                foreach ($_actions as $_action => $_perms) :
                    foreach ($_perms as $_perm) :
                        $_entry = $permissionService->getUserPermissionById($_perm->id, $_user->id);
                        if (! is_null($_entry)) {
                            try {
                                $_entry->delete();
                            } catch (Exception $e) {
                                $_success = false;
                            }
                        }
                    endforeach;
                endforeach;
            }
            if ($_success)
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_denyallPerm_flashMessenger_success@backoffice'));
            else
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_denyallPerm_flashMessenger_error@backoffice'));
            
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'permission',
                'action' => 'user',
                'user_id' => $_user->id
            ), null, true));
        }
    }

    /**
     * List the permissions
     */
    public function indexAction()
    {
        $permissionService = $this->getPermissionService();
        
        $this->view->groups = $permissionService->getAllGroup();
        $this->view->perms = $permissionService->getAllGroupPermission()->toArray();
        $this->view->permissions = $permissionService->permissionTree($permissionService->getAllPermission());
    }

    /**
     * List the permissions for a user
     */
    public function userAction()
    {
        $permissionService = $this->getPermissionService();
        
        $this->view->user = $permissionService->getUserById($this->_getParam('user_id'));
        $this->view->perms = $permissionService->getAllUserPermission()->toArray();
        $this->view->permissions = $permissionService->permissionTree($permissionService->getAllPermission());
    }

    /**
     * Grant a permission for a group
     */
    public function addpermAction()
    {
        $permissionService = $this->getPermissionService();
        
        if (array_key_exists('group_id', $this->_getAllParams()) && array_key_exists('permission_id', $this->_getAllParams())) {
            $group_id = $this->_getParam('group_id');
            $permission_id = $this->_getParam('permission_id');
            
            try {
                $permission = $permissionService->getValidatedGroupPermission($group_id, $permission_id);
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_addPerm_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'index'
                ), null, true));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_addPerm_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'index'
                ), null, true));
            }
        } elseif (array_key_exists('user_id', $this->_getAllParams()) && array_key_exists('permission_id', $this->_getAllParams())) {
            $user_id = $this->_getParam('user_id');
            $permission_id = $this->_getParam('permission_id');
            
            try {
                $permission = $permissionService->getValidatedUserPermission($user_id, $permission_id);
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_addPerm_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'user',
                    'user_id' => $this->_getParam('user_id')
                ), null, true));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_addPerm_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'user',
                    'user_id' => $this->_getParam('user_id')
                ), null, true));
            }
        } else {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_addPerm_flashMessenger_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'permission',
                'action' => 'index'
            ), null, true));
        }
    }

    /**
     * Deny a permission for a group
     */
    public function deletepermAction()
    {
        $permissionService = $this->getPermissionService();
        
        if (array_key_exists('group_id', $this->_getAllParams()) && array_key_exists('permission_id', $this->_getAllParams())) {
            $group_id = $this->_getParam('group_id');
            $permission_id = $this->_getParam('permission_id');
            try {
                $permissionService->deleteGroupPermission($group_id, $permission_id);
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_deletePerm_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'index'
                ), null, true));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_deletePerm_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'index'
                ), null, true));
            }
        } elseif (array_key_exists('user_id', $this->_getAllParams()) && array_key_exists('permission_id', $this->_getAllParams())) {
            
            $user_id = $this->_getParam('user_id');
            $permission_id = $this->_getParam('permission_id');
            try {
                $permissionService->deleteUserPermission($user_id, $permission_id);
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_deletePerm_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'user',
                    'user_id' => $this->_getParam('user_id')
                ), null, true));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_deletePerm_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'user',
                    'user_id' => $this->_getParam('user_id')
                ), null, true));
            }
        } else {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_deletePerm_flashMessenger_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'permission',
                'action' => 'index'
            ), null, true));
        }
    }

    /**
     * Display the creation formula
     * Manage the insert
     */
    public function addAction()
    {
        $permissionService = $this->getPermissionService();
        
        if ($this->getRequest()->isPost()) {

            if ($permissionService->isValid($this->_getAllParams())) {
             
                try {
                    $permission = $permissionService->getValidated($this->_getParam('id'));
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'permission',
                        'action' => 'index'
                    )));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'permission',
                        'action' => 'add'
                    )));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_add_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($permissionService->getValidator($this->_getAllParams())
                    ->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'add'
                )));
            }
        }
    }

    /**
     * Display the edition formula
     * Manage the update
     */
    public function editAction()
    {
        $permissionService = $this->getPermissionService();
        
        if ($this->getRequest()->isPost()) {
            if ($permissionService->isValid($this->_getAllParams())) {
                
                try {
                    
                    $permission = $permissionService->getValidated($this->_getParam('id'));
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_edit_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                        'controller' => 'permission',
                        'action' => 'index'
                    )));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_edit_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array(
                        'controller' => 'permission',
                        'action' => 'edit'
                    )));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__permission_edit_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($permissionService->getValidator($this->_getAllParams())
                    ->getMessages());
                $this->_redirect($this->view->url(array(
                    'controller' => 'permission',
                    'action' => 'edit'
                )));
            }
        }
        
        $this->view->permission = $permissionService->getPermissionById($this->_getParam('id'));
    }

    /**
     * Manage the deletion
     */
    public function deleteAction()
    {
        $permissionService = $this->getPermissionService();
        
        if ($this->getRequest()->isGet()) {
            try {
                $permissionService->deletePermission($this->_getParam('id'));
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__permission_delete_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'index'
                )));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('navigation__statflow__permission_delete_flashMessenger_error@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'permission',
                    'action' => 'index'
                )));
            }
        }
    }
}

