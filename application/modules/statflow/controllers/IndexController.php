<?php

class Statflow_IndexController extends Centurion_Controller_Action
{
    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
    }

    public function indexAction()
    {
        $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'structure', 'action' => 'index'), null, true));
    }
}