<?php
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/Structure.php';
require_once __DIR__ . '/../services/StudyUser.php';
require_once __DIR__ . '/../services/Translation.php';
require_once __DIR__ . '/../services/FiltersKpi.php';
require_once __DIR__ . '/../services/FeedbackForm.php';

use Statflow\Service\Study;
use Statflow\Service\Structure;
use Statflow\Service\StudyUser;
use Statflow\Service\Translation;
use Statflow\Service\FiltersKpi;
use Statflow\Service\FeedbackForm;

class Statflow_StudyController extends Centurion_Controller_Action
{
    /**
     *
     * @var Structure
     */
    private $structureService;

    /**
     * @var Study
     */
    private $studyService;

    /**
     * @var StudyUser
     */
    private $studyUserService;

    /**
     * @var Common
     */
    private $commonService;

    /**
     * @var TranslationService
     */
    private $translationService;

    /**
     * @var FiltersKpi
     */
    private $filterService;

    /**
     * @var FeedbackForm
     */

    private $feedbackFormService;

    /**
     * @return FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (!$this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
        return $this->feedbackFormService;
    }

    /**
     * @return FiltersKpi
     */
    public function getFilterService()
    {
        if (!$this->filterService) {
            $this->filterService = new FiltersKpi();
        }
        return $this->filterService;
    }

    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }

        return $this->structureService;
    }

    /**
     *
     * @param \Statflow\Service\Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    }

    /**
     *
     * @return \Statflow\Service\Study
     */
    public function getStudyService()
    {
        if (! $this->studyService) {
            $this->studyService = new Study();
        }

        return $this->studyService;
    }

    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }

    /**
     *
     * @return \Statflow\Service\StudyUser
     */
    public function getStudyUserService()
    {
        if (! $this->studyUserService) {
            $this->studyUserService = new StudyUser();
        }

        return $this->studyUserService;
    }

    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStudyUserService($studyUserService)
    {
        $this->studyUserService = $studyUserService;
    }

    /**
     *
     * @return \Statflow\Service\Common
     */
    public function getCommonService()
    {
        if (! $this->commonService) {
            $this->commonService = new Common();
        }

        return $this->commonService;
    }

    /**
     *
     * @param \Statflow\Service\Common $commonService
     */
    public function setCommonService($commonService)
    {
        $this->commonService = $commonService;
    }

    /**
     *
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (! $this->translationService) {
            $this->translationService = new Translation();
        }

        return $this->translationService;
    }

    /**
     *
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }

    /**
     *
     * @var Statflow_Model_DbTable_Question
     */
    protected $_questionModel = null;

    /**
     * Initialize the Study controller
     */
    public function init()
    {
        $this->view->pageLayout = 'study';
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * List the studies
     */
    public function indexAction()
    {
        $storage = (array)$_SESSION['Zend_Auth']['storage'];

        $studyService = $this->getStudyService();
        $this->view->params = $this->_getAllParams();
        $this->view->studies = $studyService->getAllStudyByUser($storage['id']);
        $this->view->feedback_forms = $studyService->getAllFeedbackForm();

        //export des traductions
        $export = $studyService->setExport($this->_getParam('export', false));
        $studyService->exportTraductions($this->_getParam('form_id'));

        //import csv des utilisateurs
        //Recupère les données loguées après import
        $this->view->importResult = $studyService->getImportResult();

        // Utilisateur log file
        $this->view->importResultInsert =$this->view->importResult[0][0];
        $this->view->importResultUpdate =$this->view->importResult[0][1];
        $this->view->importResultErrors =$this->view->importResult[0][2];
        $this->view->importResultErrorLines = ($this->view->importResultErrors > 0) ? $this->view->importResult[1][0] : "";
    }

    /**
     * Display the creation formula
     * Manage the insert
     */
    public function addAction()
    {
        $storage = (array) $_SESSION['Zend_Auth']['storage'];

        $studyService = $this->getStudyService();
        $structureService = $this->getStructureService();
        $studyUserService = $this->getStudyUserService();
        $filterService    = $this->getFilterService();
        $feedbackFormService = $this->getFeedbackFormService();

        if ($this->getRequest()->isPost()) {
            if (!$studyService->isValid($this->_getAllParams(), 'manage_study')) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('study_add_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($studyService->getValidator($this->_getAllParams())->getMessages());
                $this->_redirect($this->view->url(array(
                    'controller' => 'study',
                    'action' => 'add'
                )));
            }

            $structure = $structureService->getById(array('id' => $this->_getParam('structure_id')));
            $structure_id = $structure->id;
            $is_published = ($this->_getParam('is_published', false) != FALSE) ? 1 : 0;
            $meta = Zend_Json::encode(isset($_POST['style']) ? array('style'=>$_POST['style']) : array('style'=>''));

            $global['is_published_global'] = (isset($_POST['is_published_global']))? $_POST['is_published_global']:0;
            $global['sel_filters_kpi'] 	   = (isset($_POST['sel_filters_kpi']))? $_POST['sel_filters_kpi']: null;
            $global['colorSatG'] = [
                'colorTS' => $_POST['colorTS'],
                'colorS'        => $_POST['colorS'],
                'colorPS'       => $_POST['colorPS'],
                'colorPSDT'     => $_POST['colorPSDT'],
                'colorAvgGlo'   => $_POST['colorAvgGlo']
              ] ;
            $global['colorExpG'] = [
                'colorTS-1' => $_POST['colorTS-1'],
                'colorS-1'    => $_POST['colorS-1'],
                'colorPS-1'   => $_POST['colorPS-1'],
                'colorPSDT-1' => $_POST['colorPSDT-1']
            ] ;
            $global['colorNps'] = [
                'colorPROM' => $_POST['colorPROM'],
                'colorPASS'      => $_POST['colorPASS'],
                'colorDETR'      => $_POST['colorDETR'],
                'colorNPS'       => $_POST['colorNPS'],
                'colorAvgNps'    => $_POST['colorAvgNps']
            ] ;
            $parseJsonGlobal = Zend_Json::encode($global);

            $imageForm = $feedbackFormService->addImageForm();
            $image = '';
            $target_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/studies/";
            if ($imageForm->isValid($this->getRequest()->getPost())) {
                try {
                    $image = $feedbackFormService->uploadImage($target_dir);

                } catch (Zend_File_Transfer_Exception $e) {
                    Zend_Debug::dump($e);
                }
            }
            echo $image;
            $_entry = $studyService->getValidated($structure_id, $is_published, $this->_getParam('id'), $meta, $parseJsonGlobal, $image);
            try {
                $_entry->save();
                $studyUserService->saveStudyUser($storage['id'], $_entry->id);
                $path = APPLICATION_PATH . '/../public/ftp/' . $_entry->id;
                if (! Sayyes_Tools_Filesystem::directoryExist($path)) {
                    Sayyes_Tools_Filesystem::createdir($path);
                }
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('study_add_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'controller' => 'study',
                    'action' => 'index'
                )));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('study_add_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'controller' => 'study',
                    'action' => 'add'
                )));
            }
        }
        $this->view->studies = $studyService->getAll();
        $this->view->structures = $structureService->getAll();
    }

    /**
     * Display the edit formula
     */
    public function editAction()
    {
        $this->addAction();
        $studyService        = $this->getStudyService();
        $structureService    = $this->getStructureService();
        $filterService       = $this->getFilterService();
        $feedbackFormService = $this->getFeedbackFormService();

        $feedbackBystudy = $feedbackFormService->getAllFeedbackByStudyId($this->_getParam('id'));
       if (count($feedbackBystudy) == 0) {
           $this->view->external_fields = [];
       }
       else {
           $crmData = [];
           foreach ($feedbackBystudy as $idFeedback) {
               $crmData[$idFeedback][] = $filterService->getAllExternalFieldByFeedbackFormId($idFeedback)->toArray();
           }
           $crm =  [];
           $j = 0;
           foreach ($crmData as $key => $value) {
               $crm[$j][] = null;
               if (isset($value[0])) {
                   foreach ($value[0] as $element) {
                       $crm[$j][$element['id']] = $element['external_id'];
                   }
               }
               $j++;
           }

           $result = [];
           if (!empty($crm) && count($crm) > 1) {
               $result = $crm[0];
               $i = 0;
               foreach ($crm as $key => $element) {
                   $result = array_intersect($result,$crm[$i]);
                   if (empty($result)) {
                       $result = [];
                       break;
                   }
                   $i++;
               }
           }
           $this->view->external_fields = array_filter($result);
       }

        $_entry = $studyService->getById(array('id' => $this->_getParam('id')));
        $this->view->study = $_entry;
        $ecranGlobal = Zend_Json::decode($_entry->ecran_global);
        $this->view->ecranGlobal = $ecranGlobal;
        $this->view->filters = $ecranGlobal['sel_filters_kpi'];
        $meta = Zend_Json::decode($_entry->meta ? $_entry->meta : '{}');
        $meta = Zend_Json::encode(isset($_POST['style']) ? array(
            'style' => $_POST['style']
        ) : array(
            'style' => ''
        ));

        $this->view->structures = $structureService->getAll();
    }

    /**
     * Delete the study
     */
    public function deleteAction()
    {
        $studyService = $this->getStudyService();
        $studyUserService = $this->getStudyUserService();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $studyService->getById(array(
                'id' => $this->_getParam('id')
            ));
            try {
                $_entry->delete();
                $studyUserService->deleteStudyByStudyId($this->_getParam('id'));
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__study_delete_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'study',
                    'action' => 'index'
                ), null, true));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__study_delete_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'study',
                    'action' => 'index'
                ), null, true));
            }
        } else {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__study_delete_flashMessenger_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'study',
                'action' => 'index'
            ), null, true));
        }
    }

    /**
     * Publish the study
     */
    public function publishAction()
    {
        $studyService = $this->getStudyService();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $studyService->getById(array(
                'id' => $this->_getParam('id')
            ));
            $_entry->is_published = 1;
            try {
                $_entry->save();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('study_publish_success@backoffice'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('study_publish_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('study_publish_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'study',
            'action' => 'index'
        ), null, true));
    }

    /**
     * Unpublish the study
     */
    public function unpublishAction()
    {
        $studyService = $this->getStudyService();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $studyService->getById(array(
                'id' => $this->_getParam('id')
            ));
            $_entry->is_published = 0;
            try {
                $_entry->save();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('study_unpublish_success@backoffice'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('study_unpublish_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('study_unpublish_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'study',
            'action' => 'index'
        ), null, true));
    }

    public function manageEmailAction()
    {
        $studyService = $this->getStudyService();
        $structureService = $this->getStructureService();
        if($this->_request->isPost()){
            $this->_flashMessenger->clearMessages($namespace=NULL);
            if(isset($_POST['btnEmailSave'])) {
                if($_POST['btnEmailSave'] == 'emailconnex') {
                    if ($studyService->isValid($this->_getAllParams(), 'manage_email_connex')) {
                        $_entry = $studyService->getById(array(
                            'id' => $this->_getParam('id')
                        ));
                        $_entry->actif_connex = 1;
                        $_entry->sender_name_connex = $_POST['sender_name_connex'];
                        $_entry->sender_email_connex = $_POST['sender_email_connex'];
                        try {
                            $_entry->save();
                            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__study_email_connex_flashMessenger_success@backoffice'));
                        }catch (Exception $e) {
                        }
                    }
                } else{
                    if ($studyService->isValid($this->_getAllParams(), 'manage_email_renew')) {
                        $_entry = $studyService->getById(array(
                            'id' => $this->_getParam('id')
                        ));
                        $_entry->actif_renew = 1;
                        $_entry->sender_name_renew = $_POST['sender_name_renew'];
                        $_entry->sender_email_renew = $_POST['sender_email_renew'];
                        try {
                            $_entry->save();
                            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__study_email_renew_flashMessenger_success@backoffice'));
                        }catch (Exception $e) {
                        }
                    }
                }
            }elseif(isset($_POST['btnDesactif'])){
                $_entry = $studyService->getById(array(
                    'id' => $this->_getParam('id')
                ));
                if($_POST['btnDesactif'] == 'emailconnex'){
                    $_entry->actif_connex = 0;
                    $_entry->sender_name_connex = "";
                    $_entry->sender_email_connex = "";
                    try {
                        $_entry->save();
                        $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__study_desactif_connex_flashMessenger_success@backoffice'));
                    }catch (Exception $e) {
                    }
                }else{
                    $_entry->actif_renew = 0;
                    $_entry->sender_name_renew = "";
                    $_entry->sender_email_renew = "";
                    try {
                        $_entry->save();
                        $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__study_desactif_renew_flashMessenger_success@backoffice'));
                    }catch (Exception $e) {
                    }
                }
            }
        }
        $this->view->study = $studyService->getById(array(
            'id' => $this->_getParam('id')
        ));
        $this->view->studies = $studyService->getAll();
        $this->view->structures = $structureService->getAll();
    }

    /**
     * Import Questionnaire
     */
    public function importAction()
    {

        //die('study import');
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $studyService = $this->getStudyService();
        $translationService = $this->getTranslationService();

        echo "file".$_FILES['fileInput']['name'];

        $count_insert=0;
        $count_update=0;
        $count_error=0;
        $line_index=1;
        $line_error="";

        if ( 0 < $_FILES['fileInput']['error'] ) {
            echo 'Error: ' . $_FILES['fileInput']['error'] . '<br>';
        }
        else {
            $csv_file = '/tmp/' . $_FILES['fileInput']['name'];

                move_uploaded_file($_FILES['fileInput']['tmp_name'], $csv_file);

                $csv_traduction_array = $studyService->readCSV($csv_file);
                if($csv_traduction_array){
                    echo "...";
                    foreach($csv_traduction_array as $csv_traduction){
                        $traduction = $studyService->importCsvTraduction($csv_traduction);

                        if($studyService->getImportStatus() == 2) {
                            $line_error = ($count_error==0) ? "[".$line_index."]" : $line_error." [".$line_index."]";
                            $count_error++;
                        }else{
                            if($studyService->getImportStatus() == 1) {
                                $count_update++;
                            }elseif($studyService->getImportStatus() == 0) {
                                $count_insert++;
                            }
                            //$user->save();

                        }
                        $line_index++;
                    }
                    if($count_update > 0 || $count_insert > 0) { // pour generer les fichiers fr/en/ etc... avec les nouvelles traductions
                        $translationService->generateTranslationFiles();
                    }
                    $log_text = $count_insert.",".$count_update.",".$count_error."\n".$line_error;
                    //chmod('/tmp/import_utilisateur_log.txt',0777);
//                     unlink('/tmp/import_questionnaire_log.txt');
//                     file_put_contents('/tmp/import_questionnaire_log.txt', $log_text, FILE_APPEND);

                }else{
                    echo "erreur format";

                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__study__index__import_error_format@backoffice'));
                    //$this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
                }

                unlink('/tmp/' . $_FILES['fileInput']['name']);

                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'study', 'action' => 'index')));
            }
        }


}

