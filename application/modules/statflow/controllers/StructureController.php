<?php
require_once __DIR__ . '/../services/Structure.php';

use Statflow\Service\Structure;

class Statflow_StructureController extends Centurion_Controller_Action
{
    /**
     * 
     * @var Structure
     */
    private $structureService;

    /**
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (!$this->structureService){
            $this->structureService = new Structure();
        }
        
        return $this->structureService;
    }
    
    /**
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }
    
    /**
     * Initialize the Structure controller
     */
    public function init()
    {
        $this->view->pageLayout = 'structure';
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_flashMessenger->setNamespace('success')->hasMessages()){
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('error')->hasMessages()){
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()){
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * Return the html children of a structure
     * @param $structure Statflow_Model_DbTable_Structure
     * @return array
     */
    public function childrenAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $structureService = $this->getStructureService();
        $structure = $structureService->getById($this->_getParam('id'));

        foreach($structure->structures as $child)
        {
            echo $this->view->partial('structure/_tree.phtml', array(
                'is_branch' => (count($child->structures) >= 0) ? TRUE : FALSE,
                'structure' => $child
            ));
        }
    }

    /**
     * List the structure
     */
    public function indexAction()
    {
        $structureService = $this->getStructureService();
        $this->view->structures = $structureService->getAllRoots();
        
        $structureService->setExport($this->_getParam('export', false));
        $structureService->exportStructure($this->_getParam('root_id'));
	
        //Recupère les données loguées après import
        $this->view->importResult = $structureService->getImportResult();
        // Structure log file
        $this->view->importResultInsert =$this->view->importResult[0][0];
        $this->view->importResultUpdate =$this->view->importResult[0][1];
        $this->view->importResultErrors =$this->view->importResult[0][2];        
        $this->view->importResultErrorLines = ($this->view->importResultErrors > 0) ? $this->view->importResult[1][0] : "";
    }

    /**
     * Display the creation formula
     * Manage the insert
     */
    public function addAction()
    {
        $structureService = $this->getStructureService();

        if ($this->getRequest()->isPost()) {
            if ($structureService->isValid($this->_getAllParams())) {
                $parent_id = $this->_getParam('parent_id', false) != FALSE ? $this->_getParam('parent_id') : NULL;
                $structure = $structureService->getValidated($parent_id, $this->_getParam('id'));

                try {
                    $structure->save();
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('structure_add_success@backoffice'));
                    $this->_redirect($this->view->url(array('controller' => 'structure', 'action' => 'index')));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('structure_add_error@backoffice') . '<br><br>' .$e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array('controller' => 'structure', 'action' => 'add')));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('structure_add_error@backoffice'));
                $this->_redirect($this->view->url(array('controller' => 'structure', 'action' => 'add')));
            }
        }

        $this->view->structures = $structureService->getAll();
    }

    /**
     * Display the edition formula
     * Manage the update
     */
    public function editAction()
    {
        $this->addAction();
        $structureService = $this->getStructureService();
        $this->view->structure = $structureService->getById($this->_getParam('id'));
    }

    /**
     * Manage the deletion
     */
    public function deleteAction()
    {
        if ($this->getRequest()->isGet())
        {   
            $structureService = $this->getStructureService();
            $_entry = $structureService->getById( $this->_getParam('id'));
            try
            {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('structure_delete_success@backoffice'));
                $this->_redirect($this->view->url(array('controller' => 'structure', 'action' => 'index')));
            } catch(Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('structure_delete_error@backoffice'));
                $this->_redirect($this->view->url(array('controller' => 'structure', 'action' => 'index')));
            }
        }
    }  
    
    /**
     * Import structure
     */
    public function importAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $structureService = $this->getStructureService();
        
        $count_insert=0;
        $count_update=0;
        $count_error=0;
        $line_index=1;
        $line_error="";
                         
        if ( 0 < $_FILES['fileInput']['error'] ) {
            echo 'Error: ' . $_FILES['fileInput']['error'] . '<br>';
        } else {
            $csv_file = '/tmp/' . $_FILES['fileInput']['name'];
            move_uploaded_file($_FILES['fileInput']['tmp_name'], $csv_file);

            $csv_structure_array = $structureService->readCSV($csv_file);
            if($csv_structure_array){
                echo "...";
                foreach($csv_structure_array as $csv_structure){
                    $structure = $structureService->importCsvStructure($csv_structure);
                    
                    if($structureService->getImportStatus() == 2){                        
                        $line_error = ($count_error==0) ? "[".$line_index."]" : $line_error." [".$line_index."]";
                        $count_error++;
                    }else{
                        if($structureService->getImportStatus() == 1){                        
                            $count_update++;
                        }elseif($structureService->getImportStatus() == 0){   
                            $count_insert++;
                        }  
                        $structure->save();
                    }
                    $line_index++;
                }
                $log_text = $count_insert.",".$count_update.",".$count_error."\n".$line_error;
                unlink('/tmp/import_structure_log.txt');
                file_put_contents('/tmp/import_structure_log.txt', $log_text, FILE_APPEND);
                
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('structure_import_error_format@backoffice'));
            }

            unlink('/tmp/' . $_FILES['fileInput']['name']);
            $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'structure', 'action' => 'index')));
        }
    }
}
