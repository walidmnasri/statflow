<?php

class Statflow_FeedbackFormTemplateController extends Centurion_Controller_AGL
{

    /**
     * @var Statflow_Model_DbTable_FeedbackFormTemplate
     */
    protected $_model = null;

    /**
     * Initialize the controller
     */
    public function init()
    {
        parent::init();

        $this->_filters = array(
            'template' => array(
                '*' => array(
                    new Zend_Filter_Alnum(array('allowwhitespace' => true)),
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripTags(),
                    new Zend_Filter_StripNewlines(),
                ),
            ),
        );
        $this->_validators = array(
            'template' => array(
                'name' => array(
                    'presence' => 'required',
                    new Zend_Validate_NotEmpty(),
                    new Zend_Validate_StringLength(array('max' => 255)),
                ),
                'file' => array(
                    new Zend_Validate_NotEmpty(),
                ),
            ),
        );

        $this->_model = Centurion_Db::getSingleton('statflow/feedbackFormTemplate');
        $this->_feedbackFormModel = Centurion_Db::getSingleton('statflow/feedbackForm');

        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_flashMessenger->setNamespace('success')->hasMessages()){
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('error')->hasMessages()){
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()){
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }


    /**
     * List the templates
     */
    public function indexAction()
    {
        $this->view->templates = $this->_model->fetchAll();
    }

    /**
     * Display the creation form
     */
    public function addAction()
    {
        if ($this->getRequest()->isPost())
        {
            $validation = new Zend_Filter_Input($this->_filters['template'], $this->_validators['template'], $this->_getAllParams());
            if ($validation->isValid()) {

                $_form = $this->_feedbackFormModel->get(array('id' => $this->_getParam('feedback_form_id')));
                $_entry = $this->_model->createRow(array(
                    'name' => $validation->name,
                    'file' => $validation->file,
                ));
                try
                {
                    $_entry->save();
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'study', 'action' => 'index', 'study_id' => $_form->study_id, 'feedback_form_id' => $_form->id), null, true));
                } catch(Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_add_flashMessenger_error@backoffice') . '<br><br>' .$e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form-template', 'action' => 'add'), null, true));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_add_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($validation->getMessages());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form-template', 'action' => 'add'), null, true));
            }

        }

        $this->view->feedback_form = $this->_feedbackFormModel->get(array('id' => $this->_getParam('feedback_form_id')));

    }

    /**
     * Display the edition form
     */
    public function editAction()
    {
        if ($this->getRequest()->isPost())
        {
            $validation = new Zend_Filter_Input($this->_filters['template'], $this->_validators['template'], $this->_getAllParams());
            if ($validation->isValid()) {

                $_entry = $this->_model->get(array('id' => $this->_getParam('id')));
                $_entry->name = $validation->name;
                $_entry->file = $validation->file;
                try
                {
                    $_entry->save();
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_edit_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'study', 'action' => 'index'), null, true));
                } catch(Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_edit_flashMessenger_error@backoffice') . '<br><br>' .$e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form-template', 'action' => 'edit', 'id' => $this->_getParam('id')), null, true));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_edit_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($validation->getMessages());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form-template', 'action' => 'edit', 'id' => $this->_getParam('id')), null, true));
            }

        }

        $this->view->feedbackform = $this->_model->get(array('id' => $this->_getParam('id')));
    }

    /**
     * delete a feedback_form_template
     */
    public function deleteAction()
    {
        if (array_key_exists('id', $this->_getAllParams()))
        {
            $_entry = $this->_model->get(array('id' => $this->_getParam('id')));
            try {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_delete_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'study', 'action' => 'index'), null, true));
            } catch( Exception $e ) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_delete_flashMessenger_error@backoffice') . '<br><br>' .$e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form-template', 'action' => 'index'), null, true));
            }
        } else {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackformtemplate_delete_flashMessenger_error@backoffice'));
            $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form-template', 'action' => 'index'), null, true));
        }
    }
}

