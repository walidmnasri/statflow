<?php
require_once __DIR__ . '/../services/QuestionEntry.php';
require_once __DIR__ . '/../services/Question.php';

use Statflow\Service\QuestionEntry;
use Statflow\Service\Question;

class Statflow_QuestionEntryController extends Centurion_Controller_AGL
{

    /**
     *
     * @var QuestionEntry
     */
    private $questionEntryService;

    /**
     *
     * @var Question
     */
    private $questionService;

    /**
     *
     * @return \Statflow\Service\QuestionEntry
     */
    public function getQuestionEntryService()
    {
        if (! $this->questionEntryService) {
            $this->questionEntryService = new QuestionEntry();
        }
        
        return $this->questionEntryService;
    }

    /**
     *
     * @param \Statflow\Service\QuestionEntry $questionEntryService
     */
    public function setQuestionEntryService($questionEntryService)
    {
        $this->questionEntryService = $questionEntryService;
    }

    /**
     *
     * @return \Statflow\Service\Question
     */
    public function getQuestionService()
    {
        if (! $this->questionService) {
            $this->questionService = new Question();
        }
        
        return $this->questionService;
    }

    /**
     *
     * @param \Statflow\Service\Question $questionService
     */
    public function setQuestionService($questionService)
    {
        $this->questionService = $questionService;
    }

    /**
     * Initialize the controller
     */
    public function init()
    {
        $questionEntryService = $this->getQuestionEntryService();
        $this->_question_types = $questionEntryService->getArrayQuestionTypes();
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    public function indexAction()
    {}

    public function deleteAction()
    {
        if (array_key_exists('id', $this->_getAllParams())) {
            $questionEntryService = $this->getQuestionEntryService();
            $_entry = $questionEntryService->getQuestionEntryById($this->_getParam('id'));
            $question = $_entry->question;
            
            try {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('questionentry_delete_success@backoffice'));
                echo ($question->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'disable',
                    'id' => $question->feedback_form_page->feedback_form->id
                ), null, true)) : 'javascript:void(0);';
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('questionentry_delete_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());

            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('questionentry_delete_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'feedback-form-page',
            'action' => 'index',
            'feedback_form_id' => $question->feedback_form_page->feedback_form->id,
        ), null, true));
    }

    public function addAction()
    {
        $questionEntryService = $this->getQuestionEntryService();
        $questionService = $this->getQuestionService();
        
        if ($this->getRequest()->isPost()) {
            if ($questionEntryService->isValid($this->_getAllParams())) {
                $question = $questionService->getQuestionById($this->_getParam('question_id'));
                $_entry = $questionEntryService->getValidated($this->_getParam('question_id'), $this->_getParam('entry_type_id'));
                
                try {
                    $_entry->save();
                    $this->_flashMessenger->setNamespace('success')
                        ->addMessage($this->view->translate('questionentry_add_success@backoffice'));
                    echo ($question->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'feedback-form',
                        'action' => 'disable',
                        'id' => $question->feedback_form_page->feedback_form->id
                    ), null, true)) : 'javascript:void(0);';

                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')
                        ->addMessage($this->view->translate('questionentry_add_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                }
            } else {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('questionentry_add_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')
                    ->addMessage($questionEntryService->getValidator($this->_getAllParams())
                    ->getMessages());
            }

            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form-page',
                'action' => 'index',
                'feedback_form_id' => $question->feedback_form_page->feedback_form->id,
            ), null, true));
        }
    }

    public function editAction()
    {
        $questionEntryService = $this->getQuestionEntryService();
        $questionService = $this->getQuestionService();
        $imageForm = $questionEntryService->addImageForm();
        
        if ($this->getRequest()->isPost()) {
            $_entry = $questionEntryService->getQuestionEntryById($this->_getParam('id'));
            $question = $questionService->getQuestionById($_entry->question_id);
            
            //condition d'affichage des modalites
            if (array_key_exists('conditionMod', $this->_getAllParams()) || array_key_exists('colourCode', $this->_getAllParams())) {
                $_entry->complement_data = json_encode(array(
                    'conditionMod' => $this->_getParam('conditionMod') ? $this->_getParam('conditionMod') : '',
                    'colourCode' => $this->_getParam('colourCode') ? $this->_getParam('colourCode') : ''
                ));
            }
            
            // MCQ
            if (array_key_exists('goto_question_id', $this->_getAllParams())) {
                $_entry->complement_data = json_encode(array(
                    'goto_question_id' => $this->_getParam('goto_question_id')
                ));
            }
            
            // ORDERED
            if (array_key_exists('max_choices', $this->_getAllParams())) {
                $_entry->complement_data = json_encode(array(
                    'max_choices' => $this->_getParam('max_choices')
                ));
            }
            
            // ARRAY
            if (array_key_exists('hidden-modalities', $this->_getAllParams())) {
                $_entry->complement_data = json_encode(array(
                    'modalities' => $this->_getParam('hidden-modalities')
                ));
            }
            
            // INTERVAL
            if (array_key_exists('interval_min', $this->_getAllParams()) && array_key_exists('interval_max', $this->_getAllParams())) {
                $_entry->complement_data = json_encode(array(
                    'interval_min' => $this->_getParam('interval_min'),
                    'interval_max' => $this->_getParam('interval_max')
                ));
            }
            
            if ($imageForm->isValid($this->getRequest()->getPost()) && array_key_exists('conditionMod', $this->_getAllParams())) {

                        try {
                            $_entry->complement_data = json_encode(array(
                            'imagefile'     => $_FILES['image']['name'],
                            'conditionMod'  => $this->_getParam('conditionMod')
                             ));
                            $questionEntryService->uploadImage();
                            
                        } catch (Zend_File_Transfer_Exception $e) {
                            Zend_Debug::dump($e);
                        }

            }elseif(!is_null($this->_getParam('imageFileName')) &&  array_key_exists('conditionMod', $this->_getAllParams())){
                
                $_entry->complement_data = json_encode(array(
                    'imagefile'     => $this->_getParam('imageFileName'),
                    'conditionMod'  => $this->_getParam('conditionMod')
                ));
            }

            try {
                $_entry->save();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('questionentry_edit_success@backoffice'));
                echo ($question->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'disable',
                    'id' => $question->feedback_form_page->feedback_form->id
                ), null, true)) : 'javascript:void(0);';

            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('questionentry_edit_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }

            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form-page',
                'action' => 'index',
                'feedback_form_id' => $question->feedback_form_page->feedback_form->id,
            ), null, true));
        }
    }
}

