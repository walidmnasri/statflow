<?php
require_once __DIR__ . '/../services/Import.php';

use Statflow\Service\Import;
class Statflow_ImportController extends Centurion_Controller_Action
{
 /**
     *
     * @var feedbackFormPage
     */
    private $importService;

    /**
     *
     * @return \Statflow\Service\Import
     */
    public function getImportService()
    {
        if (! $this->importService) {
            $this->importService = new Import();
        }
        
        return $this->importService;
    }

    /**
     *
     * @param \Statflow\Service\Import $importService
     */
    public function setImportService($importService)
    {
        $this->importService = $importService;
    }
    /**
     * Initialize the Import controller
     */
    public function init()
    {
        set_time_limit(60 * 60); //1h time limit

        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages;
        }

        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $this->view->flashMessageType = "error";
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }   
    }
  
    /**
     * @void Action cron import/export
     */
    public function cronAction()
    {
        $importService = $this->getImportService();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $study_id = (int)$this->_getParam('study_id');
        $feedback_form_id = (int)$this->_getParam('feedback_form_id');
        $error = $this->view->translate('import__statflow__import_index_flashMessenger_error__check_and_validate@backoffice');
        $serverUrl = $this->view->serverUrl();
        if ($study_id && $feedback_form_id) {
            
            $importService->cronImportExport($study_id, $feedback_form_id,$error,$serverUrl);
        }
    }
    /**
     * @void Action import flux
     */
    public function fluxAction()
    {
        $importService = $this->getImportService();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $questionnaire_id = (int)$this->_getParam('questionnaire_id');        
        $nom_flux = $this->_getParam('nom_flux');
        $error = $this->view->translate('import__statflow__import_index_flashMessenger_error__check_and_validate@backoffice');
        $serverUrl = $this->view->serverUrl();
        if ($questionnaire_id && $nom_flux) {
            $importService->importFlux($questionnaire_id, $nom_flux);
        }
    }

}