<?php
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/KpiManagement.php';
require_once __DIR__ . '/../services/FeedbackForm.php';

use Statflow\Service\Study;
use Statflow\Service\KpiManagement;
use Statflow\Service\FeedbackForm;

class Statflow_KpiManagementController extends Centurion_Controller_Action
{
    /**
     *
     * @var feedbackForm
     */
    private $feedbackFormService;
    
    /**
     *
     * @var kpiManagement
     */
    private $kpi_management;

    /**
     *
     * @var Study
     */
    private $studyService;
    
    /**
     *
     * @return \Statflow\Service\KpiManagement
     */
    public function getKpiManagement()
    {
        if (! $this->kpi_management) {
            $this->kpi_management = new KpiManagement();
        }
    
        return $this->kpi_management;
    }
    
    /**
     *
     * @param \Statflow\Service\KpiManagement $kpiManagement
     */
    public function setKpiManagement($kpiManagement)
    {
        $this->kpi_management = $kpiManagement;
    }
    
    /**
     *
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (! $this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
    
        return $this->feedbackFormService;
    }

    /**
     *
     * @return \Statflow\Service\Study
     */
    public function getStudyService()
    {
        if (!$this->studyService) {
            $this->studyService = new Study();
        }

        return $this->studyService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }
    
    /**
     * Initialize the controller
     */
    public function init()
    {
        $this->view->pageLayout = 'kpi_management';
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
    
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
    
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }
    
    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }
    
    public function indexAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $kpi_management = $this->getKpiManagement();
        $this->view->formId = $formId = $this->_getParam('form', null);
        if (!$formId) {
            $studyService = $this->getStudyService();
            $this->view->studies = $studyService->getAll();
            return;
        }

        $form = $feedbackFormService->getById($this->_getParam('form'));
        $validation = $kpi_management->getValidator($this->_getAllParams());
        $kpi_management->writeFileConfig($this->_getParam('form'));
        $meta=Zend_Json::encode(array('bornes'=>array(),'style'=>array()));
        $borne=null;
        $style = null;
        if ($this->getRequest()->isPost()) {
            if ($kpi_management->isValid($this->_getAllParams())) {
                $is_open = $this->_getParam('is_open', false) != false ? 1 : 0;
                if ($_POST['type_kpi']=='1' || $_POST['type_kpi']=='2') {
                    $borne = array(
                            'bornePDTSMin'  => $_POST['bornePDTSMin'],
                            'bornePDTSMax'  => $_POST['bornePDTSMax'],
                            'bornePSMin'    => $_POST['bornePSMin'],
                            'bornePSMax'    => $_POST['bornePSMax'],
                            'borneSMin'     => $_POST['borneSMin'],
                            'borneSMax'     => $_POST['borneSMax'],
                            'borneTSMin'    => $_POST['borneTSMin'],
                            'borneTSMax'    => $_POST['borneTSMax'],
                        );
                    $style= array(
                                'colorTS'   =>'00BF57',
                                'colorS'    =>'9FE357',
                                'colorPS'   =>'FFD000',
                                'colorPSDT' =>'FF0000',
                                'color'     =>'satisfaction'
                                );
                } elseif ($_POST['type_kpi']=='5'){
                    $borne = array(
                            'borneVeryGood'     => $_POST['borneVeryGood'],
                            'borneGood'         => $_POST['borneGood'],
                            'bornePoor'         => $_POST['bornePoor'],
                            'borneVeryPoor'     => $_POST['borneVeryPoor'],
                        );
                    $style= array(
                        'colorTS'   =>'#00BF57',
                        'colorS'    =>'#9FE357',
                        'colorPS'   =>'#FFD000',
                        'colorPSDT' =>'#FF0000',
                        'color'     =>'satisfaction'
                    );
                } elseif ($_POST['type_kpi'] == '3' ||$_POST['type_kpi'] == '4'){
                    $style= array(
                        'colorPROM'   =>'#00BF57',
                        'colorPASS'   =>'#FFD000',
                        'colorDETR'   =>'#FF0000',
                        'color'       =>'nps'
                    );
                }
                
                $meta=Zend_Json::encode(array('bornes'=>$borne,'style'=>$style));
                try {
                    $entry          = $kpi_management->getValidated($is_open,$meta,$this->_getParam('id'));
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('kpi_management_add_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'kpi-management',
                    'action' => 'index',
                    'form' => $form->id
                ), null, true));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('kpi_management_add_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                }
            }else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('kpi_management_add_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($validation->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'kpi-management',
                    'action' => 'add',
                    'form' => $validation->form
                ), null, true));
            }
            $kpi_management->writeFileConfig($this->_getParam('form'));
        }

        if($this->_getParam('color') == true) {
            $this->view->color = '#00BF57';
        }

        $this->view->classKpi   = $kpi_management;
        $this->view->form       = $form;
        $this->view->ignore     = $kpi_management->getAllQuestionParent($this->_getParam('form'));
        $this->view->kpis       = $kpi_management->getAllKpiByFeedbackForm($this->_getParam('form'));
        $this->view->study      = $form->study;
    }
    
    /**
     * Publish the KPI
     */
    public function publishAction()
    {
        $kpi_management = $this->getKpiManagement();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $kpi_management->getById($this->_getParam('id'));
            $_entry->is_open = 1;
            $form = $_entry->feedback_form_id;
            try {
                $_entry->save();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('kpi_publish_success@backoffice'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('kpi_publish_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('kpi_publish_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'kpi-management',
            'action' => 'index',
            'form' => $form
        ), null, true));
    }
    
    /**
     * Unpublish the feedback form
     */
    public function unpublishAction()
    {
        $kpi_management = $this->getKpiManagement();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $kpi_management->getById($this->_getParam('id'));
            $_entry->is_open = 0;

            $form = $_entry->feedback_form_id;
            try {
                $_entry->save();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('kpi_publish_success@backoffice'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('kpi_publish_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('kpi_publish_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'kpi-management',
            'action' => 'index',
            'form' => $form
        ), null, true));
    }

    public function deleteAction(){
         $kpi_management = $this->getKpiManagement();   
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $kpi_management->getById($this->_getParam('id'));
            $form = $_entry->feedback_form_id;
            try {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('kpi_management_delete_success@backoffice'));
                $kpi_management->writeFileConfig($this->_getParam('form'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('kpi_management_delete_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('kpi_management_delete_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'kpi-management',
            'action' => 'index',
            'form' => $form
        ), null, true));
    }
    
    public function changecolorAction() {
        $kpi_management = $this->getKpiManagement();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $kpi_management->getById($this->_getParam('id'));
            $form = $_entry->feedback_form_id;
            $meta =Zend_Json::decode($_entry->meta);
            $meta['style'] = $_POST;
            $_entry->meta = Zend_Json::encode($meta);
            
            try {
                $_entry->save();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('kpi_management_changeColor_success@backoffice'));
                $kpi_management->writeFileConfig($this->_getParam('form'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('kpi_management_changeColor_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('kpi_management_changeColor_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'kpi-management',
            'action' => 'index',
            'form' => $form
        ), null, true));
    }

    public  function editAction(){
        $kpi_management = $this->getKpiManagement();
        print_r(Zend_Json::encode($kpi_management->getById($this->_getParam('id'))->toArray()));die;
    }
    
    public function changeAction(){
        $kpiManagementService = $this->getKpiManagement();
        $form = $this->_getParam('form');
        $type = $this->_getParam('type');
        $result = array();
        if(($type == '1') || ($type == '3') || ($type == '4')){
            $data = $kpiManagementService->getQuestionByTypeAndFeedbackForm($form,5);
            foreach ($data as $val){
               if(count($kpiManagementService->getSubquestion($val['parent_id'],$form)) === 1){
                   $result[$val['parent_id']] = $kpiManagementService->getSubquestion($val['parent_id'],$form);     
               }
            } 
            print_r(Zend_Json::encode($result));
            exit;
        } elseif($type == '2') {
            $result = array();
             $data = $kpiManagementService->getQuestionByTypeAndFeedbackForm($form,5);
             
            foreach ($data as $val){
               if(count($kpiManagementService->getSubquestion($val['parent_id'], $form)) > 1){
                   $result[$val['parent_id']] = $kpiManagementService->getSubquestion($val['parent_id'], $form);
               }
            }

            print_r(Zend_Json::encode($result));
            exit;
        } elseif ($type == '5') {
            $data1 = $kpiManagementService->getQuestionByTypeAndFeedbackForm($form,1);
            $data2 = $kpiManagementService->getQuestionByTypeAndFeedbackForm($form,5);
            $data = array_merge($data1, $data2);
            print_r(Zend_Json::encode($data));exit;
        } else {
            print_r(Zend_Json::encode(null));exit;
        }
    }
}
