<?php
require_once __DIR__ . '/../services/Preview.php';

use Statflow\Service\Preview;

class Statflow_PreviewController extends Centurion_Controller_AGL
{

    /**
     *
     * @var feedbackFormPage
     */
    private $previewService;

    /**
     *
     * @return \Statflow\Service\Preview
     */
    public function getPreviewService()
    {
        if (! $this->previewService) {
            $this->previewService = new Preview();
        }
        
        return $this->previewService;
    }

    /**
     *
     * @param \Statflow\Service\Preview $previewService
     */
    public function setPreviewService($previewService)
    {
        $this->previewService = $previewService;
    }
    
    public function preDispatch()
    {
        parent::preDispatch();
        $this->_helper->layout()->setLayout('form_preview');
    }

    public function indexAction()
    {
        $previewService = $this->getPreviewService();
        $this->view->availableLanguages = $previewService->getAllLanguages();
        
        $formId = intval($this->_getParam('form', null));
        $data = $previewService->gestionErreur($formId, $this->_getParam('rid'));
        $form = $previewService->getFormById($formId);
        if ($this->_getParam('language', false) == false) {
            $lang = "en";
        } else {
            $lang = $this->_getParam('language');
        }
        $this->view->language = Centurion_Db::getSingleton('translation/language')->fetchRow('`language` = "' . $lang . '"');
        
        if (!empty($data['error'])) {
            $this->view->errors = $data['error'];
            $this->render('error');
        } else {
            $pages = $previewService->getAllPageByForm($form);

            $data['form']['introduction_text'] = $this->view->translate('form__' . $form->id . '__introduction-text@form_' . $form->id);
            $data['form']['end_text'] = $this->view->translate('form__' . ($form->id) . '__end-text@Form_' . $form->id);
            $data['pages'] = array();
            foreach ($pages as $page) {
                if ($page->meta) {
                    $page->meta = Zend_Json::decode($page->meta);
                }

                $pageData = $page->toArray();
                $pageData['name'] = $this->view->translate('form__page_name-' . $page->id . '@form_' . $form->id);
                $pageData['title_is_visible'] = $pageData['title_is_visible'] == "1";
                
                $pageData['children'] = array();
                $questions = $page->findDependentRowset('Statflow_Model_DbTable_Question', null, $previewService->getFormWhereParentIdNull());
                foreach ($questions as $question) {
                    if ($question->meta) {
                        $question->meta = Zend_Json::decode($question->meta);
                    }

                    $pageData['children'][$question->order] = $question->toArray();
                    $pageData['children'][$question->order]['content'] = $this->view->translate('form__' . $form->id . '__question_title-' . $question->code . '@Form_' . $form->id);
                    $pageData['children'][$question->order]['entries'] = array();
                    $pageData['children'][$question->order]['helps'] = array();
                    
                    $help = array();
                    $helpText = $this->view->translate('form__' . $form->id . '__question_help-' . $question->code . '@Form_' . $form->id);
                    $help['content'] = ($helpText == 'form__' . $form->id . '__question_help-' . $question->code) ? '' : $helpText;
                    $pageData['children'][$question->order]['helps'][] = $help;
                    
                    if ($question->question_type_id == Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY) {
                        $pageData['children'][$question->order]['subquestions'] = array();
                        $subquestions = $question->findDependentRowset('Statflow_Model_DbTable_Question');
                        foreach ($subquestions as $sq) {
                            
                            if ($sq->meta)
                                $sq->meta = Zend_Json::decode($sq->meta);
                            $entryData = $sq->toArray();
                            $entryData['content'] = $sq->code ? $this->view->translate('form__' . $form->id . '__question_title-' . $sq->code . '@Form_' . $form->id) : $this->view->translate('form__question_title-missing-code@Backoffice');
                            $pageData['children'][$question->order]['subquestions'][$sq->id] = $entryData;
                        }
                    }
                    
                    $entries = $question->findDependentRowset('Statflow_Model_DbTable_QuestionEntry');
                    foreach ($entries as $entry) {
                        if ($entry->complement_data)
                            $entry->complement_data = Zend_Json::decode($entry->complement_data);
                        
               
                        $entryData = $entry->toArray();
                        $entryData['content'] = $entry instanceof Statflow_Model_DbTable_Row_Question ? $this->view->translate('form__' . $form->id . '__question_title-' . $entry->code . '@Form_' . $form->id) : $this->view->translate('form__question_' . $question->code . '_modality-' . $entry->id . '@Form_' . $form->id);
                        $pageData['children'][$question->order]['entries'][$entry->id] = $entryData;
                    }
                }
                $data['pages'][] = $pageData;

                $this->view->jsonData = Zend_Json::encode($data);
                //var_dump($data);die;
                if ($form->meta) {
                    $meta = Zend_Json::decode($form->meta);
                    $this->view->formStyle = $meta['style'];
                }
                $this->view->form = $form;
            }
        }
    }

    /**
     * Manage the deletion
     */
    /*public function deleteAction()
    {
        if ($this->getRequest()->isGet()) {
            $_entry = $this->_model->get(array(
                'id' => $this->_getParam('id')
            ));
            $study_id = $_entry->feedback_form_page->feedback_form->study_id;
            $feedback_form_id = $_entry->feedback_form_page->feedback_form->id;
            $feedback_form_page_id = $_entry->feedback_form_page->id;
            $question_id = $_entry->id;
            
            try {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__question_delete_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'study',
                    'action' => 'index',
                    'study_id' => $study_id,
                    'feedback_form_id' => $feedback_form_id,
                    'feedback_form_page_id' => $feedback_form_page_id,
                    'question_id' => $question_id
                )));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('navigation__statflow__question_delete_flashMessenger_error@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'study',
                    'action' => 'index'
                )));
            }
        }
    }*/
}

