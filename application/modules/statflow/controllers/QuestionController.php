<?php
require_once __DIR__ . '/../services/Question.php';
require_once __DIR__ . '/../services/QuestionEntry.php';
require_once __DIR__ . '/../services/Translation.php';

use Statflow\Service\Question;
use Statflow\Service\QuestionEntry;
use Statflow\Service\Translation;

class Statflow_QuestionController extends Centurion_Controller_AGL
{

    /**
     *
     * @var Question
     */
    private $questionService;
    
    /**
     *
     * @var QuestionEntry
     */
    private $questionEntryService;
    
    /**
     *
     * @var Translation
     */
    private $translationService;
   
    
    /**
     *
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (! $this->translationService) {
            $this->translationService = new Translation();
        }
    
        return $this->translationService;
    }
    
    /**
     *
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }

    /**
     *
     * @return \Statflow\Service\Question
     */
    public function getQuestionService()
    {
        if (! $this->questionService) {
            $this->questionService = new Question();
        }
        
        return $this->questionService;
    }

    /**
     *
     * @param \Statflow\Service\Question $questionService
     */
    public function setQuestionService($questionService)
    {
        $this->questionService = $questionService;
    }
    
    
    /**
     *
     * @return \Statflow\Service\QuestionEntry
     */
    public function getQuestionEntryService()
    {
        if (! $this->questionEntryService) {
            $this->questionEntryService = new QuestionEntry();
        }
    
        return $this->questionEntryService;
    }
    
    /**
     *
     * @param \Statflow\Service\QuestionEntry $questionEntryService
     */
    public function setQuestionEntryService($questionEntryService)
    {
        $this->questionEntryService = $questionEntryService;
    }

    /**
     * Initialize the controller
     */
    public function init()
    {
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    public function indexAction()
    {
        $questionService = $this->getQuestionService();
        $this->view->pages = $questionService->getAllFeedbackFormPage();
        $this->view->questions = $questionService->getAllQuestion();
   
    }

    public function addAction()
    {
        $questionService = $this->getQuestionService();
      
        if ($this->getRequest()->isPost()) {
            $post = $this->_request->getPost();
            
            if (isset($post['parent_id'])) {

                $sousquestion = $questionService->getSousQuestion($post['question_type_id'], $post['feedback_form_page_id'], $post['feedback_form_id'], $post['parent_id']);
                $parentOrder= $questionService->getSousQuestionParentOrder($post['parent_id']);
                $sousquestion->order = $parentOrder;
                $sousquestion->save();
                echo ($sousquestion->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'disable',
                    'id' => $sousquestion->feedback_form_page->feedback_form->id
                ), null, true)) : 'javascript:void(0);';

                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form-page',
                    'action' => 'index',
                    'feedback_form_id' => $sousquestion->feedback_form_id,
                ), null, true));
            } else {
                if ($questionService->isValid($this->_getAllParams())) {
                    
                    $feedback_form_id = $questionService->getFeedbackFormId();
                    $meta = Zend_Json::encode(array(
                        'max' => 0,
                        'required' => isset($_POST['required']) && $_POST['required'] == '1'
                    ));
                    
                
                    $question = $questionService->getValidated($this->_getParam('id'), $feedback_form_id, $meta);
   
                    try {
                        $question->save();
                        $help = $this->view->translate('form__' . $question->feedback_form_id . '__question_help-' . $question->code . '@form_' . $question->feedback_form_id);
                        $this->_flashMessenger->setNamespace('success')
                            ->addMessage($this->view->translate('question_add_success@backoffice'));
                        echo ($question->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                            'module' => 'statflow',
                            'controller' => 'feedback-form',
                            'action' => 'disable',
                            'id' => $question->feedback_form_page->feedback_form->id
                        ), null, true)) : 'javascript:void(0);';

                    } catch (Exception $e) {
                        $this->_flashMessenger->setNamespace('error')
                            ->addMessage($this->view->translate('question_add_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    }

                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'feedback-form-page',
                        'action' => 'index',
                        'feedback_form_id' =>$question->feedback_form_page->feedback_form->id,
                    ), null, true));
                } else {
                    $this->_flashMessenger->setNamespace('error')
                        ->addMessage($this->view->translate('question_add_error@backoffice'));
                    $this->_flashMessenger->setNamespace('validation')
                        ->addMessage($questionService->getValidator($this->_getAllParams())
                        ->getMessages());
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'question',
                        'action' => 'add'
                    )));
                }
            }
        }
        
        $this->view->pages = $questionService->getAllFeedbackFormPage();
        $this->view->page = $questionService->getFeedbackFormPageById($this->_getParam('feedback_form_page_id'));
        $this->view->question_types = $questionService->getArrayQuestionTypes();
    }

    public function duplicateAction()
    {
        $questionService = $this->getQuestionService();
        $questionEntryService = $this->getQuestionEntryService();
        $translationService = $this->getTranslationService();

        $question = $questionService->getQuestionById($this->_getParam('id'));
        $orderNextQuestion = $questionService->getNextOrderQuestionByPage($question->feedback_form_page_id);
        $questionRow = $questionService->duplicateQuestion($this->_getParam('id'),$orderNextQuestion, null, null);

        //$translationService->duplicateTraductionQuestion($this->_getParam('id'),$questionRow->code,null);
        
        $idQuestionCloned = $questionRow->id;
        $codeQuestionCloned = $questionRow->code;
        $codeQuestionOrig = $question->code;
    

        if($questionRow->question_type_id == Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY){
            $sousQuestion = $questionService->duplicateSousQuestions($this->_getParam('id'), $idQuestionCloned, $codeQuestionOrig, $codeQuestionCloned, $orderNextQuestion, null, null);
        }
        
        $modalities = $questionEntryService->duplicateModalities($this->_getParam('id'), $idQuestionCloned);
        
        $translationService->generateTranslationFiles();
        
        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'feedback-form-page',
            'action' => 'index',
            'feedback_form_id' => $question->feedback_form_id,
        ), null, true));
    }
    
    public function editAction()
    {
        $questionService = $this->getQuestionService();
        
        if ($this->getRequest()->isPost()) {
            $post = $this->_request->getPost();
            
            $question = $questionService->getQuestionById($this->_getParam('id'));
            if (isset($post['parent_id'])) {
                $code = $post['code'];
                $meta = Zend_Json::encode(array(
                    'fixed' => isset($post['fixed']) && $post['fixed'],
                    'conditionsq' => isset($_POST['conditionsq']) ? $_POST['conditionsq'] : ''
                ));

                $question = $questionService->getEditSousQuestion($this->_getParam('id'), $code, $meta);
                $question->save();
                echo ($question->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'disable',
                    'id' => $question->feedback_form_page->feedback_form->id
                ), null, true)) : 'javascript:void(0);';

            } else {
                
                if ($questionService->isValid($this->_getAllParams())) {
                    
                    $feedback_form_id = $questionService->getFeedbackFormId();
                    $meta = Zend_Json::encode(array_merge(Zend_Json::decode($question->meta), array(
                        'required' => isset($_POST['required']) && $_POST['required'] == 1,
                        'condition' => isset($_POST['conditional']) ? $_POST['condition'] : ''
                    )));
                    
                    $question = $questionService->getValidated($this->_getParam('id'), $feedback_form_id, $meta);
                    
                    try {
                        $question->save();
                        
                        $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('question_add_success@backoffice'));
                        echo ($question->feedback_form_page->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                            'module' => 'statflow',
                            'controller' => 'feedback-form',
                            'action' => 'disable',
                            'id' => $question->feedback_form_page->feedback_form->id
                        ), null, true)) : 'javascript:void(0);';

                    } catch (Exception $e) {
                        $this->_flashMessenger->setNamespace('error')
                            ->addMessage($this->view->translate('question_add_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    }
                } else {
                    $this->_flashMessenger->setNamespace('error')
                        ->addMessage($this->view->translate('question_add_error@backoffice'));
                    $this->_flashMessenger->setNamespace('validation')->addMessage($questionService->getValidator($this->_getAllParams())
                        ->getMessages());
                }
            }

            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form-page',
                'action' => 'index',
                'feedback_form_id' => $question->feedback_form_page->feedback_form->id,
            )));
        }
        
        $this->view->pages = $questionService->getAllFeedbackFormPage();
        $this->view->question_types = $questionService->getArrayQuestionTypes();
        $this->view->question = $questionService->getQuestionById($this->_getParam('id'));
    }

    public function editIntervalAction()
    {
      
        $questionService = $this->getQuestionService();
        $imageForm = $questionService->addImageForm();
        
        if ($this->getRequest()->isPost()) {
            
            $id = $this->_request->getPost('question_id');
           // $id = $this->_getParam('question_id');
            $question = $questionService->getQuestionById($id);
            $post = $this->_request->getPost();
            $meta = $question->meta ? Zend_Json::decode($question->meta) : array();
           
            $meta = $questionService->validateMeta($meta, $post);
            
            if ($imageForm->isValid($this->getRequest()
                ->getPost())) {
                try {
                    $uploadPath = realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'questions');
                    
                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $upload->setDestination($uploadPath);
                    $upload->addFilter('Rename', array(
                        'target' => $uploadPath . '/' . $upload->getFileName(null, false),
                        'overwrite' => true
                    ));
                    $upload->receive();
                    $meta['image'] = $upload->getFileName(null, false);
                } catch (Zend_File_Transfer_Exception $e) {
                    Zend_Debug::dump($e);
                }
            }
            
            $meta = Zend_Json::encode($meta);
            $question->meta = $meta;
            $question->save();
        }
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Manage the deletion
     */
    public function deleteAction()
    {
        $questionService = $this->getQuestionService();
        
        if ($this->getRequest()->isGet()) {
            $question = $questionService->getQuestionById($this->_getParam('id'));
            $feedback_form_id = $question->feedback_form_page->feedback_form->id;

            try {
                $question->delete();
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('question_delete_success@backoffice'));
                echo ($question->feedback_form->is_visible == 1) ? $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'disable',
                    'id' => $question->feedback_form_id
                ), null, true)) : 'javascript:void(0);';

            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('question_delete_error@backoffice'));
            }

            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form-page',
                'action' => 'index',
                'feedback_form_id' => $feedback_form_id
            )));
        }
    }

    public function reorderAction()
    {
        $questionService = $this->getQuestionService();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->_getParam('sort', FALSE) != FALSE) {
            $ids = explode(',', $this->_getParam('sort'));
            $pos = 0;
            foreach ($ids as $id) :
                $_entry = $questionService->getQuestionById($id);
                $_entry->order = $pos;
                $_entry->save();
                $sousQuestionEntries = $questionService->getAllSousQuestionByQuestion($id);
                
                if ($sousQuestionEntries) {
                    foreach ($sousQuestionEntries as $sousQuestionEntry) {
                        $sousQuestionEntry->order = $pos;
                        $sousQuestionEntry->save();
                    }
                }
                
                $pos ++;
            endforeach
            ;
            echo json_encode(array(
                'success' => true
            ));
        } else {
            echo json_encode(array(
                'success' => false
            ));
        }
    }
}

