<?php
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/FeedbackForm.php';
require_once __DIR__ . '/../services/FiltersKpi.php';
require_once __DIR__ . '/../services/UserFeedbackForm.php';
require_once __DIR__ . '/../services/XmlConfigSolr.php';
require_once __DIR__ . '/../services/FeedbackFormPage.php';
require_once __DIR__ . '/../services/Question.php';
require_once __DIR__ . '/../services/QuestionEntry.php';
require_once __DIR__ . '/../services/Translation.php';
require_once __DIR__ . '/../services/RespondentFieldHidden.php';
require_once __DIR__ . '/../services/NominativeFieldHidden.php';


use Statflow\Service\Study;
use Statflow\Service\FeedbackForm;
use Statflow\Service\UserFeedbackForm;
use Statflow\Service\XmlConfigSolr;
use Statflow\Service\FiltersKpi;
use Statflow\Service\FeedbackFormPage;
use Statflow\Service\Question;
use Statflow\Service\QuestionEntry;
use Statflow\Service\Translation;
use Statflow\Service\RespondentFieldHidden;
use Statflow\Service\NominativeFieldHidden;

class Statflow_FeedbackFormController extends Centurion_Controller_Action
{

    /**
     *
     * @var feedbackForm
     */
    private $feedbackFormService;

    /**
     *
     * @var Study
     */
    private $studyService;

    /**
     *
     * @var xmlConfigSolr
     */
    private $xmlConfigSolrService;

    /**
     *
     * @var UserFeedbackForm
     */
    private $userFeedbackFormService;

    /**
     *
     * @var FiltersKpi
     */
    private $filtersKpiService;

    /**
     *
     * @var feedbackFormPage
     */
    private $formPageService;
    /**
     *
     * @var Question
     */
    private $questionService;

    /**
     *
     * @var QuestionEntry
     */
    private $questionEntryService;


    private $translationService;

    /**
     *
     * @var RespondentFieldHidden
     */
    private $respondentFieldHiddenService;

    /**
     *
     * @var NominativeFieldHidden
     */
    private $nominativeFieldHiddenService;


    /**
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (!$this->translationService) {
            $this->translationService = new Translation();
        }
        return $this->translationService;
    }

    /**
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }

    /**
     *
     * @return \Statflow\Service\Question
     */
    public function getQuestionService()
    {
        if (!$this->questionService) {
            $this->questionService = new Question();
        }

        return $this->questionService;
    }

    /**
     *
     * @param \Statflow\Service\Question $questionService
     */
    public function setQuestionService($questionService)
    {
        $this->questionService = $questionService;
    }

    /**
     0*
     * @return \Statflow\Service\QuestionEntry
     */
    public function getQuestionEntryService()
    {
        if (!$this->questionEntryService) {
            $this->questionEntryService = new QuestionEntry();
        }

        return $this->questionEntryService;
    }

    /**
     *
     * @param \Statflow\Service\QuestionEntry $questionEntryService
     */
    public function setQuestionEntryService($questionEntryService)
    {
        $this->questionEntryService = $questionEntryService;
    }

    /**
     *
     * @return \Statflow\Service\feedbackFormPage
     */
    public function getFormPageService()
    {
        if (!$this->formPageService) {
            $this->formPageService = new FeedbackFormPage();
        }

        return $this->formPageService;
    }

    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setFormPageService($formPageService)
    {
        $this->formPageService = $formPageService;
    }

    /**
     *
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (!$this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }

        return $this->feedbackFormService;
    }

    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }

    /**
     *
     * @return \Statflow\Service\XmlConfigSolr
     */
    public function getXmlConfigSolrService()
    {
        if (!$this->xmlConfigSolrService) {
            $this->xmlConfigSolrService = new XmlConfigSolr();
        }

        return $this->xmlConfigSolrService;
    }

    /**
     *
     * @param \Statflow\Service\XmlConfigSolr $xmlConfigSolrService
     */
    public function setXmlConfigSolrService($xmlConfigSolrService)
    {
        $this->xmlConfigSolrService = $xmlConfigSolrService;
    }

    /**
     *
     * @param \Statflow\Service\Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    }

    /**
     *
     * @return \Statflow\Service\Study
     */
    public function getStudyService()
    {
        if (!$this->studyService) {
            $this->studyService = new Study();
        }

        return $this->studyService;
    }

    /**
     *
     * @param \Statflow\Service\UserFeedbackForm $userFeedbackFormService
     */
    public function setUserFeedbackFormService($userFeedbackFormService)
    {
        $this->userFeedbackFormService = $userFeedbackFormService;
    }

    /**
     *
     * @return \Statflow\Service\UserFeedbackForm
     */
    public function getUserFeedbackFormService()
    {
        if (!$this->userFeedbackFormService) {
            $this->userFeedbackFormService = new UserFeedbackForm();
        }

        return $this->userFeedbackFormService;
    }

    /**
     *
     * @param \Statflow\Service\FiltersKpi $filtersKpiService
     */
    public function setFiltersKpiService($filtersKpiService)
    {
        $this->filtersKpiService = $filtersKpiService;
    }

    /**
     *
     * @return \Statflow\Service\FiltersKpi
     */
    public function getFiltersKpiService()
    {
        if (!$this->filtersKpiService) {
            $this->filtersKpiService = new FiltersKpi();
        }
        return $this->filtersKpiService;
    }

    /**
     *
     * @param \Statflow\Service\RespondentFieldHidden $respondentFieldHiddenService
     */
    public function setRespondentFieldHiddenService($respondentFieldHiddenService)
    {
        $this->filtersKpiService = $respondentFieldHiddenService;
    }

    /**
     *
     * @return \Statflow\Service\RespondentFieldHidden
     */
    public function getRespondentFieldHiddenService()
    {
        if (!$this->respondentFieldHiddenService) {
            $this->respondentFieldHiddenService = new RespondentFieldHidden();
        }

        return $this->respondentFieldHiddenService;
    }


    /**
     *
     * @param \Statflow\Service\NominativeFieldHidden $nominativeFieldHiddenService
     */
    public function setNominativeFieldHiddenService($nominativeFieldHiddenService)
    {
        $this->filtersKpiService = $nominativeFieldHiddenService;
    }

    /**
     *
     * @return \Statflow\Service\NominativeFieldHidden
     */
    public function getNominativeFieldHiddenService()
    {
        if (!$this->nominativeFieldHiddenService) {
            $this->nominativeFieldHiddenService = new NominativeFieldHidden();
        }

        return $this->nominativeFieldHiddenService;
    }


    /**
     * Initialize the controller
     */
    public function init()
    {
        $this->view->pageLayout = 'feedback_form';
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();;
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * List the studies
     */
    public function indexAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $studyService = $this->getStudyService();
        $this->view->studies = $studyService->getAll();
        $this->view->study = null;
        if (array_key_exists('study_id', $this->_getAllParams())) {
            $this->view->feedbackforms = $feedbackFormService->getAllRoots($this->_getParam('study_id'));
            $this->view->study = $this->_getParam('study_id', null);
        } else {
            $this->view->feedbackforms = $feedbackFormService->getAll();
        }
    }

    /**
     * enabled Customer Interface
     */
    public function enableAction()
    {

        $xmlConfigSolrService = $this->getXmlConfigSolrService();
        $feedbackFormService = $this->getFeedbackFormService();

        if (!array_key_exists('id', $this->_getAllParams())) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('solr_active_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }

        $solrConfig = Statflow_Model_Solr::getDefaultConfig();
        $solrConfig = $solrConfig['endpoint']['localhost'];
        $entry = $feedbackFormService->getById($this->_getParam('id'));

        $xmlConfigSolrService->setCore($entry->id);
        $xmlConfigSolrService->WriteXMlFileSchema($entry->id);
        $xmlConfigSolrService->WriteXMlFileDataConf($entry->id);
        $entry->is_visible = 1;
        exec("find " . APPLICATION_PATH . "/../bin/solr/example/solr/" . $entry->id . " -type d -exec chmod -R 777 {} +");
        try {
            file_get_contents('http://' . $solrConfig['host'] . ':' . $solrConfig['port'] . $solrConfig['path'] . 'admin/cores?action=CREATE&name=' . $entry->id);
            file_get_contents('http://' . $solrConfig['host'] . ':' . $solrConfig['port'] . $solrConfig['path'] . $entry->id . '/dataimport?command=full-import');
            $entry->save();
            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('solr_active_success@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index',
                'study_id' => $entry->study_id
            ), null, true));
        } catch (Exception $e) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('solr_active_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }
    }

    /**
     * dinabled Customer Interface
     */
    public function disableAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        if (!array_key_exists('id', $this->_getAllParams())) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('feedbackform_disable_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }

        $solrConfig = Statflow_Model_Solr::getDefaultConfig();
        $solrConfig = $solrConfig['endpoint']['localhost'];
        $entry = $feedbackFormService->getById($this->_getParam('id'));
        $entry->is_visible = 0;
        try {
            file_get_contents('http://' . $solrConfig['host'] . ':' . $solrConfig['port'] . $solrConfig['path'] . 'admin/cores?action=UNLOAD&core=' . $entry->id . '&deleteIndex=true');
            $entry->save();
            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('feedbackform_disable_success@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index',
                'study_id' => $entry->study_id
            ), null, true));
        } catch (Exception $e) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('feedbackform_disable_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }

    }

    /**
     * Publish the feedback form
     */
    public function publishAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        if (!array_key_exists('id', $this->_getAllParams())) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('feedbackform_publish_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }

        $_entry = $feedbackFormService->getById($this->_getParam('id'));
        $_entry->is_published = 1;
        try {
            $_entry->save();
            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('feedbackform_publish_success@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index',
                'study_id' => $_entry->study_id
            ), null, true));
        } catch (Exception $e) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('feedbackform_publish_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }
    }

    /**
     * Unpublish the feedback form
     */
    public function unpublishAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        if (!array_key_exists('id', $this->_getAllParams())) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('feedbackform_unpublish_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }

        $_entry = $feedbackFormService->getById($this->_getParam('id'));
        $_entry->is_published = 0;
        try {
            $_entry->save();
            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('feedbackform_unpublish_success@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index',
                'study_id' => $_entry->study_id
            ), null, true));
        } catch (Exception $e) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('feedbackform_unpublish_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }
    }

    /**
     * Display the creation form
     */
    public function addAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $studyService = $this->getStudyService();
        $studyId = $this->_getParam('study_id', null);
        $this->view->studyId = $studyId;
        if (!$studyId) {
            $this->view->studies = $studyService->getAll();
            return;
        }

        $imageForm = $feedbackFormService->addImageForm();

        if ($this->getRequest()->isPost()) {
            if ($feedbackFormService->isValid($this->_getAllParams(), 'feedback')) {
                if ($this->_getParam('publication_start', false) != false) {
                    $publicationStart = $feedbackFormService->formatDate($this->_getParam('publication_start'));
                } else {
                    $publicationStart = null;
                }

                if ($this->_getParam('publication_end', false) != FALSE) {
                    $publicationEnd = $feedbackFormService->formatDate($this->_getParam('publication_end'));
                } else {
                    $publicationEnd = null;
                }

                $isPublished = ($this->_getParam('is_published', false) != false) ? 1 : 0;
                $isOpen = ($this->_getParam('is_open', false) != false) ? 1 : 0;
                $introductionIsVisible = ($this->_getParam('introduction_is_visible', false) != false) ? 1 : 0;

                //$meta = Zend_Json::encode(isset($_POST['style']) ? array('style' => $_POST['style'],'respondentInfosOrder'=>array(),'externalFieldsOrder'=>array()) : array('style' => '','respondentInfosOrder'=>array(),'externalFieldsOrder'=>array()));
                $meta = Zend_Json::encode(isset($_POST['style']) ? array(
                    'style' => $_POST['style']
                ) : array(
                    'style' => ''
                ));

                $image = '';
                if ($imageForm->isValid($this->getRequest()->getPost(), 'feedback')) {
                    try {
                        $image = $feedbackFormService->uploadImage();

                    } catch (Zend_File_Transfer_Exception $e) {
                        Zend_Debug::dump($e);
                    }
                }
                try {
                    $_entry = $feedbackFormService->getValidated($studyId, $isPublished, $isOpen, $publicationStart, $publicationEnd, $introductionIsVisible, $meta, $image, $this->_getParam('id'));
                    // creation des dossier sous le core solr
                    $feedbackFormService->addDirectorySolr($_entry->id);
                    // creation des fichiers de configs necessaire
                    $core_solr = $feedbackFormService->addfileConfigSolr($_entry->id);
                    // creation des fichiers de logs, imports, exports, flux necessaire
                    $feedbackFormService->createFeedbackFormDirectories($_entry->study_id, $_entry->id);


                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__feedbackform_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'feedback-form',
                        'action' => 'index',
                    ), null, true));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'feedback-form',
                        'action' => 'add',
                        'study_id' => $validation->study_id
                    ), null, true));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_add_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($validation->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'add',
                    'study_id' => $validation->study_id
                ), null, true));
            }
        }

        $this->view->study = $studyService->getById(array('id' => $studyId));
        $this->view->studies = $studyService->getAll();
    }

    /**
     * delete a feedback_form
     */
    public function deleteAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $userFeedbackFormService = $this->getUserFeedbackFormService();
        $translationService = $this->getTranslationService();

        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $feedbackFormService->getById(array(
                'id' => $this->_getParam('id')
            ));

            $study_id = $_entry->study_id;
            try {
                if ($_entry->is_visible == 1) {
                    $solrConfig = Statflow_Model_Solr::getDefaultConfig();
                    $solrConfig = $solrConfig['endpoint']['localhost'];
                    file_get_contents('http://' . $solrConfig['host'] . ':' . $solrConfig['port'] . $solrConfig['path'] . 'admin/cores?action=UNLOAD&core=' . $_entry->id . '&deleteIndex=true');
                }
                $directory = APPLICATION_PATH . '/../bin/solr/example/solr/';
                if (\Sayyes_Tools_Filesystem::fileExist($directory . $_entry->id))
                    rename($directory . $_entry->id, $directory . $_entry->id . '_archive');
                $_entry->delete();
                $userFeedbackFormService->deleteByFeedbackFormId($this->_getParam('id'));
                $delete_translation = $translationService->deleteTranslation($this->_getParam('id'));
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__feedbackform_delete_flashMessenger_success@backoffice'));

            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_delete_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_delete_flashMessenger_error@backoffice'));
        }
        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'feedback-form',
            'action' => 'index',
        ), null, true));
    }

    /**
     * Display the edition form
     */
    public function editAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $studyService = $this->getStudyService();
        $imageForm = $feedbackFormService->addImageForm();

        if ($this->getRequest()->isPost()) {
            $studyId = $this->_getParam('study_id');
            $feedBackFormId = $this->_getParam('id');

            if ($feedbackFormService->isValid($this->_getAllParams(), 'feedback')) {
                if ($this->_getParam('publication_start', false) != FALSE) {
                    $publicationStart = $feedbackFormService->formatDate($this->_getParam('publication_start'));
                } else
                    $publicationStart = NULL;

                if ($this->_getParam('publication_end', false) != FALSE) {
                    $publicationEnd = $feedbackFormService->formatDate($this->_getParam('publication_end'));
                } else
                    $publicationEnd = NULL;


                $isPublished = ($this->_getParam('is_published', false) != FALSE) ? 1 : 0;
                $isOpen = ($this->_getParam('is_open', false) != FALSE) ? 1 : 0;
                $introductionIsVisible = ($this->_getParam('introduction_is_visible', false) != FALSE) ? 1 : 0;
                $_entry = $feedbackFormService->getById($this->_getParam('id'));
                $metaStyle = Zend_Json::decode($_entry->meta ? $_entry->meta : '{}');

                if (isset($_POST['style'])) {
                    $metaStyle['style'] = $_POST['style'];
                } else {
                    $metaStyle['style'] = '';
                }

                $meta = Zend_Json::encode($metaStyle);

                $image = $this->_getParam('image-value');
                if ($imageForm->isValid($this->getRequest()
                    ->getPost(), 'feedback')
                ) {
                    try {
                        $image = $feedbackFormService->uploadImage();
                    } catch (Zend_File_Transfer_Exception $e) {
                        Zend_Debug::dump($e);
                    }
                }

                try {
                    $saved = $feedbackFormService->getValidated($studyId, $isPublished, $isOpen, $publicationStart, $publicationEnd, $introductionIsVisible, $meta, $image, $this->_getParam('id'));
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__feedbackform_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'feedback-form',
                        'action' => 'index',
                    ), null, true));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'feedback-form',
                        'action' => 'edit',
                        'id' => $feedBackFormId
                    ), null, true));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_add_flashMessenger_error@backoffice'));
                //$this->_flashMessenger->setNamespace('validation')->addMessage($validation->getMessages());
                $this->_flashMessenger->setNamespace('validation')->addMessage($feedbackFormService->getValidator($this->_getAllParams(), 'feedback')->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'edit',
                    'id' => $feedBackFormId
                ), null, true));
            }
        }

        $this->view->feedbackform = $feedbackFormService->getById($this->_getParam('id'));
    }

    public function resendAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $study_id = (int)$this->_getParam('study_id');
        $feedback_form_id = (int)$this->_getParam('feedback_form_id');
        $days = (int)$this->_getParam('days', 1);
        $status = $this->_getParam('status', '1');

        $status = explode(',', $status);

        $configFilePath = APPLICATION_PATH . '/modules/statflow/configs/streams/' . $study_id . '/' . $feedback_form_id . '/config.php';
        if (Sayyes_Tools_Filesystem::fileExist($configFilePath)) {
            include_once($configFilePath);
            $this->_config = ${'config__' . $study_id . '__' . $feedback_form_id};
        } else {
            throw new Zend_Exception('mandatory config file is missing -> ' . $configFilePath);
        }

        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        // main request
        $select = $adapter->select();
        $select->from('statflow_respondent')
            ->reset(Zend_Db_Select::COLUMNS)
            ->where('statflow_respondent.status IN (' . join(',', $status) . ')')
            ->where('statflow_respondent.feedback_form_id = ?', $feedback_form_id)
            ->where('statflow_respondent.created_at <= ?', Zend_Date::now()->subDay($days)
                ->get('YYYY-MM-dd HH:mm:ss'));

        $select->columns('statflow_respondent.id as respondent_id')
            ->columns('statflow_respondent.token as token')
            ->columns('statflow_respondent.email as email')
            ->columns('statflow_respondent.locale as language');

        // External fields
        $fields = $this->_config['exportedFields'];
        foreach ($fields as $field) {
            $subSelect = $adapter->select();
            $subSelect->from('statflow_respondent_external_field', array(
                'statflow_respondent_external_field.value'
            ))
                ->joinInner('statflow_import_field', 'statflow_import_field.id = statflow_respondent_external_field.import_field_id', array())
                ->where('statflow_respondent_external_field.respondent_id = statflow_respondent.id')
                ->where('statflow_import_field.label = ?', $field);
            $select->columns('(' . new Zend_Db_Expr($subSelect->assemble()) . ') as ' . $field);
        }
        $respondents = $adapter->fetchAll($select);
        $parsedRespondent = array();
        $serverUrl = $this->view->serverUrl();
        $ids = array();
        for ($i = 0; $i < count($respondents); $i++) {
            $ids[] = $respondents[$i]['respondent_id'];
            $respondents[$i] = array_merge(array(
                'link' => sprintf($serverUrl . '/statflow/form/%s' . '/token/' . $respondents[$i]['token'], $feedback_form_id)
            ), $respondents[$i]);
            $parsedRespondentData = array(
                'link' => $respondents[$i]['link'],
                'email' => $respondents[$i]['email'],
                'language' => $respondents[$i]['language']
            );
            foreach ($fields as $field) {
                $parsedRespondentData[$field] = $respondents[$i][$field];
            }
            $parsedRespondent[] = $parsedRespondentData;

            unset($respondentRow);
            if ($i % 100 == 0)
                gc_collect_cycles();
        }
        if (count($ids) > 0) {
            Zend_Db_Table_Abstract::getDefaultAdapter()->query('UPDATE statflow_respondent SET status = 1 WHERE id IN (' . join(',', $ids) . ')');
        }
        if (count($parsedRespondent) > 0) {
            $fileName = Zend_Date::now()->get('yyyyMMddHHmmss') . '__' . $study_id . '__' . $feedback_form_id . '__export_statflow.csv';
            $file = fopen(APPLICATION_PATH . '/../public/ftp/' . $study_id . '/' . $feedback_form_id . '/export/' . $fileName, 'w');
            fwrite($file, $feedbackFormService->_arrayToCSV($parsedRespondent));
            fclose($file);
            Sayyes_Tools_Console::printLine('Exported to -> ' . $fileName);
        } else
            Sayyes_Tools_Console::printLine('Nothing to export');

        die('ok');
    }

    ///////////////////////////////////////////////////////
    //                  Configure Alert                 //
    /////////////////////////////////////////////////////

    public function listAlertAction()
    {
        $this->view->pageLayout = 'alert';
        $feedbackFormService = $this->getFeedbackFormService();

        $this->view->form = $formId = $this->_getParam('form', null);
        if (!$formId) {
            $studyService = $this->getStudyService();
            $this->view->studies = $studyService->getAll();
            return;
        }

        $form = $feedbackFormService->getById($this->_getParam('form'));
        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');
        if (isset($meta['alerts']) and count($meta['alerts'] > 0)) {
            $allAlert = array();
            foreach ($meta['alerts'] as $key => $metaAlert) {
                if (!array_key_exists('format_pj', $metaAlert)) {
                    $metaAlert['format_pj'] = true;
                }
                if (!array_key_exists('attribut_xml', $metaAlert)) {
                    $metaAlert['attribut_xml'] = '';
                }
                $allAlert[$key] = $metaAlert;
            }
            $meta['alerts'] = $allAlert;
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }

        $defaultAlerts = array();
        $this->view->study = $form->study;
        $this->view->alerts = isset($meta['alerts']) ? $meta['alerts'] : $defaultAlerts;
    }

    public function editAlertAction()
    {
        $this->view->pageLayout = 'alert';
        $feedbackFormService = $this->getFeedbackFormService();
        $form = $feedbackFormService->getById($this->_getParam('form'));
        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');

        $key = $this->_getParam('key');
        if ($this->_request->isPost()) {
            foreach ($_POST['alert'] as $key => $alert) {
                $rule = array('enabled' => (bool)$alert['enabled'],
                    'libelle' => $alert['libelle'],
                    'send_email' => (bool)$alert['send_email'],
                    'rule' => $alert['rule'],
                    'alertRecipients' => (isset($alert['recipients']) && is_array($alert['recipients'])) ? $alert['recipients'] : array(),
                    'senderName' => $alert['senderName'],
                    'senderEmail' => $alert['senderEmail'],
                    'attachementName' => $alert['attachementName'],
                    'format_pj' => (bool)$alert['format_pj'],
                    'attribut_xml' => $alert['attribut_xml']
                );
            }
            $meta['alerts'][$key] = $rule;

            $form->meta = Zend_Json::encode($meta);

            $result_xml = preg_match('/^\S+=\S+$/', $rule['attribut_xml']);
            if ($rule['format_pj'] == FALSE) {
                if (($result_xml === 0 || !$result_xml)) {
                    //echo "no match";
                    $this->view->error_msg = "Value required with format xmlns=http://XXXXX";
                } else {
                    $form->save();
                }
                // champ xml est a vide
            } else {
                $form->save();
            }
        }

        $this->view->key = $key;
        $this->view->alert = $meta['alerts'][$key];
        $this->view->form = $form;
    }

    public function alertAction()
    {
        $this->view->pageLayout = 'alert';
        $feedbackFormService = $this->getFeedbackFormService();
        $form = $feedbackFormService->getById($this->_getParam('form'));

        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');
        $defaultAlerts = array();

        $metaAlert = array();

        if (isset($meta['alerts']) and count($meta['alerts'] > 0)) {
            $allAlert = array();
            foreach ($meta['alerts'] as $key => $metaAlert) {
                if (!array_key_exists('format_pj', $metaAlert)) {
                    $metaAlert['format_pj'] = false;
                }
                if (!array_key_exists('attribut_xml', $metaAlert)) {
                    $metaAlert['attribut_xml'] = '';
                }
                $allAlert[$key] = $metaAlert;
            }
            $meta['alerts'] = $allAlert;
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }

        if ($this->_request->isPost()) {
            $alertRules = array();
            foreach ($_POST['alert'] as $key => $alert) {
                $rule = array('enabled' => (bool)$alert['enabled'],
                    'libelle' => $alert['libelle'],
                    'send_email' => (bool)$alert['send_email'],
                    'rule' => $alert['rule'],
                    'alertRecipients' => (isset($alert['recipients']) && is_array($alert['recipients'])) ? $alert['recipients'] : array(),
                    'senderName' => $alert['senderName'],
                    'senderEmail' => $alert['senderEmail'],
                    'attachementName' => $alert['attachementName'],
                    'format_pj' => (bool)$alert['format_pj'],
                    'attribut_xml' => $alert['attribut_xml']
                );
                $alertRules[$key] = $rule;
            }

            $meta['alerts'] = $alertRules;

            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }

        $this->view->form = $form;
        $this->view->study = $form->study;
        $this->view->alerts = isset($meta['alerts']) ? $meta['alerts'] : $defaultAlerts;
    }

    public function addAlertAction()
    {
        $this->view->pageLayout = 'alert';
        $feedbackFormService = $this->getFeedbackFormService();
        $form = $feedbackFormService->getById($this->_getParam('form'));

        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');
        $numAlert = count($meta['alerts']);

        if ($numAlert > 0) {
            $id = intval(max(array_keys($meta['alerts']))) + 1;

            $meta['alerts'][$id] = array('enabled' => false,
                'libelle' => '',
                'send_email' => false,
                'rule' => "",
                'alertRecipients' => array(),
                'senderName' => '',
                'senderEmail' => '',
                'attachementName' => '',
                'format_pj' => true,
                'attribut_xml' => '');
        } else {
            $id = 1;
            $meta['alerts'] = array($id => array('enabled' => false,
                'libelle' => '',
                'send_email' => false,
                'rule' => "",
                'alertRecipients' => array(),
                'senderName' => '',
                'senderEmail' => '',
                'attachementName' => '',
                'format_pj' => true,
                'attribut_xml' => ''));
        }

        $form->meta = Zend_Json::encode($meta);
        $form->save();
        $this->_redirect($this->view->url(array(
            'action' => 'edit-alert',
            'form' => $form->id,
            'key' => $id
        )));
    }

    public function removeAlertAction()
    {
        $this->view->pageLayout = 'alert';
        $feedbackFormService = $this->getFeedbackFormService();
        $form = $feedbackFormService->getById($this->_getParam('form'));
        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');


        $key = intval($this->_getParam('key'));
        $alerts = $meta['alerts'];

        // array_splice($alerts, $key, 1);
        unset($alerts[$key]);
        $meta['alerts'] = $alerts;

        $form->meta = Zend_Json::encode($meta);
        $form->save();
        $this->_redirect($this->view->url(array(
            'action' => 'list-alert'
        )));
    }

    public function updateAlertAction()
    {
        $this->view->pageLayout = 'alert';
        $feedbackFormService = $this->getFeedbackFormService();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $feedbackForms = $feedbackFormService->getAll();

        foreach ($feedbackForms as $form) {
            $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');
            if (isset($meta['alerts']) && is_array($meta['alerts'])) {
                $numAlert = 1;
                $alertRules = array();
                foreach ($meta['alerts'] as $alert) {
                    $rule = array('enabled' => (bool)$alert['enabled'],
                        'libelle' => $alert['libelle'],
                        'send_email' => (bool)$alert['send_email'],
                        'rule' => $alert['rule'],
                        'alertRecipients' => (isset($meta['alertRecipients']) && is_array($meta['alertRecipients'])) ? $meta['alertRecipients'] : array(),
                        'senderName' => (isset($meta['senderName'])) ? $meta['senderName'] : '',
                        'senderEmail' => (isset($meta['senderEmail'])) ? $meta['senderEmail'] : '',
                        'attachementName' => (isset($meta['attachementName'])) ? $meta['attachementName'] : '');
                    $alertRules[$numAlert] = $rule;
                    $numAlert++;
                }
                $meta['alerts'] = $alertRules;
                $form->meta = Zend_Json::encode($meta);
                $form->save();
            }
        }

        echo 'Alert updated in all feedback forms!!!';
        exit;
    }

    //////////////////////////////////////////////////////////

    public function filterAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $filtersKpiService = $this->getFiltersKpiService();

        if ($this->getRequest()->isPost()) {
            $feedback_form_id = $this->_getParam('id');

            try {
                // external fields
                //delete all external fields for this feedback form
                $filtersKpiService->deleteFiltersByFeedbackFormId($feedback_form_id);

                if (!empty($_POST['sel_filters_kpi'])) {
                    $postExternalFields = $_POST['sel_filters_kpi'];
                    foreach ($postExternalFields as $import_field_id):
                        $filters = $filtersKpiService->saveFilters($feedback_form_id, $import_field_id);
                    endforeach;
                }

                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__filters_add_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'filter')));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__filters_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'filter')));
            }

        }

        $this->view->feedbackform = $feedbackFormService->getById($this->_getParam('id'));
        $this->view->external_fields = $filtersKpiService->getAllExternalFieldByFeedbackFormId($this->_getParam('id'));
        $this->view->filters = $filtersKpiService->getFiltersByFeedbackFormId($this->_getParam('id'));
    }

    public function manageImportAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $form = $feedbackFormService->getById($this->_getParam('form'));

        if ($this->_request->isPost()) {
            //echo "val " . $this->_getParam('rule_numdays');
            if ($feedbackFormService->isValid($this->_getAllParams(), 'manage-import')) {
                echo " id study bon" . $form->id;
                //die(' import');

                $form->column_separator = $_POST['column_separator'];
                $form->label_info = $_POST['label_info'];
                $form->rule_numdays = $_POST['rule_numdays'];
                $form->field_label = $_POST['fieldLabel'];
                $form->field_format = $_POST['fieldFormat'];
                try {
                    $form->save();
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_import_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'manage-import')));
                } catch (Exception $e) {

                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_import_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($feedbackFormService->getValidator($this->_getAllParams(), 'manage-import')->getMessages());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'manage-import')));
                //echo "pas bon";
            }
            //echo "die here";
        }

        $this->view->form = $form;
        $this->view->study = $form->study;

    }

    public function duplicateAction()
    {

        $formPageService = $this->getFormPageService();
        $questionService = $this->getQuestionService();
        $questionEntryService = $this->getQuestionEntryService();
        $feedbackFormService = $this->getFeedbackFormService();
        $translationService = $this->getTranslationService();

        $form = $feedbackFormService->getById($this->_getParam('form'));
        $formRow = $feedbackFormService->duplicateFeedbackForm($this->_getParam('form'));
        $idFormCloned = $formRow->id;
        // creation des dossier sous le core solr
        $feedbackFormService->addDirectorySolr($idFormCloned);
        // creation des fichiers de configs necessaire
        $core_solr = $feedbackFormService->addfileConfigSolr($idFormCloned);
        // creation des fichiers de logs, flux, import, export necessaires
        $feedbackFormService->createFeedbackFormDirectories($formRow->study_id, $formRow->id);

        //duplicate translations
        $newtagId = $translationService->insertRowTag('form_' . $idFormCloned);

        $newFormTranslationUid = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__introduction-text');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid->id, $newtagId);
        $oldTranslationUidRow = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__introduction-text');
        if ($oldTranslationUidRow) {
            $translationService->duplicateTranslation($oldTranslationUidRow->id, $newFormTranslationUid->id);
        }

        $newFormTranslationUid1 = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__next-question');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid1->id, $newtagId);
        $oldTranslationUidRow1 = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__next-question');
        if ($oldTranslationUidRow1) {
            $translationService->duplicateTranslation($oldTranslationUidRow1->id, $newFormTranslationUid1->id);
        }

        $newFormTranslationUid2 = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__previous-question');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid2->id, $newtagId);
        $oldTranslationUidRow2 = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__previous-question');
        if ($oldTranslationUidRow2) {
            $translationService->duplicateTranslation($oldTranslationUidRow2->id, $newFormTranslationUid2->id);
        }

        $newFormTranslationUid3 = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__validate-form');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid3->id, $newtagId);
        $oldTranslationUidRow3 = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__validate-form');
        if ($oldTranslationUidRow3) {
            $translationService->duplicateTranslation($oldTranslationUidRow3->id, $newFormTranslationUid3->id);
        }

        $newFormTranslationUid4 = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__invalid-question');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid4->id, $newtagId);
        $oldTranslationUidRow4 = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__invalid-question');
        if ($oldTranslationUidRow4) {
            $translationService->duplicateTranslation($oldTranslationUidRow4->id, $newFormTranslationUid4->id);
        }

        $newFormTranslationUid5 = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__invalid-openquestion');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid5->id, $newtagId);
        $oldTranslationUidRow5 = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__invalid-openquestion');
        if ($oldTranslationUidRow5) {
            $translationService->duplicateTranslation($oldTranslationUidRow5->id, $newFormTranslationUid5->id);
        }

        $newFormTranslationUid6 = $translationService->insertRowTranslationUid('form__' . $idFormCloned . '__invalid-mcquestion');
        $rowDataTranslationTagUid = $translationService->insertRowTranslationTagUid($newFormTranslationUid6->id, $newtagId);
        $oldTranslationUidRow6 = $translationService->getTranslationUidRow('form__' . $this->_getParam('form') . '__invalid-mcquestion');
        if ($oldTranslationUidRow6) {
            $translationService->duplicateTranslation($oldTranslationUidRow6->id, $newFormTranslationUid6->id);
        }


        $pages = $formPageService->getAllPagesByFeedbackForm($this->_getParam('form'));


        foreach ($pages as $page) {
            $pageRow = $formPageService->duplicatePage($page->id, null, $idFormCloned);
            $idPageCloned = $pageRow->id;
            $questions = $questionService->getAllQuestionsByPage($page->id);

            foreach ($questions as $question) {
                $questionToCopy = $questionService->getQuestionById($question->id);
                $questionRow = $questionService->duplicateQuestion($question->id, null, $idPageCloned, $idFormCloned);
                $idQuestionCloned = $questionRow->id;
                $codeQuestionCloned = $questionRow->code;
                $codeQuestionOrig = $questionToCopy->code;
                if ($questionRow->question_type_id == Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY) {
                    $sousQuestion = $questionService->duplicateSousQuestions($question->id, $idQuestionCloned, $codeQuestionOrig, $codeQuestionCloned, null, $idPageCloned, $idFormCloned);
                }

                $modalities = $questionEntryService->duplicateModalities($question->id, $idQuestionCloned);
            }

        }

        $translationService->generateTranslationFiles();

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'study',
            'action' => 'index',
            'study_id' => $formRow->study_id,
            'feedback_form_id' => $formRow->id
        ), null, true));

    }

    public function validityAction()
    {
        //$form = $this->_model->get(array('id' => $this->_getParam('form')));
        $feedbackFormService = $this->getFeedbackFormService();
        $form = $feedbackFormService->getById($this->_getParam('form'));

        if ($this->getRequest()->isPost()) {
            //$form->validity_days = $this->_getParam('duration');
            $post = $this->_request->getPost();
            $form->validity_days = $post['duration'];
            $form->save();
        }

        $this->view->feedbackForm = $form;
        $this->view->study = $form->study;
        $this->view->validityDuration = $form->validity_days;
    }

    public function hideAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $respondentFieldHiddenService = $this->getRespondentFieldHiddenService();
        $filtersKpiService = $this->getFiltersKpiService();
        $conf_solr = $this->getXmlConfigSolrService();

        $feedbackFormId = $this->_getParam('id');

        $requete = $conf_solr->generateSqlCode($feedbackFormId);
        $headers = $requete['header'];

        $externalFields = $filtersKpiService->getAllExternalFieldByFeedbackFormId($feedbackFormId);

        $respondentHiddenFields = array();

        if ($this->getRequest()->isPost()) {
            try {
                //delete all fields selected
                $respondentFieldHiddenService->deleteRespondentFieldHiddenByFeedbackFormId($feedbackFormId);

                if (!empty($_POST['sel_respondent_infos'])) {

                    $postRespondentInfos = $_POST['sel_respondent_infos'];
                    foreach ($postRespondentInfos as $respondentInfo):
                        $respondentHiddenFields[] = $respondentInfo;
                    endforeach;
                }

                if (!empty($_POST['sel_external_fields'])) {
                    $postExternalFields = $_POST['sel_external_fields'];
                    foreach ($postExternalFields as $externalField):
                        $respondentHiddenFields[] = $externalField;
                    endforeach;
                }

                if (!empty($_POST['sel_questions'])) {
                    $postQuestions = $_POST['sel_questions'];
                    foreach ($postQuestions as $question):
                        $respondentHiddenFields[] = $question;
                    endforeach;
                }

                if (!empty($respondentHiddenFields)) {
                    foreach ($respondentHiddenFields as $respondentHiddenField):
                        $result = $respondentFieldHiddenService->saveRespondentFieldHidden($feedbackFormId, $respondentHiddenField);
                    endforeach;
                }

                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__respondent_field_hide_add_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'hide')));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__respondent_field_hide_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'hide')));
            }

        }

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);
        $this->view->respondent_infos = $respondentFieldHiddenService->getRespondentInfos($headers, $externalFields->toArray());
        $this->view->external_fields = $respondentFieldHiddenService->getRespondentExternalFields($externalFields->toArray());
        $this->view->questions = $respondentFieldHiddenService->getRespondentQuestions($headers);
        $this->view->hiddenFields = $this->respondentFieldHiddenService->getRespondentFieldHiddenByFeedbackFormId($feedbackFormId);
    }

    public function orderAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $respondentFieldHiddenService = $this->getRespondentFieldHiddenService();
        $filtersKpiService = $this->getFiltersKpiService();
        $conf_solr = $this->getXmlConfigSolrService();

        $feedbackFormId = $this->_getParam('id');
        $form = $feedbackFormService->getById($feedbackFormId);

        $requete = $conf_solr->generateSqlCode($feedbackFormId);
        //respondent infos
        $headers = $requete['header'];
        //all external fields
        $externalFields = array();

        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');

        if (!isset($meta['respondentInfosOrder'])) {
            $meta['respondentInfosOrder'] = array();
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }
        if (isset($meta['externalFieldsOrder'])) {
            unset($meta['externalFieldsOrder']);
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }

        $numRespondentInfosOrder = count($meta['respondentInfosOrder']);

        $this->view->feedbackform = $form;
        if ($this->_getParam('sortInfos', FALSE) != FALSE) {
            $sortInfos = explode(',', $this->_getParam('sortInfos'));

            $meta['respondentInfosOrder'] = $sortInfos;
            $form->meta = Zend_Json::encode($meta);
            $form->save();

            $this->view->respondent_infos = $sortInfos;
        } else {
            if ($numRespondentInfosOrder > 0) {
                $this->view->respondent_infos = $meta['respondentInfosOrder'];
            } else {
                $this->view->respondent_infos = $respondentFieldHiddenService->getRespondentInfos($headers, $externalFields);
            }
        }
    }

    public function resetOrderAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $respondentFieldHiddenService = $this->getRespondentFieldHiddenService();

        $conf_solr = $this->getXmlConfigSolrService();

        $feedbackFormId = $this->_getParam('id');
        $form = $feedbackFormService->getById($feedbackFormId);

        $requete = $conf_solr->generateSqlCode($feedbackFormId);
        //respondent infos
        $headers = $requete['header'];

        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');

        if (isset($meta['respondentInfosOrder'])) {
            $meta['respondentInfosOrder'] = array();
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }

        $this->view->respondent_infos = $respondentFieldHiddenService->getRespondentInfos($headers, array());

    }

    public function changeAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $questionService = $this->getQuestionService();
        $questionEntryService = $this->getQuestionEntryService();

        $feedbackFormId = $this->_getParam('form');
        $questionCode = $this->_getParam('question_code');

        $questionId = $this->questionService->getQuestionIdByCode($feedbackFormId, $questionCode);

        $result = array();

        if ($questionId) {
            $data = $questionEntryService->getModalitiesByQuestionId($questionId);
            print_r(Zend_Json::encode($data));
            die;
        } else {
            print_r(Zend_Json::encode(null));
            die;
        }

    }

    public function resetAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $questionService = $this->getQuestionService();

        $feedbackFormId = $this->_getParam('form');
        $question = NULL;
        $modality = NULL;

        try {
            $result = $feedbackFormService->updateAnonymousQuestion($feedbackFormId, $question, $modality);

            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_anonymous_question_delete_flashMessenger_success@backoffice'));
            $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'anonymous')));

        } catch (Exception $e) {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_anonymous_question_delete_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'anonymous')));
        }

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);
        $this->view->questions = $questionService->getQuestionCodeTypeMCQ($feedbackFormId);
    }

    public function anonymousAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $questionService = $this->getQuestionService();

        $feedbackFormId = $this->_getParam('id');
        $question = NULL;
        $modality = NULL;
        if ($this->getRequest()->isPost()) {
            try {
                if (!empty($_POST['sel_questions'])) {
                    $question = $_POST['sel_questions'];
                }

                if (!empty($_POST['sel_modalities'])) {
                    $modality = $_POST['sel_modalities'];
                }

                if (isset($question) && isset($modality)) {
                    $result = $feedbackFormService->updateAnonymousQuestion($feedbackFormId, $question, $modality);

                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_anonymous_question_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'anonymous')));
                }


            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_anonymous_question_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'anonymous')));
            }
        }

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);
        $this->view->questions = $questionService->getQuestionCodeTypeMCQ($feedbackFormId);
        $this->view->questionSaved = $feedbackFormService->getAnonymousQuestionByFeedbackFormId($feedbackFormId);
        $this->view->modalitySaved = $feedbackFormService->getAnonymousModalityByFeedbackFormId($feedbackFormId);

    }

    public function nominativeAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $questionService = $this->getQuestionService();

        $respondentFieldHiddenService = $this->getRespondentFieldHiddenService();
        $nominativeFieldHiddenService = $this->getNominativeFieldHiddenService();
        $filtersKpiService = $this->getFiltersKpiService();
        $conf_solr = $this->getXmlConfigSolrService();
        $feedbackFormId = $this->_getParam('id');


        $requete = $conf_solr->generateSqlCode($feedbackFormId);
        $headers = $requete['header'];

        $externalFields = $filtersKpiService->getAllExternalFieldByFeedbackFormId($feedbackFormId);

        $question = NULL;
        $modality = NULL;

        if ($this->getRequest()->isPost()) {
            try {

                //delete all fields selected
                $nominativeFieldHiddenService->deleteNominativeFieldHiddenByFeedbackFormId($feedbackFormId);

                if (!empty($_POST['sel_questions'])) {
                    $question = $_POST['sel_questions'];
                }

                if (!empty($_POST['sel_modalities'])) {
                    $modality = $_POST['sel_modalities'];
                }

                if (!empty($_POST['sel_respondent_infos'])) {

                    $postRespondentInfos = $_POST['sel_respondent_infos'];
                    foreach ($postRespondentInfos as $respondentInfo):
                        $respondentHiddenFields[] = $respondentInfo;
                    endforeach;
                }

                if (!empty($_POST['sel_external_fields'])) {
                    $postExternalFields = $_POST['sel_external_fields'];
                    foreach ($postExternalFields as $externalField):
                        $respondentHiddenFields[] = $externalField;
                    endforeach;
                }


                if (!empty($_POST['sel_respondentquestions'])) {
                    $postQuestions = $_POST['sel_respondentquestions'];
                    foreach ($postQuestions as $respondentquestion):
                        $respondentHiddenFields[] = $respondentquestion;
                    endforeach;
                }

                if (!empty($respondentHiddenFields)) {
                    foreach ($respondentHiddenFields as $respondentHiddenField):
                        $result = $nominativeFieldHiddenService->saveNominativeFieldHidden($feedbackFormId, $respondentHiddenField);
                    endforeach;
                }


                //if (isset($question) && isset($modality)) {
                // MAJ table feedbackForm
                $result = $feedbackFormService->updateNominativeQuestion($feedbackFormId, $question, $modality);

                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_nominative_question_add_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'nominative')));
                //}

            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__nominative_field_hide_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'nominative')));
            }
        }

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);
        $this->view->questions = $questionService->getQuestionCodeTypeMCQ($feedbackFormId);

        $this->view->respondent_infos = $respondentFieldHiddenService->getRespondentInfos($headers, $externalFields->toArray());
        $this->view->external_fields = $respondentFieldHiddenService->getRespondentExternalFields($externalFields->toArray());
        $this->view->respondentquestions = $respondentFieldHiddenService->getRespondentQuestions($headers);
        $this->view->hiddenFields = $this->nominativeFieldHiddenService->getNominativeFieldHiddenByFeedbackFormId($feedbackFormId);

        $this->view->questionSaved = $feedbackFormService->getNominativeQuestionByFeedbackFormId($feedbackFormId);
        $this->view->modalitySaved = $feedbackFormService->getNominativeModalityByFeedbackFormId($feedbackFormId);

    }

    ///////////////////////////////////////////////////////
    //            Configure External fields             //
    /////////////////////////////////////////////////////

    public function listExternalAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        $feedbackFormId = $this->_getParam('id');
        $form = $feedbackFormService->getById($feedbackFormId);

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);

        $this->view->externals = $feedbackFormService->getFeedbackFormParameterExternalField($feedbackFormId);
    }

    public function addExternalAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        $feedbackFormId = $this->_getParam('id');
        $form = $feedbackFormService->getById($feedbackFormId);
        $study_id = $form->study_id;

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);

        if ($this->_request->isPost()) {
            try {
                if (!empty($_POST['import_field_external_id'])) {
                    $external_id = $_POST['import_field_external_id'];
                }
                if (!empty($_POST['import_field_label'])) {
                    $label = $_POST['import_field_label'];
                }

                if ($external_id && $label) {
                    $feedbackFormService->insertImportInfos($study_id, $feedbackFormId, $external_id, $label);
                }

                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__filters_add_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'list-external')));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__filters_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'list-external')));
            }
        }

    }

    public function editExternalAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        $feedbackFormId = $this->_getParam('id');
        $importFieldId = $this->_getParam('id_external');
        $form = $feedbackFormService->getById($feedbackFormId);

        $this->view->feedbackform = $feedbackFormService->getById($feedbackFormId);


        $importFieldInfos = $feedbackFormService->getImportFieldInfosByID($importFieldId);
        $importId = $importFieldInfos['import_id'];

        if ($this->_request->isPost()) {
            try {
                if (!empty($_POST['import_field_external_id'])) {
                    $external_id = $_POST['import_field_external_id'];
                }
                if (!empty($_POST['import_field_label'])) {
                    $label = $_POST['import_field_label'];
                }

                if ($external_id && $label) {
                    $feedbackFormService->updateImportInfos($importFieldId, $external_id, $label, $importId);
                }

                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__filters_add_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'list-external')));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__filters_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'feedback-form', 'action' => 'list-external')));
            }
        }

        $this->view->external_id = $importFieldInfos['external_id'];
        $this->view->label = $importFieldInfos['label'];;

    }

    public function removeExternalAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();

        $feedbackFormId = $this->_getParam('id');
        $importFieldId = $this->_getParam('id_external');

        $feedbackFormService->deleteImportField($importFieldId);
        $this->_redirect($this->view->url(array(
            'action' => 'list-external'
        )));

    }

}

