<?php
require_once __DIR__ . '/../services/Parameter.php';

use Statflow\Service\Parameter;

class Statflow_ParameterController extends Centurion_Controller_Action
{
    /**
     *
     * @var parameter
     */
    private $parameterService;

    /**
     *
     * @return \Statflow\Service\Parameter
     */
    public function getParameterService()
    {
        if (! $this->parameterService) {
            $this->parameterService = new Parameter();
        }
    
        return $this->parameterService;
    }
    
    /**
     *
     * @param \Statflow\Service\Parameter $parameterService
     * 
     */
    public function setParameterService($parameterService)
    {
        $this->parameterService = $parameterService;
    }
    
    
    /**
     * Initialize the Parameter controller
     */
    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    
    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * List the parameter
     */
    public function indexAction()
    {      
        $parameterService = $this->getParameterService();
        
        // Affichage des parametres des emails par defauts
        $parameters = $parameterService->getEmailParameters();
        
        $this->view->sender_name_connex  = $parameters[0]['value'];
        $this->view->sender_email_connex = $parameters[1]['value'];
        $this->view->bcc_email_connex    = $parameters[2]['value'];
        $this->view->sender_name_renew   = $parameters[3]['value'];
        $this->view->sender_email_renew  = $parameters[4]['value'];
        $this->view->bcc_email_renew     = $parameters[5]['value'];
        
        $this->view->languages = $parameterService->getAllLanguages();        
        $this->view->hiddenFields =  $parameterService->getLanguagesDirectionR2L();
    }
    

      
    /**
     * save the email parameter
     */
    public function saveEmailAction()
    {
        $parameterService = $this->getParameterService();
        
        if($this->_request->isPost()){
            if(isset($_POST['btnEmailSave'])) {
                if($_POST['btnEmailSave'] == 'emailconnex') {
                    
                    if ($parameterService->isValid($this->_getAllParams(),'manage_email_connex')) {
                        $sender_name_connex = $_POST['sender_name_connex'];
                        $sender_email_connex = $_POST['sender_email_connex']; 
                        $bcc_email_connex   = $_POST['bcc_email_connex'];
                        
                        $criteria = array('sender_name_connex','sender_email_connex','bcc_email_connex');
                        $value    = array($sender_name_connex, $sender_email_connex, $bcc_email_connex);
                        
                        try 
                        {
                            $parameterService->updateEmailParameters($criteria, $value);
                            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_parameter_connex_flashMessenger_success@backoffice'));
                        }
                        catch(Exception $e) 
                        {
                            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_parameter_connex_flashMessenger_error@backoffice'));
                        }
                    }
                    else 
                    {
                        $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_parameter_connex_flashMessenger_error@backoffice'));
                        $this->_flashMessenger->setNamespace('validation')->addMessage($parameterService->getValidator($this->_getAllParams(),'manage_email_connex')->getMessages());
                    } 
                    
                        
                }
                else
                {
                    if ($parameterService->isValid($this->_getAllParams(),'manage_email_renew')) {
                        $sender_name_renew  = $_POST['sender_name_renew'];
                        $sender_email_renew = $_POST['sender_email_renew'];
                        $bcc_email_renew    = $_POST['bcc_email_renew'];
                        
                        $criteria = array('sender_name_renew','sender_email_renew','bcc_email_renew');
                        $value    = array($sender_name_renew, $sender_email_renew, $bcc_email_renew);
                        
                        try
                        {
                            $parameterService->updateEmailParameters($criteria, $value);
                            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_parameter_renew_flashMessenger_success@backoffice'));
                        }
                        catch(Exception $e)
                        {
                            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_parameter_renew_flashMessenger_error@backoffice'));
                        }
                    }
                    else 
                    {
                        $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_parameter_renew_flashMessenger_error@backoffice'));
                        $this->_flashMessenger->setNamespace('validation')->addMessage($parameterService->getValidator($this->_getAllParams(),'manage_email_renew')->getMessages());
                    }
                    
                }
            }
            
            $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'parameter', 'action' => 'index')));
        }
    }
    
    public function directionLanguageAction()
    {
        $parameterService = $this->getParameterService();
    
        $languages = array();
        if ($this->getRequest()->isPost())
        {
            try
            {
                $parameterService->updateAllLanguagesDirectionL2R();
    
                if(!empty($_POST['sel_languages'])){
                    $postLanguages = $_POST['sel_languages'];
                    foreach($postLanguages as $language):
                        $languages[] = $language;
                    endforeach;
                }
    
                if(!empty($languages)){
                    foreach($languages as $direction_language):
                    $result = $parameterService->updateLanguagesDirectionR2L($direction_language);
                    endforeach;
                }
    
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__manage_parameter_direction_language_flashMessenger_success@backoffice'));
            } catch(Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__manage_parameter_direction_language_flashMessenger_error@backoffice'));
            }
    
        }
        
        $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'parameter', 'action' => 'index')));
    }    
}