<?php
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/FeedbackForm.php';
require_once __DIR__ . '/../services/Respondent.php';

use Statflow\Service\Respondent;
use Statflow\Service\Study;
use Statflow\Service\FeedbackForm;

class Statflow_RespondentController extends Centurion_Controller_Action
{
    
    /**
    *
    * @var Respondent
    */
    private $respondent;

    /**
     * @var Study
     */
    private $studyService;

    /**
     * @var FeedbackForm
     */
    private $feedbackFormService;
    
    /**
     * @return \Statflow\Service\Respondent
     */
    public function setRespondent($respondent)
    {
        $this->respondent = $respondent;
    } 
    
    /**
    *
    * @return \Statflow\Service\Respondent
    */
    public function getRespondent()
    {
        if (! $this->respondent) {
            $this->respondent = new Respondent();
        }
    
        return $this->respondent;
    }

    /**
     *
     * @param \Statflow\Service\Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    }

    /**
     *
     * @return Study
     */
    public function getStudyService()
    {
        if (!$this->studyService) {
            $this->studyService = new Study();
        }

        return $this->studyService;
    }

    /**
     * @param \Statflow\Service\FeedbackForm $feedbackFormService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }

    /**
     *
     * @return FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (!$this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }

        return $this->feedbackFormService;
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    public function indexAction()
    {
        $this->view->pageLayout = 'respondent';
        $respondent = $this->getRespondent();
        $this->view->all_json_structures = array();
        
        $serverUrl =$this->view->serverUrl();       
        foreach($respondent->getAllStructure() as $structure)  {
            $this->view->all_json_structures[] = $structure->external_id.': '.$structure->name;
        }
        $formId = $this->_getParam('feedback_form_id', null);
        $this->view->formId = $formId;
        if (!$formId) {
            $studyService = $this->getStudyService();
            $this->view->studies = $studyService->getAll();
            return;
        }

        $status             = $respondent->setStatus($this->_getParam('status', false));
        $respondent_id      = $respondent->setRespondentId($this->_getParam('respondent_id', false));
        $from               = $respondent->setFrom($this->_getParam('answered_at_from', ''));
        $to                 = $respondent->setTo($this->_getParam('answered_at_to', ''));
        $struct             = $respondent->setStructure($this->_getParam('structures', false));
        $export 	        = $respondent->setExport($this->_getParam('export', false));
        $page		        = $respondent->setPage($this->_getParam('page', 1));
        $alert              = $respondent->setAlert($this->_getParam('alert', 2));
        
        $postStructures = array();
        
        if ($struct != false) {
            foreach (explode(',', $struct) as $structure) {
                $structureParts     = explode(': ', $structure);
                $postStructures[]   = $structureParts[0];
            }
        }
        $this->view->associated_json_structures = $postStructures;
        $buildQuery =  $respondent->buildQuery($formId, $serverUrl);

        if(!$export){
            
            $this->view->headers = $buildQuery['headers'];
            $this->view->paginator = $buildQuery['paginator'];
            $this->view->modalityQuestions = $buildQuery['modalityQuestions'];
            $this->view->modalityQuestionsCodes = $buildQuery['modalityQuestionsCodes'];
            $this->view->openQuestions = $buildQuery['openQuestions'];
            $this->view->status = $buildQuery['status'];
            $this->view->feedback_form_id = $buildQuery['feedback_form_id'];
            $this->view->answered_at_from = $buildQuery['from'];
            $this->view->answered_at_to = $buildQuery['to'];
            $this->view->page = $this->_getParam('page', 1);
            $this->view->alert = $respondent->getAlert();
            $this->view->respondent_id = $buildQuery['respondent_id'];
        }
    }

    public function getFormsByStudyAction()
    {
        $studyId = $this->_getParam('study_id', null);
        $feedbackFormService =  $this->getFeedbackFormService();
        echo json_encode($feedbackFormService->getFeedbacksByStudyId($studyId));
        exit;
    }
}

