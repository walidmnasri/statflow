<?php
require_once __DIR__ . '/../services/GlobalManagement.php';
require_once __DIR__ . '/../services/FeedbackForm.php';

use Statflow\Service\KpiManagement;
use Statflow\Service\FeedbackForm;
use Statflow\Service\GlobalManagement;

class Statflow_GlobalManagementController extends Centurion_Controller_Action
{
    /**
     *
     * @var feedbackForm
     */
    private $feedbackFormService;
    
    /**
     *
     * @var GlobalManagement
     */
    private $global_management;
    
    /**
     *
     * @return \Statflow\Service\KpiManagement
     */
    public function getKpiManagement()
    {
        if (! $this->global_management) {
            $this->global_management = new GlobalManagement();
        }
    
        return $this->global_management;
    }
    
    /**
     *
     * @param \Statflow\Service\KpiManagement $kpiManagement
     */
    public function setKpiManagement($global_management)
    {
        $this->global_management = $global_management;
    }
    
    /**
     *
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (! $this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
    
        return $this->feedbackFormService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }
    
    /**
     * Initialize the controller
     */
    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
    
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
    
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }
    
    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }
    
    public function indexAction()
    {
        $feedbackFormService = $this->getFeedbackFormService();
        $kpi_management = $this->getKpiManagement();
        $form = $feedbackFormService->getById($this->_getParam('form'));
        $validation = $kpi_management->getValidator($this->_getAllParams());
        $kpi_management->writeFileConfig($this->_getParam('form'));
        $meta = Zend_Json::encode(array('bornes'=>array(),'style'=>array()));
        $borne=null;
        $style = null;
        if ($this->getRequest()->isPost()) {
            
            if ($kpi_management->isValid($this->_getAllParams())) {
                $is_open            =  ($this->_getParam('is_open', false) != FALSE) ? 1 : 0;
                if($_POST['type_kpi']=='1' || $_POST['type_kpi']=='2'){
                    $borne = array(
                            'bornePDTSMin'  => $_POST['bornePDTSMin'],
                            'bornePDTSMax'  => $_POST['bornePDTSMax'],
                            'bornePSMin'    => $_POST['bornePSMin'],
                            'bornePSMax'    => $_POST['bornePSMax'],
                            'borneSMin'     => $_POST['borneSMin'],
                            'borneSMax'     => $_POST['borneSMax'],
                            'borneTSMin'    => $_POST['borneTSMin'],
                            'borneTSMax'    => $_POST['borneTSMax'],
                        );
                    $style= array(
                                'colorTS'   =>'00BF57',
                                'colorS'    =>'9FE357',
                                'colorPS'   =>'FFD000',
                                'colorPSDT' =>'FF0000',
                                'color'     =>'satisfaction'
                                );
                }elseif ($_POST['type_kpi']=='5'){
                    $borne = array(
                            'borneVeryGood'     => $_POST['borneVeryGood'],
                            'borneGood'         => $_POST['borneGood'],
                            'bornePoor'         => $_POST['bornePoor'],
                            'borneVeryPoor'     => $_POST['borneVeryPoor'],
                        );
                    $style= array(
                        'colorTS'   =>'00BF57',
                        'colorS'    =>'9FE357',
                        'colorPS'   =>'FFD000',
                        'colorPSDT' =>'FF0000',
                        'color'     =>'satisfaction'
                    );
                }
                elseif ($_POST['type_kpi']=='3' ||$_POST['type_kpi']=='4'){
                    $style= array(
                        'colorPROM'   =>'00BF57',
                        'colorPASS'   =>'FFD000',
                        'colorDETR'   =>'FF0000',
                        'color'       =>'nps'
                    );
                }
                
                $meta=Zend_Json::encode(array('bornes'=>$borne,'style'=>$style));
                try {
                    $entry          = $kpi_management->getValidated($is_open,$meta,$this->_getParam('id'));
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__kpi_management_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'kpi-management',
                    'action' => 'index',
                    'form' => $form->id
                ), null, true));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__kpi_management_add_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                }
            }else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow_kpi_management_add_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($validation->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'kpi-management',
                    'action' => 'add',
                    'form' => $validation->form
                ), null, true));
            }
            $kpi_management->writeFileConfig($this->_getParam('form'));
        }
        if($this->_getParam('color') == true)
             $this->view->color = 'sadasd';
        $this->view->classKpi   = $kpi_management;
        $this->view->form       = $form;
        $this->view->ignore     = $kpi_management->getAllQuestionParent($this->_getParam('form'));
        $this->view->kpis       = $kpi_management->getAllKpiByFeedbackForm($this->_getParam('form'));
        $this->view->study      = $form->study;
    }
   
    

    public function deleteAction(){
         $kpi_management = $this->getKpiManagement();   
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $kpi_management->getById($this->_getParam('id'));
            $form = $_entry->feedback_form_id;
            try {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__kpi_management_delete_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'kpi-management',
                    'action' => 'index',
                    'form' => $form
                ), null, true));
                $kpi_management->writeFileConfig($this->_getParam('form'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__kpi_management_delete_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'feedback-form',
                    'action' => 'index'
                ), null, true));
            }
        } else {
            $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__feedbackform_delete_flashMessenger_error@backoffice'));
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }
    }
    


    

}

?>