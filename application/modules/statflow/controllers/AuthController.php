<?php
require_once __DIR__ . '/../services/Auth.php';
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/Group.php';
require_once __DIR__ . '/../services/Parameter.php';

use Statflow\Service\Auth;
use Statflow\Service\Study;
use Statflow\Service\Group;
use Statflow\Service\Parameter;

class Statflow_AuthController extends Centurion_Controller_Action
{
    /**
     * @var feedbackFormPage
     */
    private $authService;

    /**
     * @var Study
     */
    private $studyService;

    /**
     * @var Group
     */
    private $groupService;

    /**
     * @var parameter
     */
    private $parameterService;

    /**
     * @return Auth
     */
    public function getAuthService()
    {
        if (! $this->authService) {
            $this->authService = new Auth();
        }
        
        return $this->authService;
    }

    /**
     * @param Auth $authService
     */
    public function setAuthService($authService)
    {
        $this->authService = $authService;
    }

    /**
     * @return Study
     */
    public function getStudyService()
    {
        if (! $this->studyService) {
            $this->studyService = new Study();
        }
        return $this->studyService;
    }

    /**
     * @param Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    }

    /**
     * @return Group
     */
    public function getGroupService()
    {
        if (! $this->groupService) {
            $this->groupService = new Group();
        }
        return $this->groupService;
    }

    /**
     * @param Group $groupService
     */
    public function setGroupService($groupService)
    {
        $this->groupService = $groupService;
    }

    /**
     * @return Parameter
     */
    public function getParameterService()
    {
        if (! $this->parameterService) {
            $this->parameterService = new Parameter();
        }
        return $this->parameterService;
    }

    /**
     * @param Parameter $parameterService
     */
    public function setParameterService($parameterService)
    {
        $this->parameterService = $parameterService;
    }

    /**
     * Initialize the Auth controller
     */
    public function init()
    {        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_errors[0];
        }
    }

    public function preDispatch()
    {
        parent::preDispatch();
        $this->_helper->layout->setLayout('login');
    }

    public function indexAction()
    {
        $authService = $this->getAuthService();
        if (null !== $this->_getParam('next', null)) {
            $this->view->next = $this->_getParam('next', null);
        }
        
        if ($this->getRequest()->isPost()) {
            $posts = $this->getRequest()->getParams();            
            if ($authService->FormLogin()->isValid($posts)) {
                $authService->getValidatedLogin();
                $this->_redirectIfAuthenticated();
            } else {
                $this->_flashMessenger->setNamespace('validation')->addMessage($authService->FormLogin()
                    ->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'auth',
                    'action' => 'index'
                )));
            }
        }
    }

    private function _redirectIfAuthenticated()
    {
        $authService = $this->getAuthService();   
        if ($authService->redirectIfAuthenticated($this->_hasParam('next'), $this->_getParam('next')) === 2)
            $this->getHelper('redirector')->gotoSimple('index', 'index', 'statflow');
        elseif ($authService->redirectIfAuthenticated($this->_hasParam('next'), $this->_getParam('next')) === 1)
        $this->getHelper('redirector')->gotoSimple('index', 'index', 'costumer');
    }

    public function logoutAction()
    {
        $authService = $this->getAuthService();
        $authService->ClearSession();
        $this->_redirect('/');
    }

    public function lockedAction()
    {
        if (!isset($_SESSION['username']) || !$_SESSION['username']) {
            $this->_redirect('/');
        }

        $this->_helper->layout->setLayout('lock');
        $authService = $this->getAuthService();
        $authService->lockedSession();

        // conserve data
        $this->view->fullName = $_SESSION['fullName'];
        $this->view->login = $_SESSION['username'];
        $this->view->email = $_SESSION['email'];

    }

    public function resetAction()
    {
        $authService = $this->getAuthService();
        $studyService = $this->getStudyService();
        $groupService = $this->getGroupService();
        if ($this->getRequest()->isPost()) {
            $validator = $authService->getValidatoreReset($this->_getAllParams());
            if ($authService->isValidReset($this->_getAllParams())) {            
                $parameterService = $this->getParameterService();
                $parameters = $parameterService->getEmailParameters();
                $rowUser = $authService->getValidated();             
                // get info from study by iduser
                $studyInfo = $studyService->getAllStudyByUser($rowUser->id);
                //get group name by user id
                $groupName = $groupService->getGroupInfoByUserId($rowUser->id)->current()->name;
                $urlReset = $this->view->serverUrl() . $this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'auth',
                    'action' => 'do-reset',
                    'token' => $rowUser->token
                ));
                $urlDoReset = $this->view->serverUrl() . $this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'auth',
                    'action' => 'do-reset',
                    'token' => $rowUser->token
                ));
                $authService->sendMail($validator->email, 'Resetting your Statflow password', $urlDoReset, $studyInfo, $groupName, $parameters);
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('reset__check_your_email@backoffice'));
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'auth',
                    'action' => 'reset'
                )
                ));
            } else {
                $this->_flashMessenger->setNamespace('validation')->addMessage($validator->getMessages());
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'auth',
                    'action' => 'reset'
                )
                ));
            }
        }
    }

    public function doResetAction()
    {
        $authService = $this->getAuthService();
        $user = $authService->getUserByToken($this->_getParam('token'));

        $this->view->user = $user;
        
        if ($this->getRequest()->isPost()) {

            if ($authService->isValidPwd($this->_getAllParams()) && $authService->getValidatorePwd($this->_getAllParams())->password1 === $authService->getValidatorePwd($this->_getAllParams())->password2) {
                $user =  $authService->getValidatedPwd($this->_getParam('token'));
                
                $this->_redirect($this->view->url(array(
                    'module' => 'statflow',
                    'controller' => 'auth',
                    'action' => 'index'
                ), null, true));
            } else 
                if ($authService->isValidPwd($this->_getAllParams()) && $authService->getValidatorePwd($this->_getAllParams())->password1 !== $authService->getValidatorePwd($this->_getAllParams())->password2)
                 {
                    $this->_flashMessenger->setNamespace('validation')->addMessage(array(
                        'password' => $this->view->translate('statflow__auth__do-reset__password-not-match@backoffice')
                    ));
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'auth',
                        'action' => 'do-reset'
                    )));
                } else {
                    $this->_flashMessenger->setNamespace('validation')->addMessage($authService->getValidatorePwd($this->_getAllParams())->getMessages());
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'auth',
                        'action' => 'do-reset'
                    )));
                }
        }
    }
}

