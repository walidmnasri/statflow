<?php
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/FeedbackFormPage.php';
require_once __DIR__ . '/../services/Question.php';
require_once __DIR__ . '/../services/QuestionEntry.php';
require_once __DIR__ . '/../services/Translation.php';

use Statflow\Service\FeedbackFormPage;
use Statflow\Service\Study;
use Statflow\Service\Question;
use Statflow\Service\QuestionEntry;
use Statflow\Service\Translation;

class Statflow_FeedbackFormPageController extends Centurion_Controller_Action
{
    /**
     * @var Study
     */
    private $studyService;

    /**
     * @var feedbackFormPage
     */
    private $formPageService;
    /**
     *
     * @var Question
     */
    private $questionService;
    
    /**
     *
     * @var QuestionEntry
     */
    private $questionEntryService;

    /**
     *
     * @var Translation
     */
    private $translationService;

    /**
     *
     * @return \Statflow\Service\Study
     */
    public function getStudyService()
    {
        if (! $this->studyService) {
            $this->studyService = new Study();
        }

        return $this->studyService;
    }
    
    /**
     * @return \Statflow\Service\Translation
     */
    public function getTranslationService()
    {
        if (! $this->translationService) {
            $this->translationService = new Translation();
        }
    
        return $this->translationService;
    }
    
    /**
     *
     * @param \Statflow\Service\Translation $translationService
     */
    public function setTranslationService($translationService)
    {
        $this->translationService = $translationService;
    }
    
    /**
     *
     * @return \Statflow\Service\Question
     */
    public function getQuestionService()
    {
        if (! $this->questionService) {
            $this->questionService = new Question();
        }
    
        return $this->questionService;
    }
    
    /**
     *
     * @param \Statflow\Service\Question $questionService
     */
    public function setQuestionService($questionService)
    {
        $this->questionService = $questionService;
    }
    
    
    /**
     *
     * @return \Statflow\Service\QuestionEntry
     */
    public function getQuestionEntryService()
    {
        if (! $this->questionEntryService) {
            $this->questionEntryService = new QuestionEntry();
        }
    
        return $this->questionEntryService;
    }
    
    /**
     *
     * @param \Statflow\Service\QuestionEntry $questionEntryService
     */
    public function setQuestionEntryService($questionEntryService)
    {
        $this->questionEntryService = $questionEntryService;
    }
 

    /**
     *
     * @return \Statflow\Service\feedbackFormPage
     */
    public function getFormPageService()
    {
        if (! $this->formPageService) {
            $this->formPageService = new FeedbackFormPage();
        }
        
        return $this->formPageService;
    }

    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setFormPageService($formPageService)
    {
        $this->formPageService = $formPageService;
    }

    /**
     * Initialize the controller
     */
    public function init()
    {
        $this->view->pageLayout = 'feedback_form';
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
        if ($this->_flashMessenger->setNamespace('success')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('error')->hasMessages()) {
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }
        
        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()) {
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * List the pages
     */
    public function indexAction()
    {
        $feedbackFormId = $this->_getParam('feedback_form_id', null);
        $studySvc = $this->getStudyService();

        if (!$feedbackFormId) {
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form',
                'action' => 'index'
            ), null, true));
        }

        $formPageService = $this->getFormPageService();
        $this->view->feedbackformpages = $formPageService->getAllPagesByFeedbackForm($feedbackFormId);
        $this->view->formId =  $feedbackFormId;

        $this->view->questions = $studySvc->getAllQuestion();
        $this->view->question_types = $studySvc->getArrayQuestionTypes();
        $this->view->question_type_icons = $studySvc->getArrayQuestionTypesIcons();
        $this->view->modalities = $studySvc->getArrayModalities();
    }

    /**
     * Display the creation form
     */
    public function addAction()
    {
        $formPageService = $this->getFormPageService();
        if ($this->getRequest()->isPost()) {
            if ($formPageService->isValid($this->_getAllParams())) {
                $title_is_visible = ($this->_getParam('title_is_visible', false) != FALSE) ? 1 : 0;
                $meta = Zend_Json::encode(array(
                    'condition' => isset($_POST['condition']) ? $_POST['condition'] : ''
                ));
                $order = 0;
                $_entry = $formPageService->getValidated($title_is_visible, $meta, $order, $this->_getParam('id'));
                try {
                    $_entry->save();
                    $this->_flashMessenger->setNamespace('success')
                        ->addMessage($this->view->translate('feedbackformpage_add_success@backoffice'));
                } catch (\Exception $e) {
                    $this->_flashMessenger->setNamespace('error')
                        ->addMessage($this->view->translate('feedbackformpage_add_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                }
            } else {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('feedbackformpage_add_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')
                    ->addMessage($validation->getMessages());
            }
            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form-page',
                'action' => 'index',
                'feedback_form_id' => $this->_getParam('feedback_form_id')
            ), null, true));
        }       
        $this->view->forms = $formPageService->getAllFeedbackForm();
        $this->view->form = $formPageService->getByIdFeedbackForm($this->_getParam('feedback_form_id'));
    }

    /**
     * Display the edition form
     */
    public function editAction()
    {
        $formPageService = $this->getFormPageService();
        if ($this->getRequest()->isPost()) {
             if ($formPageService->isValid($this->_getAllParams())) {
                $title_is_visible = ($this->_getParam('title_is_visible', false) != FALSE) ? 1 : 0;
                $meta = Zend_Json::encode(array(
                    'condition' => isset($_POST['condition']) ? $_POST['condition'] : ''
                ));
                $order = 0;
                $_entry = $formPageService->getValidated($title_is_visible, $meta, $order, $this->_getParam('id'));
                try {
                    $_entry->save();
                    $this->_flashMessenger->setNamespace('success')
                        ->addMessage($this->view->translate('feedbackformpage_edit_success@backoffice'));
                } catch (Exception $e) {
                    $this->_flashMessenger->setNamespace('error')
                        ->addMessage($this->view->translate('feedbackformpage_edit_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                }
            } else {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('feedbackformpage_edit_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')
                    ->addMessage($validation->getMessages());
            }

            $this->_redirect($this->view->url(array(
                'module' => 'statflow',
                'controller' => 'feedback-form-page',
                'action' => 'index',
                'feedback_form_id' => $this->_getParam('feedback_form_id')
            ), null, true));
        }
        $this->view->forms = $formPageService->getAllFeedbackForm();
        $this->view->feedbackformpage = $formPageService->getById($this->_getParam('id'));
    }

    /**
     * delete a feedback_form_page
     */
    public function deleteAction()
    {
        $formPageService = $this->getFormPageService();
        if (array_key_exists('id', $this->_getAllParams())) {
            $_entry = $formPageService->getById( $this->_getParam('id'));
            $study_id = $_entry->feedback_form->study_id;
            $feedback_form_id = $_entry->feedback_form->id;
            try {
                $_entry->delete();
                $this->_flashMessenger->setNamespace('success')
                    ->addMessage($this->view->translate('feedbackformpage_delete_success@backoffice'));
            } catch (Exception $e) {
                $this->_flashMessenger->setNamespace('error')
                    ->addMessage($this->view->translate('feedbackformpage_delete_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
            }
        } else {
            $this->_flashMessenger->setNamespace('error')
                ->addMessage($this->view->translate('feedbackformpage_delete_error@backoffice'));
        }

        $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'feedback-form-page',
            'action' => 'index',
            'feedback_form_id' => $feedback_form_id
        ), null, true));
    }

    public function reorderAction()
    {
        $formPageService = $this->getFormPageService();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        if ($this->_getParam('sort', FALSE) != FALSE) {
            $ids = explode(',', $this->_getParam('sort'));
            $pos = 0;
            foreach ($ids as $id) :
                $_entry = $formPageService->getById($id);
                $_entry->order = $pos;
                $_entry->save();
                $pos ++;
            endforeach;
            echo json_encode(array(
                'success' => true
            ));
        } else {
            echo json_encode(array(
                'success' => false
            ));
        }
    }
    
    public function duplicateAction()
    {
       
        $formPageService = $this->getFormPageService();
        $questionService = $this->getQuestionService();
        $questionEntryService = $this->getQuestionEntryService();
        $translationService = $this->getTranslationService();
    
        $page = $formPageService->getById($this->_getParam('id'));
        $orderNextPage = $formPageService->getNextOrderPageByFeedbackForm($page->feedback_form_id);
        $pageRow = $formPageService->duplicatePage($this->_getParam('id'),$orderNextPage,null);
        $idPageCloned = $pageRow->id;
       
        $questions = $questionService->getAllQuestionsByPage($this->_getParam('id'));
      
        foreach ($questions as $question) {
              $questionToCopy = $questionService->getQuestionById($question->id);
              $questionRow = $questionService->duplicateQuestion($question->id,null,$idPageCloned,null);
              $idQuestionCloned = $questionRow->id;
              $codeQuestionCloned = $questionRow->code;
              $codeQuestionOrig = $questionToCopy->code;
              if($questionRow->question_type_id == Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY){
                    $sousQuestion = $questionService->duplicateSousQuestions($question->id, $idQuestionCloned, $codeQuestionOrig,$codeQuestionCloned, null,$idPageCloned,null );
              }
    
            $modalities = $questionEntryService->duplicateModalities($question->id, $idQuestionCloned);
        }
    
       $translationService->generateTranslationFiles();
        
       $this->_redirect($this->view->url(array(
            'module' => 'statflow',
            'controller' => 'feedback-form-page',
            'action' => 'index',
            'feedback_form_id' => $page->feedback_form->id
        ), null, true));
         
    }
}

