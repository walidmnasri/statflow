<?php
require_once __DIR__ . '/../services/User.php';
require_once __DIR__ . '/../services/Structure.php';
require_once __DIR__ . '/../services/StructureUser.php';
require_once __DIR__ . '/../services/Study.php';
require_once __DIR__ . '/../services/StudyUser.php';
require_once __DIR__ . '/../services/Group.php';
require_once __DIR__ . '/../services/FeedbackForm.php';
require_once __DIR__ . '/../services/UserFeedbackForm.php';
require_once __DIR__ . '/../services/Parameter.php';
require_once __DIR__ . '/../services/FrontItem.php';
require_once __DIR__ . '/../services/UserFrontItem.php';

use Statflow\Service\User;
use Statflow\Service\Structure;
use Statflow\Service\StructureUser;
use Statflow\Service\Group;
use Statflow\Service\StudyUser;
use Statflow\Service\Study;
use Statflow\Service\FeedbackForm;
use Statflow\Service\UserFeedbackForm;
use Statflow\Service\Parameter;
use Statflow\Service\FrontItem;
use Statflow\Service\UserFrontItem;

class Statflow_UserController extends Centurion_Controller_Action
{
   
    private $userService;
    private $structureService;
    private $structureUserService;
    private $studyService;
    private $studyUserService;
    private $groupService;
    private $feedbackFormService;
    private $userFeedbackFormService;
    private $frontItemService;
    private $userFrontItemService;
    
    /**
     *
     * @var parameter
     */
    private $parameterService;


    /**
     *
     * @return \Statflow\Service\User
     */
    public function getUserService()
    {
        if (! $this->userService) {
            $this->userService = new User();
        }
        return $this->userService;
    }
    
    /**
     *
     * @param \Statflow\Service\User $userService
     */
    public function setUserService($userService)
    {
        $this->userService = $userService;
    }
    
    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
        return $this->structureService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }
    
    /**
     *
     * @return \Statflow\Service\Study
     */
    public function getStudyService()
    {
        if (! $this->studyService) {
            $this->studyService = new Study();
        }
        return $this->studyService;
    }
    
    /**
     *
     * @param \Statflow\Service\Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    }
    
    /**
     *
     * @return \Statflow\Service\StructureUser
     */
    public function getStructureUserService()
    {
        if (! $this->structureUserService) {
            $this->structureUserService = new StructureUser();
        }
        return $this->structureUserService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureUserService
     */
    public function setStructureUserService($structureUserService)
    {
        $this->structureUserService = $structureUserService;
    }
    
    /**
     *
     * @return \Statflow\Service\StudyUser
     */
    public function getStudyUserService()
    {
        if (! $this->studyUserService) {
            $this->studyUserService = new StudyUser();
        }
        return $this->studyUserService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureUserService
     */
    public function setStudyUserService($studyUserService)
    {
        $this->studyUserService = $studyUserService;
    }
    
    /**
     *
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (! $this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
        return $this->feedbackFormService;
    }
    
    /**
     *
     * @param \Statflow\Service\FeedbackForm $feedbackFormService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }
    
    /**
     *
     * @return \Statflow\Service\UserFeedbackForm
     */
    public function getUserFeedbackFormService()
    {
        if (! $this->userFeedbackFormService) {
            $this->userFeedbackFormService = new UserFeedbackForm();
        }
        return $this->userFeedbackFormService;
    }
    
    /**
     *
     * @param \Statflow\Service\UserFeedbackForm $userFeedbackFormService
     */
    public function setUserFeedbackFormService($userFeedbackFormService)
    {
        $this->userFeedbackFormService = $userFeedbackFormService;
    }
    
    /**
     *
     * @return \Statflow\Service\Group
     */
    public function getGroupService()
    {
        if (! $this->groupService) {
            $this->groupService = new Group();
        }
        return $this->groupService;
    }
    
    /**
     *
     * @param \Statflow\Service\Group $groupService
     */
    public function setGroupService($groupService)
    {
        $this->groupService = $groupService;
    }
    
    /**
     *
     * @return \Statflow\Service\Parameter
     */
    public function getParameterService()
    {
        if (! $this->parameterService) {
            $this->parameterService = new Parameter();
        }
        return $this->parameterService;
    }
    /**
     *
     * @param \Statflow\Service\Parameter $parameterService
     *
     */
    public function setParameterService($parameterService)
    {
        $this->parameterService = $parameterService;
    }

    /**
     *
     * @return \Statflow\Service\FrontItem
     */
    public function getFrontItemService()
    {
        if (! $this->frontItemService) {
            $this->frontItemService = new FrontItem();
        }
        return $this->frontItemService;
    }
    
    /**
     *
     * @param \Statflow\Service\FrontItem $frontItemService
     */
    public function setFrontItemService($frontItemService)
    {
        $this->frontItemService = $frontItemService;
    }
    
    /**
     *
     * @return \Statflow\Service\UserFrontItem
     */
    public function getUserFrontItemService()
    {
        if (! $this->userFrontItemService) {
            $this->userFrontItemService = new UserFrontItem();
        }
        return $this->userFrontItemService;
    }
    
    /**
     *
     * @param \Statflow\Service\UserFrontItem $userFrontItemService
     */
    public function setUserFrontItemService($userFrontItemService)
    {
        $this->userFrontItemService = $userFrontItemService;
    }
    
    /**
     * Initialize the User controller
     */
    public function init()
    {
        $this->view->pageLayout = 'user';
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_flashMessenger->setNamespace('success')->hasMessages()){
            $this->view->flashMessageType = $this->view->translate('statflow__message_success@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('error')->hasMessages()){
            $this->view->flashMessageType = $this->view->translate('statflow__message_error@backoffice');
            $_messages = $this->_flashMessenger->getMessages();
            $this->view->flashMessage = $_messages[0];
        }

        if ($this->_flashMessenger->setNamespace('validation')->hasMessages()){
            $_errors = $this->_flashMessenger->getMessages();
            $this->view->validationErrors = $_errors[0];
        }
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('statflow');
    }

    /**
     * List the user
     */
    public function indexAction()
    {
        $userService = $this->getUserService();
        $this->view->users = $userService->getAll();
        
        $export = $userService->setExport($this->_getParam('export', false));
        $userService->exportUser();
        
        $this->view->actionPage = 'user';
        
        //import csv des utilisateurs
        //Recupère les données loguées après import
        $this->view->importResult = $userService->getImportResult();
        
        // Utilisateur log file
        $this->view->importResultInsert =$this->view->importResult[0][0];
        $this->view->importResultUpdate =$this->view->importResult[0][1];
        $this->view->importResultErrors =$this->view->importResult[0][2];
        $this->view->importResultErrorLines = ($this->view->importResultErrors > 0) ? $this->view->importResult[1][0] : "";
        
    }

    public function activateAction()
    {
        $userService = $this->getUserService();
        $studyService = $this->getStudyService();
        // get email par defaut
        $parameterService = $this->getParameterService();
        $parameters = $parameterService->getEmailParameters();
        $rowUser = $userService->getById($this->_getParam('user_id'));
        $rowUser->token = sha1($rowUser->salt . $rowUser->email . time());
        $group_id = $userService->getAllAuthBelong($this->_getParam('user_id'))->current()->group_id;
        $rowUser->is_active = 1;
        $rowUser->save();

        $urlLink = $this->view->serverUrl() . $this->view->url(array('module' => 'statflow', 'controller' => 'auth', 'action' => 'do-reset', 'token' => $rowUser->token));
        // get info from study by iduser
        $studyInfo = $studyService->getAllStudyByUser($this->_getParam('user_id'));
        $userService->sendMail($rowUser->email, 'Setting your Statflow password', $urlLink, $this->_getParam('user_id'), $group_id, $studyInfo, $parameters);
        $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index'), null, true));
    }

    /**
     * Display the creation formula
     * Manage the insert
     */
    public function addAction()
    {   
        $userService = $this->getUserService();
        $structureService = $this->getStructureService();
        $structureUserService = $this->getStructureUserService();
        $studyService = $this->getStudyService();
        $studyUserService = $this->getStudyUserService();
        $groupService = $this->getGroupService();
        $feedbackFormService = $this->getFeedbackFormService();
        $userFeedbackFormService = $this->getUserFeedbackFormService();
        
        if ($this->getRequest()->isPost())
        {
           if ($userService->isValid($this->_getAllParams(),'user_add')) {
                
                $can_be_deleted = ($this->_getParam('can_be_deleted', false) != FALSE) ? 1 : 0;
                $is_active = ($this->_getParam('is_active', false) != FALSE) ? 1: 0;
                $is_super_admin = ($this->_getParam('is_super_admin', false) != FALSE) ? 1 : 0;
                $is_staff = ($this->_getParam('is_staff', false) != FALSE) ? 1 : 0 ;
                $groupId = $this->_getParam('group_id');
                $clientStudyId = $this->_getParam('client_study_id');
                $mdp = $_POST['password'];
               
                try
                {
                  //User creation
                  $_entry = $userService->getValidated($mdp, $can_be_deleted, $is_active, $is_super_admin,$is_staff,$this->_getParam('id'));
                  
                  $_entry->save();
                  //Profile creation
                  $userService->saveProfile($_entry->id);

                  //User belong creation
                  $userService->saveUserBelong($_entry->id, $groupId);
                   // User structures creation
                   if ($this->_getParam('structure_user[]', false)) {
                        foreach($this->_getParam('structure_user') as $structure_id):
                             $structureUserService->saveStructureUser($_entry->id, $structure_id);
                        endforeach;
                   }
                    
                    // User study creation          
                    if ($groupId == 1) {
                        if ($this->_getParam('study_admin', false)) {
                            foreach($this->_getParam('study_admin') as $study_id) {
                                $studyUserService->saveStudyUser($_entry->id, $study_id);
                            }
                        }
                    } elseif ($groupId == 2 && !empty($clientStudyId)) {
                            $studyUserService->saveStudyUser($_entry->id, $clientStudyId);
                    };                   
                    
                    // User Questionnaire creation
                    if ($this->_getParam('user_feedbackform', false)) {
                        foreach($this->_getParam('user_feedbackform') as $feedbackform_id) {
                            $userFeedbackFormService->saveUserFeedbackForm($_entry->id, $feedbackform_id);
                        }
                    }
                                        
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__user_add_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
                } catch(Exception $e) {
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__user_add_flashMessenger_error@backoffice') . '<br><br>' .$e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'add')));
                }
            } else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__user_add_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($userService->getValidator($this->_getAllParams(),'user_add')->getMessages());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'add')));
            }
        }

        $this->view->groups = $groupService->getAllGroup();
        $this->view->client_studies = $studyService->getAll();
        $this->view->all_json_structures = array();
        foreach($structureService->getAll() as $structure)
            $this->view->all_json_structures[] = ['id'=> $structure->id, 'text'=>$structure->id.': '.$structure->name];
        
        $this->view->all_json_studies = array();
        foreach ($studyService->getAll() as $study)
            $this->view->all_json_studies[] = ['id'=> $study->id, 'text'=>$study->id.': '.$study->name];
        
    }

    /**
     * Update questionnaire list
     *
     */
    public function updateAction()
    {
         $feedbackFormService = $this->getFeedbackFormService();
         
         $this->_helper->layout()->disableLayout();
         $this->_helper->viewRenderer->setNoRender(true);
         
         $study_id = $this->_getParam('client_study_id');

         $this->view->all_json_questionnaires = array();
         foreach($feedbackFormService->getAllStructuresById($study_id) as $questionnaire) 
             $this->view->all_json_questionnaires[] = ['id'=> $questionnaire->id, 'text' => $questionnaire->id.': '.$questionnaire->name];
         print_r(Zend_Json::encode($this->view->all_json_questionnaires));

    }
    
    /**
     * Display the edition formula
     * Manage the update
     */
    public function editAction()
    {        
        $userService = $this->getUserService();
        $structureService = $this->getStructureService();
        $studyService = $this->getStudyService();
        $structureUserService = $this->getStructureUserService();
        $studyUserService = $this->getStudyUserService();
        $groupService = $this->getGroupService();
        $feedbackFormService = $this->getFeedbackFormService();
        $userFeedbackFormService = $this->getUserFeedbackFormService();
        
        if ($this->getRequest()->isPost())
        {
          if ($userService->isValid($this->_getAllParams(),'user_edit')) {                
                              
                $can_be_deleted = ($this->_getParam('can_be_deleted', false) != FALSE) ? 1 : 0;
                $is_active = ($this->_getParam('is_active', false) != FALSE) ? 1: 0;
                $is_super_admin = ($this->_getParam('is_super_admin', false) != FALSE) ? 1 : 0;
                $is_staff = ($this->_getParam('is_staff', false) != FALSE) ? 1 : 0 ;
                $groupId = $this->_getParam('group_id');                        
                $clientStudyId = $this->_getParam('client_study_id');
                
                try
                {
                    // User update
                    $_entry = $userService->getValidated(null, $can_be_deleted, $is_active, $is_super_admin,$is_staff,$this->_getParam('id'));                     
                    
                    // User profile update
                    $profile = $userService->saveProfile();
                    
                    // User belong update
                    foreach ($userService->getAllAuthBelong($_entry->id)as $authBelong) {
                              $authBelong->delete();
                    }
                    $_group = $userService->saveUserBelong($_entry->id,$groupId);
                    
                    // User structures update
                    foreach ($structureUserService->getAllStructureByUserId($_entry->id) as $structureUser) :
                      $structureUser->delete();
                    endforeach;
                    
                    $postStructures = $this->_getParam('structure_user', false);
                    if ($postStructures) {
                         foreach ($postStructures as $structure_id) :
                              $structureUserService->saveStructureUser($_entry->id, $structure_id);
                         endforeach;
                     }

                     // User studies update
                     foreach ($studyUserService->getAllStudyByUserId($_entry->id) as $studyUser) :
                       $studyUser->delete();
                     endforeach;
                     
                     if ($groupId == 1){
                         $postStudies = $this->_getParam('study_admin', false);
                         if ($postStudies) {
                             foreach ($postStudies as $study_id) :
                                $studyUserService->saveStudyUser($_entry->id, $study_id);
                             endforeach;
                         }
                     }  elseif ($groupId == 2 && !empty($clientStudyId)) {
                            $studyUserService->saveStudyUser($_entry->id,$clientStudyId);
                     };
                     
                     // User feedbackform update
                     foreach ($userFeedbackFormService->getAllFeedbackFormByUserId($_entry->id) as $userFeedbackForm) :
                       $userFeedbackForm->delete();
                     endforeach;
                     
                     if ($this->_getParam('user_feedbackform', false)) {
                         foreach ($this->_getParam('user_feedbackform') as $feedbackform_id) :
                            $userFeedbackFormService->saveUserFeedbackForm($_entry->id,$feedbackform_id);
                         endforeach;
                     }   
                                                          
                    $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__user_edit_flashMessenger_success@backoffice'));
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'user',
                        'action' => 'index'
                    )));
                 } catch (Exception $e) {
                     $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__user_edit_flashMessenger_error@backoffice') . '<br><br>' . $e->getCode() . ': ' . $e->getMessage());
                    $this->_redirect($this->view->url(array(
                        'module' => 'statflow',
                        'controller' => 'user',
                        'action' => 'edit'
                    )));
                }
            }
            else {
                $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__user_edit_flashMessenger_error@backoffice'));
                $this->_flashMessenger->setNamespace('validation')->addMessage($userService->getValidator($this->_getAllParams(),'user_edit')->getMessages());
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'edit')));
            }
        }
        

        $this->view->groups = $groupService->getAllGroup();
        
        $this->view->client_studies = $studyService->getAll();

        $this->view->user = $userService->getById($this->_getParam('id'));

        $this->view->associated_json_structures = array();
        foreach($this->view->user->structures as $structure) $this->view->associated_json_structures[] = ['id'=> $structure->id, 'text'=>$structure->id.': '.$structure->name];

        $this->view->all_json_structures = array();
        foreach($structureService->getAll() as $structure) $this->view->all_json_structures[] = ['id'=> $structure->id, 'text'=>$structure->id.': '.$structure->name];

        $this->view->associated_json_studies = array();
        foreach($this->view->user->studies as $study) $this->view->associated_json_studies[] = ['id'=> $study->id, 'text'=>$study->id.': '.$study->name];
        
        $this->view->all_json_studies = array();
        foreach($studyService->getAll() as $study) $this->view->all_json_studies[] = ['id'=> $study->id, 'text'=>$study->id.': '.$study->name];

        $this->view->associated_json_questionnaires = array();
        foreach($this->view->user->questionnaires as $questionnaire) $this->view->associated_json_questionnaires[] = ['id'=> $questionnaire->id, 'text' => $questionnaire->id.': '.$questionnaire->name];
        
        //$this->view->all_json_questionnaires = array();      
        //foreach($feedbackFormService->getAll() as $questionnaire) $this->view->all_json_questionnaires[] = $questionnaire->id.': '.$questionnaire->name;
        
    }

    /**
     * Manage the deletion
     */
    public function deleteAction()
    {
        if ($this->getRequest()->isGet())
        {
            $userService = $this->getUserService();
            $studyUserService = $this->getStudyUserService();
            $userFeedbackFormService = $this->getUserFeedbackFormService();
            try
            {
                $userService->deleteUser($this->_getParam('id'));
                $studyUserService->deleteStudyByUserId($this->_getParam('id'));
                $userFeedbackFormService->deleteByUserId($this->_getParam('id'));
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__user_delete_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
            } catch(Exception $e) {
                $this->_flashMessenger->setNamespace('error')->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('navigation__statflow__user_delete_flashMessenger_error@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
            }
        }
    }

    /**
     * Manage the activation of selected users
     */
    public function multiselectAction()
    {
        $userService = $this->getUserService();
        $parameterService = $this->getParameterService();
        $parameters = $parameterService->getEmailParameters();
        
        if ($this->getRequest()->isPost()) {
            $post = $this->_request->getPost();
    
            if (isset($post['multiSelectBox'])) {
                $multiSelectBoxStr = implode(",",$post['multiSelectBox']);
            }

            if (isset($multiSelectBoxStr)) {
                if (isset($post['selection'])) {
                    if($post['selection'] == "0") {
                        //Suppression des utilisateurs
                        $userFeedbackFormService = $this->getUserFeedbackFormService();
                        $studyUserService = $this->getStudyUserService();
                        try {
                            $studyUserService->deleteByListUserId($multiSelectBoxStr);
                            $userFeedbackFormService->deleteByListUserId($multiSelectBoxStr);
                            $userService->deleteByListUserId($multiSelectBoxStr);
    
                            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__multi_select_delete_flashMessenger_success@backoffice'));
                        } catch(Exception $e) {
                            $this->_flashMessenger->setNamespace('error')->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('navigation__statflow__multi_select_delete_flashMessenger_error@backoffice'));
                        }
                    } else {
                        //Activation des utilisateurs
                        $studyService = $this->getStudyService();
    
                        try {
                            foreach ($post['multiSelectBox'] as $user_id) {
                                $rowUser = $userService->getById($user_id);
                                $rowUser->token = sha1($rowUser->salt . $rowUser->email . time());
                                $group_id = $userService->getAllAuthBelong($user_id)->current()->group_id;
                                $rowUser->is_active = 1;
                                $rowUser->save();
                                $urlLink = $this->view->serverUrl() . $this->view->url(array('module' => 'statflow', 'controller' => 'auth', 'action' => 'do-reset', 'token' => $rowUser->token));
                                $studyInfo = $studyService->getAllStudyByUser($user_id);
                                $userService->sendMail($rowUser->email, 'Setting your Statflow password', $urlLink, $user_id, $group_id, $studyInfo, $parameters);
                            }
                        
                            //$userService->activateByListUserId($multiSelectBoxStr);
                            $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__multi_select_activate_flashMessenger_success@backoffice'));
                        } catch(Exception $e) {
                            $this->_flashMessenger->setNamespace('error')->addMessage($e->getCode() . ': ' . $e->getMessage() . $this->view->translate('navigation__statflow__multi_select_activate_flashMessenger_error@backoffice'));
                        }
                    }
                }
            }
        }
        $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
    }

    /**
     * Masquer les onglets pour un user client
     */
    public function hideAction()
    {
        $frontItemService = $this->getFrontItemService();
        $userFrontItemService = $this->getUserFrontItemService();
        
        if ($this->getRequest()->isPost())
        {
            $user_id = $this->_getParam('id');
        
            try
            {
                // front items
                //delete all front items for this user
                $userFrontItemService->deleteFrontItemByUserId($user_id);
        
                if(!empty($_POST['sel_front_item'])){
                    $postFrontItems = $_POST['sel_front_item'];
                    foreach($postFrontItems as $front_item_id):
                        $filters = $userFrontItemService->saveUserFrontItems($user_id, $front_item_id);
                    endforeach;
                }
        
                $this->_flashMessenger->setNamespace('success')->addMessage($this->view->translate('navigation__statflow__user_hide_flashMessenger_success@backoffice'));
                $this->_redirect($this->view->url(array('module' => 'statflow',
                                                        'controller' => 'user',
                                                        'action' => 'index')));
            } catch(Exception $e) {
                  $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__user_hide_flashMessenger_error@backoffice') . '<br><br>' .$e->getCode() . ': ' . $e->getMessage());
                  $this->_redirect($this->view->url(array('module' => 'statflow',
                                                          'controller' => 'user',
                                                          'action' => 'edit')));
            }
        
        }
         
        $this->view->front_items = $frontItemService->getAll();
        $this->view->chosen_items = $userFrontItemService->getFrontItemIDByUserId($this->_getParam('id'));
    }


    /**
     * Import Utilisateur
     */
    public function importAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $userService = $this->getUserService();
        $studyService =  $this->getStudyUserService();
        $userFeedBackFormService =  $this->getUserFeedbackFormService();
        $userStructService =  $this->getStructureUserService();
        $userFrontItemService = $this->getUserFrontItemService();
        
        echo "file".$_FILES['fileInput']['name'];
    
        //die('mort');
    
        $count_insert=0;
        $count_update=0;
        $count_error=0;
        $line_index=1;
        $line_error="";
         
        if ( 0 < $_FILES['fileInput']['error'] ) {
            echo 'Error: ' . $_FILES['fileInput']['error'] . '<br>';
        }
        else {
            $csv_file = '/tmp/' . $_FILES['fileInput']['name'];
            //$pathTmp = APPLICATION_PATH . '/tmp/';
            //if (! Sayyes_Tools_Filesystem::directoryExist($pathTmp)) {
            //    chmod($pathTmp,0777);
                //   Sayyes_Tools_Filesystem::createdir($pathTmp);
    
                //}
                //chmod($csv_file,0777);
                move_uploaded_file($_FILES['fileInput']['tmp_name'], $csv_file);
    
    
                $csv_utilisateur_array = $userService->readCSV($csv_file);
                //var_dump($csv_utilisateur_array);
                //die('stop');
    
                if($csv_utilisateur_array){
                    echo "...";
                    foreach($csv_utilisateur_array as $csv_utilisateur){
                        $user = $userService->importCsvUtilisateur($csv_utilisateur);
    
                        if($userService->getImportStatus() == 2) {
                            $line_error = ($count_error==0) ? "[".$line_index."]" : $line_error." [".$line_index."]";
                            $count_error++;
                        }else{
                            if($userService->getImportStatus() == 1) {
                                $count_update++;
                            }elseif($userService->getImportStatus() == 0) {
                                $count_insert++;
                            }
                            $user->save();
    
                            $userId = $user->id;
    
                            //get id for new user
                            if($userService->getImportStatus() == 0) {
                                $userId = $user->id;
    
                                // save profile for new user
                                $userService->saveProfilImport($userId);
                                //die('new'.$newuser);
                            }
    
                            $groupService = $this->getGroupService();
                            // get group id through group name $csv_utilisateur[4]
    
                            foreach($groupService->getGroupIdByName($csv_utilisateur[5]) as $groupBelong){
                                $groupId = $groupBelong->id;
                            }
    
                            // User belong update
                            foreach ($userService->getAllAuthBelong($userId)as $authBelong) {
                                $authBelong->delete();
                            }
                            $_group = $userService->saveUserBelong($userId,$groupId);
    
                            // delete all entries from table study_user
                            $studyService->deleteStudyByUserId($userId);
                            
                            // delete entries from table user_feedback_form
                            $userFeedBackFormService->deleteByUserId($userId);
                            
                            // Column Study Id
                            if ($csv_utilisateur[8] != ""){
                                $strStudyId = $csv_utilisateur[8];
                                $arrStudyId = explode("-", $strStudyId);
                                
                                // Save study by user
                                foreach ($arrStudyId as $studyId){
                                    $studyService->saveStudyUser($userId, $studyId);
                                }
                                
                                // Column FeedBackForm Id
                                if($csv_utilisateur[9] != "") {
                                    $strFeedBackFormId = $csv_utilisateur[9];
                                    $arrFeedBackFormId = explode("-", $strFeedBackFormId);
                                    
                                    foreach($arrFeedBackFormId as $feedBackFormId) {
                                        $userFeedBackFormService->saveUserFeedbackForm($userId, $feedBackFormId);
                                    }
                                    
                                }
                            }
                            
                            
                            $userStructService->deleteStructureByUserId($userId);
                            // Column Structure Id
                            if ($csv_utilisateur[10] != ""){
                                $arrStructureId = explode("-", $csv_utilisateur[10]);
                                foreach ($arrStructureId as $structureId){
                                    $userStructService->saveStructureUser($userId, $structureId);
                                }
                            }
                            
                            //delete all entries from table user front item
                            $userFrontItemService->deleteFrontItemByUserId($userId);
                           
                            if ($csv_utilisateur[11] != ""){
                                $frontItems = explode("-", $csv_utilisateur[11]);
                                foreach ($frontItems as $item){
                                    $userFrontItemService->saveUserFrontItems($userId, $item);
                                }
                            }       
    
                        }
                        $line_index++;
                    }
                    $log_text = $count_insert.",".$count_update.",".$count_error."\n".$line_error;
                    //chmod('/tmp/import_utilisateur_log.txt',0777);
                    if(file_exists('/tmp/import_user_log.txt'))
                        unlink('/tmp/import_user_log.txt');
                    
                    file_put_contents('/tmp/import_user_log.txt', $log_text, FILE_APPEND);
    
                }else{
                    echo "erreur format";
    
                    $this->_flashMessenger->setNamespace('error')->addMessage($this->view->translate('navigation__statflow__user__index__import_error_format@backoffice'));
                    //$this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
                }
    
                unlink('/tmp/' . $_FILES['fileInput']['name']);
    
                $this->_redirect($this->view->url(array('module' => 'statflow', 'controller' => 'user', 'action' => 'index')));
            }
        }
    

}
