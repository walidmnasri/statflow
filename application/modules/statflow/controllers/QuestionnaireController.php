<?php
require_once __DIR__ . '/../services/Questionnaire.php';
require_once __DIR__ . '/../services/FeedbackForm.php';
require_once __DIR__ . '/../services/XmlConfigSolr.php';
require_once __DIR__ . '/../services/FiltersKpi.php';
require_once __DIR__ . '/../services/RespondentFieldHidden.php';
require_once __DIR__ . '/../../statflow/services/NominativeFieldHidden.php';
require_once __DIR__ . '/../services/Structure.php';

use Statflow\Service\Questionnaire;
use Statflow\Service\FeedbackForm;
use Statflow\Service\XmlConfigSolr;
use Statflow\Service\FiltersKpi;
use Statflow\Service\RespondentFieldHidden;
use Statflow\Service\NominativeFieldHidden;
use Statflow\Service\Structure;

class Statflow_QuestionnaireController extends Centurion_Controller_Action
{

    /**
     *
     * @var Questionnaire
     */
    private $questionnaireService;
    
    /**
     *
     * @var feedbackForm
     */
    private $feedbackFormService;
    
    /**
     *
     * @var XmlConfigSolr
     */
    private $xmlConfigSolrService;
    /**
     *
     * @var FiltersKpi
     */
    private $filtersKpiService;
    
    /**
     *
     * @var RespondentFieldHidden
     */
    private $respondentFieldHiddenService;
    
    /**
     *
     * @var NominativeFieldHidden
     */
    private $nominativeFieldHiddenService;

    /**
     *
     * @var Structure
     */
    private $structureService;
        
    
    /**
     * @return the $nominativeFieldHiddenService
     */
    public function getNominativeFieldHiddenService()
    {
        if (! $this->nominativeFieldHiddenService) {
            $this->nominativeFieldHiddenService = new NominativeFieldHidden();
        }
        return $this->nominativeFieldHiddenService;
    }
    
    /**
     * @param NominativeFieldHidden $nominativeFieldHiddenService
     */
    public function setNominativeFieldHiddenService($nominativeFieldHiddenService)
    {
        $this->nominativeFieldHiddenService = $nominativeFieldHiddenService;
    }
    
    
    /**
     * @return \Statflow\Service\Questionnaire
     */
    public function getQuestionnaireService()
    {
        if (!$this->questionnaireService){
            $this->questionnaireService = new Questionnaire();
        }
    
        return $this->questionnaireService;
    }
    
    /**
     * @param \Statflow\Service\Questionnaire $questionnaireService
     */
    public function setQuestionnaireService($questionnaireService)
    {
        $this->questionnaireService = $questionnaireService;
    }
    
    /**
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (!$this->feedbackFormService){
            $this->feedbackFormService = new FeedbackForm();
        }
    
        return $this->feedbackFormService;
    }
    
    /**
     * @param \Statflow\Service\Structure $structureService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }

    /**
     *
     * @return \Statflow\Service\XmlConfigSolr
     */
    public function getXmlConfigSolrService()
    {
        if (! $this->xmlConfigSolrService) {
            $this->xmlConfigSolrService = new XmlConfigSolr();
        }
    
        return $this->xmlConfigSolrService;
    }
    
    /**
     *
     * @param \Statflow\Service\XmlConfigSolr $xmlConfigSolrService
     */
    public function setXmlConfigSolrService($xmlConfigSolrService)
    {
        $this->xmlConfigSolrService = $xmlConfigSolrService;
    }

    /**
     *
     * @return \Statflow\Service\FiltersKpi
     */
    public function getFiltersKpiService()
    {
        if (! $this->filtersKpiService) {
            $this->filtersKpiService = new FiltersKpi();
        }
    
        return $this->filtersKpiService;
    }
    
    /**
     *
     * @param \Statflow\Service\FiltersKpi $filtersKpiService
     */
    public function setFiltersKpiService($filtersKpiService)
    {
        $this->filtersKpiService = $filtersKpiService;
    }
    
    /**
     *
     * @return \Statflow\Service\RespondentFieldHidden
     */
    public function getRespondentFieldHiddenService()
    {
        if (! $this->respondentFieldHiddenService) {
            $this->respondentFieldHiddenService = new RespondentFieldHidden();
        }
    
        return $this->respondentFieldHiddenService;
    }
    
    /**
     *
     * @param \Statflow\Service\RespondentFieldHidden $respondentFieldHiddenService
     */
    public function setRespondentFieldHiddenService($respondentFieldHiddenService)
    {
        $this->respondentFieldHiddenService = $respondentFieldHiddenService;
    }    
    
    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
        return $this->structureService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }
        
    /**
     * 
     * @see Centurion_Controller_Action::preDispatch()
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $this->_helper->layout()->setLayout('form_preview');
    }

    public function formAction()
    {

        $questionnaire       = $this->getQuestionnaireService();
        $feedbackFormService = $this->getFeedbackFormService();
        $structureService    = $this->getStructureService();

        $formId = (int) $this->_getParam('id', null);

        $form = $questionnaire->getFormById($formId);
        $token = $questionnaire->setToken($this->_getParam('token', false));
        $data = $questionnaire->gestionErreur($formId);
        $structure = $feedbackFormService->getStructuresByIdFeedback($formId);

        $feedbackFormService            = $this->getFeedbackFormService();
        $nominativeFieldHiddenService   = $this->getNominativeFieldHiddenService();

        if(!empty($data['store']['name']))
            $data['store']['name'] = $this->view->translate($data['store']['name']);

        if (!empty($data['error'])) {
            $this->view->errors = $data['error'];
            $this->render('error');
        } elseif($form->is_open == true && $this->_getParam('token', false) == false) {

            $feedbackFormStructureId = $feedbackFormService->getStructureIDByFeedbackFormId($formId);
            $stid = $this->_getParam('stid', null);
            
            if (is_null($stid)){
                $structure_belonging = $structure;
            }
            else {
                if (in_array($stid, $structureService->getAllChildStructuresById($feedbackFormStructureId))){
                    $structure_belonging = $structureService->getExternalIdbyStructureId($stid);
                }
                else {
                    $this->view->errors = $this->view->translate('form__'.($form->id).'__bad_structure_id-text@Form_'.$form->id);
                    $this->render('error');
                }
            }

            $respondent = Centurion_Db::getSingleton('statflow/respondent')->createRow();
            $token = sha1(uniqid());
            $respondent->token = $token;
            $respondent->status = 0;
            $respondent->feedback_form_id = $form->id;
            $respondent->study_id = $form->study->id;
            $respondent->email = "form__{$form->id}__email__open__$token";
            $respondent->firstname = "form__{$form->id}__firstname__open__$token";
            $respondent->lastname = "form__{$form->id}__lastname__open__$token";
            $respondent->external_id = "form__{$form->id}__external_id__open__$token";
            $respondent->structure_belonging = $structure_belonging;
            try {
                $respondent->save();
            } catch (\Exception $e) {
                die($e->getMessage());
            }

            $importFieldParameters = $feedbackFormService->getFeedbackFormParameterExternalField($form->id);

            $parameterURL = "";
            $url = $this->view->url(array('token'=>$respondent->token));

            if (count($importFieldParameters) > 0){
                foreach ($importFieldParameters as $importFieldParameter){
                    if ($this->_hasParam($importFieldParameter['label'])){                        
                        $value = $this->_getParam($importFieldParameter['label']);
                        $feedbackFormService->insertRespondentExternalField($respondent->id, $importFieldParameter['id'], $importFieldParameter['label'], $value);
                        $parameterURL = "/".rawurlencode($importFieldParameter['label'])."/".rawurlencode($value);
                        $url = str_replace($parameterURL, "", $url);
                    }              
                }
            }          

            $this->_redirect($url);
            exit();
        } else {
            
            $pages = $questionnaire->getAllPageByForm($form);

            $data['form']['introduction_text'] = $this->view->translate('form__'.($form->id).'__introduction-text@Form_'.$form->id);
            $data['form']['end_text'] = $this->view->translate('form__'.($form->id).'__end-text@Form_'.$form->id);
            $data['pages'] = array();
            foreach ($pages as $page) {
                if($page->meta) $page->meta = Zend_Json::decode($page->meta);
                $pageData = $page->toArray();
                $pageData['name'] = $this->view->translate('form__page_name-'.$page->id.'@form_'.$form->id);
                $pageData['title_is_visible'] = $pageData['title_is_visible'] == "1";

                $pageData['children'] = array();
                $questions = $questionnaire->getAllPageByQuestion($page);
                //var_dump ($questions);
                
                foreach($questions as $question){
                    if($question->meta) $question->meta = Zend_Json::decode($question->meta);
                    $pageData['children'][$question->order] = $question->toArray();
                    $pageData['children'][$question->order]['content'] = $this->view->translate('form__'.$form->id.'__question_title-'.$question->code.'@Form_'.$form->id);
                    $pageData['children'][$question->order]['entries'] = array();
                    $pageData['children'][$question->order]['helps'] = array();

                    $help = array();
                    $helpText = $this->view->translate('form__'.$form->id.'__question_help-'.$question->code.'@Form_'.$form->id);
                    $help['content'] = ($helpText == 'form__'.$form->id.'__question_help-'.$question->code) ? '' : $helpText;
                    $pageData['children'][$question->order]['helps'][] = $help;

                    if($question->question_type_id == Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_ARRAY){
                        $pageData['children'][$question->order]['subquestions'] = array();
                        $subquestions = $questionnaire->getAllSubQuestion($question);
                        foreach($subquestions as $sq){

                            if($sq->meta) $sq->meta = Zend_Json::decode($sq->meta);
                            $entryData = $sq->toArray();
                            $entryData['content'] = $sq->code ? $this->view->translate('form__'.$form->id.'__question_title-'.$sq->code.'@Form_'.$form->id) : $this->view->translate('form__question_title-missing-code@Backoffice');
                            $pageData['children'][$question->order]['subquestions'][$sq->id] = $entryData;
                        }
                    }

                    $entries = $questionnaire->getAllQuestionEntry($question);
                    foreach($entries as $entry){
                        if ($entry->complement_data)
                            $entry->complement_data = Zend_Json::decode($entry->complement_data);

                        $entryData = $entry->toArray();
                        $entryData['content'] = $entry instanceof Statflow_Model_DbTable_Row_Question ?  $this->view->translate('form__'.$form->id.'__question_title-'.$entry->code.'@Form_'.$form->id) : $this->view->translate('form__question_'.$question->code.'_modality-'.$entry->id.'@Form_'.$form->id);
                        $pageData['children'][$question->order]['entries'][$entry->id] = $entryData;
                    }
                }
                $data['pages'][] = $pageData;
            }

            if ($form->meta) {
                $meta = Zend_Json::decode($form->meta);
                $this->view->formStyle = $meta['style'];
            }            

            $this->view->jsonData = Zend_Json::encode($data);
            $this->view->form = $form;
            $this->view->availableLanguages = Centurion_Db::getSingleton('translation/language')->fetchAll();

        }
        if ($this->_getParam('language', false) == false){
            $lang = "en";
        }
        else {
            $lang = $this->_getParam('language');
        }
        $this->view->language = Centurion_Db::getSingleton('translation/language')->fetchRow('`language` = "' . $lang . '"');
    }

    public function savePageAction()
    {
        $last_page = $this->_getParam('last_page', 0);
        $data = $this->_getParam('current_state');
        $formId = intval($this->_getParam('id', null));
        
        $questionnaire = $this->getQuestionnaireService();
        $form   = $questionnaire->getFormById($formId);
        $token  = $questionnaire->setToken($this->_getParam('token', null));
        $questionnaire->savePage($last_page, $data, $formId, $token);
    }

    public function saveAction()
    {    
        $questionnaire                  = $this->getQuestionnaireService();
        $conf_solr                      = $this->getXmlConfigSolrService();
        $respondentFieldHiddenService   = $this->getRespondentFieldHiddenService();
        $feedbackFormService            = $this->getFeedbackFormService();
        $nominativeFieldHiddenService   = $this->getNominativeFieldHiddenService();
        
        $filtersKpiService              = $this->getFiltersKpiService();
        
        $formId = intval($this->_getParam('id', null));
        $model = $this->_getParam('model', "{error:true}");
        $token = $questionnaire->setToken($this->_getParam('token', false));

        $form = $feedbackFormService->getById($formId);
        $meta = Zend_Json::decode($form->meta ? $form->meta : '{}');
        if(!isset($meta['respondentInfosOrder'])){
            $meta['respondentInfosOrder'] = array();
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }
        
        if(isset($meta['externalFieldsOrder'])){
            unset($meta['externalFieldsOrder']);
            $form->meta = Zend_Json::encode($meta);
            $form->save();
        }
        
        $requete = $conf_solr->generateSqlCode($formId);     
        $headers = $requete['header'];        
        $externalFields = array();

        $respondentInfosOrder = array();
        $header_infos = array();
        if (count($meta['respondentInfosOrder']) > 0){
            $header_infos = $meta['respondentInfosOrder'];
        }
        else {           
            $header_infos = $respondentFieldHiddenService->getRespondentInfos($headers, $externalFields);
        }

        
        // Donnee nominatives
        $nominative_ignore = NULL;
        $nominative_check = NULL;
        
        $nominative_question = $feedbackFormService->getNominativeQuestionByFeedbackFormId($formId);
        $nominative_modality = $feedbackFormService->getNominativeModalityByFeedbackFormId($formId);
        
        if(!empty($nominative_question)) {
            $nominative_ignore = $nominativeFieldHiddenService->getNominativeFieldHiddenByFeedbackFormId($formId);
            $nominative_check  = $nominative_question.$nominative_modality;
        }
        
       $questionnaire->saveQuestionnaire($model, $formId, $header_infos, $nominative_ignore, $nominative_check);
    }
    
}
