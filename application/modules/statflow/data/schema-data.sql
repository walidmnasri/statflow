-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cawi
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_belong`
--

DROP TABLE IF EXISTS `auth_belong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_belong` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `fk_belong__group_id___group__user_id` (`group_id`),
  CONSTRAINT `fk_belong__group_id___group__user_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `fk_reference_32` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_belong`
--

LOCK TABLES `auth_belong` WRITE;
/*!40000 ALTER TABLE `auth_belong` DISABLE KEYS */;
INSERT INTO `auth_belong` VALUES (1,1);
/*!40000 ALTER TABLE `auth_belong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `group_parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group__group_parent_id___group__id` (`group_parent_id`),
  CONSTRAINT `fk_group__group_parent_id___group__id` FOREIGN KEY (`group_parent_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'Administrator','Administrator',NULL);
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permission`
--

DROP TABLE IF EXISTS `auth_group_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permission` (
  `group_id` int(11) unsigned NOT NULL,
  `permission_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`permission_id`),
  KEY `fk_group_permission__permission_id___permission__id` (`permission_id`),
  CONSTRAINT `fk_group_permission__group_id___group__id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `fk_group_permission__permission_id___permission__id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permission`
--

LOCK TABLES `auth_group_permission` WRITE;
/*!40000 ALTER TABLE `auth_group_permission` DISABLE KEYS */;
INSERT INTO `auth_group_permission` VALUES (1,0),(1,1),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9);
/*!40000 ALTER TABLE `auth_group_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'auth_admin-user_get','View an user'),(3,'auth_admin-user_post','Create an user'),(4,'auth_admin-user_put','Update user information'),(5,'auth_admin-user_index','View user index'),(6,'auth_admin-user_list','View user list'),(7,'auth_admin-group-permission_index','View permission per group'),(8,'auth_admin-group-permission_switch','Switch permission'),(9,'all','All');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) DEFAULT NULL,
  `algorithm` varchar(128) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `can_be_deleted` int(1) unsigned NOT NULL DEFAULT '1',
  `is_active` int(1) unsigned NOT NULL DEFAULT '0',
  `is_super_admin` int(1) unsigned NOT NULL DEFAULT '0',
  `is_staff` int(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `can_be_deleted` (`can_be_deleted`),
  KEY `is_active` (`is_active`),
  KEY `is_super_admin` (`is_super_admin`),
  KEY `is_staff` (`is_staff`),
  KEY `fk_user__user_parent_id___user__id` (`user_parent_id`),
  CONSTRAINT `fk_user__user_parent_id___user__id` FOREIGN KEY (`user_parent_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'admin',NULL,NULL,'ce@octaveoctave.com','692a868f105e05b46f6f798ef3ec8554b7d658f3','a73056c2bbdca2d4148049493e296e70','sha1',NULL,0,1,1,0,'2009-11-23 10:36:31','2013-11-23 01:40:42','2013-11-23 13:40:42',NULL);
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_permission`
--

DROP TABLE IF EXISTS `auth_user_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_permission` (
  `user_id` int(11) unsigned NOT NULL,
  `permission_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `fk_persmission__action_id___action__id` (`permission_id`),
  CONSTRAINT `fk_permission__user_id___user__id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `fk_persmission__action_id___action__id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_permission`
--

LOCK TABLES `auth_user_permission` WRITE;
/*!40000 ALTER TABLE `auth_user_permission` DISABLE KEYS */;
INSERT INTO `auth_user_permission` VALUES (1,1);
/*!40000 ALTER TABLE `auth_user_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centurion_content_type`
--

DROP TABLE IF EXISTS `centurion_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centurion_content_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centurion_content_type`
--

LOCK TABLES `centurion_content_type` WRITE;
/*!40000 ALTER TABLE `centurion_content_type` DISABLE KEYS */;
INSERT INTO `centurion_content_type` VALUES (2,'Cms_Model_DbTable_Flatpage'),(3,'Cms_Model_DbTable_Row_Flatpage'),(8,'Core_Model_DbTable_Navigation');
/*!40000 ALTER TABLE `centurion_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centurion_navigation`
--

DROP TABLE IF EXISTS `centurion_navigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centurion_navigation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(150) DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` text,
  `permission` varchar(255) DEFAULT NULL,
  `route` varchar(100) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `is_visible` int(1) NOT NULL DEFAULT '1',
  `is_in_menu` int(1) NOT NULL DEFAULT '1',
  `class` varchar(50) DEFAULT NULL,
  `mptt_lft` int(11) unsigned NOT NULL,
  `mptt_rgt` int(11) unsigned NOT NULL,
  `mptt_level` int(11) unsigned NOT NULL,
  `mptt_tree_id` int(11) unsigned DEFAULT NULL,
  `mptt_parent_id` int(11) unsigned DEFAULT NULL,
  `proxy_model` int(11) unsigned DEFAULT NULL,
  `proxy_pk` int(11) unsigned DEFAULT NULL,
  `can_be_deleted` int(11) unsigned DEFAULT '1',
  `original_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proxy_model` (`proxy_model`,`proxy_pk`),
  KEY `fk_navigation__navigation_parent_id___navigation__id` (`mptt_parent_id`),
  CONSTRAINT `centurion_navigation_ibfk_1` FOREIGN KEY (`proxy_model`) REFERENCES `centurion_content_type` (`id`),
  CONSTRAINT `fk_navigation__navigation_parent_id___navigation__id` FOREIGN KEY (`mptt_parent_id`) REFERENCES `centurion_navigation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centurion_navigation`
--

LOCK TABLES `centurion_navigation` WRITE;
/*!40000 ALTER TABLE `centurion_navigation` DISABLE KEYS */;
INSERT INTO `centurion_navigation` VALUES (1,'Users','user','admin-profile',NULL,NULL,NULL,'default',NULL,1,0,1,NULL,48,55,1,5,12,NULL,NULL,1,NULL,1),(2,'Manage group permissions','auth','admin-group-permission',NULL,NULL,NULL,'default',NULL,3,1,1,NULL,49,50,2,5,1,NULL,NULL,1,NULL,NULL),(3,'Pages','admin','admin-navigation',NULL,NULL,NULL,NULL,NULL,2,1,1,NULL,64,69,1,5,12,NULL,NULL,1,NULL,1),(4,'Settings','user','admin-profile',NULL,NULL,NULL,'default',NULL,3,0,1,NULL,76,79,1,5,12,NULL,NULL,1,NULL,1),(5,'Template','cms','admin-flatpage-template',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,67,68,2,5,3,NULL,NULL,1,NULL,NULL),(6,'Position','cms','admin-flatpage-position',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,65,66,2,5,3,NULL,NULL,1,NULL,NULL),(7,'Cache','admin','index','cache',NULL,NULL,NULL,NULL,2,1,1,NULL,72,75,1,5,12,NULL,NULL,1,NULL,NULL),(8,'Clear cache','admin','index','clear-cache',NULL,NULL,NULL,NULL,NULL,1,1,NULL,73,74,2,5,7,NULL,NULL,1,NULL,NULL),(11,'Translation','translation','admin',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,70,71,1,5,12,NULL,NULL,1,NULL,NULL),(12,'Backoffice',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,80,0,5,NULL,NULL,NULL,0,NULL,NULL),(13,'Error','admin','index','log',NULL,NULL,NULL,NULL,NULL,1,1,NULL,77,78,2,5,4,NULL,NULL,1,NULL,NULL),(14,'Pages unactivated',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'unactived',1,26,0,20,NULL,NULL,NULL,0,NULL,NULL),(16,'Frontoffice',NULL,NULL,NULL,NULL,'all',NULL,NULL,NULL,1,1,NULL,1,126,0,18,NULL,NULL,NULL,0,NULL,NULL),(20,'Contents',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,2,47,1,5,12,NULL,NULL,1,NULL,1),(105,'Navigation','admin','admin-navigation',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,56,57,1,5,12,NULL,NULL,1,NULL,1),(118,'Permission','auth','admin-permission',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,51,52,2,5,1,NULL,NULL,1,NULL,NULL),(119,'Script permissions','auth','admin-script-permission',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,53,54,2,5,1,NULL,NULL,1,NULL,NULL),(135,'Annuaire','statflow','structure','index',NULL,NULL,NULL,NULL,NULL,1,1,NULL,90,95,1,18,16,NULL,NULL,1,NULL,1),(136,'Gérer les utilisateurs','statflow','user','index',NULL,NULL,NULL,NULL,NULL,1,1,NULL,96,101,1,18,16,NULL,NULL,1,NULL,1),(138,'Permissions','statflow','permission','index',NULL,NULL,NULL,NULL,NULL,1,1,NULL,102,107,1,18,16,NULL,NULL,1,NULL,1),(139,'Directory',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,86,89,1,18,16,NULL,NULL,1,135,2),(140,'Users',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,86,89,1,18,16,NULL,NULL,1,136,2),(142,'Permissions',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,86,89,1,18,16,NULL,NULL,1,138,2),(143,'Etudes','statflow','study','index',NULL,NULL,NULL,NULL,NULL,1,1,NULL,86,89,1,18,16,NULL,NULL,1,NULL,1),(144,'Studies',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,NULL,NULL,NULL,1,143,2),(145,'Questionnaires','statflow','feedback-form','index',NULL,NULL,NULL,NULL,NULL,0,1,NULL,112,115,1,18,16,NULL,NULL,1,NULL,1),(146,'Feedback forms',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,113,114,2,18,145,NULL,NULL,1,145,2),(147,'Nouveau questionnaire','statflow','feedback-form','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,113,114,2,18,145,NULL,NULL,1,NULL,1),(148,'New feedback form',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,21,NULL,NULL,NULL,1,147,2),(150,'Edit a feedback form',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,91,92,2,18,135,NULL,NULL,1,149,2),(151,'Nouvelle structure','statflow','structure','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,91,92,2,18,135,NULL,NULL,1,NULL,1),(152,'New structure',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,97,98,2,18,NULL,NULL,NULL,1,151,2),(153,'Nouvel utilisateur','statflow','user','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,97,98,2,18,136,NULL,NULL,1,NULL,1),(154,'New user',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,99,100,2,18,NULL,NULL,NULL,1,153,2),(155,'Edition d\'un utilisateur','statflow','user','edit',NULL,NULL,NULL,NULL,NULL,1,1,NULL,99,100,2,18,136,NULL,NULL,1,NULL,1),(156,'Edit a user',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,NULL,NULL,NULL,1,155,2),(157,'Edition d\'une structure','statflow','structure','edit',NULL,NULL,NULL,NULL,NULL,1,1,NULL,93,94,2,18,135,NULL,NULL,1,NULL,1),(158,'Edit a structure',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,NULL,NULL,NULL,1,157,2),(159,'Nouvelle étude','statflow','study','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,87,88,2,18,143,NULL,NULL,1,NULL,1),(160,'New study',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,NULL,NULL,NULL,1,159,2),(163,'Nouvelle permission','statflow','permission','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,103,104,2,18,138,NULL,NULL,1,NULL,1),(164,'New permission',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,NULL,NULL,NULL,1,163,2),(165,'Edition d\'une permission','statflow','permission','edit',NULL,NULL,NULL,NULL,NULL,1,1,NULL,105,106,2,18,138,NULL,NULL,1,NULL,1),(166,'Edit a permission',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,NULL,NULL,NULL,1,165,2),(167,'Templates questionnaire','statflow','feedback-form-template','index',NULL,NULL,NULL,NULL,NULL,0,1,NULL,122,125,1,18,16,NULL,NULL,1,NULL,1),(168,'Feedback form templates',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,167,NULL,NULL,1,167,2),(169,'Nouveau template questionnaire','statflow','feedback-form-template','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,25,167,NULL,NULL,1,NULL,1),(170,'New template feedback form',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,23,NULL,NULL,NULL,1,169,2),(171,'Edition template questionnaire','statflow','feedback-form-template','edit',NULL,NULL,NULL,NULL,NULL,1,1,NULL,123,124,2,18,167,NULL,NULL,1,NULL,1),(172,'Edit a template feedback form',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,117,118,2,18,NULL,NULL,NULL,1,171,2),(173,'Pages questionnaire','statflow','feedback-form-page','index',NULL,NULL,NULL,NULL,NULL,0,1,NULL,116,121,1,18,16,NULL,NULL,1,NULL,1),(174,'Feedback form pages',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,117,118,2,18,NULL,NULL,NULL,1,173,2),(175,'Nouvelle page questionnaire','statflow','feedback-form-page','add',NULL,NULL,NULL,NULL,NULL,1,1,NULL,117,118,2,18,173,NULL,NULL,1,NULL,1),(176,'New feedback form page',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,23,NULL,NULL,NULL,1,175,2),(177,'Edition page questionnaire','statflow','feedback-form-page','edit',NULL,NULL,NULL,NULL,NULL,1,1,NULL,119,120,2,18,173,NULL,NULL,1,NULL,1),(178,'Edit a feedback form page',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,23,NULL,NULL,NULL,1,177,2),(179,'Questions','statflow','question','index',NULL,NULL,NULL,NULL,NULL,0,1,NULL,110,111,1,18,16,NULL,NULL,1,NULL,1),(180,'Questions',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,2,0,22,NULL,NULL,NULL,1,179,2),(181,'Translation','translation','admin',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,108,109,1,18,16,NULL,NULL,1,NULL,1);
/*!40000 ALTER TABLE `centurion_navigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centurion_site`
--

DROP TABLE IF EXISTS `centurion_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centurion_site` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centurion_site`
--

LOCK TABLES `centurion_site` WRITE;
/*!40000 ALTER TABLE `centurion_site` DISABLE KEYS */;
INSERT INTO `centurion_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `centurion_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_flatpage`
--

DROP TABLE IF EXISTS `cms_flatpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_flatpage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `body` text,
  `url` varchar(100) DEFAULT NULL,
  `flatpage_template_id` int(11) unsigned NOT NULL,
  `flatpage_position_id` int(11) unsigned DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_published` int(1) unsigned DEFAULT '0',
  `mptt_lft` int(11) unsigned NOT NULL,
  `mptt_rgt` int(11) unsigned NOT NULL,
  `mptt_level` int(11) unsigned NOT NULL,
  `mptt_tree_id` int(11) unsigned DEFAULT NULL,
  `mptt_parent_id` int(11) unsigned DEFAULT NULL,
  `original_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `forward_url` varchar(255) DEFAULT NULL,
  `flatpage_type` int(1) NOT NULL DEFAULT '1',
  `route` varchar(50) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `cover_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flatpage_template_id` (`flatpage_template_id`),
  KEY `flatpage_position_id` (`flatpage_position_id`),
  KEY `flatpage_cover_id` (`cover_id`),
  KEY `mptt_parent_id` (`mptt_parent_id`),
  CONSTRAINT `cms_flatpage_ibfk_1` FOREIGN KEY (`mptt_parent_id`) REFERENCES `cms_flatpage` (`id`),
  CONSTRAINT `cms_flatpage__banner_id___media_file__id` FOREIGN KEY (`cover_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `cms_flatpage__flatpage_position_id___cms_flatpage_position__id` FOREIGN KEY (`flatpage_position_id`) REFERENCES `cms_flatpage_position` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `cms_flatpage__flatpage_template_id___cms_flatpage_template__id` FOREIGN KEY (`flatpage_template_id`) REFERENCES `cms_flatpage_template` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_flatpage`
--

LOCK TABLES `cms_flatpage` WRITE;
/*!40000 ALTER TABLE `cms_flatpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_flatpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_flatpage_position`
--

DROP TABLE IF EXISTS `cms_flatpage_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_flatpage_position` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_flatpage_position`
--

LOCK TABLES `cms_flatpage_position` WRITE;
/*!40000 ALTER TABLE `cms_flatpage_position` DISABLE KEYS */;
INSERT INTO `cms_flatpage_position` VALUES (1,'main','main');
/*!40000 ALTER TABLE `cms_flatpage_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_flatpage_template`
--

DROP TABLE IF EXISTS `cms_flatpage_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_flatpage_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `view_script` varchar(50) NOT NULL,
  `class` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_flatpage_template`
--

LOCK TABLES `cms_flatpage_template` WRITE;
/*!40000 ALTER TABLE `cms_flatpage_template` DISABLE KEYS */;
INSERT INTO `cms_flatpage_template` VALUES (1,'Basic','_generic/basic.phtml',NULL),(2,'Basic no title','_generic/basic-notitle.phtml',NULL);
/*!40000 ALTER TABLE `cms_flatpage_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_answer`
--

DROP TABLE IF EXISTS `statflow_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `respondent_id` int(10) unsigned NOT NULL,
  `feedback_form_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `value` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_answer_statflow_question1_idx` (`question_id`),
  KEY `fk_statflow_answer_statflow_feedback_form1_idx` (`feedback_form_id`),
  KEY `fk_statflow_answer_statflow_respondent1_idx` (`respondent_id`),
  CONSTRAINT `statflow_answer_ibfk_1` FOREIGN KEY (`respondent_id`) REFERENCES `statflow_respondent` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_answer_ibfk_2` FOREIGN KEY (`feedback_form_id`) REFERENCES `statflow_feedback_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_answer_ibfk_3` FOREIGN KEY (`question_id`) REFERENCES `statflow_question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_answer`
--

LOCK TABLES `statflow_answer` WRITE;
/*!40000 ALTER TABLE `statflow_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_answer_entry`
--

DROP TABLE IF EXISTS `statflow_answer_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_answer_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_entry_id` int(10) unsigned NOT NULL,
  `answer_id` int(10) unsigned NOT NULL,
  `entry_type_id` int(11) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_answer_entry_statflow_answer1_idx` (`answer_id`),
  KEY `fk_statflow_answer_entry_statflow_question_entry1_idx` (`question_entry_id`),
  CONSTRAINT `statflow_answer_entry_ibfk_1` FOREIGN KEY (`question_entry_id`) REFERENCES `statflow_question_entry` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_answer_entry_ibfk_2` FOREIGN KEY (`answer_id`) REFERENCES `statflow_answer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_answer_entry`
--

LOCK TABLES `statflow_answer_entry` WRITE;
/*!40000 ALTER TABLE `statflow_answer_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_answer_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_feedback_form`
--

DROP TABLE IF EXISTS `statflow_feedback_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_feedback_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `study_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `is_published` tinyint(4) DEFAULT NULL,
  `is_open` tinyint(1) DEFAULT '0',
  `introduction_file_id` varchar(100) DEFAULT NULL,
  `introduction_is_visible` tinyint(1) DEFAULT NULL,
  `publication_start` timestamp NULL DEFAULT NULL,
  `publication_end` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta` text,
  `image` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_feedback_form_statflow_study_idx` (`study_id`),
  CONSTRAINT `statflow_feedback_form_ibfk_1` FOREIGN KEY (`study_id`) REFERENCES `statflow_study` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_feedback_form`
--

LOCK TABLES `statflow_feedback_form` WRITE;
/*!40000 ALTER TABLE `statflow_feedback_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_feedback_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_feedback_form_page`
--

DROP TABLE IF EXISTS `statflow_feedback_form_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_feedback_form_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feedback_form_id` int(10) unsigned NOT NULL,
  `title_is_visible` tinyint(4) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta` text,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_feedback_form_page_statflow_feedback_fo_idx` (`feedback_form_id`),
  CONSTRAINT `statflow_feedback_form_page_ibfk_1` FOREIGN KEY (`feedback_form_id`) REFERENCES `statflow_feedback_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_feedback_form_page`
--

LOCK TABLES `statflow_feedback_form_page` WRITE;
/*!40000 ALTER TABLE `statflow_feedback_form_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_feedback_form_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_import`
--

DROP TABLE IF EXISTS `statflow_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_import` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `study_id` int(10) unsigned NOT NULL,
  `feedback_form_id` int(10) unsigned NOT NULL,
  `number` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_import_statflow_study1_idx` (`study_id`),
  KEY `fk_statflow_import_statflow_feedback_form1_idx` (`feedback_form_id`),
  CONSTRAINT `statflow_import_ibfk_1` FOREIGN KEY (`study_id`) REFERENCES `statflow_study` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_import_ibfk_2` FOREIGN KEY (`feedback_form_id`) REFERENCES `statflow_feedback_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_import`
--

LOCK TABLES `statflow_import` WRITE;
/*!40000 ALTER TABLE `statflow_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_import_field`
--

DROP TABLE IF EXISTS `statflow_import_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_import_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_id` int(10) unsigned NOT NULL,
  `study_id` int(10) unsigned NOT NULL,
  `feedback_form_id` int(10) unsigned NOT NULL,
  `label` varchar(45) NOT NULL DEFAULT '',
  `external_id` varchar(45) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_import_field_statflow_import1_idx` (`import_id`),
  KEY `fk_statflow_import_field_statflow_study1_idx` (`study_id`),
  KEY `fk_statflow_import_field_statflow_feedback_form1_idx` (`feedback_form_id`),
  CONSTRAINT `statflow_import_field_ibfk_1` FOREIGN KEY (`import_id`) REFERENCES `statflow_import` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_import_field_ibfk_2` FOREIGN KEY (`study_id`) REFERENCES `statflow_study` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_import_field_ibfk_3` FOREIGN KEY (`feedback_form_id`) REFERENCES `statflow_feedback_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_import_field`
--

LOCK TABLES `statflow_import_field` WRITE;
/*!40000 ALTER TABLE `statflow_import_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_import_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_question`
--

DROP TABLE IF EXISTS `statflow_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_id` int(10) unsigned DEFAULT NULL,
  `feedback_form_id` int(10) unsigned DEFAULT NULL,
  `question_type_id` int(10) unsigned NOT NULL,
  `feedback_form_page_id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta` text,
  `parent_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_question_statflow_feedback_form_page1_idx` (`feedback_form_page_id`),
  KEY `original_id` (`original_id`),
  KEY `feedback_form_id` (`feedback_form_id`),
  CONSTRAINT `statflow_question_ibfk_3` FOREIGN KEY (`original_id`) REFERENCES `statflow_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `statflow_question_ibfk_4` FOREIGN KEY (`feedback_form_id`) REFERENCES `statflow_feedback_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_question_ibfk_5` FOREIGN KEY (`feedback_form_page_id`) REFERENCES `statflow_feedback_form_page` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_question`
--

LOCK TABLES `statflow_question` WRITE;
/*!40000 ALTER TABLE `statflow_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_question_entry`
--

DROP TABLE IF EXISTS `statflow_question_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_question_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `entry_type_id` int(10) unsigned NOT NULL,
  `complement_data` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_question_entry_statflow_question1_idx` (`question_id`),
  CONSTRAINT `statflow_question_entry_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `statflow_question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_question_entry`
--

LOCK TABLES `statflow_question_entry` WRITE;
/*!40000 ALTER TABLE `statflow_question_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_question_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_recovery`
--

DROP TABLE IF EXISTS `statflow_recovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_recovery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `respondent_id` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `store` int(11) DEFAULT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `respondent_id` (`respondent_id`),
  CONSTRAINT `statflow_recovery_ibfk_1` FOREIGN KEY (`respondent_id`) REFERENCES `statflow_respondent` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_recovery`
--

LOCK TABLES `statflow_recovery` WRITE;
/*!40000 ALTER TABLE `statflow_recovery` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_recovery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_recovery_entry`
--

DROP TABLE IF EXISTS `statflow_recovery_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_recovery_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `recovery_id` int(10) unsigned NOT NULL,
  `treated_by` varchar(255) NOT NULL,
  `how` int(11) NOT NULL,
  `cause1` int(11) DEFAULT NULL,
  `cause2` int(11) DEFAULT NULL,
  `comment` text,
  `transfered` int(11) DEFAULT NULL,
  `actions` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recovery_id` (`recovery_id`),
  CONSTRAINT `statflow_recovery_entry_ibfk_1` FOREIGN KEY (`recovery_id`) REFERENCES `statflow_recovery` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_recovery_entry`
--

LOCK TABLES `statflow_recovery_entry` WRITE;
/*!40000 ALTER TABLE `statflow_recovery_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_recovery_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_respondent`
--

DROP TABLE IF EXISTS `statflow_respondent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_respondent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `study_id` int(10) unsigned NOT NULL,
  `feedback_form_id` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `external_id` varchar(45) DEFAULT NULL,
  `email` varchar(354) DEFAULT NULL,
  `is_alert` tinyint(1) DEFAULT NULL,
  `is_satisfied` tinyint(1) DEFAULT NULL,
  `is_very_satisfied` tinyint(1) DEFAULT NULL,
  `is_unsatisfied` tinyint(1) DEFAULT NULL,
  `is_very_unsatisfied` tinyint(1) DEFAULT NULL,
  `answered_at` timestamp NULL DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_respondent_statflow_feedback_form1_idx` (`feedback_form_id`),
  KEY `fk_statflow_respondent_statflow_study1_idx` (`study_id`),
  CONSTRAINT `statflow_respondent_ibfk_1` FOREIGN KEY (`study_id`) REFERENCES `statflow_study` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_respondent_ibfk_2` FOREIGN KEY (`feedback_form_id`) REFERENCES `statflow_feedback_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_respondent`
--

LOCK TABLES `statflow_respondent` WRITE;
/*!40000 ALTER TABLE `statflow_respondent` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_respondent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_respondent_external_field`
--

DROP TABLE IF EXISTS `statflow_respondent_external_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_respondent_external_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `respondent_id` int(10) unsigned NOT NULL,
  `import_field_id` int(11) NOT NULL,
  `label` varchar(45) DEFAULT NULL,
  `value` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_respondent_external_field_statflow_resp_idx` (`respondent_id`),
  KEY `fk_statflow_respondent_external_field_statflow_impo_idx` (`import_field_id`),
  CONSTRAINT `fk_statflow_respondent_external_field_statflow_import1` FOREIGN KEY (`import_field_id`) REFERENCES `statflow_import_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_statflow_respondent_external_field_statflow_respon1` FOREIGN KEY (`respondent_id`) REFERENCES `statflow_respondent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_respondent_external_field`
--

LOCK TABLES `statflow_respondent_external_field` WRITE;
/*!40000 ALTER TABLE `statflow_respondent_external_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_respondent_external_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_structure`
--

DROP TABLE IF EXISTS `statflow_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_structure` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statflow_structure_statflow_structure1` (`parent_id`),
  CONSTRAINT `statflow_structure_ibfk_7` FOREIGN KEY (`parent_id`) REFERENCES `statflow_structure` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5578 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_structure`
--

LOCK TABLES `statflow_structure` WRITE;
/*!40000 ALTER TABLE `statflow_structure` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_structure_user`
--

DROP TABLE IF EXISTS `statflow_structure_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_structure_user` (
  `user_id` int(11) unsigned NOT NULL,
  `structure_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`structure_id`),
  KEY `fk_auth_user_has_statflow_structure_statflow_structure1` (`structure_id`),
  KEY `fk_auth_user_has_statflow_structure_auth_user1` (`user_id`),
  CONSTRAINT `statflow_structure_user_ibfk_2` FOREIGN KEY (`structure_id`) REFERENCES `statflow_structure` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `statflow_structure_user_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_structure_user`
--

LOCK TABLES `statflow_structure_user` WRITE;
/*!40000 ALTER TABLE `statflow_structure_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_structure_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statflow_study`
--

DROP TABLE IF EXISTS `statflow_study`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statflow_study` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `structure_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `is_published` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `structure_id` (`structure_id`),
  CONSTRAINT `statflow_study_ibfk_2` FOREIGN KEY (`structure_id`) REFERENCES `statflow_structure` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statflow_study`
--

LOCK TABLES `statflow_study` WRITE;
/*!40000 ALTER TABLE `statflow_study` DISABLE KEYS */;
/*!40000 ALTER TABLE `statflow_study` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_duplicate`
--

DROP TABLE IF EXISTS `media_duplicate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_duplicate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` varchar(100) NOT NULL,
  `adapter` varchar(50) NOT NULL,
  `params` text NOT NULL,
  `dest` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_id` (`file_id`),
  CONSTRAINT `media_duplicate_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_duplicate`
--

LOCK TABLES `media_duplicate` WRITE;
/*!40000 ALTER TABLE `media_duplicate` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_duplicate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_file`
--

DROP TABLE IF EXISTS `media_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_file` (
  `id` varchar(100) NOT NULL,
  `file_id` varchar(32) DEFAULT NULL,
  `local_filename` varchar(255) DEFAULT NULL,
  `mime` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filesize` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `use_count` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` int(11) unsigned DEFAULT NULL,
  `proxy_model` varchar(150) DEFAULT NULL,
  `proxy_pk` int(11) unsigned DEFAULT NULL,
  `belong_model` varchar(150) DEFAULT NULL,
  `belong_pk` int(11) unsigned DEFAULT NULL,
  `description` text,
  `sha1` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file__user_id___user__id` (`user_id`),
  CONSTRAINT `fk_file__user_id___user__id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_file`
--

LOCK TABLES `media_file` WRITE;
/*!40000 ALTER TABLE `media_file` DISABLE KEYS */;
INSERT INTO `media_file` VALUES ('88888888','88888888','../../public/layouts/backoffice/images/px.png','image/png','px.png','px.png',NULL,'0000-00-00 00:00:00',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `media_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_image`
--

DROP TABLE IF EXISTS `media_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` int(11) unsigned NOT NULL,
  `height` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_image`
--

LOCK TABLES `media_image` WRITE;
/*!40000 ALTER TABLE `media_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_multiupload_ticket`
--

DROP TABLE IF EXISTS `media_multiupload_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_multiupload_ticket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket` varchar(32) NOT NULL,
  `expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `proxy_model_id` int(11) unsigned DEFAULT NULL,
  `proxy_pk` int(11) unsigned DEFAULT NULL,
  `form_class_model_id` int(11) unsigned NOT NULL,
  `element_name` varchar(255) NOT NULL,
  `values` text,
  PRIMARY KEY (`id`),
  KEY `proxy_model` (`proxy_model_id`),
  KEY `form_class_model` (`form_class_model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_multiupload_ticket`
--

LOCK TABLES `media_multiupload_ticket` WRITE;
/*!40000 ALTER TABLE `media_multiupload_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_multiupload_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_video`
--

DROP TABLE IF EXISTS `media_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` int(11) unsigned NOT NULL,
  `height` int(11) unsigned NOT NULL,
  `duration` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_video`
--

LOCK TABLES `media_video` WRITE;
/*!40000 ALTER TABLE `media_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_language`
--

DROP TABLE IF EXISTS `translation_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `flag` varchar(255) NOT NULL,
  `language` varchar(5) DEFAULT NULL,
  `region` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_language`
--

LOCK TABLES `translation_language` WRITE;
/*!40000 ALTER TABLE `translation_language` DISABLE KEYS */;
INSERT INTO `translation_language` VALUES (1,'fr','Français','/layouts/backoffice/images/flags/fr.png','fr','FR'),(2,'en','English','/layouts/backoffice/images/flags/uk.png','en','GB');
/*!40000 ALTER TABLE `translation_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_tag`
--

DROP TABLE IF EXISTS `translation_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_tag`
--

LOCK TABLES `translation_tag` WRITE;
/*!40000 ALTER TABLE `translation_tag` DISABLE KEYS */;
INSERT INTO `translation_tag` VALUES (2,'backoffice'),(3,'cms'),(23,'test');
/*!40000 ALTER TABLE `translation_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_tag_uid`
--

DROP TABLE IF EXISTS `translation_tag_uid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_tag_uid` (
  `uid_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`uid_id`,`tag_id`),
  KEY `FK_translation_tag_uid2` (`tag_id`),
  CONSTRAINT `FK_translation_tag_uid` FOREIGN KEY (`uid_id`) REFERENCES `translation_uid` (`id`),
  CONSTRAINT `FK_translation_tag_uid2` FOREIGN KEY (`tag_id`) REFERENCES `translation_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_tag_uid`
--

LOCK TABLES `translation_tag_uid` WRITE;
/*!40000 ALTER TABLE `translation_tag_uid` DISABLE KEYS */;
INSERT INTO `translation_tag_uid` VALUES (124,2),(125,2),(126,2),(127,2),(128,2),(142,2),(143,2),(169,2),(170,2),(172,2),(173,2),(174,2),(175,2),(176,2),(177,2),(178,2),(181,2),(182,2),(183,2),(184,2),(185,2),(186,2),(187,2),(188,2),(189,2),(190,2),(191,2),(195,2),(196,2),(197,2),(198,2),(201,2),(202,2),(203,2),(205,2),(206,2),(207,2),(208,2),(209,2),(210,2),(211,2),(212,2),(213,2),(214,2),(215,2),(216,2),(217,2),(218,2),(219,2),(220,2),(221,2),(222,2),(223,2),(224,2),(225,2),(226,2),(227,2),(228,2),(229,2),(230,2),(231,2),(232,2),(233,2),(234,2),(235,2),(236,2),(237,2),(238,2),(240,2),(297,2),(298,2),(299,2),(300,2),(301,2),(302,2),(303,2),(304,2),(305,2),(309,2),(310,2),(311,2),(312,2),(313,2),(314,2),(315,2),(316,2),(317,2),(318,2),(319,2),(320,2),(321,2),(322,2),(323,2),(324,2),(325,2),(326,2),(327,2),(328,2),(329,2),(330,2),(331,2),(332,2),(333,2),(334,2),(335,2),(336,2),(337,2),(338,2),(339,2),(340,2),(341,2),(342,2),(343,2),(344,2),(348,2),(349,2),(350,2),(351,2),(352,2),(353,2),(355,2),(356,2),(357,2),(358,2),(359,2),(360,2),(364,2),(366,2),(367,2),(373,2),(374,2),(375,2),(377,2),(378,2),(379,2),(380,2),(381,2),(382,2),(383,2),(385,2),(386,2),(387,2),(388,2),(389,2),(390,2),(391,2),(392,2),(394,2),(396,2),(398,2),(399,2),(400,2),(401,2),(402,2),(403,2),(404,2),(405,2),(406,2),(407,2),(408,2),(409,2),(410,2),(411,2),(412,2),(413,2),(414,2),(415,2),(416,2),(417,2),(418,2),(419,2),(420,2),(421,2),(422,2),(425,2),(426,2),(427,2),(428,2),(429,2),(430,2),(434,2),(435,2),(436,2),(437,2),(438,2),(440,2),(441,2),(442,2),(443,2),(444,2),(445,2),(446,2),(447,2),(448,2),(449,2),(450,2),(451,2),(452,2),(453,2),(454,2),(455,2),(456,2),(458,2),(459,2),(461,2),(463,2),(465,2),(466,2),(467,2),(468,2),(469,2),(470,2),(471,2),(472,2),(473,2),(474,2),(476,2),(477,2),(479,2),(480,2),(481,2),(482,2),(483,2),(484,2),(488,2),(489,2),(490,2),(491,2),(492,2),(493,2),(494,2),(495,2),(496,2),(497,2),(498,2),(499,2),(831,2),(832,2),(833,2),(834,2),(835,2),(836,2),(837,2),(838,2),(839,2),(840,2),(841,2),(882,2),(1025,2),(1026,2),(1029,2),(1030,2),(1031,2),(1034,2),(1035,2),(1258,2),(1259,2),(1281,2),(1317,2),(1319,2),(1320,2),(1323,2),(1324,2),(1329,2),(1331,2),(1334,2),(1336,2),(1337,2),(1338,2),(1354,2),(1461,2),(1470,2),(1471,2),(1472,2),(1473,2),(1477,2),(1524,2),(126,3),(127,3),(128,3);
/*!40000 ALTER TABLE `translation_tag_uid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translation`
--

DROP TABLE IF EXISTS `translation_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translation` (
  `translation` text NOT NULL,
  `uid_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`uid_id`,`language_id`),
  KEY `uid_id` (`uid_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `translation_translation_ibfk_1` FOREIGN KEY (`uid_id`) REFERENCES `translation_uid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `translation_translation_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `translation_language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translation`
--

LOCK TABLES `translation_translation` WRITE;
/*!40000 ALTER TABLE `translation_translation` DISABLE KEYS */;
INSERT INTO `translation_translation` VALUES ('Identifiant',142,1),('Login',142,2),('Mot de passe',143,1),('Password',143,2),('Lister les structures',172,1),('Structures list',172,2),('Gérer les structures',173,1),('Manage structures',173,2),('Gérer les utilisateurs',174,1),('Manage users',174,2),('Nouvelle structure',175,1),('New structure',175,2),('Connect',176,1),('Logged in as',176,2),('Gérer les structures',177,1),('Manage structures',177,2),('Retour ',178,1),('Back to list',178,2),('Nom',181,1),('Name',181,2),('Parent',182,1),('Parent',182,2),('Enregistrer',183,1),('Save',183,2),('Aucun',184,1),('None',184,2),('Une erreur est survenue pendant l\'enregistrement de la structure',185,1),('An error occured while saving the structure',185,2),('La structure a été sauvegardée',186,1),('The structure has been saved',186,2),('Editer',187,1),('Edit',187,2),('Supprimer',188,1),('Delete',188,2),('Edition d\'une structure',189,1),('Edit structure',189,2),('ID externe',190,1),('External ID',190,2),('Une erreur est survenue pendant la suppression de la structure',195,1),('An error occured while deleting the structure',195,2),('Une erreur est survenue pendant l\'enregistrement de la structure',196,1),('An error occured while saving the structure',196,2),('La structure a été sauvegardée',197,1),('The structure has been saved',197,2),('La structure a été supprimée',198,1),('The structure has been deleted',198,2),('Gérer les utilisateurs',201,1),('Manage users',201,2),('Nouvel utilisateur',202,1),('New user',202,2),('Surnom',203,1),('Nickname',203,2),('Créé le',205,1),('Created at',205,2),('Dernière connexion',206,1),('Last login',206,2),('Nom d\'utilisateur',207,1),('Username',207,2),('Nom complet',208,1),('Fullname',208,2),('Gérer les utilisateurs',209,1),('Manage users',209,2),('Num d\'utilisateur',210,1),('Username',210,2),('Nom',211,1),('Firstname',211,2),('Prénom',212,1),('Lastname',212,2),('Email',213,1),('Email',213,2),('Email confirmation',214,1),('Email confirmation',214,2),('Mot de passe',215,1),('Password',215,2),('Mot de passe confirmation',216,1),('Password confirmation',216,2),('Suppressible',217,1),('Can be deleted',217,2),('Actif',218,1),('Is active',218,2),('Super administrateur',219,1),('Is super admin',219,2),('Fait parti de l\'équipe',220,1),('Is staff',220,2),('Structure',221,1),('Structure',221,2),('Edition d\'un utilisateur',222,1),('Edit a user',222,2),('Une erreur est survenue pendant l\'enregistrement de l\'utilisateur',223,1),('An error occured while saving the user',223,2),('L\'utilisateur a été sauvegardé',224,1),('The user has been saved',224,2),('L\'utilisateur a été sauvegardé',225,1),('The user has been saved',225,2),('L\'utilisateur a été supprimé',226,1),('The user has been deleted',226,2),('Profil utilisateur',227,1),('User profile',227,2),('Surnom',228,1),('Nickname',228,2),('A propos',229,1),('About',229,2),('Site internet',230,1),('Website',230,2),('Télephone',231,1),('Phone',231,2),('Mobile',232,1),('Cellular',232,2),('Mon profil',233,1),('My profile',233,2),('Déconnexion',234,1),('Logout',234,2),('Une erreur est survenue pendant l\'enregistrement de l\'utilisateur',235,1),('An error occured while saving the user',235,2),('Merci de vous authentifier',236,1),('Please sign in',236,2),('Connexion',237,1),('Sign in',237,2),('Nom d\'utilisateur',238,1),('Username',238,2),('Mot de passe',239,1),('Password',239,2),('Mot de passe',240,1),('Password',240,2),('G',297,1),('Manage groups',297,2),('G',298,1),('Manage permissions',298,2),('Gérer les permissions',299,1),('Manage permissions',299,2),('Nouvelle permission',300,1),('New permission',300,2),('Nom',301,1),('Name',301,2),('Description',302,1),('Description',302,2),('Liste',303,1),('List',303,2),('Groupes',304,1),('Groups',304,2),('Permissions',305,1),('Permissions',305,2),('Une erreur est survenue pendant l\'enregistrement de la permission pour le groupe ou l\'utilisateur s',309,1),('An error occured while saving the permission for the selected group or user',309,2),('La permission a ',310,1),('The permission for the selected group or user has been saved',310,2),('Une erreur est survenue pendant la suppression de la permission pour le groupe ou l\'utilisateur s',311,1),('An error occured while deleting the permission for the selected group or user',311,2),('La permission a ',312,1),('The permission for the selected group or user has been deleted',312,2),('La permission a ',313,1),('The permission has been saved',313,2),('La permission a ',314,1),('The permission has been deleted',314,2),('Une erreur est survenue pendant l\'enregistrement de la permission',315,1),('An error occured while saving the permission',315,2),('Edition d\'une permission',316,1),('Edit permission',316,2),('Une erreur est survenue pendant l\'enregistrement de la permission',317,1),('An error occured while saving the permission',317,2),('La permission a ',318,1),('The permission has been saved',318,2),('Tout autoriser pour',319,1),('Grant all for',319,2),('Tout bloquer pour',320,1),('Deny all for',320,2),('Actions',321,1),('Actions',321,2),('Les permissions ont ',322,1),('The permissions have been saved',322,2),('Une erreur est survenue pendant la suppression des permissions pour le groupe',323,1),('An error occured while deleting the permissions for the group',323,2),('Une erreur est survenue pendant l\'enregistrement des permissions pour le groupe',324,1),('An error occured while saving the permissions for the group',324,2),('Les permissions ont ',325,1),('The permissions have been deleted',325,2),('Gestion des ',326,1),('Manage studies',326,2),('Nouvelle ',327,1),('New study',327,2),('Nom',328,1),('Name',328,2),('Langue',329,1),('Language',329,2),('Structure',330,1),('Structure',330,2),('Publi',331,1),('Published',331,2),('Oui',332,1),('Yes',332,2),('Non',333,1),('No',333,2),('L\'',334,1),('The study has been saved',334,2),('Cr',335,1),('Created at',335,2),('L\'',336,1),('The study has been unpublished',336,2),('L\'',337,1),('The study has been published',337,2),('Une erreur est survenue pendant la d',338,1),('An error occured while unpublishing the study',338,2),('Une erreur est survenue pendant la publication de l\'',339,1),('An error occured while publishing the study',339,2),('L\'',340,1),('The study has been deleted',340,2),('Une erreur est survenue pendant la suppression de l\'',341,1),('An error occured while deleting the study',341,2),('Edition d\'une ',342,1),('Edit study',342,2),('L\'',343,1),('The study has been saved',343,2),('Questionnaires',344,1),('Feedback forms',344,2),('Nom',348,1),('Name',348,2),('Language',349,1),('Language',349,2),('Template',350,1),('Template',350,2),('Publi',351,1),('Is published',351,2),('Oui',352,1),('Yes',352,2),('Non',353,1),('No',353,2),('Texte d\'intrduction',355,1),('Introduction text',355,2),('Visible',356,1),('Is visible',356,2),('Oui',357,1),('Yes',357,2),('Non',358,1),('No',358,2),('D',359,1),('Publication start',359,2),('Fin de publication',360,1),('Publication end',360,2),('Actions',364,1),('Actions',364,2),('Etudes',366,1),('Studies',366,2),('Questionnaires',367,1),('Feedback forms',367,2),('Nouveau template de questionnaire',373,1),('New feedback form template',373,2),('Nom',374,1),('Name',374,2),('Fichier',375,1),('File',375,2),('G',377,1),('Manage templates',377,2),('G',378,1),('Manage pages',378,2),('Publi',380,1),('Is published',380,2),('G',381,1),('Manage feedback forms',381,2),('Nouveau questionnaire',382,1),('New feedback form',382,2),('Une erreur est survenue pendant la d',385,1),('An error occured while unpublishing the feedback form',385,2),('Le questionnaire a ',386,1),('The feedback form has been unpublished',386,2),('Le questionnaire a ',387,1),('The feedback form has been published',387,2),('Une erreur est survenue pendant la publication du questionnaire',388,1),('An error occured while publishing the feedback form',388,2),('Le questionnaire a ',389,1),('The feedback form has been saved',389,2),('Le questionnaire a ',390,1),('The feedback form has been deleted',390,2),('Une erreur est survenue pendant la suppression du questionnaire',391,1),('An error occured while deleting the feedback form',391,2),('Edition d\'un questionnaire',392,1),('Edit a feedback form',392,2),('Etude',394,1),('Study',394,2),('Le questionnaire a ',396,1),('The feedback form has been saved',396,2),('Voir les questionnaires',398,1),('Display the feedback forms',398,2),('G',399,1),('Manage feedback form templates',399,2),('Nouveau template',400,1),('New template',400,2),('Nom',401,1),('Name',401,2),('Nouveau template',402,1),('New template',402,2),('Fichier',403,1),('File',403,2),('Une erreur est survenue pendant la suppression du template questionnaire',404,1),('An error occured while deleting the feedback form template',404,2),('Edition d\'un template questionnaire',405,1),('Edit a feedback form template',405,2),('Le template questionnaire a ',406,1),('The feedback form template has been deleted',406,2),('G',407,1),('Manage feedback form pages',407,2),('Nouvelle page',408,1),('New page',408,2),('Nom',409,1),('Name',409,2),('G',410,1),('Manage feedback form pages',410,2),('Nouvelle page',411,1),('New page',411,2),('Questionnaire',412,1),('Feedback form',412,2),('Visible',413,1),('Is visible',413,2),('Une erreur est survenue pendant l\'enregistrement de la page questionnaire',414,1),('An error occured while saving the feedback form page',414,2),('La page questionnaire a ',415,1),('The feedback form page has been saved',415,2),('La page questionnaire a ',416,1),('The feedback form page has been deleted',416,2),('Une erreur est survenue pendant la suppression de la page questionnaire',417,1),('An error occured while deleting the feedback form page',417,2),('Edition d\'une page questionnaire',418,1),('Edit a feedback form page',418,2),('La page questionnaire a ',419,1),('The feedback form page has been saved',419,2),('Fermer',420,1),('Close',420,2),('Publi',421,1),('Publish',421,2),('D',422,1),('Unpublish',422,2),('Confirmation de suppression',425,1),('Deletion confirmation',425,2),('Je confirme',426,1),('I confirm',426,2),('',427,1),('Are you sure to delete ?',427,2),('Une erreur est survenue pendant l\'enregistrement du template questionnaire',428,1),('An error occured while saving the feedback form template',428,2),('G',429,1),('Manage questions',429,2),('Nouvelle question',430,1),('New question',430,2),('Code',434,1),('Code',434,2),('Questionnaire',435,1),('Feedback form',435,2),('Page',436,1),('Page',436,2),('Langue',437,1),('Language',437,2),('Section',438,1),('Section',438,2),('Type de question',440,1),('Question type',440,2),('Contenu',441,1),('Content',441,2),('Choix uniques',442,1),('Unique choices',442,2),('Intervalle',443,1),('Interval',443,2),('Classement',444,1),('Ordered',444,2),('Ouvert',445,1),('Open',445,2),('Tableau',446,1),('Array',446,2),('Une erreur est survenue pendant l\'enregistrement de la question',447,1),('An error occured while saving the question',447,2),('La question a ',448,1),('The question has been saved',448,2),('Edition d\'une question',449,1),('Edit a question',449,2),('Aide',450,1),('Help',450,2),('Page',451,1),('Page',451,2),('Questions',452,1),('Questions',452,2),('Mise ',453,1),('Last update',453,2),('Questions',454,1),('Questions',454,2),('Ajouter une question',455,1),('Add a question',455,2),('Ajouter',456,1),('Add',456,2),('Une erreur est survenue pendant la suppression de la question',458,1),('An error occured while deleting the question',458,2),('La question a ',459,1),('The question has been deleted',459,2),('Modalit',461,1),('Exclusive modality',461,2),('Modalit',463,1),('Simple modality',463,2),('Modalit',465,1),('Complement modality',465,2),('Une erreur est survenue pendant l\'enregistrement de la modalit',466,1),('An error occured while saving the question entry',466,2),('La modalit',467,1),('The question entry has been saved',467,2),('Libell',468,1),('Label',468,2),('Si s',469,1),('If selected, go to question',469,2),('La modalit',470,1),('The question entry form template has been deleted',470,2),('Une erreur est survenue pendant la suppression de la modalit',471,1),('An error occured while deleting the question entry',471,2),('Une erreur est survenue pendant l\'enregistrement de la modalit',472,1),('An error occured while saving the question entry',472,2),('La modalit',473,1),('The question entry has been saved',473,2),('Classement',474,1),('Ordered',474,2),('Nombre de choix maximum',476,1),('Maximum choices',476,2),('Intervalle',477,1),('Interval',477,2),('Texte suppl',479,1),('Addional text',479,2),('Min',480,1),('Min',480,2),('Max',481,1),('Max',481,2),('Libell',482,1),('Label',482,2),('Modalit',483,1),('Modality',483,2),('Type',484,1),('Type',484,2),('Filtrer',498,1),('Filter',498,2),('Mot de passe oublié',831,1),('Forgot password',831,2),('Email d\'activation',832,1),('Activation mail',832,2),('Réinitialisation du mot de passe',833,1),('Password reset',833,2),('Email',834,1),('Email',834,2),('Valider',835,1),('Submit',835,2),('Merci de saisir votre nouveau mot de passe',836,1),('Enter your new password',836,2),('Mot de passe',837,1),('Password',837,2),('Confirmation',838,1),('Save',838,2),('Valider',839,1),('Save',839,2),('Groupe',840,1),('Group',840,2),('Un email vous a été envoyé. Merci de cliquer sur le lien que vous y trouverez pour finir la réinitialisation.',841,1),('Please, check your inbox and click the link in the mail to finish the password reset process.',841,2),('Les mots de passe ne correspondent pas, merci de vérifier votre saisie',882,1),('Password don\'t match, please check your input',882,2),('Prévisualisation',1025,1),('Preview',1025,2),('Choix multiples',1026,1),('Multiple choices',1026,2),('Figée au bas',1029,1),('Fixed to bottom',1029,2),('Champs requis',1030,1),('Required field',1030,2),('Ouvert à tous',1034,1),('Open to all',1034,2),('Répondants identifiés uniquement',1035,1),('Identified respondants only',1035,2),('Définissable dans l\'interface de traduction une fois la question crée',1258,1),('Can be defined in the translation interface once the question created',1258,2),('Définissable dans l\'interface de traduction une fois la question crée',1259,1),('Can be defined in the translation interface once the question created',1259,2),('Définissable dans l\'interface de traduction une fois le formulaire créé',1281,1),('Should be defined in the translations interface once the form created',1281,2),('Conditions d\'affichage',1317,1),('Display conditions',1317,2),('Alertes',1319,1),('Alerts',1319,2),('Relances',1320,1),('Reminders',1320,2),('Ajouter une sous-question',1323,1),('Add a subquestion',1323,2),('Ajouter un choix',1324,1),('Add a choice',1324,2),('Bannière du questionnaire',1329,1),('Form banner image',1329,2),('Image',1331,1),('Image',1331,2),('Images multiples',1334,1),('Multiple images',1334,2),('Image',1338,1),('Image',1338,2),('Styles personnalisés (en CSS)',1354,1),('Custom styles (in CSS)',1354,2),('Affeicher les questionnaires',1472,1),('Display forms',1472,2),('Accès au questionnaire :',1473,1),('Access to this form :',1473,2),('Code question manquant',1524,1),('Missing question code',1524,2);
/*!40000 ALTER TABLE `translation_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_uid`
--

DROP TABLE IF EXISTS `translation_uid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_uid` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1534 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_uid`
--

LOCK TABLES `translation_uid` WRITE;
/*!40000 ALTER TABLE `translation_uid` DISABLE KEYS */;
INSERT INTO `translation_uid` VALUES (123,'back'),(124,'Log out'),(125,'View website'),(126,'Name'),(127,'View script'),(128,'Manage flatpage templates'),(129,'Delete'),(130,'Are you sure? This operation can not be undone'),(131,'Edit flatpage position'),(132,'Filters'),(133,'Submit'),(134,'Add a new %s'),(135,'<strong>%d-%d</strong> of <strong>%d</strong>'),(136,'Nickname'),(137,'Login'),(138,'Created at'),(139,'Last login'),(140,'Manage users'),(141,'user'),(142,'Username'),(143,'Password'),(144,'Email'),(145,'User parent'),(146,'Can be deleted'),(147,'Is staff'),(148,'Is active'),(149,'Is super admin'),(150,'Groups'),(151,'Permissions'),(152,'Special characters are not allowed'),(153,'Password confirmation'),(154,'Email confirmation'),(155,'Manage permissions'),(158,'Reference text'),(159,'Translations'),(160,'Id'),(161,'Reference language'),(162,'Language to translate'),(163,'Tags'),(164,'Manage translation'),(165,'Next'),(166,'Last'),(167,'Reference (%s)'),(168,'Translate'),(169,'Save'),(170,'Save and add another'),(171,'Saving has been done.'),(172,'navigation__statflow__structure__index__manage_structure_list'),(173,'navigation__statflow__structure__index__manage_structures'),(174,'navigation__statflow__structure__index__manage_users'),(175,'navigation__statflow__structure__index__manage_structure_add'),(176,'navigation__statflow__layout__logged_as'),(177,'navigation__statflow__structure__index__manage_structure_title'),(178,'navigation__statflow__layout__back_to_list'),(181,'navigation__statflow__structure_field_name'),(182,'navigation__statflow__structure_field_parent'),(183,'navigation__statflow__layout_submit'),(184,'navigation__statflow__layout_none'),(185,'navigation__statflow__structure_add_flashMessenger_error'),(186,'navigation__statflow__structure_add_flashMessenger_success'),(187,'navigation__statflow__layout_edit'),(188,'navigation__statflow__layout_delete'),(189,'navigation__statflow__structure__index__manage_structure_edit'),(190,'navigation__statflow__structure_field_external_id'),(191,'Remember me'),(192,'Log in'),(193,'First'),(194,'Previous'),(195,'navigation__statflow__structure_delete_flashMessenger_error'),(196,'navigation__statflow__structure_edit_flashMessenger_error'),(197,'navigation__statflow__structure_edit_flashMessenger_success'),(198,'navigation__statflow__structure_delete_flashMessenger_success'),(199,'User'),(200,'Profile'),(201,'navigation__statflow__user__index__manage_user_title'),(202,'navigation__statflow__user__index__manage_user_add'),(203,'navigation__statflow__user_field_nickname'),(205,'navigation__statflow__user_field_created_at'),(206,'navigation__statflow__user_field_last_login'),(207,'navigation__statflow__user_field_username'),(208,'navigation__statflow__user_field_fullname'),(209,'navigation__statflow__structure__index__manage_user_title'),(210,'navigation__statflow__user_field_name'),(211,'navigation__statflow__user_field_first_name'),(212,'navigation__statflow__user_field_last_name'),(213,'navigation__statflow__user_field_email'),(214,'navigation__statflow__user_field_email_confirmation'),(215,'navigation__statflow__user_field_password'),(216,'navigation__statflow__user_field_password_confirmation'),(217,'navigation__statflow__user_field_can_be_deleted'),(218,'navigation__statflow__user_field_is_active'),(219,'navigation__statflow__user_field_is_super_admin'),(220,'navigation__statflow__user_field_is_staff'),(221,'navigation__statflow__user_field_structure'),(222,'navigation__statflow__user__index__manage_user_edit'),(223,'navigation__statflow__user_add_flashMessenger_error'),(224,'navigation__statflow__user_add_flashMessenger_success'),(225,'navigation__statflow__user_edit_flashMessenger_success'),(226,'navigation__statflow__user_delete_flashMessenger_success'),(227,'navigation__statflow__user__index__manage_user_profile'),(228,'navigation__statflow__user_profile_field_nickname'),(229,'navigation__statflow__user_profile_field_about'),(230,'navigation__statflow__user_profile_field_website'),(231,'navigation__statflow__user_profile_field_phone'),(232,'navigation__statflow__user_profile_field_mobile'),(233,'navigation__statflow__layout__my_profile'),(234,'navigation__statflow__layout__logout'),(235,'navigation__statflow__user_edit_flashMessenger_error'),(236,'navigation__statflow__auth__index__please_sign_in'),(237,'navigation__statflow__auth__index__sign_in'),(238,'navigation__statflow__auth__index__username'),(239,'navigation__statflow__auth__index__passwordbackoffice'),(240,'navigation__statflow__auth__index__password'),(241,'Backoffice'),(242,'Contents'),(243,'Answer'),(244,'Answer entry'),(245,'Feedback form'),(246,'Feedback form page'),(247,'Feedback form template'),(248,'Import'),(249,'Import field'),(250,'Question'),(251,'Question entry'),(252,'Question help'),(253,'Respondent'),(254,'Respondent external field'),(255,'Section'),(256,'Structure'),(257,'Study'),(258,'Users'),(259,'Manage group permissions'),(260,'Permission'),(261,'Script permissions'),(262,'Pages'),(263,'Position'),(264,'Template'),(265,'Settings'),(266,'Cache'),(267,'Clear cache'),(268,'Translation'),(269,'Error'),(270,'Frontoffice'),(271,'Pages unactivated'),(272,'Manage navigation'),(273,'navigation'),(274,'Language'),(275,'Title'),(276,'Is published'),(277,'At'),(278,'Actions'),(279,'Offline'),(280,'Online'),(281,'Edit'),(282,'Edit properties'),(283,'Label'),(284,'Module name'),(285,'Controller name'),(286,'Action name'),(287,'Params (json)'),(288,'Route name'),(289,'URI'),(290,'Visible?'),(291,'Stylesheet'),(292,'Proxy'),(293,'Translated from'),(294,'On'),(295,'Off'),(296,'Description'),(297,'navigation__statflow__structure__index__manage_groups'),(298,'navigation__statflow__structure__index__manage_permissions'),(299,'navigation__statflow__permission__index__manage_permission_title'),(300,'navigation__statflow__permission__index__manage_permission_add'),(301,'navigation__statflow__permission_field_name'),(302,'navigation__statflow__permission_field_description'),(303,'navigation__statflow__permission__index__manage_permission_tab1'),(304,'navigation__statflow__permission__index__manage_permission_tab2'),(305,'navigation__statflow__permission_field_permissions'),(306,'Module:'),(307,'Controller:'),(308,'Ressource:'),(309,'navigation__statflow__permission_addPerm_flashMessenger_error'),(310,'navigation__statflow__permission_addPerm_flashMessenger_success'),(311,'navigation__statflow__permission_deletePerm_flashMessenger_error'),(312,'navigation__statflow__permission_deletePerm_flashMessenger_success'),(313,'navigation__statflow__permission_add_flashMessenger_success'),(314,'navigation__statflow__permission_delete_flashMessenger_success'),(315,'navigation__statflow__permission_add_flashMessenger_error'),(316,'navigation__statflow__permission__index__manage_permission_edit'),(317,'navigation__statflow__permission_edit_flashMessenger_error'),(318,'navigation__statflow__permission_edit_flashMessenger_success'),(319,'navigation__statflow__permission_grant_all'),(320,'navigation__statflow__permission_deny_all'),(321,'navigation__statflow__permission_btn_mass_actions'),(322,'navigation__statflow__permission_grantallPerm_flashMessenger_success'),(323,'navigation__statflow__permission_denyallPerm_flashMessenger_error'),(324,'navigation__statflow__permission_grantallPerm_flashMessenger_error'),(325,'navigation__statflow__permission_denyallPerm_flashMessenger_success'),(326,'navigation__statflow__study__index__manage_study_title'),(327,'navigation__statflow__study__index__manage_study_add'),(328,'navigation__statflow__study_field_name'),(329,'navigation__statflow__study_field_language'),(330,'navigation__statflow__study_field_structure'),(331,'navigation__statflow__study_field_is_published'),(332,'navigation__statflow__study_field_is_published_yes'),(333,'navigation__statflow__study_field_is_published_no'),(334,'navigation__statflow__study_add_flashMessenger_success'),(335,'navigation__statflow__study_field_created_at'),(336,'navigation__statflow__study_unpublish_flashMessenger_success'),(337,'navigation__statflow__study_publish_flashMessenger_success'),(338,'navigation__statflow__study_unpublish_flashMessenger_error'),(339,'navigation__statflow__study_publish_flashMessenger_error'),(340,'navigation__statflow__study_delete_flashMessenger_success'),(341,'navigation__statflow__study_delete_flashMessenger_error'),(342,'navigation__statflow__study__index__manage_study_edit'),(343,'navigation__statflow__study_edit_flashMessenger_success'),(344,'navigation__statflow__study_field_forms'),(348,'navigation__statflow__feedbackform_field_name'),(349,'navigation__statflow__feedbackform_field_language'),(350,'navigation__statflow__feedbackform_field_feedback_form_template'),(351,'navigation__statflow__feedbackform_field_is_published'),(352,'navigation__statflow__feedbackform_field_is_published_yes'),(353,'navigation__statflow__feedbackform_field_is_published_no'),(355,'navigation__statflow__feedbackform_field_introduction_text'),(356,'navigation__statflow__feedbackform_field_introduction_is_visible'),(357,'navigation__statflow__feedbackform_field_introduction_is_visible_yes'),(358,'navigation__statflow__feedbackform_field_introduction_is_visible_no'),(359,'navigation__statflow__feedbackform_field_publication_start'),(360,'navigation__statflow__feedbackform_field_publication_end'),(364,'navigation__statflow__study_btn_mass_actions'),(366,'navigation__statflow__study_field_studies'),(367,'navigation__statflow__study_field_feedbackforms'),(373,'navigation__statflow__feedbackform_field_feedback_form_template_add'),(374,'navigation__statflow__feedbackform_template_field_name'),(375,'navigation__statflow__feedbackform_template_field_file'),(377,'navigation__statflow__study__index__manage_feedbackformtemplate_title'),(378,'navigation__statflow__study__index__manage_feedbackformpage_title'),(379,'navigation__statflow__study_field_study'),(380,'navigation__statflow__feedback_field_is_published'),(381,'navigation__statflow__feedbackform__index__manage_feedbackform_title'),(382,'navigation__statflow__feedbackform__index__manage_feedbackform_add'),(383,'navigation__statflow__study_action_add_feedback_form'),(385,'navigation__statflow__feedbackform_unpublish_flashMessenger_error'),(386,'navigation__statflow__feedbackform_unpublish_flashMessenger_success'),(387,'navigation__statflow__feedbackform_publish_flashMessenger_success'),(388,'navigation__statflow__feedbackform_publish_flashMessenger_error'),(389,'navigation__statflow__feedbackform_edit_flashMessenger_success'),(390,'navigation__statflow__feedbackform_delete_flashMessenger_success'),(391,'navigation__statflow__feedbackform_delete_flashMessenger_error'),(392,'navigation__statflow__feedbackform__index__manage_feedbackform_edit'),(394,'navigation__statflow__feedbackform_field_study'),(396,'navigation__statflow__feedbackform_add_flashMessenger_success'),(398,'navigation__statflow__study_see_feedbackforms'),(399,'navigation__statflow__feedbackformtemplate__index__manage_feedbackformtemplate_title'),(400,'navigation__statflow__feedbackform__index__manage_feedbackformtemplate_add'),(401,'navigation__statflow__feedbackformtemplate_field_name'),(402,'navigation__statflow__feedbackformtemplate__index__manage_feedbackformtemplate_add'),(403,'navigation__statflow__feedbackformtemplate_field_file'),(404,'navigation__statflow__feedbackformtemplate_delete_flashMessenger_error'),(405,'navigation__statflow__feedbackformtemplate__index__manage_feedbackformtemplate_edit'),(406,'navigation__statflow__feedbackformtemplate_delete_flashMessenger_success'),(407,'navigation__statflow__feedbackformpage__index__manage_feedbackformpage_title'),(408,'navigation__statflow__feedbackform__index__manage_feedbackformpage_add'),(409,'navigation__statflow__feedbackformpage_field_name'),(410,'navigation__statflow__feedbackformpage__index__manage_feedbackformtemplate_title'),(411,'navigation__statflow__feedbackformpage__index__manage_feedbackformpage_add'),(412,'navigation__statflow__feedbackformpage_field_feedback_form'),(413,'navigation__statflow__feedbackformpage_field_title_is_visible'),(414,'navigation__statflow__feedbackformpage_add_flashMessenger_error'),(415,'navigation__statflow__feedbackformpage_add_flashMessenger_success'),(416,'navigation__statflow__feedbackformpage_delete_flashMessenger_success'),(417,'navigation__statflow__feedbackformpage_delete_flashMessenger_error'),(418,'navigation__statflow__feedbackformpage__index__manage_feedbackformpage_edit'),(419,'navigation__statflow__feedbackformpage_edit_flashMessenger_success'),(420,'navigation__statflow__layout_close'),(421,'navigation__statflow__feedbackform__index__publish'),(422,'navigation__statflow__feedbackform__index__unpublish'),(425,'navigation__statflow__layout_delete_confirmation'),(426,'navigation__statflow__layout_confirm'),(427,'navigation__statflow__layout_delete_confirmation_question'),(428,'navigation__statflow__feedbackformtemplate_add_flashMessenger_error'),(429,'navigation__statflow__question__index__manage_question_title'),(430,'navigation__statflow__question__index__manage_question_add'),(434,'navigation__statflow__question_field_code'),(435,'navigation__statflow__question_field_feedback_form'),(436,'navigation__statflow__question_field_feedback_form_page'),(437,'navigation__statflow__question_field_language'),(438,'navigation__statflow__question_field_section'),(440,'navigation__statflow__question_field_type'),(441,'navigation__statflow__question_field_content'),(442,'QUESTION_TYPE_MCQ'),(443,'QUESTION_TYPE_INTERVAL'),(444,'QUESTION_TYPE_ORDERED'),(445,'QUESTION_TYPE_OPEN'),(446,'QUESTION_TYPE_ARRAY'),(447,'navigation__statflow__question_add_flashMessenger_error'),(448,'navigation__statflow__question_add_flashMessenger_success'),(449,'navigation__statflow__question__index__manage_question_edit'),(450,'navigation__statflow__question_field_help'),(451,'navigation__statflow__question__index__field_page'),(452,'navigation__statflow__page__index__field_questions'),(453,'navigation__statflow__question__index__field_last_update'),(454,'navigation__statflow__page__index_field_questions'),(455,'navigation__statflow__page__index_field_question_add_title'),(456,'navigation__statflow__layout_add'),(458,'navigation__statflow__question_delete_flashMessenger_error'),(459,'navigation__statflow__question_delete_flashMessenger_success'),(461,'MCQ_MODALITY_EXCLUSIVE'),(463,'MCQ_MODALITY_SIMPLE'),(465,'MCQ_MODALITY_COMPLEMENT'),(466,'navigation__statflow__questionentry_add_flashMessenger_error'),(467,'navigation__statflow__questionentry_add_flashMessenger_success'),(468,'navigation__statflow__questionentry__index_field_label'),(469,'navigation__statflow__questionentry__index_if_yes_goto_question'),(470,'navigation__statflow__questionentry_delete_flashMessenger_success'),(471,'navigation__statflow__questionentry_delete_flashMessenger_error'),(472,'navigation__statflow__questionentry_edit_flashMessenger_error'),(473,'navigation__statflow__questionentry_edit_flashMessenger_success'),(474,'ORDERED_MODALITY'),(476,'navigation__statflow__questionentry__index_field_ordered_max_choices'),(477,'navigation__statflow__questionentry__index_interval'),(479,'navigation__statflow__questionentry__index_field_additional_text'),(480,'navigation__statflow__questionentry__index_interval_min'),(481,'navigation__statflow__questionentry__index_interval_max'),(482,'navigation__statflow__questionentry__index_label'),(483,'navigation__statflow__questionentry__index_modality'),(484,'navigation__statflow__questionentry__index_field_type'),(485,'study'),(486,'feedback_form'),(487,'number'),(488,'navigation__statflow__import_index_flashMessenger_error'),(489,'import__statflow__import_index_flashMessenger_error'),(490,'import__statflow__import_index_flashMessenger_error__validation'),(491,'import__statflow__import_index_flashMessenger_error__check'),(492,'import__statflow__import_index_flashMessenger_error__check_and_validate'),(493,'navigation__statflow__feedbackform_add_flashMessenger_error'),(494,'navigation__statflow__import_index_flashMessenger_error__invalid_form'),(495,'import__statflow__import_index_flashMessenger_error__saving'),(496,'navigation__statflow__import_index_flashMessenger_success'),(497,''),(498,'navigation__statflow__user_btn_filter'),(499,'navigation__sephora__allvoices__index__page_title'),(644,'Active'),(645,'Not active'),(646,'Status'),(647,'Yes'),(648,'No'),(649,'Activate'),(650,'Desactivate'),(651,'An error occur when validating the form. See below.'),(654,'Unauthorized access - '),(655,'Unauthorized action'),(656,'As a %s user, you don\'t have the permission to accomplish this action.'),(657,'Go back'),(831,'navigation__statflow__auth__index__forgotten_password'),(832,'navigation__statflow__user_field_activate'),(833,'navigation__statflow__auth__index__reset_form_header'),(834,'navigation__statflow__auth__index__email'),(835,'navigation__statflow__auth__index__submit'),(836,'navigation__statflow__auth__do-reset__form_header'),(837,'navigation__statflow__auth__do-reset__password'),(838,'navigation__statflow__auth__do-reset__confirmation'),(839,'navigation__statflow__auth__do-reset__submit'),(840,'navigation__statflow__user_field_group'),(841,'statflow__auth__reset__check_your_email'),(882,'statflow__auth__do-reset__password-not-match'),(1025,'navigation__statflow__layout_preview'),(1026,'QUESTION_TYPE_MULTIPLE_CHOICE'),(1029,'navigation__statflow__question_field_fixed-bottom'),(1030,'navigation__statflow__question_field_required'),(1031,'navigation__statflow__layout_save'),(1034,'navigation__statflow__feedbackform_field_is_open'),(1035,'navigation__statflow__feedbackform_field_is_closed'),(1258,'navigation__statflow__feedbackform_question_name-pending'),(1259,'navigation__statflow__feedbackform_field_introduction_help_pending'),(1281,'navigation__statflow__feedbackform_field_introduction_text_pending'),(1317,'navigation__statflow__field_conditional_display'),(1319,'navigation__statflow__layout_alert'),(1320,'navigation__statflow__layout_relance'),(1323,'navigation__statflow__layout_add_question'),(1324,'navigation__statflow__layout_add_choice'),(1329,'navigation__statflow__feedbackform_field_image'),(1331,'QUESTION_TYPE_IMAGE'),(1334,'QUESTION_TYPE_MULTIPLE_IMAGES'),(1336,'MCQ_MODALITY_SIMPLE_IMAGE'),(1337,'MCQ_MODALITY_EXCLUSIVE_IMAGE'),(1338,'navigation__statflow__questionentry__index_field_image'),(1354,'navigation__statflow__feedbackform_field_style'),(1461,'navigation__statflow__feedbackform__alerts'),(1470,'navigation__statflow__feedbackform_edit_flashMessenger_error'),(1471,'navigation__statflow__study_add_flashMessenger_error'),(1472,'navigation__statflow__study__index__see_feedback_forms'),(1473,'navigation__statflow__feedbackform__access'),(1477,'form__page_name-pending'),(1524,'form__question_title-missing-code');
/*!40000 ALTER TABLE `translation_uid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `about` text,
  `website` varchar(150) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `avatar_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profile__user_id___user__id` (`user_id`),
  KEY `fk_profile__avatar_id___file__id` (`avatar_id`),
  CONSTRAINT `fk_profile__avatar_id___file__id` FOREIGN KEY (`avatar_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `fk_profile__user_id___user__id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` VALUES (1,1,'admin',NULL,NULL,NULL,NULL,'2013-04-22 13:00:11','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-23 14:51:37
