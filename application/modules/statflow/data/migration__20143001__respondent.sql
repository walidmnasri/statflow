ALTER TABLE `statflow_respondent`
ADD COLUMN `started_answering_at` TIMESTAMP NULL AFTER `is_very_unsatisfied`,
ADD COLUMN `completion_duration` INT NULL AFTER `answered_at`,
ADD COLUMN `structure_belonging` VARCHAR(45) NULL AFTER `token`,
ADD COLUMN `locale` VARCHAR(45) NULL AFTER `structure_belonging`,
ADD COLUMN `comment` TEXT NULL AFTER `locale`;
