<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_FeedbackFormTemplateTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/feedbackFormTemplate');
        $this->addColumns(
                    array(
                        'id',
                        'name',
                        'file',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

