<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_ImportTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/import');
        $this->addColumns(
                    array(
                        'id',
                        'study_id',
                        'feedback_form_id',
                        'number',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

