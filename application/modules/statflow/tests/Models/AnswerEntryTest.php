<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_AnswerEntryTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/answerEntry');
        $this->addColumns(
                    array(
                        'id',
                        'question_entry_id',
                        'answer_id',
                        'entry_type_id',
                        'content',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

