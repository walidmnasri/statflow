<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_StructureTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/structure');
        $this->addColumns(
                    array(
                        'id',
                        'parent_id',
                        'name',
                        'slug',
                        'external_id',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

