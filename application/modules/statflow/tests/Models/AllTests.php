<?php

if (! defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'Statflow_Test_Models_AllTests::main');
}

require_once dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_AllTests
{

    public static function main ()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }

    public static function suite ()
    {
        $suite = new PHPUnit_Framework_TestSuite('Statflow Model test suite Suite');
        $suite->addTestSuite('Statflow_Test_Models_AnswerTest');
        $suite->addTestSuite('Statflow_Test_Models_AnswerEntryTest');
        $suite->addTestSuite('Statflow_Test_Models_FeedbackFormTest');
        $suite->addTestSuite('Statflow_Test_Models_FeedbackFormPageTest');
        $suite->addTestSuite('Statflow_Test_Models_FeedbackFormTemplateTest');
        $suite->addTestSuite('Statflow_Test_Models_ImportTest');
        $suite->addTestSuite('Statflow_Test_Models_ImportFieldTest');
        $suite->addTestSuite('Statflow_Test_Models_QuestionTest');
        $suite->addTestSuite('Statflow_Test_Models_QuestionEntryTest');
        $suite->addTestSuite('Statflow_Test_Models_QuestionHelpTest');
        $suite->addTestSuite('Statflow_Test_Models_RespondentTest');
        $suite->addTestSuite('Statflow_Test_Models_RespondentExternalFieldTest');
        $suite->addTestSuite('Statflow_Test_Models_SectionTest');
        $suite->addTestSuite('Statflow_Test_Models_StructureTest');
        $suite->addTestSuite('Statflow_Test_Models_StudyTest');

        return $suite;
    }
}
if (PHPUnit_MAIN_METHOD == 'Statflow_Test_Models_AllTests::main') {
    Statflow_Test_Models_AllTests::main();
}    