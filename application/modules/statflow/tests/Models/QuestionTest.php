<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_QuestionTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/question');
        $this->addColumns(
                    array(
                        'id',
                        'section_id',
                        'question_type_id',
                        'feedback_form_page_id',
                        'content',
                        'code',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

