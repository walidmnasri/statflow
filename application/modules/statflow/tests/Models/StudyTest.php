<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_StudyTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/study');
        $this->addColumns(
                    array(
                        'id',
                        'language_id',
                        'name',
                        'slug',
                        'is_published',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

