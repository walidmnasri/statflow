<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_RespondentExternalFieldTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/respondentExternalField');
        $this->addColumns(
                    array(
                        'id',
                        'respondent_id',
                        'import_field_id',
                        'label',
                        'value',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

