<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_RespondentTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/respondent');
        $this->addColumns(
                    array(
                        'id',
                        'study_id',
                        'feedback_form_id',
                        'firstname',
                        'lastname',
                        'external_id',
                        'email',
                        'is_alert',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

