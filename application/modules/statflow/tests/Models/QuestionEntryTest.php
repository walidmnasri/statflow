<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_QuestionEntryTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/questionEntry');
        $this->addColumns(
                    array(
                        'id',
                        'question_id',
                        'entry_type_id',
                        'content',
                        'complement_data',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

