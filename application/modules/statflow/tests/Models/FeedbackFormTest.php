<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_FeedbackFormTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/feedbackForm');
        $this->addColumns(
                    array(
                        'id',
                        'study_id',
                        'language_id',
                        'feedback_form_template_id',
                        'name',
                        'slug',
                        'is_published',
                        'introduction_text',
                        'introduction_file_id',
                        'introduction_is_visible',
                        'publication_start',
                        'publication_end',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

