<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_QuestionHelpTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/questionHelp');
        $this->addColumns(
                    array(
                        'id',
                        'question_id',
                        'content',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

