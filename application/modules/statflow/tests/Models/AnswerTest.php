<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_AnswerTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/answer');
        $this->addColumns(
                    array(
                        'id',
                        'respondent_id',
                        'feedback_form_id',
                        'question_id',
                        'value',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

