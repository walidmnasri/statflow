<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_FeedbackFormPageTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/feedbackFormPage');
        $this->addColumns(
                    array(
                        'id',
                        'feedback_form_id',
                        'name',
                        'title_is_visible',
                        'order',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

