<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_ImportFieldTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/importField');
        $this->addColumns(
                    array(
                        'id',
                        'import_id',
                        'study_id',
                        'feedback_form_id',
                        'label',
                        'external_id',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

