<?php

require_once ''.dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Models_SectionTest extends Centurion_Test_DbTable
{

    public function setUp()
    {
        $this->setTable('statflow/section');
        $this->addColumns(
                    array(
                        'id',
                        'name',
                        'created_at',
                        'updated_at',
                )
            );
    }


}

