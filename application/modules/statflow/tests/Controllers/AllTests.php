<?php

if (! defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'Statflow_Test_Controllers_AllTests::main');
}

require_once dirname(__FILE__) . '/../../../../../tests/TestHelper.php';

class Statflow_Test_Controllers_AllTests
{

    public static function main ()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }

    public static function suite ()
    {
        $suite = new PHPUnit_Framework_TestSuite('Statflow Controller test suite Suite');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminAnswerControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminAnswerEntryControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminFeedbackFormControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminFeedbackFormPageControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminFeedbackFormTemplateControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminImportControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminImportFieldControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminQuestionControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminQuestionEntryControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminQuestionHelpControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminRespondentControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminRespondentExternalFieldControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminSectionControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminStructureControllerTest');
        $suite->addTestSuite('Statflow_Test_Controllers_AdminStudyControllerTest');

        return $suite;
    }
}
if (PHPUnit_MAIN_METHOD == 'Statflow_Test_Controllers_AllTests::main') {
    Statflow_Test_Controllers_AllTests::main();
}    