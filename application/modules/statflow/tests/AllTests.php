<?php

if (! defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'Statflow_Test_AllTests::main');
}

require_once dirname(__FILE__) . '/../../../../tests/TestHelper.php';

class Statflow_Test_AllTests
{
    public static function main ()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }

    public static function suite ()
    {
        $suite = new PHPUnit_Framework_TestSuite('Statflow test suite');
        $suite->addTest(Statflow_Test_Models_AllTests::suite());
        $suite->addTest(Statflow_Test_Controllers_AllTests::suite());
        return $suite;
    }
}
if (PHPUnit_MAIN_METHOD == 'Statflow_Test_AllTests::main') {
    Statflow_Test_AllTests::main();
}    