<?php
require_once APPLICATION_PATH . '/../library/Solarium/Autoloader.php';
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcherInterface.php' ;
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/Event.php' ;
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcher.php' ;
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/ImmutableEventDispatcher.php';
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/GenericEvent.php';
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventSubscriberInterface.php';
require_once APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/ContainerAwareEventDispatcher.php';
require_once __DIR__ . '/../../statflow/services/Structure.php';
require_once __DIR__ . '/../../statflow/services/FeedbackForm.php';
require_once __DIR__ . '/../../statflow/services/KpiManagement.php';
require_once __DIR__ . '/../../statflow/services/Study.php';
require_once __DIR__ . '/Solr.php';


use Statflow\Service\Structure;
use Statflow\Service\FeedbackForm;
use Statflow\Service\KpiManagement;
use Statflow\Service\Study;

class Costumer_Service_Global
{

    /**
     * @var Solarium\Client
     */
    protected $_client;
    
    /**
     * @var string
     */
    protected $_filters;

    /**
     * @var Structure
     */
    private $structureService;
    
    /**
     * @var FeedbackForm
     */
    private $feedbackFormService;
    
    /**
     * @var FeedbackForm
     */
    private $kpiService;
    
    private $studyService;
    
    public function getFilters()
    {
        return $this->_filters;
    }
    
    public function getKpiService()
    {
        if (!$this->kpiService) {
            $this->kpiService = new KpiManagement();
        }
        return $this->kpiService ;
    }
    
    public function getFeedbackFormService()
    {
        if (!$this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
        return $this->feedbackFormService ;
    }

    public function getStudyService() 
    {
        if (!$this->studyService) {
            $this->studyService = new Study();
        }
        return $this->studyService ;
    }
    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
    
        return $this->structureService;
    }
    
    public function __construct()
    {
        $this->_filters = new Costumer_Model_Filters();
    }
    
    public function GetAllFeedbackAndKpiByTypeAndStudy($type, $studyId, $userId)
    {
        $kpiService = $this->getKpiService();
        $result = [];
        $feedbacks = $kpiService->getFeedbackByTypeAndStudyId($type, $studyId, $userId);
        if (!empty($feedbacks)) {
            foreach ($feedbacks as $feedback) {
                $kpis = $kpiService->getKpiByFeedbackAndType($type, $feedback['id']);
                 $arrayKpi = [];
                if (!empty($kpis)) {
                    foreach ($kpis as $kpi) {
                        $arrayKpi[$kpi['idKpi']] = $kpi['Kpiname'];
                    }
                }
                $result[$feedback['id']] = [ 'name' => $feedback['FeedbackName'],
                                             'arraykpi' => $arrayKpi
                                             ];           
            }
        }
        return $result;
    }
    
    /**
     * 
     * @param array $kpiIds
     * @return multitype:multitype:Ambigous <string, boolean, mixed>
     */
    public function getQuestionBykpiStatDetail($kpiIds)
    {
        $kpiService = $this->getKpiService();
        $result = [];
        if (!is_array($kpiIds)) {
            die ('kpi empty !!');
        }
        foreach ($kpiIds as $kpiId) {
            $questions = $kpiService->getQuestionBykpi($kpiId);
            $questionArray = explode(',', $questions);
            
            $codes = [];
            foreach ($questionArray as $value) {
                $codes[$value] = KpiManagement::getCodeQuestion($value);
            }
            $result[$kpiId] = $codes;
        }
       return $result;
    }
    
    /**
     * 
     * @param array $Kpi
     * @return array
     */    
    public function getOverAllResult($Kpis, $study, $filters) 
    {
        $this->getFilters()->setReceivedFilters($filters, false);
        $kpiService     = $this->getKpiService();
        $feedbackForm   = $this->getFeedbackFormService();
        $studyService   =  $this->getStudyService();
        $translator     = Zend_Registry::get('Zend_Translate');
        
        $codes_questions = $this->getCodesQuestions($kpiService, $Kpis);
        $color = $studyService->getById($study)->ecran_global;
        $allResult = [];
        $dataVerySatisfied   = [];
        $dataSatisfied       = [];
        $dataUnsatisfied     = [];
        $dataVeryUnsatisfied = [];
        $averageScore        = [];
        $xLabel              = [];
        foreach ($codes_questions as $key => $code_question) {
            $solr = new Costumer_Service_Solr($this->getFilters()->getReceivedFilters(), $key);
            $name = $feedbackForm->getById($key)->name;
            foreach ($code_question as $k => $code) {
                $allResult = $solr->getOverAllSatisfaction($code[0], $k);
                $dataVerySatisfied[]    = $allResult['graphData'][3]['data'][0];
                $dataSatisfied[]        = $allResult['graphData'][2]['data'][0];
                $dataUnsatisfied[]      = $allResult['graphData'][1]['data'][0];
                $dataVeryUnsatisfied[]  = $allResult['graphData'][0]['data'][0];
                $averageScore[]         = (float) $allResult['average'];
                $xLabel[]               = $name . '(' .$code[0]. ')';
            }
        }
        // add global column
        array_unshift($dataVerySatisfied, array_sum($dataVerySatisfied));
        array_unshift($dataSatisfied, array_sum($dataSatisfied));
        array_unshift($dataUnsatisfied, array_sum($dataUnsatisfied));
        array_unshift($dataVeryUnsatisfied, array_sum($dataVeryUnsatisfied));
        if (count($averageScore) == 0) {
            return ['error' => true,
                    'msg' => $translator->translate('label__study_'.$study.'__no-selected-msg@study_'.$study)
                ];
        } 
        array_unshift($averageScore, array_sum($averageScore) / count($averageScore));
        array_unshift($xLabel, $translator->translate('label__study_'.$study.'__index__main__global@study_'.$study));
        
        return  ['type' => KpiManagement::KPI_SATGLO,
            'result' => [
                'Very_satisfied'       => $dataVerySatisfied,
                'Satisfied'            => $dataSatisfied,
                'Not_satisfied'        => $dataUnsatisfied,
                'Not_satisfied_at_all' => $dataVeryUnsatisfied,
                'Average_score'        => $averageScore
            ],
            'label' => $xLabel,
            'color' => Zend_Json::decode($color)
        ];
    }
    
    /**
     *
     * @param array $Kpi
     * @return array
     */
    public function getDetailedResult($Kpis, $study, $filters)
    {
        $this->getFilters()->setReceivedFilters($filters, false);
        $kpiService     = $this->getKpiService();
        $feedbackForm   = $this->getFeedbackFormService();
        $studyService   =  $this->getStudyService();
        $translator     = Zend_Registry::get('Zend_Translate');
    
        $codes_questions = $this->getCodeQuestionsToDetailedSatisfaction($kpiService, $Kpis);
        $allResult = [];
        $dataVerySatisfied   = [];
        $dataSatisfied       = [];
        $dataUnsatisfied     = [];
        $dataVeryUnsatisfied = [];
        $averageScore        = [];
        $xLabel              = [];
        
        $color = $studyService->getById($study)->ecran_global;
        foreach ($codes_questions as $key => $code_question) {
            $solr = new Costumer_Service_Solr($this->getFilters()->getReceivedFilters(), $key);
            $name = $feedbackForm->getById($key)->name;
            foreach ($code_question as $k => $code) {
                $allResult = $solr->getSatisfactionDetailed($code, $k, [$Kpis['question_' . $k]]);
                $dataVerySatisfied[]    = array_values($allResult['graphData'])[3]['data'][0];
                $dataSatisfied[]        = array_values($allResult['graphData'])[2]['data'][0];
                $dataUnsatisfied[]      = array_values($allResult['graphData'])[1]['data'][0];
                $dataVeryUnsatisfied[]  = array_values($allResult['graphData'])[0]['data'][0];
                $averageScore[]         = (float) array_values($allResult['averageData'])[0];
                $xLabel[]               = $name . ' (' .$Kpis['question_' .$k]. ')';
            }
        }
        // add global column
        array_unshift($dataVerySatisfied, array_sum($dataVerySatisfied));
        array_unshift($dataSatisfied, array_sum($dataSatisfied));
        array_unshift($dataUnsatisfied, array_sum($dataUnsatisfied));
        array_unshift($dataVeryUnsatisfied, array_sum($dataVeryUnsatisfied));
        if (count($averageScore) == 0) {
            return ['error' => true,
                    'msg' => $translator->translate('label__study_'.$study.'__no-selected-msg@study_'.$study)
                ];
        } 
        array_unshift($averageScore, array_sum($averageScore) / count($averageScore));
        array_unshift($xLabel, $translator->translate('label__study_'.$study.'__index__main__global@study_'.$study));
    
        return  ['type' => KpiManagement::KPI_SATDET,
            'result' => [
                'Very_satisfied'       => $dataVerySatisfied,
                'Satisfied'            => $dataSatisfied,
                'Not_satisfied'        => $dataUnsatisfied,
                'Not_satisfied_at_all' => $dataVeryUnsatisfied,
                'Average_score'        => $averageScore
            ],
            'label' => $xLabel,
            'color' => Zend_Json::decode($color)
        ];
    }
    
    /**
     * 
     * @param array $Kpis
     * @return multitype:number multitype:multitype:NULL  multitype:number
     */
    public function getExpGloResult($Kpis, $study, $filters)
    {
        $this->getFilters()->setReceivedFilters($filters, false);
        $kpiService = $this->getKpiService();
        $feedbackForm   = $this->getFeedbackFormService();
        $studyService   =  $this->getStudyService();
        $translator = Zend_Registry::get('Zend_Translate');
        $codes_questions = $this->getCodesQuestions($kpiService, $Kpis);
        $color = $studyService->getById($study)->ecran_global;
        
        $allResult = [];
        $dataVeryGood = [];
        $dataGood     = [];
        $dataPoor     = [];
        $dataVeryPoor = []; 
        $xLabel       = [];
        
        foreach ($codes_questions as $key => $code_question) {
            $solr = new Costumer_Service_Solr($this->getFilters()->getReceivedFilters(), $key);
            $name = $feedbackForm->getById($key)->name;
            foreach ($code_question as $k => $code) {
                $allResult = $solr->getSatisfactionGrading($code[0], $k);
                $dataVeryGood[]    = $allResult[3][1];
                $dataGood[]        = $allResult[2][1];
                $dataPoor[]        = $allResult[1][1];
                $dataVeryPoor[]    = $allResult[0][1];
                $xLabel[]          = $name . ' (' .$code[0]. ')';
            }
        }
        
        if (!count($xLabel)) {
            return ['error' => true,
                'msg' => $translator->translate('label__study_'.$study.'__no-selected-msg@study_'.$study)
            ];
        }
        // calculate global column
        array_unshift($dataVeryGood, array_sum($dataVeryGood));
        array_unshift($dataGood, array_sum($dataGood));
        array_unshift($dataPoor, array_sum($dataPoor));
        array_unshift($dataVeryPoor, array_sum($dataVeryPoor));
        array_unshift($xLabel, $translator->translate('label__study_'.$study.'__index__main__global@study_'.$study));
    
        return  ['type' => KpiManagement::KPI_EXPGLO,
            'result' => [
                'Very_satisfied'       => $dataVeryGood,
                'Satisfied'            => $dataGood,
                'Not_satisfied'        => $dataPoor,
                'Not_satisfied_at_all' => $dataVeryPoor
            ],
            'label' => $xLabel,
            'color' => Zend_Json::decode($color)
        ];
    }
    
    
    /**
     *
     * @param array $Kpis
     * @return multitype:number multitype:multitype:NULL  multitype:number
     */
    public function getNpsGloResult($Kpis, $study, $filters)
    {
        $this->getFilters()->setReceivedFilters($filters, false);
        $kpiService = $this->getKpiService();
        $feedbackForm   = $this->getFeedbackFormService();
        $studyService   =  $this->getStudyService();
        $translator = Zend_Registry::get('Zend_Translate');
        $codes_questions = $this->getCodesQuestions($kpiService, $Kpis);
        $color = $studyService->getById($study)->ecran_global;
        
        $allResult = [];
        $dataPromoters  = [];
        $dataPassive    = [];
        $dataDetractors = [];
        $dataNPS        = [];
        $averageScore   = [];
    
        foreach ($codes_questions as $key => $code_question) {
            $solr = new Costumer_Service_Solr($this->getFilters()->getReceivedFilters(), $key);
            $name = $feedbackForm->getById($key)->name;
            foreach ($code_question as $k => $code) {
                $allResult = $solr->getNPS($code[0], $k);

                $dataPromoters[]    = $allResult['graphData'][2]['data'][0];
                $dataPassive[]      = $allResult['graphData'][1]['data'][0];
                $dataDetractors[]   = $allResult['graphData'][0]['data'][0];
                $dataNPS[]          = (float) $allResult['npsValue'];
                $averageScore[]     = (float) $allResult['averageData'];
                $xLabel[]           = $name . ' (' .$code[0]. ')';
            }
        }
        // add global column        
        array_unshift($dataPromoters, array_sum($dataPromoters));
        array_unshift($dataPassive, array_sum($dataPassive));
        array_unshift($dataDetractors, array_sum($dataDetractors));
        if (!count($averageScore) || !count($dataNPS)) {
            return ['error' => true,
                'msg' => $translator->translate('label__study_'.$study.'__no-selected-msg@study_'.$study)
            ];
        }
        array_unshift($averageScore, array_sum($averageScore) / count($averageScore));
        array_unshift($dataNPS, array_sum($dataNPS) / count($dataNPS));
        array_unshift($xLabel, $translator->translate('label__study_'.$study.'__index__main__global@study_'.$study));
        
        return  ['type' => KpiManagement::KPI_NPSGLO,
            'result' => [
                'Promoters'     => $dataPromoters,
                'Passive'       => $dataPassive,
                'Detractors'    => $dataDetractors,
                'NPS'           => $dataNPS,
                'Average_score' => $averageScore
            ],
            'label' => $xLabel,
            'color' => Zend_Json::decode($color)
        ];
    }
    
    private function getCodesQuestions($kpiService, $Kpis)
    {
        $codes_questions = [];
        foreach ($Kpis as $key => $kpi) {
            if (is_array($kpi) && $key != 'sel_kpi_filter' && $key !='sel_kpi_filters') {
                $feedbackForm = explode('-', $key)[1];
                foreach ($Kpis[$key] as $value) {
                    $idQuestion  = $kpiService->getQuestionBykpi($value);
                    $codes_questions[$feedbackForm][$value][] =  KpiManagement::getCodeQuestion($idQuestion);
                }
            }
        }
        
        return $codes_questions;
    }
    
    private function getCodeQuestionsToDetailedSatisfaction($kpiService, $Kpis)
    {
        $codes_questions = [];
        
        foreach ($Kpis as $key => $kpi) {
            if (is_array($kpi) && $key != 'sel_kpi_filter' && $key !='sel_kpi_filters') {
                $feedbackForm = explode('-', $key)[1];
                foreach ($Kpis[$key] as $value) {
                    $idQuestion  = $kpiService->getQuestionBykpi($value);
                    $detailedQuestions  = explode(',', $idQuestion);       
                    foreach ($detailedQuestions as $id) {
                        $codes_questions[$feedbackForm][$value][] =  KpiManagement::getCodeQuestion($id);
                    }     
                }
            }
        }
        return $codes_questions;
    }
}

