<?php
require_once __DIR__ . '/../../statflow/services/Structure.php';
require_once __DIR__ . '/../../statflow/services/FeedbackForm.php';

use Statflow\Service\Structure;
use Statflow\Service\FeedbackForm;

class Costumer_Service_Solr
{

    /**
     * @var Solarium\Client
     */
    protected $_client;


    /**
     * @var string
     */
    protected $_filters;

    /**
     * @var integer
     */
    protected $_feedback_form_id;

    /**
     * @var integer
     */
    protected $_user_id;


    /**
     * @var array
     */
    protected $_rawFilters;

    /**
     * @var Structure
     */
    private $structureService;

    /**
     * @var FeedbackForm
     */
    private $feedbackFormService;

    public function getFeedbackFormService()
    {
        if (!$this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
        return $this->feedbackFormService ;
    }

    /**
     * @return \Solarium\Client
     */
    public function getClient()
    {
        return $this->_client;
    }
    /**
     *
     * @return \Statflow\Service\KpiManagement
     */
    public function getFeedbackFormId()
    {
        return $this->_feedback_form_id;
    }

    /**
     *
     * @param \Costumer_Service_Solr $value
     */
    public function setUserId($value)
    {
        $this->_user_id = $value;
    }
    /**
     *
     * @return \Costumer_Service_Solr
     */
    public function getUserId()
    {
        return $this->_user_id;
    }

    /**
     *
     * @param \Statflow\Service\KpiManagement $kpiManagement
     */
    public function setFeedbackFormId($value)
    {
        $this->_feedback_form_id = $value;
    }

    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }

        return $this->structureService;
    }

    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }

    /**
     * @void
     */
    public function __construct($filters = array(), $feedback_id)
    {

        require_once(APPLICATION_PATH . '/../library/Solarium/Autoloader.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcherInterface.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/Event.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventDispatcher.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/ImmutableEventDispatcher.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/GenericEvent.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/EventSubscriberInterface.php');
        require_once(APPLICATION_PATH . '/../library/Symfony/Component/EventDispatcher/ContainerAwareEventDispatcher.php');

        $this->setFeedbackFormId($feedback_id);
        Solarium\Autoloader::register();
        $config = array(
            'endpoint' => array(
                'localhost' => array(
                    'host' => 'java',
                    'port' => 8986,
                    'path' => '/solr/',
                    'core' => $this->_feedback_form_id,
                )
            )
        );
        $this->_client = new Solarium\Client($config);
        $this->_client->getPlugin('postbigrequest');
        $this->_rawFilters = $filters;
        $this->_filters = $this->_convertFiltersForSolr($filters, false);

    }

    /**
     *
     */
    public function convertFiltersForSolr()
    {
        $this->_filters = $this->_convertFiltersForSolr($this->_rawFilters, true);
    }

    /**
     * @param $filters
     * @param $getAll
     * @return string
     */
    protected function _convertFiltersForSolr($filters, $getAll)
    {
        $structureService = $this->getStructureService();
        $feedbackForm = $this->getFeedbackFormService();
        $form = $feedbackForm->getById($this->_feedback_form_id);

        $user_id=0;
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
        }
        $this->_user_id = $user_id;

        $queryFilters = array();
        if (isset($filters['periodStart']) && $filters['periodEnd']) {
            if ($filters['periodStart'] != '' || $filters['periodEnd'] != '') {
                if ($filters['periodStart'] != '' && $filters['periodEnd'] != '') {
                    $periodStart = DateTime::createFromFormat('d/m/Y', $filters['periodStart']);
                    $periodEnd = DateTime::createFromFormat('d/m/Y', $filters['periodEnd']);
                    $queryFilters[] = 'answer_date:[' . $periodStart->format('Y-m-d') . 'T00:00:00Z TO ' . $periodEnd->format('Y-m-d') . 'T23:59:59Z]';
                }
                else if ($filters['periodStart'] != '') {
                    $periodStart = DateTime::createFromFormat('d/m/Y', $filters['periodStart']);
                    $queryFilters[] = 'answer_date:[' . $periodStart->format('Y-m-d') . 'T00:00:00Z TO *]';
                }
                else if ($filters['periodEnd'] != '') {
                    $periodEnd = DateTime::createFromFormat('d/m/Y', $filters['periodEnd']);
                    $queryFilters[] = 'answer_date:[* TO ' . $periodEnd->format('Y-m-d') . 'T23:59:59Z]';
                }
            }
        }

        if (isset($filters['purchaseStart'])) {
            if ($filters['purchaseStart'] != '' || $filters['purchaseEnd'] != '') {
                if ($filters['purchaseStart'] != '' && $filters['purchaseEnd'] != '') {
                    $purchaseStart = DateTime::createFromFormat('d/m/Y' , $filters['purchaseStart']);
                    $purchaseEnd = DateTime::createFromFormat('d/m/Y' , $filters['purchaseEnd']);
                    $queryFilters[] = $form->field_label. ' :[' . $purchaseStart->format('Y-m-d') . 'T00:00:00Z TO '. $purchaseEnd->format('Y-m-d') . 'T23:59:59Z]';
                }
                else if ($filters['purchaseStart'] != '') {
                    $periodStart = DateTime::createFromFormat('d/m/Y', $filters['purchaseStart']);
                    $queryFilters[] = $form->field_label. ' :[' . $periodStart->format('Y-m-d') . 'T00:00:00Z TO *]';
                }
                else if ($filters['purchaseEnd'] != '') {
                    $purchaseEnd = DateTime::createFromFormat('d/m/Y', $filters['purchaseEnd']);
                    $queryFilters[] = $form->field_label. ' :[* TO ' . $purchaseEnd->format('Y-m-d') . 'T23:59:59Z]';
                }
            }
        }

        if (isset($filters['filter_structure']) && $filters['filter_structure'] != '') {
            $querySubStructures = str_replace(',', ' OR ', $filters['filter_structure']);
            $queryFilters[] = 'structure_belonging:(' . $querySubStructures . ')';
        } elseif ($this->_user_id) {
            $structures = $structureService->getAllChildStructuresByUserId($this->_user_id);

            if(!empty($structures)) {
                $dataStruct = [];
                $querySubStructures = implode(' OR ', $structures);
                $queryFilters[] = 'structure_belonging:(' . $querySubStructures . ')';
            } else {
                $queryFilters[] = 'structure_belonging : 0';
            }
        }

        if (isset($filters['alert'])) {
            if ($filters['alert'] != '') {
                if($filters['alert'] == 1)
                    $queryFilters[] = '-alert:*';
                elseif($filters['alert'] == 2)
                    $queryFilters[] = 'alert:*';
            }
        }

        if (isset($filters['sel_action'])) {
            if (($filters['sel_action']!= '')) {
                $queryFilters[] = 'action:' . $filters['sel_action'];
            }
        }

        if (isset($filters['anonymous'])) {
            if ($filters['anonymous'] != '') {
                $queryFilters[] = $filters['anonymous'];
            }
        }
        if (!empty($filters['sel_kpi_filters'])) {
            $kpi_filters =  json_decode($filters['sel_kpi_filters']);
            $sel_kpi_filters =  json_decode($filters['sel_kpi_filter']);

            foreach($kpi_filters as $import_field_id => $label) {
                $sel_kpi_filter = $sel_kpi_filters->$import_field_id;
                $solr_field = str_replace(" ", "_", $label);
                if ($sel_kpi_filter != '') {
                    $queryFilters[] = $solr_field.':"'. $sel_kpi_filter.'"';
                }
            }
        }


        if ($getAll === false) {
            $queryFilters[] = 'status:20';
        }

        if(count($queryFilters)) {
            return join(' AND ', $queryFilters);
        }

        return '*:*';
    }

    public function convertFiltersForSolrEvol()
    {
        $filters = $this->_rawFilters;
        $filters['periodStart'] = '';
        $filters['periodEnd'] = '';
        $filters['purchaseStart'] = '';
        $filters['purchaseEnd'] = '';
        return $this->_convertFiltersForSolr($filters, true);
    }

    /**
     * @return string
     */
    private function _getFilters()
    {
        if($this->_filters == '*:*')
            return '';
        else
            return ' AND ' . $this->_filters;
    }

    /**
     * @return string
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @param string $params
     * @return int
     */
    public function getResponseCount($params = 'status:20', $questions = [])
    {
        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        if (is_array($questions) && !empty($questions)) {
            $params = $params . ' AND (' . implode('-Value:[0 TO 10] OR ', $questions) . '-Value:[0 TO 10])';
        }
        $query->addParam('q', $params);
        try {
            // this executes the query and returns the result
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }

        return $resultset->getNumFound();
    }

    /**
     * @param string $params
     * @return int
     */
    public function getAllRightResponse($params = 'status:20')
    {

    }

    /**
     * @return array
     */
    public function getOverAllSatisfaction($code_question, $kpi)
    {
        $limitsConfig   = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q1Config       = $limitsConfig->{'sat-global'};
        $question       = $code_question;
        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        $query->addParam('q', $this->_filters.' AND '.$question.'-Value :[0 TO 10]');
        $bornMax = $q1Config->$question->very_satisfied->to;
        // get the facetset component
        $facetSet = $query->getFacetSet();
        // create a facet query instance and set options
        $facetSet->createFacetMultiQuery('OverAllSatisfaction')
            ->createQuery('respondent_count', $this->_filters)
            ->createQuery('respondent__very_satisfied__count',  $question.'-Value :[' . $q1Config->$question->very_satisfied->from .' TO ' . $bornMax.']')
            ->createQuery('respondent__satisfied__count',       $question.'-Value :[' . $q1Config->$question->satisfied->from .' TO ' . $q1Config->$question->satisfied->to .']')
            ->createQuery('respondent__unsatisfied__count',     $question.'-Value :[' . $q1Config->$question->unsatisfied->from .' TO ' . $q1Config->$question->unsatisfied->to .']')
            ->createQuery('respondent__very_unsatisfied__count',$question.'-Value :[' . $q1Config->$question->very_unsatisfied->from .' TO ' . $q1Config->$question->very_unsatisfied->to .']');
        $statsSet = $query->getStats();
        $statsSet->createField($question.'-Value');
        try {
            // this executes the query and returns the result
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            Zend_Debug::dump($this->_filters);

        }

        $results = $resultset->getFacetSet()->getFacet('OverAllSatisfaction')->getValues();
        $stats = $resultset->getStats();
        $translator = Zend_Registry::get('Zend_Translate');
        $mean = $bornMax == 5 ? $stats->getResults()[$question.'-Value']->getMean() * 2 : $stats->getResults()[$question.'-Value']->getMean();

        return array(
            'graphData' => array(
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__not-satisfied-at-all@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__very_unsatisfied__count'])
                ),
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__not-satisfied@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__unsatisfied__count'])
                ),
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__satisfied@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__satisfied__count'])
                ),
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-satisfied@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__very_satisfied__count'])
                )
            ),
            'average' => (float) number_format($mean, 2)
        );
    }

    /**
     * @return array
     */
    public function getOverAllSatisfactionByMonth($code_question)
    {
        $limitsConfig   = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q1Config       = $limitsConfig->{'sat-global'};
        $question       = $code_question;
        $currentYear    = new DateTime();
        $currentYear    = $currentYear->format('Y');
        $bornMax = $q1Config->$question->very_satisfied->to;

        $dataByMonthAverageQueries = array(
            'janv.' => 'answer_date:['.$currentYear.'-01-01T00:00:00Z TO '.$currentYear.'-01-31T23:59:59Z]',
            'févr.' => 'answer_date:['.$currentYear.'-02-01T00:00:00Z TO '.$currentYear.'-02-29T23:59:59Z]',
            'mars'  => 'answer_date:['.$currentYear.'-03-01T00:00:00Z TO '.$currentYear.'-03-31T23:59:59Z]',
            'avr.'  => 'answer_date:['.$currentYear.'-04-01T00:00:00Z TO '.$currentYear.'-04-30T23:59:59Z]',
            'mai'   => 'answer_date:['.$currentYear.'-05-01T00:00:00Z TO '.$currentYear.'-05-31T23:59:59Z]',
            'juin'  => 'answer_date:['.$currentYear.'-06-01T00:00:00Z TO '.$currentYear.'-06-30T23:59:59Z]',
            'juill.'=> 'answer_date:['.$currentYear.'-07-01T00:00:00Z TO '.$currentYear.'-07-31T23:59:59Z]',
            'août'  => 'answer_date:['.$currentYear.'-08-01T00:00:00Z TO '.$currentYear.'-08-31T23:59:59Z]',
            'sept.' => 'answer_date:['.$currentYear.'-09-01T00:00:00Z TO '.$currentYear.'-09-30T23:59:59Z]',
            'oct.'  => 'answer_date:['.$currentYear.'-10-01T00:00:00Z TO '.$currentYear.'-10-31T23:59:59Z]',
            'nov.'  => 'answer_date:['.$currentYear.'-11-01T00:00:00Z TO '.$currentYear.'-11-30T23:59:59Z]',
            'déc.'  => 'answer_date:['.$currentYear.'-12-01T00:00:00Z TO '.$currentYear.'-12-31T23:59:59Z]'
        );

        $filters = $this->convertFiltersForSolrEvol();

        $dataByMonthAverageData = array();

        foreach ($dataByMonthAverageQueries as $month => $queryParam) {
            $query = $this->_client->createQuery('select');
            $query->setHandler('query');
            $dataByMonthAverageStatsSet = $query->getStats();
            $dataByMonthAverageStatsSet->createField($question.'-Value');
            if($filters != '*:*')
                $queryParam = $filters . ' ' . $queryParam;
            $query->addParam('q', $queryParam.' AND '.$question.'-Value :[0 TO 10] ');
            try {
                // this executes the query and returns the result
                $resultset = $this->_client->select($query);
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }
            $stats = $resultset->getStats();
            $mean = $bornMax == 5 ? $stats->getResults()[$question.'-Value']->getMean() * 2 : $stats->getResults()[$question.'-Value']->getMean();
            $dataByMonthAverageData[$month] = (float) number_format($mean, 1);
        }

        return $dataByMonthAverageData;
    }

    /**
     * @return array
     */
    public function getSatisfactionGrading($code_question, $kpi)
    {
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q2Config = $limitsConfig->{'exp-global'};
        $question       = $code_question;
        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        $query->addParam('q', $this->_filters);
        // get the facetset component
        $facetSet = $query->getFacetSet();

        // create a facet query instance and set options
        $facetSet->createFacetMultiQuery('SatisfactionGrading')
            ->createQuery('respondent_count', $this->_filters)
            ->createQuery('respondent__very_good__count',   $question.':' . $q2Config->$question->values->very_good)
            ->createQuery('respondent__good__count',        $question.':' . $q2Config->$question->values->good)
            ->createQuery('respondent__poor__count',        $question.':' . $q2Config->$question->values->poor)
            ->createQuery('respondent__very_poor__count',   $question.':' . $q2Config->$question->values->very_poor);
        try {
            // this executes the query and returns the result
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
        $results = $resultset->getFacetSet()->getFacet('SatisfactionGrading')->getValues();

        if($results['respondent_count'] > 0) {
            $roundedVeryPoor = round($results['respondent__very_poor__count']);
            $roundedPoor = round($results['respondent__poor__count']);
            $roundedGood = round($results['respondent__good__count']);
            $roundedVeryGood = round($results['respondent__very_good__count']);
        }
        else {
            $roundedVeryPoor = 0;
            $roundedPoor = 0;
            $roundedGood = 0;
            $roundedVeryGood = 0;
        }

        $translator = Zend_Registry::get('Zend_Translate');
        return array(
            array($translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-poor@study_'.$this->_getStudyIdByFeedbackForm()), $roundedVeryPoor),
            array($translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__poor@study_'.$this->_getStudyIdByFeedbackForm()), $roundedPoor),
            array($translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__good@study_'.$this->_getStudyIdByFeedbackForm()), $roundedGood),
            array($translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-good@study_'.$this->_getStudyIdByFeedbackForm()), $roundedVeryGood)
        );
    }

    /**
     * @return array
     */
    public function getSatisfactionGradingByMonth($code_question, $kpi)
    {
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q2Config = $limitsConfig->{'exp-global'};
        $question       = $code_question;

        $currentYear = new DateTime();
        $currentYear = $currentYear->format('Y');


        $dataByMonthAverageQueries = array(
            'janv.' => 'answer_date:['.$currentYear.'-01-01T00:00:00Z TO '.$currentYear.'-01-31T23:59:59Z]',
            'févr.' => 'answer_date:['.$currentYear.'-02-01T00:00:00Z TO '.$currentYear.'-02-29T23:59:59Z]',
            'mars'  => 'answer_date:['.$currentYear.'-03-01T00:00:00Z TO '.$currentYear.'-03-31T23:59:59Z]',
            'avr.'  => 'answer_date:['.$currentYear.'-04-01T00:00:00Z TO '.$currentYear.'-04-30T23:59:59Z]',
            'mai'   => 'answer_date:['.$currentYear.'-05-01T00:00:00Z TO '.$currentYear.'-05-31T23:59:59Z]',
            'juin'  => 'answer_date:['.$currentYear.'-06-01T00:00:00Z TO '.$currentYear.'-06-30T23:59:59Z]',
            'juill.'=> 'answer_date:['.$currentYear.'-07-01T00:00:00Z TO '.$currentYear.'-07-31T23:59:59Z]',
            'août'  => 'answer_date:['.$currentYear.'-08-01T00:00:00Z TO '.$currentYear.'-08-31T23:59:59Z]',
            'sept.' => 'answer_date:['.$currentYear.'-09-01T00:00:00Z TO '.$currentYear.'-09-30T23:59:59Z]',
            'oct.'  => 'answer_date:['.$currentYear.'-10-01T00:00:00Z TO '.$currentYear.'-10-31T23:59:59Z]',
            'nov.'  => 'answer_date:['.$currentYear.'-11-01T00:00:00Z TO '.$currentYear.'-11-30T23:59:59Z]',
            'déc.'  => 'answer_date:['.$currentYear.'-12-01T00:00:00Z TO '.$currentYear.'-12-31T23:59:59Z]'
        );

        $filters = $this->convertFiltersForSolrEvol();

        $translator = Zend_Registry::get('Zend_Translate');

        $dataByMonthAverageData = array(
            $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-poor@study_'.$this->_getStudyIdByFeedbackForm()) => array(),
            $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__poor@study_'.$this->_getStudyIdByFeedbackForm()) => array(),
            $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__good@study_'.$this->_getStudyIdByFeedbackForm()) => array(),
            $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-good@study_'.$this->_getStudyIdByFeedbackForm()) => array()
        );

        foreach ($dataByMonthAverageQueries as $month => $queryParam) {
            $query = $this->_client->createQuery('select');
            $query->setHandler('query');
            if($filters != '*:*')
                $queryParam = $filters . ' ' . $queryParam;
            $query->addParam('q', $queryParam);
            $facetSet = $query->getFacetSet();
            // create a facet query instance and set options
            $facetSet->createFacetMultiQuery('SatisfactionGradingByMonth')
                ->createQuery('respondent_count', '*:*')
                ->createQuery('respondent__very_good__count',   $question.':' . $q2Config->$question->values->very_good)
                ->createQuery('respondent__good__count',        $question.':' . $q2Config->$question->values->good)
                ->createQuery('respondent__poor__count',        $question.':' . $q2Config->$question->values->poor)
                ->createQuery('respondent__very_poor__count',   $question.':' . $q2Config->$question->values->very_poor);
            try {
                // this executes the query and returns the result
                $resultset = $this->_client->select($query);
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }

            $results = $resultset->getFacetSet()->getFacet('SatisfactionGradingByMonth')->getValues();
            $dataByMonthAverageData[$translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-poor@study_'.$this->_getStudyIdByFeedbackForm())][$month] = ($results['respondent_count'] == 0) ? 0 : round((($results['respondent__very_poor__count'] / $results['respondent_count']) * 100));
            $dataByMonthAverageData[$translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__poor@study_'.$this->_getStudyIdByFeedbackForm())][$month] = ($results['respondent_count'] == 0) ? 0 : round((($results['respondent__poor__count'] / $results['respondent_count']) * 100));
            $dataByMonthAverageData[$translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__good@study_'.$this->_getStudyIdByFeedbackForm())][$month] = ($results['respondent_count'] == 0) ? 0 : round((($results['respondent__good__count'] / $results['respondent_count']) * 100));
            $dataByMonthAverageData[$translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-good@study_'.$this->_getStudyIdByFeedbackForm())][$month] = ($results['respondent_count'] == 0) ? 0 : round((($results['respondent__very_good__count'] / $results['respondent_count']) * 100));
        }

        return $dataByMonthAverageData;
    }

    /**
     * @return array
     */
    public function getSatisfactionDetailed($code_question, $kpi, $oneQuestion = null)
    {
        $limitsConfig   = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q56789Config = $limitsConfig->{'sat-detail'};
        $question     = $code_question;
        $ignore='';

        $bornMax = $q56789Config->$question->very_satisfied->to;

        if($limitsConfig->{'sat-detail.'.$question.'.ignore.value'})
            $ignore       = $q56789Config->$question->ignore->value;
        $veryUnsatisfied = join('-',    [$q56789Config->$question->very_unsatisfied->from,  $q56789Config->$question->very_unsatisfied->to]);
        $unsatisfied = join('-',        [$q56789Config->$question->unsatisfied->from,       $q56789Config->$question->unsatisfied->to]);
        $satisfied = join('-',          [$q56789Config->$question->satisfied->from,         $q56789Config->$question->satisfied->to]);
        $verySatisfied = join('-',      [$q56789Config->$question->very_satisfied->from,    $bornMax]);

        $averageSatisfactionDetailed = array();
        $total = $this->getResponseCount($this->_filters);
        $translator = Zend_Registry::get('Zend_Translate');

        $roundedSatisfactionDetailed = array(
            $veryUnsatisfied => array(
                'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__not-satisfied-at-all@study_'.$this->_getStudyIdByFeedbackForm()),
                'data' => array()
            ),
            $unsatisfied => array(
                'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__not-satisfied@study_'.$this->_getStudyIdByFeedbackForm()),
                'data' => array()
            ),
            $satisfied => array(
                'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__satisfied@study_'.$this->_getStudyIdByFeedbackForm()),
                'data' => array()
            ),
            $verySatisfied => array(
                'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__very-satisfied@study_'.$this->_getStudyIdByFeedbackForm()),
                'data' => array()
            ),
        );
        if ($oneQuestion && is_array($oneQuestion)) {
            $fields = $oneQuestion;
        }
        else {
            $fields = explode('_', $question);
        }

        foreach ($fields as $field) {
            $statField = $field . '-Value';
            $roundedSatisfactionDetailedQueries = array(
                $veryUnsatisfied => $field . '-Value:[' .   $q56789Config->$question->very_unsatisfied->from . ' TO '   . $q56789Config->$question->very_unsatisfied->to . ']',
                $unsatisfied => $field . '-Value:[' .       $q56789Config->$question->unsatisfied->from . ' TO '        . $q56789Config->$question->unsatisfied->to . ']',
                $satisfied => $field . '-Value:[' .         $q56789Config->$question->satisfied->from . ' TO '          . $q56789Config->$question->satisfied->to . ']',
                $verySatisfied => $field . '-Value:[' .     $q56789Config->$question->very_satisfied->from . ' TO '     . $q56789Config->$question->very_satisfied->to . ']'
            );

            foreach ($roundedSatisfactionDetailedQueries as $rate => $queryParam) {

                $query = $this->_client->createQuery('select');
                $query->setHandler('query');

                $roundedSatisfactionDetailedStatsSet = $query->getStats();
                $roundedSatisfactionDetailedStatsSet->createField($statField);
                if($ignore !=''){
                    preg_match('/(.+?)-[0-9]/', $field, $matches);
                    if(in_array($matches[1], explode('_', $ignore))) {
                        $queryParam .= ' AND -' . $field . ':' . $q56789Config->$question->{$matches[1]}->ignore;
                    }
                }
                /**
                 * Evolution GLAF #1786
                 */

                $queryParamFilter = $queryParam . $this->_getFilters();
                //Zend_Debug::dump($queryParamFilter);

                $query->addParam('q', $queryParamFilter);
                try {
                    // this executes the query and returns the result
                    $resultset = $this->_client->select($query);
                } catch (Exception $e) {
                    Zend_Debug::dump($e->getMessage());
                }
                $stats = $resultset->getStats();

                if($stats->getResults()[$statField]->getCount() > 0) {
                    $roundedSatisfactionDetailed[$rate]['data'][] = (float) str_replace(',', '', $stats->getResults()[$statField]->getCount());
                }
                else {
                    $roundedSatisfactionDetailed[$rate]['data'][] = 0;
                }

            }
            $query = $this->_client->createQuery('select');
            $query->setHandler('query');
            $filter = $statField . ':[0 TO 10]';
            if($ignore !=''){
                preg_match('/(.+?)-[0-9]/', $field, $matches);
                if(in_array($matches[1], explode('_', $ignore))) {
                    $filter .= ' AND -' . $field . ':' . $q56789Config->$question->{$matches[1]}->ignore;

                }
            }
            /**
             * Evolution GLAF #1786
             */
            $queryParamFilter = $filter . ' ' . $this->_filters;
            $query->addParam('q',  $queryParamFilter);
            $roundedSatisfactionDetailedStatsSet = $query->getStats();
            $roundedSatisfactionDetailedStatsSet->createField($statField);
            try {
                // this executes the query and returns the result
                $resultset = $this->_client->select($query);
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }
            $stats = $resultset->getStats();
            $mean = $bornMax == 5 ? $stats->getResults()[$statField]->getMean() * 2 : $stats->getResults()[$statField]->getMean();

            if($total > 0)
                $averageSatisfactionDetailed[$translator->translate('form__'.$this->_feedback_form_id.'__question_title-' . $field, 'fr')] = (float)round($mean, 3);
            else
                $averageSatisfactionDetailed[$translator->translate('form__'.$this->_feedback_form_id.'__question_title-' . $field, 'fr')] = (float)number_format(0, 3);
        }

        return array(
            'graphData' => $roundedSatisfactionDetailed,
            'averageData' => $averageSatisfactionDetailed
        );
    }

    /**
     * @var string $code_question
     * @return array
     */
    public function getSatisfactionDetailedByMonth($codeQuestions)
    {

        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q56789Config = $limitsConfig->{'sat-detail'};
        $total = $this->getResponseCount($this->_filters);
        $satisfactionDetailedByMonthAverage = array();
        $bornMax = $q56789Config->$codeQuestions->very_satisfied->to;

        foreach (explode('_', $codeQuestions) as $name) {
            $satisfactionDetailedByMonthAverage[] = array('name' => $name, 'data' => array());
        }

        $currentYear = new DateTime();
        $currentYear = $currentYear->format('Y');

        $dataByMonthAverageQueries = array(
            'janv.' => 'answer_date:['.$currentYear.'-01-01T00:00:00Z TO '.$currentYear.'-01-31T23:59:59Z]',
            'févr.' => 'answer_date:['.$currentYear.'-02-01T00:00:00Z TO '.$currentYear.'-02-29T23:59:59Z]',
            'mars'  => 'answer_date:['.$currentYear.'-03-01T00:00:00Z TO '.$currentYear.'-03-31T23:59:59Z]',
            'avr.'  => 'answer_date:['.$currentYear.'-04-01T00:00:00Z TO '.$currentYear.'-04-30T23:59:59Z]',
            'mai'   => 'answer_date:['.$currentYear.'-05-01T00:00:00Z TO '.$currentYear.'-05-31T23:59:59Z]',
            'juin'  => 'answer_date:['.$currentYear.'-06-01T00:00:00Z TO '.$currentYear.'-06-30T23:59:59Z]',
            'juill.'=> 'answer_date:['.$currentYear.'-07-01T00:00:00Z TO '.$currentYear.'-07-31T23:59:59Z]',
            'août'  => 'answer_date:['.$currentYear.'-08-01T00:00:00Z TO '.$currentYear.'-08-31T23:59:59Z]',
            'sept.' => 'answer_date:['.$currentYear.'-09-01T00:00:00Z TO '.$currentYear.'-09-30T23:59:59Z]',
            'oct.'  => 'answer_date:['.$currentYear.'-10-01T00:00:00Z TO '.$currentYear.'-10-31T23:59:59Z]',
            'nov.'  => 'answer_date:['.$currentYear.'-11-01T00:00:00Z TO '.$currentYear.'-11-30T23:59:59Z]',
            'déc.'  => 'answer_date:['.$currentYear.'-12-01T00:00:00Z TO '.$currentYear.'-12-31T23:59:59Z]'
        );

        $filters = $this->convertFiltersForSolrEvol();

        foreach ($satisfactionDetailedByMonthAverage as &$question) {

            foreach ($dataByMonthAverageQueries as $month => $queryParam) {
                $query = $this->_client->createQuery('select');
                $query->setHandler('query');
                $filter = $question['name'] . '-Value:[0 TO 10]';
                if($filters!= '*:*')
                    $filter = $filters . ' AND ' . $filter;
                /**
                 * Evolution GLAF #1786
                 */
                $queryParamFilter =  $filter . ' AND ' . $queryParam;
                //Zend_Debug::dump($queryParamFilter);


                $query->addParam('q', $queryParamFilter);

                $roundedSatisfactionDetailedStatsSet = $query->getStats();
                $statField = $question['name'] . '-Value';
                $totalMonth = $this->getResponseCount($queryParamFilter);

                $roundedSatisfactionDetailedStatsSet->createField($statField);
                try {
                    // this executes the query and returns the result
                    $resultset = $this->_client->select($query);
                } catch (Exception $e) {
                    Zend_Debug::dump($e->getMessage());
                }
                $stats = $resultset->getStats();
                $mean = $bornMax == 5 ? ($stats->getResults()[$statField]->getSum() / $totalMonth) * 2 : $stats->getResults()[$statField]->getSum() / $totalMonth;

                if ($totalMonth > 0) {
                    $question['data'][] = (float)number_format($mean, 1);
                } else {
                    $question['data'][] = (float)number_format(0, 1);
                }
            }
        }
        $translator = Zend_Registry::get('Zend_Translate');
        foreach($satisfactionDetailedByMonthAverage as &$question) {
            $question['name'] = $translator->translate('form__'.$this->_feedback_form_id.'__question_title-' . $question['name'], 'fr');
        }

        return $satisfactionDetailedByMonthAverage;
    }

    /**
     * @return array
     */
    public function getNPS($code_question, $kpi)
    {
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q3Config = $limitsConfig->{'nps-global'};
        $question = $code_question;

        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        $filters = '';
        if($this->_filters == '*:*') {
            $filters = $question.'-Value:[0 TO 10]';
        }
        else {
            $filters .= $this->_filters .' '. $question.'-Value:[0 TO 10]';
        }
        $query->addParam('q', $filters);

        // get the facetset component
        $facetSet = $query->getFacetSet();
        // create a facet query instance and set options
        $facetSet->createFacetMultiQuery('NPS')
            ->createQuery('respondent_count', $filters)
            ->createQuery('respondent__promoters__count', $question.'-Value:[' .    $q3Config->$question->promoters->from . ' TO ' .    $q3Config->$question->promoters->to . ']')
            ->createQuery('respondent__passive__count', $question.'-Value:[' .      $q3Config->$question->passive->from . ' TO ' .      $q3Config->$question->passive->to . ']')
            ->createQuery('respondent__detractors__count', $question.'-Value:[' .   $q3Config->$question->detractors->from . ' TO ' .   $q3Config->$question->detractors->to . ']');
        $statsSet = $query->getStats();

        $statsSet->createField($question.'-Value');
        try {
            // this executes the query and returns the result
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }

        $results = $resultset->getFacetSet()->getFacet('NPS')->getValues();
        $stats = $resultset->getStats();

        $respondentCount = $results['respondent_count'];

        if($respondentCount > 0)
            $npsValue = round((($results['respondent__promoters__count'] / $respondentCount) * 100)  - (($results['respondent__detractors__count'] / $respondentCount) * 100));
        else
            $npsValue = 0;

        $translator = Zend_Registry::get('Zend_Translate');

        return array(
            'graphData' => array(
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__detractor@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__detractors__count'])
                ),
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__passive@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__passive__count'])
                ),
                array(
                    'name' => $translator->translate('category__study_'.$this->_getStudyIdByFeedbackForm().'__kpi_'.$kpi.'__graph__promoter@study_'.$this->_getStudyIdByFeedbackForm()),
                    'data' => array($results['respondent__promoters__count'])
                )
            ),

            'npsValue' => $npsValue,
            'averageData' => round($stats->getResults()[$question.'-Value']->getMean(), 1)
        );
    }

    /**
     * @return array
     */
    public function getNPSEvolution($code_question)
    {
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q3Config = $limitsConfig->{'nps-global'};
        $question = $code_question;

        $currentYear = new DateTime();
        $currentYear = $currentYear->format('Y');

        $dataByMonthAverageQueries = array(
            'janv.' => 'answer_date:['.$currentYear.'-01-01T00:00:00Z TO '.$currentYear.'-01-31T23:59:59Z]',
            'févr.' => 'answer_date:['.$currentYear.'-02-01T00:00:00Z TO '.$currentYear.'-02-29T23:59:59Z]',
            'mars'  => 'answer_date:['.$currentYear.'-03-01T00:00:00Z TO '.$currentYear.'-03-31T23:59:59Z]',
            'avr.'  => 'answer_date:['.$currentYear.'-04-01T00:00:00Z TO '.$currentYear.'-04-30T23:59:59Z]',
            'mai'   => 'answer_date:['.$currentYear.'-05-01T00:00:00Z TO '.$currentYear.'-05-31T23:59:59Z]',
            'juin'  => 'answer_date:['.$currentYear.'-06-01T00:00:00Z TO '.$currentYear.'-06-30T23:59:59Z]',
            'juill.'=> 'answer_date:['.$currentYear.'-07-01T00:00:00Z TO '.$currentYear.'-07-31T23:59:59Z]',
            'août'  => 'answer_date:['.$currentYear.'-08-01T00:00:00Z TO '.$currentYear.'-08-31T23:59:59Z]',
            'sept.' => 'answer_date:['.$currentYear.'-09-01T00:00:00Z TO '.$currentYear.'-09-30T23:59:59Z]',
            'oct.'  => 'answer_date:['.$currentYear.'-10-01T00:00:00Z TO '.$currentYear.'-10-31T23:59:59Z]',
            'nov.'  => 'answer_date:['.$currentYear.'-11-01T00:00:00Z TO '.$currentYear.'-11-30T23:59:59Z]',
            'déc.'  => 'answer_date:['.$currentYear.'-12-01T00:00:00Z TO '.$currentYear.'-12-31T23:59:59Z]'
        );

        $filters = $this->convertFiltersForSolrEvol();

        foreach ($dataByMonthAverageQueries as $month => $queryParam) {
            $query = $this->_client->createQuery('select');
            $query->setHandler('query');
            if($filters != '*:*')
                $queryParam = $filters . ' ' . $queryParam;
            $query->addParam('q', $queryParam);
            $facetSet = $query->getFacetSet();
            // create a facet query instance and set options
            $facetSet->createFacetMultiQuery('NPSEvolution')
                ->createQuery('respondent_count', $queryParam)
                ->createQuery('respondent__promoters__count', $question.'-Value:[' . $q3Config->$question->promoters->from . ' TO ' . $q3Config->$question->promoters->to . ']')
                ->createQuery('respondent__detractors__count', $question.'-Value:[' . $q3Config->$question->detractors->from . ' TO ' . $q3Config->$question->detractors->to . ']');
            try {
                // this executes the query and returns the result
                $resultset = $this->_client->select($query);
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }

            $results = $resultset->getFacetSet()->getFacet('NPSEvolution')->getValues();
            $respondentCount = $results['respondent_count'];
            if($respondentCount > 0)
                $npsEvolutionRounded[$month] = round((($results['respondent__promoters__count'] / $respondentCount) * 100)  - (($results['respondent__detractors__count'] / $respondentCount) * 100));
            else
                $npsEvolutionRounded[$month] = 0;
        }

        return $npsEvolutionRounded;
    }

    /**
     * @return array
     */
    public function getNPSDistribution($code_question)
    {
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q3Config = $limitsConfig->{'nps-global'};
        $question = $code_question;

        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        $filters = '';
        if($this->_filters == '*:*') {
            $filters = $question.'-Value:[0 TO 10]';
        }
        else {
            $filters .= $this->_filters .' '.$question.'-Value:[0 TO 10]';
        }
        $query->addParam('q', $filters);
        // get the facetset component
        $facetSet = $query->getFacetSet();
        // create a facet query instance and set options
        $facetSet->createFacetMultiQuery('OverAllSatisfaction')
            ->createQuery('respondent_count', $this->_filters)
            ->createQuery('respondent__0__count', $question.'-Value:0')
            ->createQuery('respondent__1__count', $question.'-Value:1')
            ->createQuery('respondent__2__count', $question.'-Value:2')
            ->createQuery('respondent__3__count', $question.'-Value:3')
            ->createQuery('respondent__4__count', $question.'-Value:4')
            ->createQuery('respondent__5__count', $question.'-Value:5')
            ->createQuery('respondent__6__count', $question.'-Value:6')
            ->createQuery('respondent__7__count', $question.'-Value:7')
            ->createQuery('respondent__8__count', $question.'-Value:8')
            ->createQuery('respondent__9__count', $question.'-Value:9')
            ->createQuery('respondent__10__count',$question.'-Value:10');
        try {
            // this executes the query and returns the result
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
        $results = $resultset->getFacetSet()->getFacet('OverAllSatisfaction')->getValues();
        if($results['respondent_count'] > 0) {
            $scores = array(
                '10'=> ($results['respondent__10__count'] / $results['respondent_count']) * 100,
                '9' => ($results['respondent__9__count'] / $results['respondent_count']) * 100,
                '8' => ($results['respondent__8__count'] / $results['respondent_count']) * 100,
                '7' => ($results['respondent__7__count'] / $results['respondent_count']) * 100,
                '6' => ($results['respondent__6__count'] / $results['respondent_count']) * 100,
                '5' => ($results['respondent__5__count'] / $results['respondent_count']) * 100,
                '4' => ($results['respondent__4__count'] / $results['respondent_count']) * 100,
                '3' => ($results['respondent__3__count'] / $results['respondent_count']) * 100,
                '2' => ($results['respondent__2__count'] / $results['respondent_count']) * 100,
                '1' => ($results['respondent__1__count'] / $results['respondent_count']) * 100,
                '0' => ($results['respondent__0__count'] / $results['respondent_count']) * 100,
            );

            $scoresDecimal = array();
            foreach($scores as $key => $score) {
                if($score > 0) {
                    $explode = explode('.', $score);
                    if (count($explode) == 1) {
                        $explode[1] = 0;
                    }
                    list($whole, $decimal) = $explode;
                    $scoresDecimal[$key] = (int)$decimal;
                }
            }
            $maxKey = array_search(max($scoresDecimal), $scoresDecimal);
            $finalScores = array();
            foreach($scores as $key => $value) {
                if($key == $maxKey)
                    $finalScores[$key] = round($value, 0);
                else
                    $finalScores[$key] = round($value, 0);
            }
            $finalScores['countRespondant'] = $this->getResponseCount($filters);
        }
        else {
            $finalScores = array(
                '10'=> 0,
                '9' => 0,
                '8' => 0,
                '7' => 0,
                '6' => 0,
                '5' => 0,
                '4' => 0,
                '3' => 0,
                '2' => 0,
                '1' => 0,
                '0' => 0,
                'countRespondant' => 0,
            );
        }
        return $finalScores;
    }

    /**
     * @return array
     */
    public function getNPSDistributionEvolutionByMonth($code_question)
    {
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$this->_feedback_form_id.'.ini', APPLICATION_ENV);
        $q3Config = $limitsConfig->{'nps-global'};
        $question = $code_question;

        $currentYear = new DateTime();
        $currentYear = $currentYear->format('Y');

        $dataByMonthAverageQueries = array(
            'janv.' => 'answer_date:['.$currentYear.'-01-01T00:00:00Z TO '.$currentYear.'-01-31T23:59:59Z]',
            'févr.' => 'answer_date:['.$currentYear.'-02-01T00:00:00Z TO '.$currentYear.'-02-29T23:59:59Z]',
            'mars'  => 'answer_date:['.$currentYear.'-03-01T00:00:00Z TO '.$currentYear.'-03-31T23:59:59Z]',
            'avr.'  => 'answer_date:['.$currentYear.'-04-01T00:00:00Z TO '.$currentYear.'-04-30T23:59:59Z]',
            'mai'   => 'answer_date:['.$currentYear.'-05-01T00:00:00Z TO '.$currentYear.'-05-31T23:59:59Z]',
            'juin'  => 'answer_date:['.$currentYear.'-06-01T00:00:00Z TO '.$currentYear.'-06-30T23:59:59Z]',
            'juill.'=> 'answer_date:['.$currentYear.'-07-01T00:00:00Z TO '.$currentYear.'-07-31T23:59:59Z]',
            'août'  => 'answer_date:['.$currentYear.'-08-01T00:00:00Z TO '.$currentYear.'-08-31T23:59:59Z]',
            'sept.' => 'answer_date:['.$currentYear.'-09-01T00:00:00Z TO '.$currentYear.'-09-30T23:59:59Z]',
            'oct.'  => 'answer_date:['.$currentYear.'-10-01T00:00:00Z TO '.$currentYear.'-10-31T23:59:59Z]',
            'nov.'  => 'answer_date:['.$currentYear.'-11-01T00:00:00Z TO '.$currentYear.'-11-30T23:59:59Z]',
            'déc.'  => 'answer_date:['.$currentYear.'-12-01T00:00:00Z TO '.$currentYear.'-12-31T23:59:59Z]'
        );

        $filters = $this->convertFiltersForSolrEvol();

        $npsEvolutionRounded = array(
            'npsPromoters' => array(),
            'npsPassive' => array(),
            'npsDetractors' => array()
        );

        foreach ($dataByMonthAverageQueries as $month => $queryParam) {
            $query = $this->_client->createQuery('select');
            $query->setHandler('query');
            if($filters != '*:*')
                $queryParam = $filters . ' ' . $queryParam;
            $query->addParam('q', $queryParam);
            $facetSet = $query->getFacetSet();
            // create a facet query instance and set options
            $facetSet->createFacetMultiQuery('NPSDistributionEvolutionByMonth')
                ->createQuery('respondent_count', $queryParam)
                ->createQuery('respondent__promoters__count', $question.'-Value:[' . $q3Config->$question->promoters->from . ' TO ' . $q3Config->$question->promoters->to . ']')
                ->createQuery('respondent__passive__count', $question.'-Value:[' . $q3Config->$question->passive->from . ' TO ' . $q3Config->$question->passive->to . ']')
                ->createQuery('respondent__detractors__count', $question.'-Value:[' . $q3Config->$question->detractors->from . ' TO ' . $q3Config->$question->detractors->to . ']');

            try {
                // this executes the query and returns the result
                $resultset = $this->_client->select($query);
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }

            $results = $resultset->getFacetSet()->getFacet('NPSDistributionEvolutionByMonth')->getValues();
            $npsEvolutionRounded['npsPromoters'][$month] = ($results['respondent_count'] == 0) ? 0 : round(($results['respondent__promoters__count'] / $results['respondent_count']) * 100);
            $npsEvolutionRounded['npsPassive'][$month] = ($results['respondent_count'] == 0) ? 0 : round(($results['respondent__passive__count'] / $results['respondent_count']) * 100);
            $npsEvolutionRounded['npsDetractors'][$month] = ($results['respondent_count'] == 0) ? 0 : round(($results['respondent__detractors__count'] / $results['respondent_count']) * 100);
        }

        return $npsEvolutionRounded;
    }

    /**
     * @return \Solarium\QueryType\Select\Result\Result
     */
    public function getData($sort = false, $start = '0', $row = null, $id =null) {
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        if (!$row) {
            $row = $adapter->fetchOne('SELECT COUNT(*) FROM statflow_respondent');
        }
        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');

        if ($id) {
            $query->addParam('q', " respondent_id : $id");
        }
        else {
            $query->addParam('q', $this->_filters);
        }
        $query->addParam('start', $start );
        $query->addParam('rows', $row );
        if($sort !== false) {
            $query->addParam('sort', $sort);
        }
        try {
            // this executes the query and returns the result
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
        return $resultset;
    }

    /**
     * @return \Solarium\QueryType\Select\Result\Result
     */
    public function getDataAlert($sort = false) {
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $row = $adapter->fetchOne('SELECT COUNT(*) FROM statflow_respondent');
        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        $query->addParam('q', $this->_filters);
        $query->addParam('fq', " alert : *");
        $query->addParam('start', 0 );
        $query->addParam('rows', $row );
        if($sort !== false) {
            $query->addParam('sort', $sort);
        }
        try {
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }

        return $resultset;
    }

    /**
     * @return \Solarium\QueryType\Select\Result\Result
     */
    public function getDataTreated($sort = false) {
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $row = $adapter->fetchOne('SELECT COUNT(*) FROM statflow_respondent');
        // get a select query instance
        $query = $this->_client->createQuery('select');
        $query->setHandler('query');
        $query->addParam('q', $this->_filters);
        $query->addParam('fq', " action : 2");
        $query->addParam('start', 0 );
        $query->addParam('rows', $row );
        if($sort !== false) {
            $query->addParam('sort', $sort);
        }
        try {
            $resultset = $this->_client->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
        return $resultset;
    }

    private function _getStudyIdByFeedbackForm(){
        $feedback_form_id = $this->getFeedbackFormId();
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $select  = $adapter->select()
            ->from(array('statflow_feedback_form'),array('study_id'))
            ->where('id = '.$feedback_form_id);
        return $adapter->fetchOne($select);
    }
}
