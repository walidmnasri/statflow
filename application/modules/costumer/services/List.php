<?php
require_once __DIR__ . '/../../statflow/services/Structure.php';
require_once APPLICATION_PATH . '/../library/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Statflow\Service\Structure;
/**
 * Class Costumer_Service_list
 */
class Costumer_Service_List
{
    private $models = array();
    
    private $translateArray ;
    
    /**
     * @var Structure
     */
    private $structureService;
    
    /**
     *
     * @return \Statflow\Service\Structure
     */
    
    public function getTranslateFile(){
        if (!$this->translateArray) {
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
           $this->translateArray =  include  APPLICATION_PATH . '/../data/locales/' .$lang. '/' .$lang. '.php';
        }
        return $this->translateArray;
    }
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
    
        return $this->structureService;
    }
    
    /**
     * __conctruct()
     */
    public function __construct()
    {
        $this->models['structure'] = \Centurion_Db::getSingleton('statflow/structure');
        $this->models['study'] = \Centurion_Db::getSingleton('statflow/study');
        $this->models['respondent'] = \Centurion_Db::getSingleton('statflow/respondent');
        $this->models['answer'] = \Centurion_Db::getSingleton('statflow/answer');
        $this->models['question'] = \Centurion_Db::getSingleton('statflow/question');
        $this->models['feedbackForm'] = \Centurion_Db::getSingleton('statflow/feedbackForm');
        $this->models['feedbackFormPage'] = \Centurion_Db::getSingleton('statflow/feedbackFormPage');
    }
    
    public function exportListExcel($study, $feedback_form_id, $headers, $headers_ignore, $respondants, $studyName,  $nominative_check, $nominative_ignore)
    {            
        ini_set('memory_limit', '2048M');
        $data = $this->getDataRespondantExcel($respondants, $headers, $headers_ignore, $study, $feedback_form_id, $nominative_check,$nominative_ignore)['data'];
        $objDateTime = new DateTime('NOW');
        $fileName = $studyName.'-'.$objDateTime->format("Y-m-d-H-i-s").'.csv';
        $filePath = APPLICATION_PATH . '/../public/ftp/costumer/export/' . $fileName;
        Sayyes_Tools_Filesystem::createFile($filePath, '');
        exec('chmod 777 ' . $filePath);
        
        $style = (new StyleBuilder())
            ->setFontBold()
            ->build();
        
        $header = [];
        $headersTraductible = array ( 'id',
                                      'respondent_id',
                                      'status',
                                      'action',
                                      'token',
                                      'completion_time',
                                      'language',
                                      'firstname',
                                      'lastname',
                                      'email',
                                      'alert',
                                      'answered_at',
                                      'answer_date',
                                      'completion_duration',
                                      'structure_belonging',
                                      'structure_name',
                                      'country_code',
                                      'terminal',
                                      'imported_at' );
        
        foreach (array_keys($data[0]) as $head) {
            if (isset(explode('@', $head)[1]) &&  explode('@', $head)[1]=== 'question') {
               $header[] = stripslashes(htmlspecialchars(strip_tags($this->translate('form__'.$feedback_form_id.'__question_title-'.explode('@', $head)[0], $feedback_form_id)),ENT_NOQUOTES , 'UTF-8'));
            } elseif (in_array($head, $headersTraductible)) {
                $header[] = htmlspecialchars($this->translate('label__study_'.$study.'__list__index__header_'.$head, null, $study),ENT_NOQUOTES , 'UTF-8');
            } else {
                $header[] = $head;
            }
        };

        $writer = WriterFactory::create(Type::CSV); // for XLSX files
        $writer->setFieldDelimiter(";");
        $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->addRowWithStyle($header, $style);
        $writer->addRows($data); // add multiple rows at a time
        $writer->close();
        
        print_r(json_encode(['url' => '/ftp/costumer/export/'. $fileName]));
        exit;        
    }
    
    function getColumnNominative($respondants, $nominative_check, $nominative_ignore)
    {
        $data = array();
        foreach ($respondants as $key=>$value){
            //echo "Key: ".$key." Data: ".$value."<br />";
            $data[] = $key.$value;
        }
    
        if(in_array($nominative_check, $data) && Null != $nominative_ignore)
            return true;
        else
            return false;
    }
 
    function getDataRespondant($respondants, $headers, $header_ignore, $study, $form, $delai_traitement, $nominative_check = null, $nominative_ignore = null)
    {   
        $translate = Zend_Registry::get('Zend_Translate');
        $delai_traitement = !empty($delai_traitement) ? intval($delai_traitement) : 0;
        $today_date = new DateTime(date("Y-m-d"));

        $result = array();
        for($i=0;$i<count($respondants);$i++) {

            $answer_date = new DateTime($respondants[$i]->answer_date);
            $no_days = intval($today_date->diff($answer_date)->days);
           
            // le statut du suivi d'action est "Pas d'action" et "En cours" et la date de réponse et supérieure au délai de traitement 
            if(($respondants[$i]->action == 0 || $respondants[$i]->action == 1) && $no_days > $delai_traitement) {
                $class = "red-sunglo";
            } else {
                $class = "green-haze";
            }
            $result[$i][] = '<a target="_blank" href="/costumer/list/display/formId/'.$form.'/respondent_id/'.$respondants[$i]->respondent_id.'
                "class="btn btn-outline '.$class.'" type="button">'.$translate->translate('Detail@frontend').'</a>';

            $resultCheck = $this->getColumnNominative($respondants[$i], $nominative_check, $nominative_ignore);
            foreach ($headers as $header){
                $data_header = explode('@', $header);
                if(!in_array(str_replace(' ', '_', $data_header[2]), $header_ignore) && !in_array('Value', explode('-', explode('@', $header)[2]))){
                    if($data_header[2]==="Date d'achat") {
                        $data_header[2]="Date_achat";
                    }
                    $item_value = str_replace(' ', '_', $data_header[2]);
                    
                    if ($resultCheck && in_array($item_value, $nominative_ignore)) {
                        $result[$i][] = $translate->translate('label__'.$study.'__nominative_substitute_text__'.$form.'@'.$study);
                    }
                    else {
                        if ($item_value === 'action') {
                            $action = $respondants[$i]->action;
                            switch ($action) {
                                case 1:
                                    $result[$i][] = '<span class="label label-sm label-warning">' . $translate->translate('label_' . $study . '_action-progress@' . $study) . '</span>';
                                    break;
                                case 2:
                                    $result[$i][] = '<span class="label label-sm bg-green-haze">' . $translate->translate('label_' . $study . '_action-close@' . $study) . '</span>';
                                    break;
                                default:
                                    $result[$i][] = '<span class="label label-sm bg-red-sunglo">' . $translate->translate('label_' . $study . '_action-none@' . $study) . '</span>';
                            }
                        } elseif ($item_value === 'completion_time') {
                            $result[$i][] = Sayyes_Tools_Time::secs_to_h($respondants[$i]->{$item_value});
                        } elseif ($item_value === 'structure_name') {
                            $result[$i][] = substr($this->getStructureName($respondants[$i]->structure_belonging, explode('_', $study)[1]),0 ,30) ;
                        } elseif ($item_value === 'alert') {
                            $result[$i][] = $respondants[$i]->{$item_value} ? '<span class="label label-sm bg-yellow-gold"><i class="icon-bell"></i>' . substr($respondants[$i]->{$item_value},0 ,30) . '</span>' : " - ";
                        }
                        elseif(count($data_header)== 4 && ($data_header[3]==='tab' || $data_header[3]==='order')){
                            $sous_question = explode('-', $data_header[2]);
                            array_pop($sous_question);
                            $parentquestion = implode('-', $sous_question);
                            $result[$i][] = 	($respondants[$i]->{$item_value}) ? $translate->translate('form__question_'.$parentquestion.'_modality-'.$respondants[$i]->{$item_value}.'@Form_'.$form ) : " - " ;
                        }
                        elseif(count($data_header)== 4 && ($data_header[3]==='open')){
                            $result[$i][] = ($respondants[$i]->{$item_value}) ? htmlentities(mb_substr($respondants[$i]->{$item_value}, 0, 30, 'utf-8')) : " - ";                        
                        }
                        
                        elseif($data_header[0] === 'question' && $data_header[1]==='int' &&  count($data_header)<4){
                            $result[$i][] =	 ($respondants[$i]->{$item_value}) ? substr($translate->translate('form__question_'.$data_header[2].'_modality-'.$respondants[$i]->{$item_value}.'@Form_'.$form), 0, 30) : " - " ;
                        }
                        elseif ($data_header[1]==='date') {
                            $result[$i][] = ($respondants[$i]->{$item_value}) ? str_replace(['T', 'Z'], ' ',$respondants[$i]->{$item_value}) : " - ";
                        }
                        elseif(count($data_header)== 4 && $data_header[3]==='multiple'){
                            if(count(explode('|', $respondants[$i]->{$item_value}))>1){
                                $multiple = array();
                                foreach (explode('|', $respondants[$i]->{$item_value}) as $modal){
                                    $multiple[] = "<span style=\"color:".$this->getColorByModality($modal, $respondants[$i]->alert)."\">".substr($translate->translate('form__question_'.$data_header[2].'_modality-'.$modal.'@Form_'.$form), 0, 30)."</span>";
                                }
                                $result[$i][] = implode('<BR>', $multiple);
                            }
                            else
                                $result[$i][] = ($respondants[$i]->{$item_value}) ? substr($translate->translate('form__question_'.$data_header[2].'_modality-'.$respondants[$i]->{$item_value}.'@Form_'.$form), 0, 30) : " - " ;
                        }
                        else {
                            $result[$i][] = ($respondants[$i]->{$item_value}) ? substr($respondants[$i]->{$item_value},0 ,30) : " - ";
                        }
                    }
                }
            }
        }
        return $result;
    }
    
    //retourne les questions et reponses pour l'affichage dans la fiche client
    function getDataRespondantFicheClient($respondant, $headers, $header_ignore,  $form, $study, $nominative_check = null, $nominative_ignore = null)
    {
        $translate = Zend_Registry::get('Zend_Translate');
    
        $result = array();
        $resultCheck = $this->getColumnNominative($respondant, $nominative_check, $nominative_ignore);
        
        foreach ($headers as $header){
            $data_header = explode('@', $header);
            if(!in_array(str_replace(' ', '_', $data_header[2]), $header_ignore) && !in_array('Value', explode('-', explode('@', $header)[2]))){
                if (explode('@', $header)[0] == 'question') {
                    $item_value = str_replace(' ', '_', $data_header[2]);
                    
                    if($resultCheck && in_array($item_value, $nominative_ignore)){
                        $result[$item_value] = "<td>".$translate->translate('label__'.$study.'__nominative_substitute_text__'.$form.'@'.$study)."</td>";
                    
                    }else{
                        if(count($data_header)== 4 && ($data_header[3]==='tab' || $data_header[3]==='order')){
                            $sous_question = explode('-', $data_header[2]);
                            array_pop($sous_question);
                            $parentquestion = implode('-', $sous_question);
                            $result[$item_value] = ($respondant->{$item_value}) ? "<td style='color:".$this->getColorByModality($respondant->$item_value, $respondant->alert)."'>".$translate->translate('form__question_'.$parentquestion.'_modality-'.$respondant->{$item_value}.'@Form_'.$form )."</td>" : "<td> - </td>" ;
                        }
                        elseif(count($data_header)== 4 && ($data_header[3]==='open')){
                            $result[$item_value] = ($respondant->{$item_value}) ? "<td class='tooltip' data-content=\"".htmlentities(trim($respondant->{$item_value}))."\" style='color:".$this->getColorOpenQuestion($form, $data_header[2], $respondant->alert)."'>" .htmlentities($respondant->{$item_value})."</td>" : "<td> - </td>";
                        }
                        elseif($data_header[0] === 'question' && $data_header[1]==='int' &&  count($data_header)<4){
                            $result[$item_value] =	 ($respondant->{$item_value}) ? "<td class='tooltip' data-content=\"".htmlentities(trim($translate->translate('form__question_'.$data_header[2].'_modality-'.$respondant->{$item_value}.'@Form_'.$form)))."\" style='color:".$this->getColorByModality($respondant->$item_value, $respondant->alert)."'>".$translate->translate('form__question_'.$data_header[2].'_modality-'.$respondant->{$item_value}.'@Form_'.$form)."</td>" : "<td> - </td>" ;
                        }
                        elseif(count($data_header)== 4 && $data_header[3]==='multiple'){
                            if(count(explode('|', $respondant->{$item_value})) >1 ){
                                $multiple = array();
                                foreach (explode('|', $respondant->{$item_value}) as $modal){
                                    $multiple[] = "<span title=\"".htmlentities(trim($translate->translate('form__question_'.$data_header[2].'_modality-'.$modal.'@Form_'.$form)))."\" style='color:".$this->getColorByModality($modal, $respondant->alert)."'>".substr($translate->translate('form__question_'.$data_header[2].'_modality-'.$modal.'@Form_'.$form), 0, 30)."</span>";
                                }
                                $result[$item_value] = "<td>" .implode('<BR>', $multiple) . "</td>";
                            }
                            else {
                                $result[$item_value] = ($respondant->{$item_value}) ? "<td class='tooltip' data-content=\"".htmlentities(trim($translate->translate('form__question_'.$data_header[2].'_modality-'.$respondant->{$item_value}.'@Form_'.$form)))."\" style='color:".$this->getColorByModality($respondant->$item_value, $respondant->alert)."'>" . $translate->translate('form__question_'.$data_header[2].'_modality-'.$respondant->{$item_value}.'@Form_'.$form)."</td>" : "<td> - </td>" ;
                            }
                        }
                        else{
                            $result[$item_value] =	 ($respondant->{$item_value}) ? "<td class='tooltip' data-content=\"".htmlentities(trim($respondant->{$item_value}))."\" style='color:".$this->getColorByModality($respondant->$item_value, $respondant->alert)."'>".$respondant->{$item_value}."</td>" : "<td> - </td>" ;
                        }
                    }   
                }
            }
        }
        return $result;
    }
    
    //retourne le detail du repondent pour l'affichage dans la fiche client
    function getRespondantDetailFicheClient($respondant, $headers, $header_ignore, $form, $study, $nominative_check = null, $nominative_ignore = null)
    {
        $translate = Zend_Registry::get('Zend_Translate');
    
        $result = array();
        $resultCheck = $this->getColumnNominative($respondant, $nominative_check, $nominative_ignore);
        foreach ($headers as $header){
            $data_header = explode('@', $header);
              
            if (!in_array(str_replace(' ', '_', $data_header[2]), $header_ignore)) {
                if (explode('@', $header)[0] == 'respondent') {
                    $item_value = $data_header[2];
                    $item_check = str_replace(' ', '_', $data_header[2]); // code used before with $item_value
            
                    if ($resultCheck && in_array($item_check, $nominative_ignore)) {
                        //if($resultCheck && in_array($item_value, $nominative_ignore)){  // code used before
                        $result[$item_value] = $translate->translate('label__'.$study.'__nominative_substitute_text__'.$form.'@'.$study);
                    } else {
                        if ($item_check === 'structure_name') {
                            //if ($item_value === 'structure_name'){ // Code used before
                            $result[$item_value] = $this->getStructureName($respondant->structure_belonging, explode('_', $study)[1]);
                        } elseif ($item_value === 'completion_time') {
                            $result[$item_value] = Sayyes_Tools_Time::secs_to_h($respondant->{$item_check});
                        } elseif ($data_header[1] === 'date') {
                            $result[$item_value] = str_replace(['T', 'Z'], ' ', $respondant->{$item_check});
                        } else {
                            $result[$item_value] = $respondant->{$item_check};
                        }
                    }
                }
            }
        }
        return $result;
    }
            
    function getDataRespondantExcel($respondants, $headers, $header_ignore, $study,$form, $nominative_check,$nominative_ignore)
    {  
        $result['data'][] = array();
        for($i=0;$i<count($respondants);$i++) {
            $resultCheck = $this->getColumnNominative($respondants[$i], $nominative_check, $nominative_ignore);     
            foreach ($headers as $header){
                $data_header = explode('@', $header);
                if(!in_array(str_replace(' ', '_', $data_header[2]), $header_ignore) && !in_array('Value', explode('-', explode('@', $header)[2]))){
                    if($data_header[2]==="Date d'achat")
                        $data_header[2]="Date_achat";
                    $item_value = str_replace(' ', '_', $data_header[2]);   
                    $header_excel = $item_value;
                    if ($data_header[0] === 'question'){
                        $header_excel = $item_value. '@question';
                    }
                    if ($resultCheck && in_array($item_value, $nominative_ignore)) {
                        $result['data'][$i][$header_excel] = $this->translate('label_study_'.$study.'__nominative_substitute_text__'.$form);
                    }
                    else 
                    {  
                        if($item_value === 'action'){
                            $action = $respondants[$i]->action;
                            if ($action === '1') {
                                    $result['data'][$i][$header_excel] = $this->translate('label_study_'.$study.'_action-progress',null, 'study_' . $study);
                            }
                            elseif ($action === '2') {
                                    $result['data'][$i][$header_excel] = $this->translate('label_study_'.$study.'_action-close',null, 'study_' . $study);
                            }
                            else {
                                    $result['data'][$i][$header_excel] = $this->translate('label_study_'.$study.'_action-none', null,'study_' . $study);
                            }
                        } elseif ($item_value === 'structure_name') {
                            $result['data'][$i][$item_value] = $this->getStructureName($respondants[$i]->structure_belonging, $study);
                        } elseif ($item_value === 'completion_time') {
                            $result['data'][$i][$item_value] = Sayyes_Tools_Time::secs_to_h($respondants[$i]->{$item_value});
                        } elseif(count($data_header)== 4 && ($data_header[3]==='tab' || $data_header[3]==='order')) {
                            $sous_question = explode('-', $data_header[2]);
                            array_pop($sous_question);
                            $parentquestion = implode('-', $sous_question);
                            $result['data'][$i][$header_excel] = ($respondants[$i]->{$item_value}) ? $this->translate('form__question_'.$parentquestion.'_modality-'.$respondants[$i]->{$item_value} ) : '-' ;
                        }
                        elseif ($data_header[1]==='date') {
                            $result['data'][$i][$header_excel] = str_replace(['T', 'Z'], ' ', $respondants[$i]->{$item_value});
                        }
                        elseif ($data_header[0] === 'question' && $data_header[1]==='int' &&  count($data_header) < 4) {
                            $result['data'][$i][$header_excel] =	($respondants[$i]->{$item_value}) ? $this->translate('form__question_'.$data_header[2].'_modality-'.$respondants[$i]->{$item_value}) : '-' ;
                        }
                        elseif (count($data_header)== 4 && ($data_header[3]==='open')) {
                            $result['data'][$i][$header_excel] =	($respondants[$i]->{$item_value}) ? ' '.$respondants[$i]->{$item_value} : '-' ;
                        } elseif (count($data_header)== 4 && $data_header[3]==='multiple') {
                            if(count(explode('|', $respondants[$i]->{$item_value}))>1){
                                $multiple = array();
                                foreach (explode('|', $respondants[$i]->{$item_value}) as $modal){
                                    $multiple[] = $this->translate('form__question_'.$data_header[2].'_modality-'.$modal);
                                }
                                $result['data'][$i][$header_excel] = implode("\n", $multiple);
                            }
                            else
                                $result['data'][$i][$header_excel] =	($respondants[$i]->{$item_value}) ? $this->translate('form__question_'.$data_header[2].'_modality-'.$respondants[$i]->{$item_value}) : '-' ;
                        }
                        else {
                            $result['data'][$i][$header_excel] =	($respondants[$i]->{$item_value}) ? $respondants[$i]->{$item_value} : '-' ;
                        }
                    }   
                }
            }
        }
        
        return $result;
    }
    
    public function translate($index, $form = null, $study =null)
    {
        $translate = Zend_Registry::get('Zend_Translate');
        $translateArray = $this->getTranslateFile();
        if (!$form && !$study) {
             return (isset($translateArray[$index])) ? $translateArray[$index] : $index ;
        }

       return ($form) ? $translate->translate($index.'@Form_'.$form) : $translate->translate($index.'@'.$study);
    }

    public function getColorByModality($idModality=null, $alert=null) {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $selectQuery = $adapter->select();
                   $selectQuery->from('statflow_question_entry',array('complement_data'=>'complement_data'))
                               ->where('statflow_question_entry.id = ?',  $idModality)
                               ->limit(1);
        $result =  $adapter->fetchOne($selectQuery);
        if ($result && $alert) {
            $complement_data = $result ? json_decode($result) : array ();
            return isset($complement_data->colourCode) ? $complement_data->colourCode : '';
        }
        else {
            return '';
        }
    }
    
    public function getStructureName($structure_belonging, $studyId) 
    {
        $stuctService = $this->getStructureService();
        $structure = $stuctService->getStructureName($structure_belonging, $studyId);
        
        return (isset($structure['name']))? $structure['name']:'-';
    }
    
    public function getColorOpenQuestion($feedback_form_id, $codeQuestion, $alert=null) {
        $adapter = \Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $selectQuery = $adapter->select();
        $selectQuery->from('statflow_question',array('meta' => 'meta'))
                    ->where('statflow_question.code = ?',  $codeQuestion)
                    ->where('statflow_question.feedback_form_id = ?',  $feedback_form_id)
                    ->limit(1);
        $result =  $adapter->fetchOne($selectQuery);

        if ($result && $alert) {
            $meta = $result ? json_decode($result) : array ();
            return isset($meta->colourCode) ? $meta->colourCode : '';
        }
        else {
            return '';
        }
    }
}