<?php

class Costumer_Service_Graph
{
    protected $_cache;

    protected $_filters;

    protected $_rawData;

    protected $_response;
    protected $_questions = array();

    protected $_satisfaction;
    protected $_satisfactionByMonth;
    protected $_satisfactionGrading;
    protected $_satisfactionGradingByMonth;
    protected $_satisfactionDetailed;
    protected $_satisfactionDetailedByMonth;

    protected $_nps;
    protected $_npsDistribution;
    protected $_npsEvolution;
    protected $_npsDistributionEvolutionByMonth;

    protected $_impactOnImage;
    protected $_impactOnImageEvolution;

    protected $_negativeAlerts;
    protected $_negativeAlertsEvolution;

    protected $_respondentRates;
    
    protected $_intentionToComeBack;
    protected $_intentionToComeBackByMonth;
    
    protected  $_detailedIntentionToComeBack;
    protected  $_detailedIntentionToComeBackByMonth;

    protected $_respondentIds = array();


    public function __construct()
    {
        $this->_filters = new Costumer_Model_Filters();
    }

    //Loading
    private function _loadOverAllSatisfaction()
    {
        $this->_satisfaction = array(
            'graphData' => array(
                array(
                    'name' => 'Not satisfied at all',
                    'data' => array(10)
                ),
                array(
                    'name' => 'Not satisfied',
                    'data' => array(15)
                ),
                array(
                    'name' => 'Satisfied',
                    'data' => array(25)
                ),
                array(
                    'name' => 'Very satisfied',
                    'data' => array(50)
                )
            ),
            'average'   => 7.5
        );

    }

    private function _loadOverAllSatisfactionByMonth()
    {
        $dataByMonthAverage = array(
            'Jan' => 6,
            'Feb' => 6.1,
            'Mar' => 6.5,
            'Apr' => 6.6,
            'May' => 7.2,
            'Jun' => 7.5,
            'Jul' => 7.2,
            'Aug' => 7.7,
            'Sep' => 7.3,
            'Oct' => 7.4,
            'Nov' => 7.9,
            'Dec' => 7.6
        );        
        $this->_satisfactionByMonth = $dataByMonthAverage;
    }

    private function _loadSatisfactionGrading()
    {
        $veryPoor = 20;
        $poor = 20;
        $good = 30;
        $veryGood = 30;
        
        $countRespondant = count($this->_response);

        $roundedVeryPoor = round((($veryPoor / $countRespondant) * 100));
        $roundedPoor = round((($poor / $countRespondant) * 100));
        $roundedGood = round((($good / $countRespondant) * 100));
        $roundedVeryGood = round((($veryGood / $countRespondant) * 100));        

        $this->_satisfactionGrading = array(
            array('Very poor', $roundedVeryPoor),
            array('Poor', $roundedPoor),
            array('Good', $roundedGood),
            array('Very good', $roundedVeryGood)
        );
    }

    private function _loadSatisfactionGradingByMonth()
    {       
        $dataByMonthAverage = array(
            'Very poor' => array(
                'Jan' => 70,
                'Feb' => 57,
                'Mar' => 51,
                'Apr' => 45,
                'May' => 37,
                'Jun' => 32,
                'Jul' => 30,
                'Aug' => 25,
                'Sep' => 24,
                'Oct' => 20,
                'Nov' => 18,
                'Dec' => 15
            ),
            'Poor'      => array(
                'Jan' => 12,
                'Feb' => 13,
                'Mar' => 18,
                'Apr' => 19,
                'May' => 10,
                'Jun' => 13,
                'Jul' => 15,
                'Aug' => 22,
                'Sep' => 21,
                'Oct' => 20,
                'Nov' => 15,
                'Dec' => 11
            ),
            'Good'      => array(
                'Jan' => 10,
                'Feb' => 18,
                'Mar' => 22,
                'Apr' => 11,
                'May' => 17,
                'Jun' => 22,
                'Jul' => 18,
                'Aug' => 20,
                'Sep' => 18,
                'Oct' => 14,
                'Nov' => 13,
                'Dec' => 11
            ),
            'Very good' => array(
                'Jan' => 20,
                'Feb' => 25,
                'Mar' => 27,
                'Apr' => 44,
                'May' => 46,
                'Jun' => 52,
                'Jul' => 55,
                'Aug' => 58,
                'Sep' => 66,
                'Oct' => 69,
                'Nov' => 71,
                'Dec' => 74
            )
        );
        $this->_satisfactionGradingByMonth = $dataByMonthAverage;
    }

    private function _loadSatisfactionDetailed()
    {
       $roundedSatisfactionDetailed = array(
            '1-4'  => array(
                'name' => 'Not satisfied at all',
                'data' => array(25, 14, 19, 12, 10, 9, 13, 10)
            ),
            '5-6'  => array(
                'name' => 'Not satisfied',
                'data' => array(12, 16, 14, 12, 16, 24, 15, 9)
            ),
            '7-8'  => array(
                'name' => 'Satisfied',
                'data' => array(33, 43, 35, 43, 37, 25, 35, 44)
            ),
            '9-10' => array(
                'name' => 'Very satisfied',
                'data' => array(30, 27, 32, 33, 37, 42, 43, 37)
            ),
        );

        $averageSatisfactionDetailed = array(35.6,37.1,14,13);   

        $this->_satisfactionDetailed = array('graphData'   => $roundedSatisfactionDetailed,
                                             'averageData' => $averageSatisfactionDetailed);
    }

    private function _loadSatisfactionDetailedByMonth()
    {
        
        $satisfactionDetailedByMonthAverage = array(
            array(
                'name' => 'Contact on phone',
                'data' => array(5.0, 5.9, 6.5, 6.5, 6.2, 6.5, 6.2, 7.5, 7.3, 7.3, 7.9, 8.6)
            ),
            array(
                'name' => 'Smoothness of repair process',
                'data' => array(3.2, 3.8, 3.7, 4.3, 5.0, 5.0, 4.8, 5.1, 5.1, 6.1, 6.6, 6.5)
            ),
            array(
                'name' => 'Provision of machine loaned',
                'data' => array(2.9, 3.6, 4.5, 4.4, 5.5, 6.0, 6.6, 7.0, 6.3, 7.0, 6.9, 7.0)
            ),
            array(
                'name' => 'Duration of repair',
                'data' => array(3.9, 4.2, 4.7, 5.5, 5.9, 5.2, 6.0, 6.6, 6.2, 6.3, 6.6, 6.8)
            ),
            array(
                'name' => 'Tracking of repair process',
                'data' => array(2.9, 3.2, 4.7, 5.5, 4.9, 4.2, 5.0, 5.6, 6.2, 6.3, 6.6, 7.8)
            ),
            array(
                'name' => 'Care given to repaired machine',
                'data' => array(3.9, 4.2, 4.7, 5.5, 6.9, 7.2, 7.0, 7.6, 7.2, 8.3, 7.6, 7.8)
            ),
            array(
                'name' => 'Clealiness of repaired machine',
                'data' => array(3.9, 4.2, 5.7, 6.5, 6.9, 7.2, 8.0, 7.6, 7.2, 8.3, 7.6, 8.8)
            ),
        );

        $this->_satisfactionDetailedByMonth = $satisfactionDetailedByMonthAverage;
    }

    private function _loadNPS()
    {
        $this->_nps = array(
            'graphData'   => array(
                array(
                    'name' => 'Detractor',
                    'data' => array(19)
                ),
                array(
                    'name' => 'Passive',
                    'data' => array(24)
                ),
                array(
                    'name' => 'Promoter',
                    'data' => array(57)
                )
            ),
            'npsValue'    => round(57 - 19),
            'averageData' => 7.5
        );
    }

    private function _loadNPSDistribution()
    {
        $countRespondant = count($this->_response);
        $scores = array(
            '10' => 10,
            '9'  => 28,
            '8'  => 32,
            '7'  => 44,
            '6'  => 52,
            '5'  => 6,
            '4'  => 10,
            '3'  => 30,
            '2'  => 60,
            '1'  => 70,
            '0'  => 79,
            //'countRespondant' => 0
        );
        $scores['countRespondant'] = $countRespondant;
        $this->_npsDistribution = $scores;
    }

    private function _loadNPSEvolution()
    {
        $npsEvolutionRounded = array(
            'Jan' => -50,
            'Feb' => -48,
            'Mar' => -32,
            'Apr' => -24,
            'May' => -12,
            'Jun' => 6,
            'Jul' => 10,
            'Aug' => 30,
            'Sep' => 60,
            'Oct' => 70,
            'Nov' => 79,
            'Dec' => 90
        );
        $this->_npsEvolution = $npsEvolutionRounded;
    }

    private function _loadNPSDistributionEvolutionByMonth()
    {    	
        $npsEvolutionRounded = array(
            'npsPromoters' => array(
                'Jan' => 70,
                'Feb' => 57,
                'Mar' => 51,
                'Apr' => 45,
                'May' => 37,
                'Jun' => 32,
                'Jul' => 30,
                'Aug' => 25,
                'Sep' => 24,
                'Oct' => 20,
                'Nov' => 18,
                'Dec' => 15
            ),
            'npsPassive'      => array(
                'Jan' => 12,
                'Feb' => 13,
                'Mar' => 18,
                'Apr' => 19,
                'May' => 10,
                'Jun' => 13,
                'Jul' => 15,
                'Aug' => 22,
                'Sep' => 21,
                'Oct' => 20,
                'Nov' => 15,
                'Dec' => 11
            ),
            'npsDetractors'      => array(
                'Jan' => 10,
                'Feb' => 18,
                'Mar' => 22,
                'Apr' => 11,
                'May' => 17,
                'Jun' => 22,
                'Jul' => 18,
                'Aug' => 20,
                'Sep' => 18,
                'Oct' => 14,
                'Nov' => 13,
                'Dec' => 11
            )
        );
        $this->_npsDistributionEvolutionByMonth = $npsEvolutionRounded;
    }

    private function _loadRespondentRates(Costumer_Service_Solr $service)
    {
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $query = $service->getClient()->createQuery('select');
        $query->setHandler('query');
        $query->setFields(array('respondent_id'));
        $query->addParam('rows', $adapter->fetchOne('SELECT COUNT(*) FROM statflow_respondent'));
        try {
            // this executes the query and returns the result
            $resultset = $service->getClient()->select($query);
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }

        $response = Zend_Json::decode($resultset->getResponse()->getBody())['response'];
        $ids = array();
        foreach($response['docs'] as $doc) {
            $ids[] = $doc['respondent_id'];
        }

        if(count($ids) > 0) {

            $select =
                'SELECT
                    SUM(
                        CASE
                            WHEN statflow_respondent.status = 0 THEN 1 ELSE 0
                        END
                    ) as not_sent,
                    SUM(
                        CASE
                            WHEN statflow_respondent.status = 1 THEN 1 ELSE 0
                        END
                    ) as sent,
                    SUM(
                        CASE
                            WHEN statflow_respondent.status = 1 THEN 1 ELSE 0
                        END
                    ) as not_started,
                    SUM(
                        CASE
                            WHEN statflow_respondent.status = 10 THEN 1 ELSE 0
                        END
                    ) as pending,
                    SUM(
                        CASE
                            WHEN statflow_respondent.status = 20 THEN 1 ELSE 0
                        END
                    ) as done,
                    COUNT(*) as total
                FROM statflow_respondent
                WHERE statflow_respondent.id IN (' .join(',', $ids) . ')';

            $data = $adapter->fetchRow($select);

            $dataPercent = array(
                'not_sent'    => round(($data['not_sent'] / $data['total']) * 100, 1),
                'sent'        => round(($data['sent'] / $data['total']) * 100, 1),
                'not_started' => round(($data['not_started'] / $data['total']) * 100, 1),
                'pending'     => round(($data['pending'] / $data['total']) * 100, 1),
                'done'        => round(($data['done'] / $data['total']) * 100, 1),
                'yes'         => round(($data['done'] / $data['total']) * 100, 1),
                'not yet'     => round((($data['pending'] + $data['not_started']) / $data['total']) * 100, 1),
                'total'       => $data['total'],
                'respondents' => $data['done'],
                'sent_emails' => $data['sent'],
            );
        }
        else {
            $dataPercent = array(
                'not_sent'    => 0,
                'sent'        => 0,
                'not_started' => 0,
                'pending'     => 0,
                'done'        => 0,
                'yes'         => 0,
                'not yet'     => 0,
                'total'       => 0,
                'respondents' => 0,
                'sent_emails' => 0
            );
        }

        $this->_respondentRates = $dataPercent;
    }

    //public getters

    public function getOverAllSatisfaction()
    {
        if ($this->_satisfaction == null) {
            $this->_loadOverAllSatisfaction();
        }
        return $this->_satisfaction;
    }

    public function getOverAllSatisfactionByMonth()
    {
        if ($this->_satisfactionByMonth == null) {
            $this->_loadOverAllSatisfactionByMonth();
        }
        return $this->_satisfactionByMonth;
    }

    public function getSatisfactionGrading()
    {
        if ($this->_satisfactionGrading == null) {
            $this->_loadSatisfactionGrading();
        }
        return $this->_satisfactionGrading;
    }

    public function getSatisfactionGradingByMonth()
    {
        if ($this->_satisfactionGradingByMonth == null) {
            $this->_loadSatisfactionGradingByMonth();
        }
        return $this->_satisfactionGradingByMonth;
    }

    public function getSatisfactionDetailed()
    {
        if ($this->_satisfactionDetailed == null) {
            $this->_loadSatisfactionDetailed();
        }
        return $this->_satisfactionDetailed;
    }

    public function getSatisfactionDetailedByMonth()
    {
        if ($this->_satisfactionDetailedByMonth == null) {
            $this->_loadSatisfactionDetailedByMonth();
        }
        return $this->_satisfactionDetailedByMonth;
    }

    public function getNPS()
    {
        if ($this->_nps == null) {
            $this->_loadNPS();
        }
        return $this->_nps;
    }

    public function getNPSDistribution()
    {
        if ($this->_npsDistribution == null) {
            $this->_loadNPSDistribution();
        }
        return $this->_npsDistribution;
    }

    public function getNPSEvolution()
    {
        if ($this->_npsEvolution == null) {
            $this->_loadNPSEvolution();
        }
        return $this->_npsEvolution;
    }

    public function getNPSDistributionEvolutionByMonth()
    {
        if ($this->_npsDistributionEvolutionByMonth == null) {
            $this->_loadNPSDistributionEvolutionByMonth();
        }
        return $this->_npsDistributionEvolutionByMonth;
    }
    
    public function getResponses()
    {
        return $this->_response;
    }

    public function setResponses($responses)
    {
        $this->_response = $responses;
    }

    public function getFilters()
    {
        return $this->_filters;
    }

    public function getQuestions()
    {
        return $this->_questions;
    }
    
    public function setQuestions($questions)
    {
        $this->_questions = $questions;
    }
    
    public function getRespondentRates(Costumer_Service_Solr $service)
    {
        if ($this->_respondentRates == null) {
            $this->_loadRespondentRates($service);
        }
        return $this->_respondentRates;
    }

    public static function generate()
    {
        $id = uniqid();
    
        $id = base_convert($id, 16, 2);
        $id = str_pad($id, strlen($id) + (8 - (strlen($id) % 8)), '0', STR_PAD_LEFT);
    
        $chunks = str_split($id, 8);
        //$mask = (int) base_convert(IDGenerator::BIT_MASK, 2, 10);
    
        $id = array();
        foreach ($chunks as $key => $chunk) {
            //$chunk = str_pad(base_convert(base_convert($chunk, 2, 10) ^ $mask, 10, 2), 8, '0', STR_PAD_LEFT);
            if ($key & 1) { // odd
                array_unshift($id, $chunk);
            } else { // even
                array_push($id, $chunk);
            }
        }
    
        return base_convert(implode($id), 2, 36);
    }  
   
}
