<?php

class Costumer_Service_Mail
{


    protected $_questions = array();

    protected $_form = null;

    protected $_answers = array();

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    public function setForm($form)
    {
        $this->_form = $form;
    }


    public function getQuestions()
    {
        return $this->_questions;
    }

    public function setQuestions($questions)
    {
        $this->_questions = $questions;
    }

    public function sendMail($form, $respondent, $answers)
    {
        $translator = Zend_Registry::get('Zend_Translate');
        $respondentData = $respondent->toArray();
        $respondentF1data = Centurion_Db::getSingleton('statflow/respondentExternalField')->fetchAll('respondent_id = ' . $respondent->id);
        foreach ($respondentF1data as $f1Entry) {
            $respondentData[$f1Entry->label] = $f1Entry->value;
        }

        $responses = array();

        foreach($form->questions as $question) {
            if(!in_array($question->code, array('Q1', 'Q3', 'Q5', 'Q6', 'Q7', 'Q8', 'Q9'))) {
                $responses[$question->code] = null;
            }
        }

        foreach ($answers as $answer) {
            if ($answer['answer'] == null) continue;
            $question = $answer['question'];
            if ($question->parent_id) {
                if (!isset($formData[$question->parent_id])) {
                    $formData[$question->parent_id] = array(
                        'question' => Centurion_Db::getSingleton('statflow/question')->fetchRow('id = ' . $question->parent_id)
                    );
                }
            }
            if (in_array($question->code, array('Q1-1', 'Q3-1'))) {
                $excelRow = array(
                    'label' => str_replace('-1', '', $question->code) . ' - ' . strip_tags($translator->translate('form__' . $question->feedback_form_id . '__question_title-' . str_replace('-1', '', $question->code) . '@Form_' . $question->feedback_form_id, 'fr'))
                );
            } else {
                $excelRow = array(
                    'label' => $question->code . ' - ' . strip_tags($translator->translate('form__' . $question->feedback_form_id . '__question_title-' . $question->code . '@Form_' . $question->feedback_form_id, 'fr'))
                );
            }
            if ($question->parent_id) {
                $excelRow['answer'] = ($answer['answer']->value != null) ?  strip_tags($translator->translate('form__question_' . ($formData[$question->parent_id]['question']->code) . '_modality-' . $answer['answer']->value . '@Form_' . $question->feedback_form_id), 'fr') : '';
            } else {
                switch ($question->question_type_id) {
                    case Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_INTERVAL:
                    case Statflow_Model_DbTable_Row_Question::QUESTION_TYPE_OPEN:
                        $excelRow['answer'] = $answer['answer']->value;
                        break;
                    default:
                        $excelRow['answer'] = array();
                        foreach ($answer['answerEntries'] as $entry) {
                            $entryText = $translator->translate('form__question_' . $question->code . '_modality-' . $entry->question_entry_id . '@Form_' . $question->feedback_form_id, 'fr');
                            if ($entry->content) $entryText .= " : " . $entry->content;
                            $excelRow['answer'][] = strip_tags($entryText);
                        }
                }
            }
            $responses[$question->code] = $excelRow;
        }

        foreach($responses as $code => $response) {
            if($response == null) {
                $responses[$code] = array(
                    'label' => $code . ' - ' . strip_tags($translator->translate('form__' . $form->id . '__question_title-' . $code . '@Form_' . $form->id, 'fr')),
                    'answer' => ''
                );
            }
            if($code == 'Q1' || $code == 'Q1-1') {
                $responses[$code]['label'] = $code . ' - ' . strip_tags(preg_replace('/ \[FILE:.+?\] /', ' ', $translator->translate('form__1__question_title-Q1', 'fr')));
            }
        }

        ksort($responses, SORT_NATURAL);
        $this->setQuestions($responses);
        $attachement = $this->_generateAttachment($respondentData, $form);
        $this->_sendMail($form, $respondent, $attachement);
        return $attachement;
    }

    protected function _generateAttachment($respondent, $form)
    {

        if (true) {

            require_once(APPLICATION_PATH . '/../library/PHPExcel/PHPExcel.php');
            require_once(APPLICATION_PATH . '/../library/PHPExcel/PHPExcel/Writer/Excel5.php');


            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            mb_internal_encoding("UTF-8");

            $line = 1;
            $col = 0;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Profil du répondant");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:B2');
            $objPHPExcel->getActiveSheet()->getStyle('A2:B2')
                ->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->getStyle('A2:B2')
                ->applyFromArray(
                    array(
                        'borders' => array(
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => 'FFF'),
                            ),
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => 'FFF'),
                            )
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0')
                        ),
                        'font' => array(
                            'bold' => true,
                            'name' => 'Arial',
                            'size' => 8
                        ),
                        'alignment' => array(
                            'wrap' => true,
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    )
                );

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'Civilité');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, 'Nom');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 5, 'Prénom');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'Identifiant unique contact');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 7, 'Date d\'achat');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 8, 'Adresse email principale');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 9, 'Portable');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 10, 'Téléphone');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 11, 'Nom lieu contact');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 12, 'Code Rayon');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 13, 'Code Groupe');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 14, 'Code famille');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 15, 'Flag salarié');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 16, 'Quantité');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 17, 'Date de création carte');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 18, 'Type de carte');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 19, 'Date de réponse au questionnaire');

            //Values
            $dateCarte = new \Zend_Date($respondent['Date de creation carte'], 'en');
            $date = new \Zend_Date($respondent['answered_at'], Zend_Date::ISO_8601);
            $dateAchat = new \Zend_Date($respondent["Date d'achat"], 'en');


            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $respondent['Civilite']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $respondent['lastname']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 5, $respondent['firstname']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, $respondent['external_id']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 7, $dateAchat->get(\Zend_Date::DATE_MEDIUM, 'fr'));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 8, $respondent['email']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 9, $respondent['Portable']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 10, $respondent['Téléphone']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 11, $respondent['Nom lieu contact']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 12, $respondent['Code Rayon ']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 13, $respondent['Code Groupe ']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 14, $respondent['Code famille ']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 15, $respondent['Flag salarié']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 16, $respondent['Quantité']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 17, $dateCarte->get(\Zend_Date::DATE_MEDIUM, 'fr'));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 18,  $respondent['Type de carte']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 19,  $date->get(\Zend_Date::DATE_MEDIUM, 'fr'));

            unset($date);

            $styleArray = array(
                'borders' => array(
                    'outline' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => 'FFF'),
                    ),
                    'inside' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FFF'),
                    ),
                ),
                'font' => array(
                    'name' => 'Arial',
                    'size' => 8
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A3:B19')->applyFromArray($styleArray);

            /*$styleArray = array(
                'font' => array(
                    'bold' => true
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($styleArray);*/


            ///------------------------ ANSWERS -------------------------- ///

            $objPHPExcel->getActiveSheet()->mergeCells('A20:B20');

            /*$objRichText = new \PHPExcel_RichText();
            $run1 = $objRichText->createTextRun("Respondent's answer ");
            $run1->getFont()->setColor(new \PHPExcel_Style_Color(\PHPExcel_Style_Color::COLOR_BLACK))->setSize(8)->setName('Arial')->setBold(true);

            $run2 = $objRichText->createTextRun('(alert in red)');
            $run2->getFont()->setColor(new \PHPExcel_Style_Color(\PHPExcel_Style_Color::COLOR_RED))->setSize(8)->setName('Arial')->setBold(true);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A12", $objRichText);
            */
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 20, "Réponse du client");

            $objPHPExcel->getActiveSheet()->getStyle('A20:B20')
                ->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->getStyle('A20:B20')
                ->applyFromArray(
                    array(
                        'borders' => array(
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => 'FFF'),
                            ),
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => 'FFF'),
                            )
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0')
                        ),
                        'font' => array(
                            'bold' => true,
                            'name' => 'Arial',
                            'size' => 8
                        ),
                        'alignment' => array(
                            'wrap' => true,
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    )
                );
            $line = 21;
            foreach ($this->getQuestions() as $question) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $line, trim($question['label']));
                $line++;
            }

            $line = 21;
            foreach ($this->getQuestions() as $question) {
                $response = $question['answer'];

                if (is_array($response)) {
                    $row = 1;
                    foreach ($response as $subResponse) {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row, $line, $subResponse);
                        $row++;
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $line, $response);
                }

                $line++;
            }

            $styleArray = array(
                'borders' => array(
                    'outline' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => 'FFF'),
                    ),
                    'inside' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FFF'),
                    ),
                ),
                'font' => array(
                    'name' => 'Arial',
                    'size' => 8
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A20:B43')->applyFromArray($styleArray);

            $styleArray = array(
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A21:A43')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->getStyle('A3:B19')->getAlignment()->setIndent(2);
            $objPHPExcel->getActiveSheet()->getStyle('A21:A43')->getAlignment()->setIndent(2);



            //$objPHPExcel->getActiveSheet()->getRowDimension(16)->setRowHeight(25);
            //$objPHPExcel->getActiveSheet()->getRowDimension(30)->setRowHeight(25);
            //$objPHPExcel->getActiveSheet()->getRowDimension(31)->setRowHeight(25);
            //$objPHPExcel->getActiveSheet()->getRowDimension(32)->setRowHeight(25);

            $styleArray = array(
                'font' => array(
                    'color' => array('argb' => 'FFFF0000'),
                )
            );
            $responses = $this->getQuestions();
            if ($responses['Q1-1']['answer'] <= 4)
                $objPHPExcel->getActiveSheet()->getStyle('A21:B21')->applyFromArray($styleArray);
            if ($responses['Q3-1']['answer'] <= 4)
                $objPHPExcel->getActiveSheet()->getStyle('A23:B23')->applyFromArray($styleArray);
            if ($responses['Q10']['answer'][0] == 'Oui')
                $objPHPExcel->getActiveSheet()->getStyle('A41:B41')->applyFromArray($styleArray);

            //----------------------------- GLOBAL ----------------------------//

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(29);

            $objPHPExcel->getActiveSheet()->getPageSetup()->setPrintArea('A2:B43');
            $objPHPExcel->getActiveSheet()->getStyle('A1:B43')
                ->getAlignment()->setWrapText(true);

            $formMeta = Zend_Json::decode($form->meta);
            $excelSavePath = APPLICATION_PATH . '/../public/ftp/costumer/alerts/';
            $excelFileName = (isset($formMeta['attachementName']) && $formMeta['attachementName'] ? $formMeta['attachementName'] : 'alert').'-'.$respondent['id'].'.xls';

            $excelWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            $excelWriter->save($excelSavePath . $excelFileName);

            return $excelSavePath . $excelFileName;
        }
    }

    protected function _sendMail($form, $respondent, $attachementPath){
        $formMeta = Zend_Json::decode($form->meta);

        $recipients = isset($formMeta['alertRecipients']) && is_array($formMeta['alertRecipients']) ? $formMeta['alertRecipients'] : array();

        // if no recipients, quit
        if( count($recipients) == 0) return;

        $respondentStructure = Centurion_Db::getSingleton('statflow/structure')->fetchRow("external_id = '{$respondent->structure_belonging}'");

        $finalRecipients = array();

        //match email recipients with users attached to respondents structure and/or parents recursively
        do {
            $structureUsers = $respondentStructure->users;
            foreach($structureUsers as $user){
                if(in_array($user->email, $recipients))
                    $finalRecipients[] = $user->email;
            }
            $respondentStructure = $respondentStructure->parent;
        } while($respondentStructure != null);

        if( count($finalRecipients)){

            $translator = Zend_Registry::get('Zend_Translate');

            $smtpHost = 'pro1.mail.ovh.net';
            $smtpConf = array(
                'port' => '587',
                'ssl' => 'tls',
                'auth' => 'login',
                'username' => 'webmaster@it-prospect.com',
                'password' => 'itprospect@@',
                'timeout' => 60
            );
            $transport = new Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);

            $mail = new Zend_Mail('UTF-8');
            $mail->setType(Zend_Mime::MULTIPART_RELATED);
            $mail->setFrom('webmaster@it-prospect.com', 'Les Galeries Lafayette');
            foreach($finalRecipients as $recipient)
                $mail->addBcc($recipient);
            $attachementContents = file_get_contents($attachementPath);
            $attachement = $mail->createAttachment($attachementContents);
            $attachement->filename = basename($attachementPath);

            $mail->setSubject( $html = $translator->translate('alert_email_subject_form_'.($form->id).'@Form_'.$form->id, 'fr'));

            $mail->setBodyText("Cher manager,
            Un client a fait part d’une expérience négative lors de sa dernière visite dans *votre rayon* :
            *Pour rappel, une expérience négative signifie que ...*
            Satisfaction globale inférieure ou égale à 4
            Indice de recommandation inférieur ou égal à 4
            Vous avez, grâce à ce questionnaire, l’opportunité de comprendre cette expérience et de reprendre contact avec ce client afin de le réassurer quant à notre volonté de lui offrir un service haut de gamme.
            C’est aussi cet échange avec le client qui vous permettra d’adapter les actions à enclencher.
            Pour ce faire :
            Prenez connaissance des réponses données par ce client via le fichier ci-joint
            Collecter dans « Direct Consumer » l’ensemble des informations concernant ce client
            Programmez un contact avec ce client pour :
            *Le remercier* d’avoir partagé son expérience avec nous
            *Approfondir* ses motifs de mécontentement
            *Présenter des excuses* pour la déception et le désagrément causés
            *Le remercier* de nouveau pour sa disponibilité
            Merci par avance pour votre implication !
            /Les Galeries Lafayette Haussmann/");

            $html = $translator->translate('alert_email_body_form_'.($form->id).'@Form_'.$form->id, 'fr');

            $mail->setBodyHtml($html);

            try{
               $mail->send($transport);
            } catch (Exception $e){
                Zend_Debug::dump($e->getMessage());
                Zend_Debug::dump($e->getTraceAsString());
            }
        }
    }

}
