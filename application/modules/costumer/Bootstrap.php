<?php

class Costumer_Bootstrap extends Centurion_Application_Module_Bootstrap
{

    protected function _initAutoload () {
        $resourceLoader = $this->getResourceLoader();
        $resourceLoader->addResourceType ('Validator', 'validators/', 'Validator_');
    }

}

