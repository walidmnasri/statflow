<?php


/**
 * Helper d'admin pour afficher les flashmessages
 * 
 * @author  Damien Camus <dcamus@nodevo.com>
 * @link    www.nodevo.com
 * @since   2011-01-07
 *
 * 
 */
class Zend_View_Helper_DisplayFlashMessages extends Zend_View_Helper_Abstract{

	/**
	* Retourne une liste HTML des messages flash
	* @return string
	*/
    public function displayFlashMessages(){
	
        $flash = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
	
		$html = '';
        if ($flash->count()) {
			$messages = $flash->getMessages();
			foreach ($messages as $message) {
				$message = explode('|',$message);
				$messagetype = ($message[1])? strtolower($message[0]) : 'info';
				$messagetext = ($message[1])? $message[1] : $message[0];
				$html .= '<div onclick="$(this).hide(\'normal\');" class="flashmessage flashmessage-type-';
				$html .= $messagetype . '"><div class="icon-' . $messagetype . '-16">';
				$html .= $messagetext . '</div></div>';
			}
        } else {
            $html = '';
        }
		
		return $html;
    }
}