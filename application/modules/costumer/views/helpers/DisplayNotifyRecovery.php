<?php

/**
 * Helper qui renvoie le code HTML affichant les notifications des recovery acts
 * 
 * @author    Yann 'YRO' Rochereau <yrochereau@nodevo.com>
 * @copyright 2012 Nodevo
 * @link      www.nodevo.com
 * @since     1.4
 */
class Zend_View_Helper_DisplayNotifyRecovery extends Zend_View_Helper_Abstract
{

    /**
     * Retourne une liste HTML des messages flash
     * 
     * @return string le code html du filtre géographique
     */
    public function displayNotifyRecovery()
    {
        $em = Zend_Registry::get('em');
        $ref = $em->getRepository( 'Entities\Core\Param' )->findOneByName( 'questionnaire_lvmh' )->getValue();


        $daoRecovery = new \DAO\Wao\Recovery;
        $recovery = $daoRecovery->getAllByQuestionnaireRef( $ref );
        $count = count($recovery['recovery']);
        $html = <<<HTML
        <div class="delighted notify" rel="{$recovery['delighting'][0]["value"]}"></div>
        <div class="recovery notify" rel="{$count}"></div>
HTML;
        return $html;
    }

}