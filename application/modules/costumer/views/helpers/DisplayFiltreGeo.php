<?php

/**
 * Helper d'admin pour afficher les filtres géograpĥiques
 * 
 * @author    Yann 'YRO' Rochereau <yrochereau@nodevo.com>
 * @copyright 2012 Nodevo
 * @link      www.nodevo.com
 * @since     1.4
 */
class Zend_View_Helper_DisplayFiltreGeo extends Zend_View_Helper_Abstract
{

    /**
     * Retourne une liste HTML des messages flash
     * 
     * @return string le code html du filtre géographique
     */
    public function displayFiltreGeo()
    {
        $controller = Zend_Controller_Front::getInstance();
        $controllerName = $controller->getRequest()->getControllerName();
        $actionName = $controller->getRequest()->getActionName();

        $whereIAm = $controllerName . '-' . $actionName;
        
        $filtregeo = $controller->getRequest()->getParam( 'filtregeo' );

        $html = '
            <form id="filtre-geo">';
        $em = \Zend_Registry::get( 'em' );

        $auth = Zend_Auth::getInstance()->getIdentity();
        $dao = new \DAO\Wao\StructureUser();
        $daoStructure = new \DAO\Wao\Structure();

        if ( !in_array( $whereIAm, array( 'recoveryact-fiche', 'recoveryact-history', 'recoveryact-table' ) ) ) {
            if ( $filtregeo == NULL ) {
                // Si on est super-admin, on va chercher la structure LV
                if ( $auth[ 'roleid' ] == 1 ) {
                    $str = $em->getRepository( '\Entities\Wao\Structure' )->findOneByNom( "Louis Vuitton" );
                } else {
                    // Sinon on cherche la structure de l'utilisateur courant
                    $structure = $dao->getStructureIdByCoreUsrId( $auth[ 'id' ] );
                    $str = $em->getRepository( '\Entities\Wao\Structure' )->find( $structure[ 0 ][ 'str_id' ] );
                }

                $childs = $daoStructure->getDirectChildrens( $str->getId() );
                if ( $childs != null && !empty( $childs ) ) {
                    // Niveau de l'utilisateur connecté
                    $html .= "
                    <select name='filtregeo[0]' rel='0' style='display:none'>
                        <option value='" . $str->getId() . "'>" . $str->getNom() . "</option>
                    </select>";

                    $html .= "
                    <select name='filtregeo[1]' rel='1'>";
                    $html .= "
                        <option value=''>All</option>";

                    foreach ( $childs as $child ) {
                        $html .= "
                        <option value='" . $child->getId() . "'>" . $child->getNom() . "</option>";
                    }
                    $html .= "
                </select>
            ";
                } else {
                    $html .= "
                    <h3>" . $str->getNom() . "</h3>
                        ";
                }
            } else {
                $filtregeo = array_filter( $filtregeo );
                $str = $em->getRepository( '\Entities\Wao\Structure' )->find( $filtregeo[ 0 ] );
                $html .= "
                    <select name='filtregeo[0]' rel='0' style='display:none;'>
                        <option value='" . $str->getId() . "'>" .$str->getNom(). "</option>
                    </select>";

                for ( $i = 0; $i < count( $filtregeo ); $i++ ) {
                    $str = $i == 1 ? $str : $em->getRepository( '\Entities\Wao\Structure' )->find( $filtregeo[ $i ] );
                    $childs = $daoStructure->getDirectChildrens( $filtregeo[ $i ] );
                    if ( $childs ) {
                        $html .= "<select name='filtregeo[" . ($i+1) . "]' rel='" . ($i+1) . "'><option value=''>All</option>";
                        foreach ( $childs as $child ) {
                            $selected = isset( $filtregeo[ $i + 1 ] ) && $child->getId() == $filtregeo[ $i+1 ] ? 'selected="selected"' : "";
                            $html .= "<option value='" . $child->getId() . "' " . $selected . " >" . $child->getNom() . "</option>";
                        }
                        $html .= "</select>";
                    }
                }
            }
        } else {
            if ( $auth[ 'roleid' ] == 1 ) {
                $str = $em->getRepository( '\Entities\Wao\Structure' )->findOneByNom( "Louis Vuitton" );
            } else {
                // Sinon on cherche la structure de l'utilisateur courant
                $structure = $dao->getStructureIdByCoreUsrId( $auth[ 'id' ] );
                $str = $em->getRepository( '\Entities\Wao\Structure' )->find( $structure[ 0 ][ 'str_id' ] );
            }
            
            $html .= "
                    <h3>" . $str->getNom() . "</h3>
                        ";
        }

        $html .= '</form>
            ';
        return $html;
    }

}