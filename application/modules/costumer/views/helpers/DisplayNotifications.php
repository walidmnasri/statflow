<?php


/**
 * Helper d'admin pour afficher les flashmessages
 * 
 * @author  Damien Camus <dcamus@nodevo.com>
 * @link    www.nodevo.com
 * @since   2011-01-07
 *
 * 
 */
class Zend_View_Helper_DisplayNotifications extends Zend_View_Helper_Abstract{

	/**
	* Retourne une liste HTML des messages flash
	* @return string
	*/
    public function displayNotifications( $nb ){
	
        $flash = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
	
		$html = '<div class="notifications">';
		$html .= '<div class="nLeft"></div>';
		$html .= '<div class="nMiddle"></div>';
		$html .= '<div class="nRight"></div>';
		$html .= '<div class="nValue"><span>' . $nb . '</span></div>';
		$html .= '<div class="clear: both"></div>';
		$html .= '</div>';
        
		
		return $html;
    }
}