<?php

class Costumer_Validator_Email extends Zend_Validate_Abstract {

    const NOT_VALID = 'notUrl';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_VALID => "'%value%' is already in the database"
    );


    /**
     * @param mixed $email
     * @return bool
     */
    public function isValid($email)
    {
        $model = Centurion_Db::getSingleton('statflow/respondent');
        $row = $model->select(true)->filter(array('email' => $email))->order('created_at desc')->fetchRow();
        if($row == null) {
            return true;
        }
        $createdAt = new Zend_Date($row->created_at, 'YYYY-MM-dd HH:mm:ss');
        $now = Zend_Date::now();
        if($now->subMonth(6)->isLater($createdAt)) {
            return true;
        }
        return false;
    }
}