<?php
/**
 * Class Costumer_Model_Filters
 */
class Costumer_Model_Filters
{

    /**
     * @var array
     */
    protected $_receivedFilter
        = array(
            'periodStart' => null,
            'periodEnd' => null,
            'purchaseStart' => null,
            'purchaseEnd' => null,
            //'group' => null,
            'compareCount' => null
        );

    /**
     * Constructor
     */
    public function __construct()
    {

    }
    
    public function getReceivedFilters()
    {
        return $this->_receivedFilter;
    }
    
    public function setReceivedFilters($filters , $isCompare = false)
    {
        if ($isCompare !== false) {
            $this->_receivedFilter['compareCount']    = $filters['compare'];
            $this->_receivedFilter['periodStart']     = $filters['compare-period-start'];
            $this->_receivedFilter['periodEnd']       = $filters['compare-period-end'];
            $this->_receivedFilter['purchaseStart']   = $filters['compare-purchase-start'];
            $this->_receivedFilter['purchaseEnd']     = $filters['compare-purchase-end'];
            //$this->_receivedFilter['group']           = isset($filters['compare-group']) ? $filters['compare-group'] : null;
            $this->_receivedFilter['filter_structure']= isset($filters['compare-filter_structure']) ? $filters['compare-filter_structure'] : null;
            $this->_receivedFilter['anonymous']       = isset($filters['anonymous']) ? $filters['anonymous'] : null;
            $this->_receivedFilter['sel_action']      = isset($filters['sel_action']) ? $filters['sel_action'] : null;
            $this->_receivedFilter['alert']           = isset($filters['alert']) ? $filters['alert'] : null;
            $this->_receivedFilter['sel_kpi_filters'] = isset($filters['compare-sel_kpi_filters']) ? $filters['compare-sel_kpi_filters'] : null;
            $this->_receivedFilter['sel_kpi_filter']      = isset($filters['compare-sel_kpi_filter']) ? $filters['compare-sel_kpi_filter'] : null;
        } else {
            $this->_receivedFilter['periodStart']          = isset($filters['period-start']) ? $filters['period-start'] : null;
            $this->_receivedFilter['periodEnd']            = isset($filters['period-end']) ? $filters['period-end'] : null;
            $this->_receivedFilter['purchaseStart']        = isset($filters['purchase-start']) ? $filters['purchase-start'] : null;
            $this->_receivedFilter['purchaseEnd']          = isset($filters['purchase-end']) ? $filters['purchase-end'] : null;
            $this->_receivedFilter['filter_structure']     = isset($filters['filter_structure']) ? $filters['filter_structure'] : null;
            $this->_receivedFilter['alert']                = isset($filters['alert']) ? $filters['alert'] : null;
            $this->_receivedFilter['sel_action']           = isset($filters['sel_action']) ? $filters['sel_action'] : null;
            $this->_receivedFilter['anonymous']            = isset($filters['anonymous']) ? $filters['anonymous'] : null;
            $this->_receivedFilter['sel_kpi_filters']      = isset($filters['sel_kpi_filters']) ? $filters['sel_kpi_filters'] : null;
            $this->_receivedFilter['sel_kpi_filter']       = isset($filters['sel_kpi_filter']) ? $filters['sel_kpi_filter'] : null;
        }

        return $this->_receivedFilter;
    }

}