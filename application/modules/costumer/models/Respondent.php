<?php

class Costumer_Model_Respondent
{

    protected $date;

    protected $clubMemberId;
    
    protected $country;

    protected $customerCategory;

    protected $TypeOrder;
    
    protected $dateOfPurchase;
    
    protected $boutique;
    
    protected $gender;
    
    protected $name;
    
    protected $surname;
    
    protected $language;
    
    protected $email;
    
    protected $isAlert;
    
    protected $responses;


    public  function __construct() {

    }


    public function getMockArray(){
        $this->date = $this->getRandomDate();
        $this->clubMemberId = $this->getRandomInt();
        $this->country = $this->getRandomString(10);
        $this->customerCategory = $this->getRandomInt();
        $this->TypeOrder = $this->getRandomString(5);
        $this->dateOfPurchase = $this->getRandomDate();
        $this->boutique = $this->getRandomString(10);
        $this->gender = $this->getRandomBoolean();
        $this->name = $this->getRandomString(7);
        $this->surname = $this->getRandomString(7);
        $this->language = 'en';
        $this->email = $this->getRandomString(10);
        $this->isAlert = $this->getRandomBoolean();
        $this->responses = array($this->getRandomString(20) ,  $this->getRandomString(20) , $this->getRandomString(20), $this->getRandomString(20) , $this->getRandomString(20) ,  $this->getRandomString(20) , $this->getRandomString(20), $this->getRandomString(20));
        return $this->objectToArray();
    }

    public function getRandomDate(){
        $int= mt_rand(1262055681,1378515705);
        return date("m/d/Y",$int);
    }


    public function getRandomInt(){
        return rand(1,999999);
    }

    public function getRandomString( $length ) {
        $chars = array_merge(range('a', 'z'), range(0, 9));

        return implode(array_map(function($key) use ($chars) {
            return $chars[$key];
        }, array_rand($chars, $length)));

    }

    public function getRandomBoolean() {
        return .01 * rand(0, 100) >= .5;
    }

    public function objectToArray()
    {
        $array=array();
        foreach($this as $member=>$data)
        {
            $array[$member]=$data;
        }
        return $array;
    }

}

