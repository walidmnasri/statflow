<?php
require_once __DIR__ . '/../../statflow/services/XmlConfigSolr.php';
require_once __DIR__ . '/../../statflow/services/RespondentExternalField.php';
require_once __DIR__ . '/../../statflow/services/FiltersKpi.php';
require_once __DIR__ . '/../../statflow/services/RespondentFieldHidden.php';
require_once __DIR__ . '/../../statflow/services/FeedbackForm.php';
require_once __DIR__ . '/../../statflow/services/Question.php';
require_once __DIR__ . '/../../statflow/services/RespondentAction.php';
require_once __DIR__ . '/../../statflow/services/Study.php';
require_once __DIR__ . '/../../statflow/services/StudyUser.php';
require_once __DIR__ . '/../../statflow/services/Structure.php';
require_once __DIR__ . '/../../statflow/services/Questionnaire.php';
require_once __DIR__ . '/../../statflow/services/UserFrontItem.php';
require_once __DIR__.'/../services/List.php';
require_once __DIR__ . '/../../statflow/services/NominativeFieldHidden.php';

use Statflow\Service\XmlConfigSolr;
use Statflow\Service\RespondentExternalField;
use Statflow\Service\FiltersKpi;
use Statflow\Service\FeedbackForm;
use Statflow\Service\Question;
use Statflow\Service\RespondentAction;
use Statflow\Service\Study;
use Statflow\Service\StudyUser;
use Statflow\Service\Structure;
use Statflow\Service\Questionnaire;
use Statflow\Service\RespondentFieldHidden;
use Statflow\Service\UserFrontItem;
use Statflow\Service\NominativeFieldHidden;

class Costumer_ListController extends Centurion_Controller_Action
{

    /**
     *
     * @var Costumer_Model_Filters
     */
    protected $_service;
    /**
     *
     * @var Costumer_Model_Filters
     */
    protected $_feedbackf_form_id;
    
    /**
     *
     * @var Costumer_Service_List
     */
    protected $listService;    
    
    /**
     *
     * @var RespondentExternalField
     */
    private $respondentExternalFieldService;

    /**
     *
     * @var FiltersKpi
     */
    private $filtersKpiService;
    
    /**
     *
     * @var RespondentFieldHidden
     */
    private $respondentFieldHiddenService;
        

    /**
     *
     * @var NominativeFieldHidden
     */
    private $nominativeFieldHiddenService;
    
   /**
    *
    * @var studyService
    */
    private $studyService;
    
    /**
     * @var FeedbackForm
     */
    private $feedbackFormService;
    
    /**
     * @var Question
     */
    private $questionService;
    
    /**
     * @var RespondentAction
     */
    private $respondentActionService;
    
    /**
     *
     * @var StudyUserService
     */
    private $studyUserService;
    
    /**
     *
     * @var xmlConfigSolr
     */
    private $xmlConfigSolrService;
    
    /**
     *
     * @var Structure
     */
    private $structureService;  
      
    /**
     *
     * @var Questionnaire
     */
    private $questionnaireService;   

    /**
     *
     * @var UserFrontItem
     */
    private $userFrontItemService;
    
    /**
     *
     * @return \Statflow\Service\Study
     */   
    public function getStudyService()
    {
        if (! $this->studyService) {
            $this->studyService = new Study();
        }
    
        return $this->studyService;
    }
    
    /**
     *
     * @param \Statflow\Service\Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    } 
    
    /**
     *
     * @return \Statflow\Service\StudyUser
     */
    public function getStudyUserService()
    {
        if (! $this->studyUserService) {
            $this->studyUserService = new StudyUser();
        }
    
        return $this->studyUserService;
    }
    
    /**
     *
     * @param \Statflow\Service\StudyUser $studyUserService
     */
    public function setStudyUserService($studyUserService)
    {
        $this->studyUserService = $studyUserService;
    }
    /**
     *
     * @return \Statflow\Service\RespondentExternalField
     */
    public function getRespondentExternalFieldService()
    {
        if (! $this->respondentExternalFieldService) {
            $this->respondentExternalFieldService = new RespondentExternalField();
        }
    
        return $this->respondentExternalFieldService;
    }
    
    /**
     *
     * @param \Statflow\Service\RespondentExternalField $respondentExternalFieldService
     */
    public function setRespondentExternalFieldService($respondentExternalFieldService)
    {
        $this->respondentExternalFieldService = $respondentExternalFieldService;
    }

    /**
     *
     * @return \Statflow\Service\FiltersKpi
     */
    public function getFiltersKpiService()
    {
        if (! $this->filtersKpiService) {
            $this->filtersKpiService = new FiltersKpi();
        }
    
        return $this->filtersKpiService;
    }
    
    /**
     *
     * @param \Statflow\Service\FiltersKpi $filtersKpiService
     */
    public function setFiltersKpiService($filtersKpiService)
    {
        $this->filtersKpiService = $filtersKpiService;
    }

    /**
     *
     * @return \Statflow\Service\RespondentFieldHidden
     */
    public function getRespondentFieldHiddenService()
    {
        if (! $this->respondentFieldHiddenService) {
            $this->respondentFieldHiddenService = new RespondentFieldHidden();
        }
    
        return $this->respondentFieldHiddenService;
    }
    
    /**
     *
     * @param \Statflow\Service\RespondentFieldHidden $respondentFieldHiddenService
     */
    public function setRespondentFieldHiddenService($respondentFieldHiddenService)
    {
        $this->respondentFieldHiddenService = $respondentFieldHiddenService;
    }
        
    /**
     *
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (! $this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
        return $this->feedbackFormService;
    }
    
    /**
     *
     * @param \Statflow\Service\FeedbackForm $feedbackFormService
     */
    public function setFeedbackFormService($feedbackFormService)
    {
        $this->feedbackFormService = $feedbackFormService;
    }

    /**
     *
     * @param \Statflow\Service\Question $questionService
     */
    public function setQuestionService($questionService)
    {
        $this->questionService = $questionService;
    }
    
    /**
     *
     * @return \Statflow\Service\Question
     */
    public function getQuestionService()
    {
        if (! $this->questionService) {
            $this->questionService = new Question();
        }
    
        return $this->questionService;
    }

    /**
     *
     * @param \Statflow\Service\RespondentAction $respondentActionService
     */
    public function setRespondentActionService($respondentActionService)
    {
        $this->respondentActionService = $respondentActionService;
    }
    
    /**
     *
     * @return \Statflow\Service\Question
     */
    public function getRespondentActionService()
    {
        if (! $this->respondentActionService) {
            $this->respondentActionService = new RespondentAction();
        }
    
        return $this->respondentActionService;
    }
    
    /**
     *
     * @return Costumer_Service_List
     */
    public function getListService()
    {
        if (! $this->listService) {
            $this->listService = new Costumer_Service_List();
        }
    
        return $this->listService;
    }
    
    /**
     *
     * @return \Statflow\Service\XmlConfigSolr
     */
    public function getXmlConfigSolrService()
    {
        if (! $this->xmlConfigSolrService) {
            $this->xmlConfigSolrService = new XmlConfigSolr();
        }
    
        return $this->xmlConfigSolrService;
    }
    
    /**
     *
     * @param \Statflow\Service\XmlConfigSolr $xmlConfigSolrService
     */
    public function setXmlConfigSolrService($xmlConfigSolrService)
    {
        $this->xmlConfigSolrService = $xmlConfigSolrService;
    }


    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
    
        return $this->structureService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }
    
    /**
     *
     * @return \Statflow\Service\Questionnaire
     */
    public function getQuestionnaireService()
    {
        if (! $this->questionnaireService) {
            $this->questionnaireService = new Questionnaire();
        }
    
        return $this->questionnaireService;
    }
    
    /**
     *
     * @param \Statflow\Service\Questionnaire $questionnaireService
     */
    public function setQuestionnaireService($questionnaireService)
    {
        $this->questionnaireService = $questionnaireService;
    }

    /**
     *
     * @return \Statflow\Service\UserFrontItem
     */
    public function getUserFrontItemService()
    {
        if (! $this->userFrontItemService) {
            $this->userFrontItemService = new UserFrontItem();
        }
    
        return $this->userFrontItemService;
    }
    
    /**
     *
     * @param \Statflow\Service\UserFrontItem $userFrontItemService
     */
    public function setUserFrontItemService($userFrontItemService)
    {
        $this->userFrontItemService = $userFrontItemService;
    }
    
    
    /**
     * @return the $nominativeFieldHiddenService
     */
    public function getNominativeFieldHiddenService()
    {
        if (! $this->nominativeFieldHiddenService) {
            $this->nominativeFieldHiddenService = new NominativeFieldHidden();
        }
        return $this->nominativeFieldHiddenService;
    }
    
    /**
     * @param NominativeFieldHidden $nominativeFieldHiddenService
     */
    public function setNominativeFieldHiddenService($nominativeFieldHiddenService)
    {
        $this->nominativeFieldHiddenService = $nominativeFieldHiddenService;
    }
    
    /**
     *
     * @var Costumer_Service_PostBoutiqueMail
     */
    protected $_postBoutiqueMailService;
    
    public function preDispatch()
    {
        $this->_helper->layout->setLayout('costumer');
        if(! $this->_getParam('formId')) {
            return $this->_redirect('/');
        }
        $this->view->pageLayout = 'respondent';
        $studyUserService     = $this->getStudyUserService();
        $studyService         = $this->getStudyService();
        $userFrontItemService = $this->getUserFrontItemService();
        $filtersKpiService = $this->getFiltersKpiService();
        $respondentExternalFieldService = $this->getRespondentExternalFieldService();

        if($this->getRequest()->isXmlHttpRequest() && !Zend_Auth::getInstance()->hasIdentity()) {
            echo 'reconnect';
            die();
        }
        else {
            $this->_helper->authCheck();
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
        $this->view->user = $user_name;
        $study_id = $studyUserService->getFirstStudyByUser($user_id)->study_id;
        $study = $studyService->getById($study_id);
        $ecranGlobal = Zend_Json::decode($study->ecran_global);
        $this->view->displayGlobal = $ecranGlobal['is_published_global'];

        $itemsToHide = $userFrontItemService->getFrontItemIDByUserId($user_id);
        $this->view->studyService = $study;
        $this->view->hide_kpi = true;
        $this->view->hide_tdr = true;
        $this->view->hide_glo = true;
        if (count($itemsToHide) > 0) {
            foreach ($itemsToHide as $item){
                if ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_KPI){
                    $this->view->hide_kpi = false;
                } elseif ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_TDR){
                    $this->view->hide_tdr = false;
                } elseif ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_GLO){
                    $this->view->hide_glo = false;
                }
        
            }
        }

        //retrieve list of filters associated
        $kpi_filters = $filtersKpiService->getAllFiltersByFeedbackFormId($this->_getParam('formId'));
        $this->view->kpi_filters = $kpi_filters;
        foreach ($kpi_filters as $external_field) {
            $selectField["external_fields" . $external_field->import_field_id] = $respondentExternalFieldService->getValuesOfExternalField($external_field->import_field_id);
        }

        $this->view->selectField = $selectField ;
    }
    
    public function indexAction()
    {
        set_time_limit(0);
        $this->view->page = 'respondent';
        $listService                    = $this->getListService();
        $studyUserService               = $this->getStudyUserService();
        $respondentExternalFieldService = $this->getRespondentExternalFieldService();        
        $filtersKpiService              = $this->getFiltersKpiService();

        $feedbackFormService            = $this->getFeedbackFormService();
        $structureService               = $this->getStructureService();
        $conf_solr                      = $this->getXmlConfigSolrService();
        $respondentFieldHiddenService   = $this->getRespondentFieldHiddenService();
        $nominativeFieldHiddenService   = $this->getNominativeFieldHiddenService();
        $userFrontItemService           = $this->getUserFrontItemService();

        $user_id = 0;
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
        }

        $feedback_form_id=0;

        $this->view->questionnaire = $studyUserService->getAllFeedbackFormByUserId($user_id) ;
        if (!empty($this->view->questionnaire)) {
            if (!$this->_getParam('formId')) {
                foreach ($this->view->questionnaire as $element) {
                    $formId0 = $element['id'];break;
                }
                $this->view->form_actif = $formId0;
                $feedback_form_id =$formId0;
            } else {
                $this->view->form_actif = $this->_getParam('formId');
                $feedback_form_id = $this->_getParam('formId',0);
            }
            $form = $feedbackFormService->getById($feedback_form_id);
            $this->view->feedback = $form;
        }

        $itemsToHide = $userFrontItemService->getFrontItemIDByUserId($user_id);
        
        $this->view->hide_export = true;
        if (count($itemsToHide) > 0) {
            foreach ($itemsToHide as $item) {
                if ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_EXP) {
                    $this->view->hide_export = false;
                }
            }
        }       
        
        //kpi filter
        $kpi_filters = $filtersKpiService->getAllFiltersByFeedbackFormId( $feedback_form_id);
        $this->view->kpi_filters = $kpi_filters;
        foreach($kpi_filters as $external_field){
            $selectField = "external_fields".$external_field->import_field_id;
            $this->view->$selectField = $respondentExternalFieldService->getValuesOfExternalField($external_field->import_field_id);
        }
        
        //filter
        $this->_service = new Costumer_Model_Filters();
        $this->_service->setReceivedFilters($this->_getAllParams(), false);
        $params = $this->_service->getReceivedFilters();

        
        // return header
        $requete = $conf_solr->generateSqlCode($this->_getParam("formId"));
        $this->view->header = $requete['header'];
        
        //hide anonymous respondantt
        $anonymousCondition = "";
        $anonymousCondition = $feedbackFormService->getAnonymousFilter($feedback_form_id);
        if (isset($anonymousCondition) && !empty($anonymousCondition)){
            $filter['anonymous'] = $anonymousCondition;
            $params = array_merge($params, $filter);
        }

        $studyService = $this->getStudyService();
        $studyMeta= $studyService->getFirstMetaByUserId($user_id);
        if ($studyMeta) {
            $meta = Zend_Json::decode($studyMeta);
            $this->view->formStyle = $meta['style'];
        }


        $all_structures     = Zend_Auth::getInstance()->getIdentity()->structures->current()['external_id'];
        $structures_user    = $structureService->getAllStructureOnlyByUser($user_id);
        $this->view->structure 		    = $all_structures;
        $this->view->form               = $feedback_form_id;

        $header_ignore = $respondentFieldHiddenService->getRespondentFieldHiddenByFeedbackFormId($feedback_form_id);

        // Donnee nominatives
        $nominative_ignore = NULL;
        $nominative_check = NULL;
        
        $nominative_question = $feedbackFormService->getNominativeQuestionByFeedbackFormId($feedback_form_id);
        $nominative_modality = $feedbackFormService->getNominativeModalityByFeedbackFormId($feedback_form_id);
        
        if(!empty($nominative_question)) {
            $nominative_ignore = $nominativeFieldHiddenService->getNominativeFieldHiddenByFeedbackFormId($feedback_form_id);
            $nominative_check  = $nominative_question.$nominative_modality;
        }
        
        $this->view->header_ignore = $header_ignore;
        $study_id = $studyUserService->getFirstStudyByUser($user_id);
        $this->view->study = 'study_'.$study_id['study_id'];
        $delai_traitement = $feedbackFormService->getById($feedback_form_id)->process_time;
        $structureActive = null;
        if($this->_getParam('filter_structure'))
            $structureActive = $this->_getParam('filter_structure');
        $this->view->filter_structure = $structureService->getFilterStructure($user_id, $structureActive);
    }

    public function displayAction()
    {
        $this->view->pageLayout = 'respondent';
        $this->view->page = "detail";
        $storage = (array)$_SESSION['Zend_Auth']['storage'];
        $user_id = $storage['id'];

        $studyUserService               = $this->getStudyUserService();
        $respondentActionService        = $this->getRespondentActionService();
        $questionnaireService           = $this->getQuestionnaireService();
        $listService                    = $this->getListService();
        $conf_solr                      = $this->getXmlConfigSolrService();
        $respondentFieldHiddenService   = $this->getRespondentFieldHiddenService();
        $filtersKpiService              = $this->getFiltersKpiService();
        $nominativeFieldHiddenService   = $this->getNominativeFieldHiddenService();
        $feedbackFormService            = $this->getFeedbackFormService();
        
        $feedback_form_id = $this->_getParam("formId");
        $respondent_id    = $this->_getParam('respondent_id');
        $this->view->form_actif = $feedback_form_id;
        $this->view->questionnaire = $studyUserService->getAllFeedbackFormByUserId($user_id) ;
        //recuperer les donnees du repondent par solr
        $solr =  new Costumer_Service_Solr(null, $feedback_form_id);
        $data  = $solr->getData(null, null, null, $respondent_id);
        $respondentData = $data->getDocuments()[0];

        //recuper les headers
        $requete = $conf_solr->generateSqlCode($this->_getParam("formId"));
        $headers = $requete['header'];
        
        //les champs a ignorer
        $header_ignore = $respondentFieldHiddenService->getRespondentFieldHiddenByFeedbackFormId($feedback_form_id);
        
        // Donnee nominatives
        $nominative_ignore = NULL;
        $nominative_check = NULL;
        
        $nominative_question = $feedbackFormService->getNominativeQuestionByFeedbackFormId($feedback_form_id);
        $nominative_modality = $feedbackFormService->getNominativeModalityByFeedbackFormId($feedback_form_id);
        
        if(!empty($nominative_question)) {
            $nominative_ignore = $nominativeFieldHiddenService->getNominativeFieldHiddenByFeedbackFormId($feedback_form_id);
            $nominative_check = $nominative_question.$nominative_modality;
        }
        
        //obtenir study associe a l'utilisateur(dans notre cas ce sera un client, donc une seule etude)
        $studyUserService = $this->getStudyUserService();
        
        $study_id = $studyUserService->getFirstStudyByUser($user_id);
        $this->view->study = 'study_'.$study_id['study_id']; //"glafayette";//
        
        
        //Donnees CRM
        $externalFields =  $filtersKpiService->getAllExternalFieldByFeedbackFormId($feedback_form_id);
        $header_infos = $respondentFieldHiddenService->getRespondentInfos($headers, array());
        $header_external = $respondentFieldHiddenService->getRespondentExternalFields($externalFields);
        
        $respondent_details = $listService->getRespondantDetailFicheClient($respondentData, $headers, $header_ignore, $feedback_form_id, 'study_'.$study_id['study_id'],  $nominative_check, $nominative_ignore);
        $this->view->respondent_details = $respondent_details;
       
        //Champs externes
        $external_label = '';
        $this->view->external_label = $questionnaireService->getExternalLabel($feedback_form_id);
        $this->view->respondent_infos = $header_infos;
        $this->view->respondent_external = $header_external;
        
        
        //Questionnaire(question-answer)
        $this->view->detailQuestionnaire = $listService->getDataRespondantFicheClient($respondentData, $headers, $header_ignore,  $feedback_form_id, 'study_'.$study_id['study_id'], $nominative_check, $nominative_ignore);
    
        //Suivi action
        $this->view->respondentId = $this->_getParam("respondent_id");

        $this->view->form = $feedback_form_id;
        $form = $feedbackFormService->getById($feedback_form_id);
        $this->view->feedback = $form;
        
        $respondent_actions = $respondentActionService->getRespondentActionByRespondentId($respondent_id);
        $current_action = $respondentActionService->getCurrentAction($respondent_id);
        
        $this->view->respondent_actions = $respondent_actions;
        $this->view->current_action = $current_action;
        $this->view->alert = $respondentData->alert;
        
        //mise a jour solr
        $solrConfig = Statflow_Model_Solr::getDefaultConfig();
        $solrConfig = $solrConfig['endpoint']['localhost'];       
        file_get_contents('http://' . $solrConfig['host'] . ':' . $solrConfig['port'] . $solrConfig['path'] . $feedback_form_id . '/dataimport?command=delta-import&synchronous=true&respondent_id='.$respondent_id);
                
    }

    public function exportAction()
    {
        $conf_solr           = $this->getXmlConfigSolrService();
        $listService         = $this->getListService();
        $studyUserService    = $this->getStudyUserService();
        $studyService        = $this->getStudyService();
        $feedbackFormService = $this->getFeedbackFormService();
        $nominativeFieldHiddenService   = $this->getNominativeFieldHiddenService();
        $respondentFieldHiddenService   = $this->getRespondentFieldHiddenService();
        
        $user_id=0;
    
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
        }
    
        set_time_limit(0);
    
        ini_set('memory_limit', '2048M');
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    
        $this->_service = new Costumer_Model_Filters();
        $this->_service->setReceivedFilters($this->_getAllParams(), false);
        $params = $this->_service->getReceivedFilters();
    
        $feedback_form_id = $this->_getParam("formId");
        $requete = $conf_solr->generateSqlCode($feedback_form_id);
        
        //hide anonymous respondantt
        $anonymousCondition = "";
        $anonymousCondition = $feedbackFormService->getAnonymousFilter($feedback_form_id);
        if (isset($anonymousCondition) && !empty($anonymousCondition)){
            $filter['anonymous'] = $anonymousCondition;
            $params = array_merge($params, $filter);
        }
         
        //data respondents
        $service = new Costumer_Service_Solr($params,$feedback_form_id);
        $respondants  = $service->getData('answer_date desc')->getDocuments();
 
        //les champs a ignorer
        $headers_ignore = $respondentFieldHiddenService->getRespondentFieldHiddenByFeedbackFormId($feedback_form_id);

        $study_id = $studyUserService->getFirstStudyByUser($user_id);
        $study = $studyService->getById($study_id['study_id']);
        
        // Donnee nominatives
        $nominative_ignore = NULL;
        $nominative_check = NULL;
        
        $nominative_question = $feedbackFormService->getNominativeQuestionByFeedbackFormId($feedback_form_id);
        $nominative_modality = $feedbackFormService->getNominativeModalityByFeedbackFormId($feedback_form_id);
        
        if(!empty($nominative_question)) {
            $nominative_ignore = $nominativeFieldHiddenService->getNominativeFieldHiddenByFeedbackFormId($feedback_form_id);
            $nominative_check = $nominative_question.$nominative_modality;
        }

        $this->listService = new Costumer_Service_List();
        $this->listService->exportListExcel($study_id['study_id'], $feedback_form_id, $requete['header'], $headers_ignore, $respondants, $study->name, $nominative_check, $nominative_ignore);
        
    }    
        
    public function commentAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $respondentActionService = $this->getRespondentActionService();
        $xmlConfigSolrService = $this->getXmlConfigSolrService();
        try {
            $respondent_id = $this->_getParam('respondent_id');
            $feedback_form_id = $this->_getParam('formId');
            $action = $_POST['sel_action'];
            $comment = $_POST['txa_comment'];
            
            $respondentActionService->saveRespondentAction($respondent_id, $feedback_form_id, $action, $comment);
            $respondentActionService->updateRespondentAction($respondent_id, $action);
            
            //mise a jour solr
            $solrConfig = Statflow_Model_Solr::getDefaultConfig();
            $solrConfig = $solrConfig['endpoint']['localhost'];
            file_get_contents('http://' . $solrConfig['host'] . ':' . $solrConfig['port'] . $solrConfig['path'] . $feedback_form_id . '/dataimport?command=delta-import&synchronous=true&respondent_id='.$respondent_id);

            print_r(Zend_Json::encode($_POST));
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }
    
    public function loadAction(){
        
        $listService                    = $this->getListService();
        $studyUserService               = $this->getStudyUserService();
        $feedbackFormService            = $this->getFeedbackFormService();
        $conf_solr                      = $this->getXmlConfigSolrService();
        $respondentFieldHiddenService   = $this->getRespondentFieldHiddenService();
        $nominativeFieldHiddenService   = $this->getNominativeFieldHiddenService();
        
        
        $feedback_form_id = $this->_getParam('formId',0);
        $page = $this->_getParam('page', null);
        
        $form = $feedbackFormService->getById($feedback_form_id);
        $this->view->feedback = $form;
        
        //filter
        $this->_service = new Costumer_Model_Filters();
        $this->_service->setReceivedFilters($this->_getAllParams(), false);
        $params = $this->_service->getReceivedFilters();
        
        //hide anonymous respondantt
        $anonymousCondition = "";
        $anonymousCondition = $feedbackFormService->getAnonymousFilter($feedback_form_id);
        if (isset($anonymousCondition) && !empty($anonymousCondition)){
            $filter['anonymous'] = $anonymousCondition;
            $params = array_merge($params, $filter);
        }
        
        //solr request
        $service = new Costumer_Service_Solr($params, $feedback_form_id);
        $user_id=0;
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
        }
        
        $header_ignore = $respondentFieldHiddenService->getRespondentFieldHiddenByFeedbackFormId($feedback_form_id);
        // Donnee nominatives
        $nominative_ignore = NULL;
        $nominative_check = NULL;
        
        $nominative_question = $feedbackFormService->getNominativeQuestionByFeedbackFormId($feedback_form_id);
        $nominative_modality = $feedbackFormService->getNominativeModalityByFeedbackFormId($feedback_form_id);
        
        if(!empty($nominative_question)) {
            $nominative_ignore = $nominativeFieldHiddenService->getNominativeFieldHiddenByFeedbackFormId($feedback_form_id);
            $nominative_check = $nominative_question.$nominative_modality;
        }
        
        
        $delai_traitement = $feedbackFormService->getById($feedback_form_id)->process_time;
        $study_id = $studyUserService->getFirstStudyByUser($user_id);
        $total = $service->getData()->getNumFound();
        $totalAlert = count($service->getDataAlert()->getDocuments());
        $totalTreated = count($service->getDataTreated()->getDocuments());
        $start = 0;
        if (isset($page) && $page) {
            $start = ($page-1)*10;
        }

        $respondants  = $service->getData('answer_date desc', $start, '10')->getDocuments();
        $requete = $conf_solr->generateSqlCode($this->_getParam("formId"));
        $dataRespondent = $listService->getDataRespondant($respondants, $requete['header'], $header_ignore, 'study_'.$study_id['study_id'], $feedback_form_id, $delai_traitement, $nominative_check, $nominative_ignore);
        header('Content-Type: application/json');
        print_r(Zend_Json::encode([
            'data'=> $dataRespondent,
            'total' => $total,
            'totalAlert' => $totalAlert,
            'totalTreated' => $totalTreated
        ]));die;
  }
}

