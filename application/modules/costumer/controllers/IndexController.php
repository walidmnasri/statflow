<?php
require_once __DIR__ . '/../../statflow/services/KpiManagement.php';
require_once __DIR__ . '/../../statflow/services/FiltersKpi.php';
require_once __DIR__ . '/../../statflow/services/RespondentExternalField.php';
require_once __DIR__ . '/../../statflow/services/Study.php';
require_once __DIR__ . '/../../statflow/services/StudyUser.php';
require_once __DIR__ . '/../../statflow/services/Structure.php';
require_once __DIR__ . '/../../statflow/services/FeedbackForm.php';
require_once __DIR__ . '/../services/GlobalService.php';
require_once __DIR__ . '/../../statflow/services/UserFrontItem.php';

use Statflow\Service\KpiManagement;
use Statflow\Service\FiltersKpi;
use Statflow\Service\RespondentExternalField;
use Statflow\Service\Study;
use Statflow\Service\StudyUser;
use Statflow\Service\Structure;
use Statflow\Service\FeedbackForm;
use Statflow\Service\UserFrontItem;


class Costumer_IndexController extends Centurion_Controller_Action
{
    
    /**
     * @var Costumer_Service_Graph
     */
    protected $_service;
    /**
     * @var Statflow_StudyUser
     */
    protected $studyUserservice;

    /**
     *
     * @var FiltersKpi
     */
    private $filtersKpiService;
    
    /**
     *
     * @var RespondentExternalField
     */
    private $respondentExternalFieldService;
    
    /**
     *
     * @var studyService
     */
    private $studyService;

    /**
     *
     * @var Structure
     */
    private $structureService;
    
    /**
     *
     * @var FeedbackForm
     */
    private $feedbackFormService;
    
    /**
     *
     * @var UserFrontItem
     */
    private $userFrontItemService;
    
    /**
     *
     * @return \Statflow\Service\StudyUser
     */
    public function getFeedbackForm()
    {
        if (! $this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
    
        return $this->feedbackFormService;
    }
    
    /**
     *
     * @return \Statflow\Service\StudyUser
     */
    public function getStudyUserService()
    {
        if (! $this->studyUserservice) {
            $this->studyUserservice = new StudyUser();
        }
    
        return $this->studyUserservice;
    }
    
    
    /**
     *
     * @param \Statflow\Service\StudyUser $studyUserService
     */
    public function setStudyUserService($studyUserService)
    {
        $this->studyUserservice = $studyUserService;
    }
    
    /**
     *
     * @return \Statflow\Service\Study
     */
    public function getStudyService()
    {
        if (! $this->studyService) {
            $this->studyService = new Study();
        }
    
        return $this->studyService;
    }
    
    /**
     *
     * @param \Statflow\Service\Study $studyService
     */
    public function setStudyService($studyService)
    {
        $this->studyService = $studyService;
    }
        
    /**
     *
     * @return \Statflow\Service\FiltersKpi
     */
    public function getFiltersKpiService()
    {
        if (! $this->filtersKpiService) {
            $this->filtersKpiService = new FiltersKpi();
        }
    
        return $this->filtersKpiService;
    }
    
    /**
     *
     * @param \Statflow\Service\FiltersKpi $filtersKpiService
     */
    public function setFiltersKpiService($filtersKpiService)
    {
        $this->filtersKpiService = $filtersKpiService;
    }    
    
    /**
     *
     * @return \Statflow\Service\RespondentExternalField
     */
    public function getRespondentExternalFieldService()
    {
        if (! $this->respondentExternalFieldService) {
            $this->respondentExternalFieldService = new RespondentExternalField();
        }
    
        return $this->respondentExternalFieldService;
    }
    
    /**
     *
     * @param \Statflow\Service\RespondentExternalField $respondentExternalFieldService
     */
    public function setRespondentExternalFieldService($respondentExternalFieldService)
    {
        $this->respondentExternalFieldService = $respondentExternalFieldService;
    }
    
    /**
     *
     * @return \Statflow\Service\Structure
     */
    public function getStructureService()
    {
        if (! $this->structureService) {
            $this->structureService = new Structure();
        }
    
        return $this->structureService;
    }
    
    /**
     *
     * @param \Statflow\Service\Structure $structureService
     */
    public function setStructureService($structureService)
    {
        $this->structureService = $structureService;
    }
    
    /**
     *
     * @return \Statflow\Service\UserFrontItem
     */
    public function getUserFrontItemService()
    {
        if (! $this->userFrontItemService) {
            $this->userFrontItemService = new UserFrontItem();
        }
    
        return $this->userFrontItemService;
    }
    
    /**
     *
     * @param \Statflow\Service\UserFrontItem $userFrontItemService
     */
    public function setUserFrontItemService($userFrontItemService)
    {
        $this->userFrontItemService = $userFrontItemService;
    }
       
    public function init()
    {
        $this->_service = new Costumer_Service_Graph();
        $this->view->pageLayout = 'dashboard';
    }
    
    public function preDispatch()
    {
        $this->_helper->layout->setLayout('costumer');
        $studyService = $this->getStudyService();
        $userFrontItemService = $this->getUserFrontItemService();
        $filtersKpiService = $this->getFiltersKpiService();
        $respondentExternalFieldService = $this->getRespondentExternalFieldService();
        $studyUserService = $this->getStudyUserService();
        $feedbackFormService = $this->getFeedbackForm();
        $kpi_management_service  = new KpiManagement();
        $structureService = $this->getStructureService();

        if($this->getRequest()->isXmlHttpRequest() && !Zend_Auth::getInstance()->hasIdentity()) {
            echo 'reconnect';
            die();
        } else {
            $this->_helper->authCheck();
            $storage = (array) $_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
        $this->view->user = $user_name;

        $study_id = $studyUserService->getFirstStudyByUser($user_id)->study_id;
        $study = $studyService->getById($study_id);
        //$ecranGlobal = Zend_Json::decode($study->ecran_global);
        //$this->view->displayGlobal = $ecranGlobal['is_published_global'];
        
        $itemsToHide = $userFrontItemService->getFrontItemIDByUserId($user_id);
        $this->view->studyService = $study;
        $this->view->hide_kpi = true;
        $this->view->hide_tdr = true;
        $this->view->hide_glo = true;
        if (count($itemsToHide) > 0) {
            foreach ($itemsToHide as $item){
                if ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_KPI){
                    $this->view->hide_kpi = false;
                } elseif ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_TDR){
                    $this->view->hide_tdr = false;
                } elseif ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_GLO){
                    $this->view->hide_glo = false;
                }
        
            }
        }

        $this->view->questionnaire = $studyUserService->getAllFeedbackFormByUserId($user_id);
        $feedback_form_id = 0;
        $defaultKPI = null;
        if (!empty($this->view->questionnaire)) {
            if (!$this->_getParam('formId')) {
                foreach ($this->view->questionnaire as $element) {
                    $formId0 = $element['id'];
                    $defaultKPI = $kpi_management_service->getAllKpiByFeedbackForm($formId0,'is_open = 1')->toArray();
                    break;
                }
                if(!empty($defaultKPI)) {
                    $this->view->default_kpi   = $defaultKPI[0]['type']  .':'. $defaultKPI[0]['question'].':'.$defaultKPI[0]['id'];                }
                $feedback_form_id          =$formId0;
                $this->view->form_actif    = $feedback_form_id;

            } else {
                $feedback_form_id           = $this->_getParam('formId',0);
                $defaultKPI                 = $kpi_management_service->getAllKpiByFeedbackForm($feedback_form_id,'is_open = 1')->toArray();
                $this->view->form_actif     = $feedback_form_id;

                if(!empty($defaultKPI))
                    $this->view->default_kpi    = $defaultKPI[0]['type']  .':'. $defaultKPI[0]['question'].':'.$defaultKPI[0]['id'];
            }
            $form = $feedbackFormService->getById($feedback_form_id);
            $this->view->feedback = $form;
        } else {
            $this->view->errors = $this->view->translate('you_dont_have_attached_servey@frontend');
            return;
        }

        if (in_array(\Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_KPI, $itemsToHide) && in_array(\Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_TDR, $itemsToHide)) {
            if (!in_array(\Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_GLO, $itemsToHide)){
                $this->_redirect('/costumer/index/global/formId/' . $feedback_form_id);
            }
        }

        if (in_array(\Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_KPI, $itemsToHide) ) {
            $this->_redirect('/costumer/list/index/formId/' . $feedback_form_id);
        }


        //retrieve list of filters associated
        $kpi_filters = $filtersKpiService->getAllFiltersByFeedbackFormId($feedback_form_id);
        $this->view->kpi_filters = $kpi_filters;
        $this->view->kpis = $kpi_management_service->getAllKpiByFeedbackForm($feedback_form_id,'is_open = 1')->toArray();
        foreach ($kpi_filters as $external_field) {
            $selectField["external_fields" . $external_field->import_field_id] = $respondentExternalFieldService->getValuesOfExternalField($external_field->import_field_id);
        }

        $this->view->selectField = $selectField ;
        $this->view->filter_structure = $structureService->getFilterStructure($user_id);
    }

    public function indexAction()
    {
        if (isset($this->view->errors) && $this->view->errors) {
            return;
        }
        $this->view->pageLayout = 'dashboard';
        try {
            $service = new Costumer_Service_Solr(array(), $this->view->form_actif);
            $this->view->totalRespondents = $service->getData()->getNumFound();;
            $this->view->totalAlerts = count($service->getDataAlert()->getDocuments());
            $this->view->totalTreated  = count($service->getDataTreated()->getDocuments());
            $this->view->totalNoTreated = count($service->getDataAlert()->getDocuments()) - count($service->getDataTreated()->getDocuments());

        } catch (Exception $e) {
            $this->view->errors = "Solr Error > " . $e->getMessage();
        }

    }
    
    public function mainAction()
    {
        if (isset($this->view->errors) && $this->view->errors) {
            return;
        }
        $this->view->pageLayout = 'kpi';
        $userFrontItemService = $this->getUserFrontItemService();

        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
        //$this->_helper->authCheck();
        //$this->_helper->aclCheck();
        
        $itemsToHide = $userFrontItemService->getFrontItemIDByUserId($user_id);

        if (count($itemsToHide) > 0){
            foreach ($itemsToHide as $item){
                if ($item == \Statflow_Model_DbTable_Row_FrontItem::FRONT_ITEM_KPI){
                    $this->view->page='respondent';
                }
            }
        }

        $this->view->user = $user_name;
        $this->view->page='kpi';
        $studyService = $this->getStudyService();
        $studyMeta= $studyService->getFirstMetaByUserId($user_id);
        if ($studyMeta) {
            $meta = Zend_Json::decode($studyMeta);
            $this->view->formStyle = $meta['style'];
        }

        //obtenir study associe a l'utilisateur(dans notre cas ce sera un client, donc une seule etude)
        $studyUserService = $this->getStudyUserService();
        $study_id = $studyUserService->getFirstStudyByUser($user_id);
        $this->view->study = 'study_'.$study_id['study_id']; 
    }
    
    public function globalAction()
    {
        if (isset($this->view->errors) && $this->view->errors) {
            return;
        }
        $this->pageLayout = 'global';
        //appel des services
        $studyUserService = $this->getStudyUserService();
        $filtersKpiService = $this->getFiltersKpiService();
        $respondentExternalFieldService = $this->getRespondentExternalFieldService();
        $structureService = $this->getStructureService();
        $feedbackFormService = $this->getFeedbackForm();
        $studyService = $this->getStudyService();
        
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
        
        $questionnaires = $studyUserService->getAllFeedbackFormByUserId($user_id) ;
        $this->view->questionnaire = $questionnaires;
        $global = 0;
        $this->view->user = $user_name;
        $this->view->form_actif    = $global;
        $study_id = $studyUserService->getFirstStudyByUser($user_id)->study_id;
        $study = $studyService->getById($study_id);
        $ecranGlobal = Zend_Json::decode($study->ecran_global);
        
        if (!empty($ecranGlobal['sel_filters_kpi'])) {
            $kpi_filters = $filtersKpiService->getDetailsFilterSelected($ecranGlobal['sel_filters_kpi']);
            $this->view->kpi_filters = $kpi_filters;
            foreach ($kpi_filters as $external_field) {
                $selectField = "external_fields".$external_field->id;
                $this->view->$selectField = $respondentExternalFieldService->getValuesOfExternalField($external_field->id);
            }
        }
        
        $this->view->filter_structure = $structureService->getFilterStructure($user_id);
        

        $this->view->study = 'study_'.$study_id;
        // set style
        $studyMeta= $studyService->getFirstMetaByUserId($user_id);
        if ($studyMeta) {
            $meta = Zend_Json::decode($studyMeta);
            $this->view->formStyle = $meta['style'];
        }
    }
    
    public function getkpiAction()
    {
        $globalService = new Costumer_Service_Global();
        $studyUserService = $this->getStudyUserService();
        $typeKpi  = $this->getRequest()->getPost('type', null);  
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
        else {
            //$this->_helper->authCheck();
            //$this->_helper->aclCheck();
        }
        $studyId = $studyUserService->getFirstStudyByUser($user_id)->study_id;
        $study =  'study_'.$studyId;
        $kpiByTypeByStudy = $globalService->GetAllFeedbackAndKpiByTypeAndStudy($typeKpi, $studyId, $user_id);
        print_r(Zend_Json::encode($kpiByTypeByStudy));
        die;
    }
    
    public function getQuestionAction()
    {
        $globalService = new Costumer_Service_Global();
        $studyUserService = $this->getStudyUserService();
        $Kpis  = $this->getRequest()->getPost('kpi', null);
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
        $studyId = $studyUserService->getFirstStudyByUser($user_id)->study_id;
        $study =  'study_'.$studyId;
        print_r(Zend_Json::encode($globalService->getQuestionBykpiStatDetail($Kpis)));
        die;
    }
    
    public function getGraphAction()
    {
        $globalService = new Costumer_Service_Global();
        $studyUserService = $this->getStudyUserService();
        $studyService = $this->getStudyService();
        $Kpi  = $this->getRequest()->getPost();
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
        }
         $studyId = $studyUserService->getFirstStudyByUser($user_id)->study_id;
         
         $arrayResult = [];
         $params = $this->getRequest()->getParams();
          if ($Kpi['kpiType'] == KpiManagement::KPI_SATGLO ) {
              $arrayResult = $globalService->getOverAllResult($Kpi, $studyId, $params);
          }
          elseif ($Kpi['kpiType'] == KpiManagement::KPI_SATDET) {
              $arrayResult = $globalService->getDetailedResult($Kpi, $studyId, $params);
          }
          elseif ($Kpi['kpiType'] == KpiManagement::KPI_NPSGLO) {
              $arrayResult = $globalService->getNpsGloResult($Kpi, $studyId, $params);
          }
          elseif ($Kpi['kpiType'] == KpiManagement::KPI_EXPGLO) {
              $arrayResult = $globalService->getExpGloResult($Kpi, $studyId, $params);
          }
          else {
              $arrayResult =  ['error' => true,
                  'msg' => $this->view->translate('study_'.$studyId.'_invalid-typeKpi-msg@study_'.$studyId)
              ];
          }
    
         print_r(Zend_Json::encode($arrayResult));die;
    }
}

