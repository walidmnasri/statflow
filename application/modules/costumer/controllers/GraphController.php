<?php
require_once __DIR__ . '/../../statflow/services/KpiManagement.php';
require_once __DIR__ . '/../../statflow/services/FeedbackForm.php';

use Statflow\Service\KpiManagement;
use Statflow\Service\FeedbackForm;
class Costumer_GraphController extends Centurion_Controller_Action
{
    /**
     * @var Costumer_Service_Graph
     */
    protected $_service;
    
    /**
     * @var Costumer_Service_Solr
     */
    protected $_solService;
    
    /**
     * @var Costumer_Service_Solr
     */
    protected $_feedback_form_id;
    
    /**
     *
     * @var feedbackForm
     */
    private $feedbackFormService;
    
    /**
     *
     * @var kpiManagement
     */
    private $kpi_management;
    
    /**
     *
     * @return \Statflow\Service\KpiManagement
     */
    public function getKpiManagement()
    {
        if (! $this->kpi_management) {
            $this->kpi_management = new KpiManagement();
        }
    
        return $this->kpi_management;
    }
    
    /**
     *
     * @param \Statflow\Service\KpiManagement $kpiManagement
     */
    public function setKpiManagement($kpiManagement)
    {
        $this->kpi_management = $kpiManagement;
    }
    
    /**
     *
     * @return \Statflow\Service\KpiManagement
     */
    public function getFeedbackFormId()
    {
        return $this->_feedback_form_id;
    }
    
    /**
     *
     * @param \Statflow\Service\KpiManagement $kpiManagement
     */
    public function setFeedbackFormId($value)
    {
        $this->_feedback_form_id = $value;
    }
    
    /**
     *
     * @return \Statflow\Service\FeedbackForm
     */
    public function getFeedbackFormService()
    {
        if (! $this->feedbackFormService) {
            $this->feedbackFormService = new FeedbackForm();
        }
    
        return $this->feedbackFormService;
    }
    
    public function preDispatch()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $feedbackService = $this->getFeedbackFormService();
        if($this->getRequest()->isXmlHttpRequest() && !Zend_Auth::getInstance()->hasIdentity()) {
            echo 'reconnect';
            die();
        }
        else if(!$this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->authCheck();
        }
        
        $this->_service = new Costumer_Service_Graph();

        $filterArray = $this->_service->getFilters()->setReceivedFilters($this->getRequest()->getParams() , $this->_getParam('compare', false));
        //TODO load Responses
        $respondent = new Costumer_Model_Respondent();
        $this->_service->setResponses(array($respondent->getMockArray(), $respondent->getMockArray(), $respondent->getMockArray(), $respondent->getMockArray()));

         $this->view->form_actif = $this->_getParam('formid');
         $form = $feedbackService->getById($this->_getParam('formid'));
         if(!$form->field_label) {
             echo "<style>
                    .purchase_{$form->id} {display:none}
                    .purchase_compare_{$form->id} {display:none}
                  </style>";
         }
    }

    public function overallSatisfactionAction()
    {
        $this->setFeedbackFormId($this->_getParam('formid'));
        $feedback_id = $this->getFeedbackFormId();
        $kpi = $this->_getParam('kpi');
        $this->_solService = new Costumer_Service_Solr($this->_service->getFilters()->getReceivedFilters(),$feedback_id);
        $kpi_management     = $this->getKpiManagement();
        $question_id = $this->_getParam('base');
        $code_question = $kpi_management->getCodeQuestion($question_id);
        
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$feedback_id.'.ini', APPLICATION_ENV);
        $q3Config = $limitsConfig->{'sat-global'};
        $question = $code_question;
        
        $borne= [   'bornePDTSMin'   =>   $q3Config->$question->very_unsatisfied->from,
                    'bornePDTSMax'   =>   $q3Config->$question->very_unsatisfied->to,
                    'bornePSMin'     =>   $q3Config->$question->unsatisfied->from,
                    'bornePSMax'     =>   $q3Config->$question->unsatisfied->to,
                    'borneSMin'      =>   $q3Config->$question->satisfied->from,
                    'borneSMax'      =>   $q3Config->$question->satisfied->to,
                    'borneTSMin'     =>   $q3Config->$question->very_satisfied->from,
                    'borneTSMax'     =>   $q3Config->$question->very_satisfied->to
                ];

        $satisfaction = $this->_solService->getOverAllSatisfaction($code_question, $kpi);
        $satisfactionByMonth = $this->_solService->getOverAllSatisfactionByMonth($code_question);
        $array = array(
            'satisfaction'          => $satisfaction['graphData'],
            'satisfactionAverage'   => $satisfaction['average'],
            'satisfactionByMonth'   => $satisfactionByMonth,
            'borne'                 => $borne,
            'color'                 => KpiManagement::getColeurByKpi($kpi),
            'respondent_base'       => $this->_solService->getResponseCount($this->_solService->getFilters().' AND '.$code_question.'-Value :[0 TO 10]'),
            'study'                 => $this->_getParam('study'),
            'kpi'                   => 'kpi_'.$kpi,
            'form_actif'            => $feedback_id
         );

        echo $this->view->partial('graph/overall-satisfaction.phtml', $array);
    }

    public function satisfactionGradingAction()
    {
        $this->setFeedbackFormId($this->_getParam('formid'));
        $feedback_id = $this->getFeedbackFormId();
        $kpi = $this->_getParam('kpi');
        $this->_solService = new Costumer_Service_Solr($this->_service->getFilters()->getReceivedFilters(),$feedback_id);
        $kpi_management = $this->getKpiManagement();
        $question_id = $this->_getParam('base');
        $code_question = $kpi_management->getCodeQuestion($question_id);
        
        $satisfactionGrading = $this->_solService->getSatisfactionGrading($code_question, $kpi);
        $satisfactionGradingByMonth = $this->_solService->getSatisfactionGradingByMonth($code_question, $kpi);
        $array = array(
            'satisfactionGrading'        => $satisfactionGrading,
            'satisfactionGradingByMonth' => $satisfactionGradingByMonth,
            'respondent_base'            => $this->_solService->getResponseCount($this->_solService->getFilters().' AND '.$code_question.' :[0 TO *]'),
            'color'                      => KpiManagement::getColeurByKpi($kpi),
            'study'                      => $this->_getParam('study'),
            'kpi'                        => 'kpi_'.$kpi,
            'form_actif'                 => $feedback_id
        );
        
        echo $this->view->partial('graph/satisfaction-grading.phtml', $array);
    }

    public function detailedSatisfactionAction()
    {
        $this->setFeedbackFormId($this->_getParam('formid'));
        $feedback_id = $this->getFeedbackFormId();
        $kpi = $this->_getParam('kpi');
        $this->_solService = new Costumer_Service_Solr($this->_service->getFilters()->getReceivedFilters(),$feedback_id);
        $kpi_management = $this->getKpiManagement();
        $question_id = $this->_getParam('base');
        $code_question_array = explode("x", $question_id);
        $code_question = array();
        foreach ($code_question_array as $value ) {
            $code_question[] = $kpi_management->getCodeQuestion($value);
        }
        $question     = implode('_', $code_question);
        $satisfactionDetailed = $this->_solService->getSatisfactionDetailed($question, $kpi);
        $satisfactionDetailedByMonth = $this->_solService->getSatisfactionDetailedByMonth($question);
        /**
         * Evolution GLAF #1786
         */
        $limitsConfig = new Zend_Config_Ini(APPLICATION_PATH . '/modules/costumer/configs/satisfaction-limits-'.$feedback_id.'.ini', APPLICATION_ENV);
        $q3Config = $limitsConfig->{'sat-detail'};


        $borne= [
            'bornePDTSMin'   =>   $q3Config->$question->very_unsatisfied->from,
            'bornePDTSMax'   =>   $q3Config->$question->very_unsatisfied->to,
            'bornePSMin'     =>   $q3Config->$question->unsatisfied->from,
            'bornePSMax'     =>   $q3Config->$question->unsatisfied->to,
            'borneSMin'      =>   $q3Config->$question->satisfied->from,
            'borneSMax'      =>   $q3Config->$question->satisfied->to,
            'borneTSMin'     =>   $q3Config->$question->very_satisfied->from,
            'borneTSMax'     =>   $q3Config->$question->very_satisfied->to
        ];

        $array = array(
            'satisfactionDetailed'        => $satisfactionDetailed['graphData'],
            'satisfactionDetailedAverage' => $satisfactionDetailed['averageData'],
            'satisfactionDetailedByMonth' => $satisfactionDetailedByMonth,
            'color'                       => KpiManagement::getColeurByKpi($kpi),
            'borne'                       => $borne,
            'respondent_base'             => $this->_solService->getResponseCount($this->_solService->getFilters(), $code_question),
            'study'                       => $this->_getParam('study'),
            'kpi'                         => 'kpi_'.$kpi,
            'form_actif'                  => $feedback_id
        );
        echo $this->view->partial('graph/detailed-satisfaction.phtml', $array);
    }

    public function netPromoterScoreAction()
    {
        $this->setFeedbackFormId($this->_getParam('formid'));
        $feedback_id = $this->getFeedbackFormId();
        $kpi = $this->_getParam('kpi');
        $this->_solService = new Costumer_Service_Solr($this->_service->getFilters()->getReceivedFilters(),$feedback_id);
        $kpi_management = $this->getKpiManagement();
        $question_id = $this->_getParam('base');
        $question = $kpi_management->getCodeQuestion($question_id);

        $filters = '';
        if($this->_solService->getFilters() == '*:*') {
            $filters = $question.'-Value:[0 TO 0]';
        }
        else {
            $filters .= $this->_solService->getFilters() .' '. $question.'-Value:[0 TO 10]';
        }
        
        $nps = $this->_solService->getNPS($question, $kpi);
        $npsEvolution = $this->_solService->getNPSEvolution($question);
        $array = array(
            'nps'                       => $nps['graphData'],
            'npsScore'                  => $nps['npsValue'],
            'npsAverageScore'           => $nps['averageData'],
            'npsEvolution'              => $npsEvolution,
            'color'                     => KpiManagement::getColeurByKpi($kpi),
            'respondent_base'           => $this->_solService->getResponseCount($filters),
            'study'                     => $this->_getParam('study'),
            'kpi'                       => 'kpi_'.$kpi,
            'form_actif'                => $feedback_id
        );
        echo $this->view->partial('graph/net-promoter-score.phtml', $array);        
    }

    public function npsEvolutionAction()
    {
        $kpi_management = $this->getKpiManagement();
        $this->setFeedbackFormId($this->_getParam('formid'));
        $kpi = $this->_getParam('kpi');
        $feedback_id = $this->getFeedbackFormId();
        $this->_solService = new Costumer_Service_Solr($this->_service->getFilters()->getReceivedFilters(),$feedback_id);
        $question_id = $this->_getParam('base');
        $code_question = $kpi_management->getCodeQuestion($question_id);
        $question = $code_question;
        
        $filters = ' ';
        if($this->_solService->getFilters() == '*:*') {
            $filters = $question.'-Value:[0 TO 10]';
        }
        else {
            $filters .=  $question.'-Value:[0 TO 10] '. $this->_solService->getFilters() ;
        }
        
        $npsDistribution = $this->_solService->getNPSDistribution($code_question);
        $npsDistributionEvolutionByMonth = $this->_solService->getNPSDistributionEvolutionByMonth($code_question);
        $array = array(
            'npsDistribution'                   => $npsDistribution,
            'npsDistributionEvolutionByMonth'   => $npsDistributionEvolutionByMonth,
            'totalRespondent'                   => $npsDistribution['countRespondant'],
            'respondent_base'                   => $this->_solService->getResponseCount($filters),
            'color'                             => KpiManagement::getColeurByKpi($kpi),
            'study'                             => $this->_getParam('study'),
            'kpi'                               => 'kpi_'.$kpi,
            'form_actif'                        => $feedback_id
        );
        echo $this->view->partial('graph/nps-evolution.phtml', $array);
    }

    public function respondentRatesAction()
    {
        $this->setFeedbackFormId($this->_getParam('formid'));
        $feedback_id = $this->getFeedbackFormId();
        $this->_solService = new Costumer_Service_Solr([], $feedback_id);
        $respondentRates = $this->_service->getRespondentRates($this->_solService);
        $array = array_merge([
            'data' => $respondentRates,
            'respondent_base' => count($this->_service->getResponses())
        ]);
        echo $this->view->partial('graph/respondent-rates.phtml', $array);
    }

}