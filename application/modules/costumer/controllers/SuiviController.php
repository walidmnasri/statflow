<?php
require_once __DIR__ . '/../../statflow/services/StudyUser.php';
use Statflow\Service\StudyUser;

class Costumer_SuiviController extends Centurion_Controller_Action
{
    /**
     * @var Statflow_StudyUser
     */
    protected $studyUserservice;
    
    /**
     *
     * @return \Statflow\Service\StudyUser
     */
    public function getStudyUserService()
    {
        if (! $this->studyUserservice) {
            $this->studyUserservice = new StudyUser();
        }
    
        return $this->studyUserservice;
    }
    
    /**
     *
     * @param \Statflow\Service\StudyUser $studyUserService
     */
    public function setStudyUserService($studyUserService)
    {
        $this->studyUserservice = $studyUserService;
    }
    
    public function indexAction()
    {
        $studyUserService = $this->getStudyUserService();
        $this->view->page = 'suivi';
        $this->view->form_actif = $this->_getParam('formId',0);
        $feedback_form_id = $this->_getParam('formId',0);
        $user_name='';
        $user_id=0;
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        if (isset($_SESSION['Zend_Auth']['storage'])){
            $storage = (array)$_SESSION['Zend_Auth']['storage'];
            $user_id = $storage['id'];
            $user_name = $storage['username'];
            
        }
        $this->view->user = $user_name;
        $this->view->questionnaire = $studyUserService->getAllFeedbackFormByUserId($user_id) ;
    }
}

?>