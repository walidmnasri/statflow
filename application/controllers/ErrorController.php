<?php

class ErrorController extends Centurion_Controller_Error
{
    public function preDispatch()
    {
        parent::preDispatch();
        $this->_helper->layout->setLayout('error');
    }
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                // 404 error -- controller or action not found
                $this->view->code = $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = $this->view->translate('Page_not_found@backoffice');
                break;

            // check for any other exception
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER:
                if ($errors->exception instanceof Exception) {
                    $this->view->code = $this->getResponse()->setHttpResponseCode(403);
                    $this->view->message = $this->view->translate( $errors->exception->getMessage() . '@backoffice');
                    break;
                }
            // fall through if not of type My_Exception_Blocked

            default:
                // application error
                $this->view->code = $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = $this->view->translate('Application_error@backoffice');
                break;
        }
    }
}

