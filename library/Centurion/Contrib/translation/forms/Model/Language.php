<?php

class Translation_Form_Model_Language extends Centurion_Form_Model_Abstract
{
    public function __construct($options = array(), Centurion_Db_Table_Row_Abstract $instance = null)
    {
        $this->_model = Centurion_Db::getSingleton('translation/language');
        $this->_elementLabels = array(
            'locale'              =>  $this->_translate('Locale'),
            'name'       =>  $this->_translate('Name'),
            'language'   =>  $this->_translate('Language'),
            'region'             =>  $this->_translate('Region'),
        );

        $this->_exclude = array('translation');
        
        parent::__construct($options, $instance);
    }

}