<?php

class Translation_AdminLanguageController extends Centurion_Controller_CRUD
{
    public function init()
    {
        $this->_formClassName = 'Translation_Form_Model_Language';

        $this->_displays = array(
            //'id'                =>  'ID',
            'flag' => array(
                'label' => $this->view->translate('Flag'),
                'type' => Centurion_Controller_CRUD::COLS_CALLBACK,
                'callback' => array($this, 'showFlag')
            ),
            'name'      =>  array(
                'type'  => self::COL_TYPE_FIRSTCOL,
                'label' => $this->view->translate('Name'),
                'param' => array(
                    'title' => 'name',
                    'cover' => null,
                    'subtitle' => 'locale',
                ),
            ),
            'language' =>  $this->view->translate('Language'),
            'region'   =>  $this->view->translate('Region')
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Manage languages'));

        parent::init();
    }

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        parent::preDispatch();

        Centurion_Signal::factory('post_insert')->connect(array($this, 'updateFlag'), 'Translation_Model_DbTable_Row_Language');
        Centurion_Signal::factory('post_update')->connect(array($this, 'updateFlag'), 'Translation_Model_DbTable_Row_Language');

    }

    public function showFlag($row)
    {
        return '<div class="text-wrapper"><img src="'.$row->flag.'" alt="'.$row->flag.'"/></div>';
    }

    public function updateFlag($signal, Translation_Model_DbTable_Row_Language $row)
    {
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $flag = '/layouts/backoffice/images/flags/'.$row->language.'.png';
        $adapter->query("UPDATE translation_language SET flag = '$flag' WHERE id = $row->id");

    }

    public function putAction()
    {

        Translation_Model_Manager::generate();

        parent::putAction();

        $this->getInvokeArg('bootstrap')
            ->getResource('cachemanager')
            ->clean(Zend_Cache::CLEANING_MODE_ALL);
    }

}