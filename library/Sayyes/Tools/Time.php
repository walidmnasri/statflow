<?php

class Sayyes_Tools_Time
{

    public static function secs_to_h($value)
    {
        $html = "";
        $bit = array(
            'j'   => $value / 86400 % 7,
            'h'   => $value / 3600 % 24,
            'min' => $value / 60 % 60,
            'sec' => $value % 60
        );

        $ret = array();
        foreach ($bit as $k => $v) {
            if ($v > 0) {
                $ret[] = $v . $k;
            }
        }

        $html .= ($ret) ? implode(' ', $ret) : '0';
        return $html;
    }
}