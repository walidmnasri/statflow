<?php

class Sayyes_Tools_String
{

    /**
     * @param $needle
     * @param $haystack
     * @return bool
     */
    public static function startWith($needle, $haystack) {
        return (substr($haystack, 0, strlen($needle))==$needle);
    }
}