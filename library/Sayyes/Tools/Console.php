<?php

class Sayyes_Tools_Console
{

    /**
     * @var Zend_Log_Formatter_Simple
     */
    protected $_formatter;

    public static function printLine($string)
    {
        ob_start();
        echo $string . PHP_EOL;
        ob_end_flush();
    }

    public function __construct() {
        $this->_formatter = new Zend_Log_Formatter_Simple();
    }

    public function info($string) {
        self::printLine($string);
    }
}