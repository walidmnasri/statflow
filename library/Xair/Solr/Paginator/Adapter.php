<?php

class Xair_Solr_Paginator_Adapter implements Zend_Paginator_Adapter_Interface {

    /**
     * Name of the row count column
     *
     * @var string
     */
    const ROW_COUNT_COLUMN = 'zend_paginator_row_count';

    /**
     * The COUNT query
     *
     * @var Zend_Db_Select
     */
    protected $_countSelect = null;

    /**
     * Solr query
     *
     * @var Statflow_Model_Solr
     */
    protected $_solr = null;

    /**
     * Total item count
     *
     * @var integer
     */
    protected $_rowCount = null;

    protected $_queryParams = array();

    protected $_sortParams = array();

    protected $_classMap = null;

    protected $_facets = array();

    protected $_facetsData = array();

    protected $_noPagination = false;

    /**
     * Constructor.
     *
     * @param Statflow_Model_Solr $solr The select query
     */
    public function __construct(Statflow_Model_Solr $solr, $params = array(), $sort = array(), $classMap = null, $facets = array(), $noPagination = false)
    {
        $this->_solr = $solr;
        $this->_queryParams = $params;
        $this->_sortParams = $sort;
        $this->_classMap = $classMap;
        $this->_facets = $facets;
        $this->_noPagination = $noPagination;
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  integer $offset Page offset
     * @param  integer $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        $data = $this->_solr->query($this->_queryParams, $this->_sortParams, $offset, $itemCountPerPage, $this->_facets);
        $this->_facetsData = $this->_solr->getFacetsData();
        if($this->_classMap != null) {
            foreach ($data as &$entry) {
                $entry = new $this->_classMap($entry);
            }
        }
        $rowCount = $this->_solr->getRowCount();
        $this->_rowCount = $this->_noPagination ? ($rowCount > $itemCountPerPage ? $itemCountPerPage : $rowCount) : $rowCount;
        return $data;
    }

    public function count()
    {
        return $this->_rowCount;
    }

    public function getFacetsData()
    {
        return $this->_solr->getFacetsData();
    }

}