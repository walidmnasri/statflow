<?php

// ------------------- 5min time limit --------------------
set_time_limit(300);
ini_set('max_execution_time', '300');

//---------------------------------------

$backline = "\n";

echo $backline."-------- Start deployZPK ----------".$backline;

//---------------------------------------------------
//              Parsing params
//---------------------------------------------------
$params = array();
foreach ($argv as $arg) {
    $e=explode("=",$arg);
    if(count($e)==2)
        $params[$e[0]]=$e[1];
    else
        $params[$e[0]]=0;
}

//---------------------------------------------------
//              Checking params
//---------------------------------------------------

echo $backline."> Checking params ...".$backline;

if(!isset($params['zpk'])){
    echo "Missing Param : zpk".$backline;
    exit(1);
}
if(!isset($params['zsurl'])){
    echo "Missing Param : zsurl".$backline;
    exit(1);
}
if(!isset($params['zskey'])){
    echo "Missing Param : zskey".$backline;
    exit(1);
}
if(!isset($params['zssecret'])){
    echo "Missing Param : zssecret".$backline;
    exit(1);
}
if(!isset($params['baseuri'])){
    echo "Missing Param : baseuri".$backline;
    exit(1);
}
if(!isset($params['zsversion'])){
    echo "Missing Param : zsversion".$backline;
    exit(1);
}

echo "done!".$backline;

//---------------------------------------------------
//              Deploying Application
//---------------------------------------------------

echo $backline."> Sending and deploying zpk to Zend Server on url : ".$params['zsurl']." ...".$backline;

$command = "php /usr/local/bin/zs-client.phar installApp";
$command .= " --zpk='".$params['zpk']."'";
$command .= " --zsurl='".$params['zsurl']."'";
$command .= " --zskey='".$params['zskey']."'";
$command .= " --zssecret='".$params['zssecret']."'";
$command .= " --baseUri='".$params['baseuri']."'";
$command .= " --zsversion='".$params['zsversion']."'";
//$command .= " --wait";

unset($output);
exec($command, $output, $return_var);

if($return_var !== 0){
    echo implode(" \n", $output);
    exit($return_var);
}

echo implode(" \n", $output).$backline;

echo "done!".$backline;

echo $backline."-------- End deployZPK ----------".$backline;             
exit(0);
