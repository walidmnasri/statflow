<?php

// ------------------- 5min time limit --------------------
set_time_limit(300);
ini_set('max_execution_time', '300');

//---------------------------------------

$backline = "\n";

echo $backline."-------- Start checkDeployZPK ----------".$backline;

//---------------------------------------------------
//              Parsing params
//---------------------------------------------------
$params = array();
foreach ($argv as $arg) {
    $e=explode("=",$arg);
    if(count($e)==2)
        $params[$e[0]]=$e[1];
    else
        $params[$e[0]]=0;
}

//---------------------------------------------------
//              Checking params
//---------------------------------------------------

echo $backline."> Checking params ...".$backline;

if(!isset($params['zsurl'])){
    echo "Missing Param : zsurl".$backline;
    exit(1);
}
if(!isset($params['zskey'])){
    echo "Missing Param : zskey".$backline;
    exit(1);
}
if(!isset($params['zssecret'])){
    echo "Missing Param : zssecret".$backline;
    exit(1);
}
if(!isset($params['zsversion'])){
    echo "Missing Param : zsversion".$backline;
    exit(1);
}
if(!isset($params['appname'])){
    echo "Missing Param : appname".$backline;
    exit(1);
}

echo "done!".$backline;

//---------------------------------------------------
//              Checking Application Deployment
//---------------------------------------------------

$i = 0;

while($i < 5){
	$i++;

	echo $backline."Loop ".$i." going to sleep for 60s".$backline;
	sleep(60);
	echo "waking up!".$backline;

	echo $backline."> check zpk deployment on Zend Server on url : ".$params['zsurl']." ...".$backline;

	$command = "php /usr/local/bin/zs-client.phar applicationGetStatus";
	$command .= " --zsurl='".$params['zsurl']."'";
	$command .= " --zskey='".$params['zskey']."'";
	$command .= " --zssecret='".$params['zssecret']."'";
	$command .= " --zsversion='".$params['zsversion']."'";
	$command .= " --output-format='json'";

	unset($output);
	exec($command, $output, $return_var);
	
	$result = json_decode(implode("", $output), true);
	
	if($return_var !== 0){
	    var_dump($result);
	    exit($return_var);
	}

	//Check response integrity
	if(!isset($result['responseData'])){
	    echo "Bad response, expected responseData array  result: ".$backline;
	    var_dump($result);
	    exit(1);
	}

	if(!isset($result['responseData']['applicationsList'])){
	    echo "Bad response, expected applicationsList array  result: ".$backline;
	    var_dump($result['responseData']);
	    exit(1);
	}

	$appFound = false;
	$appDetail = array();
	foreach ($result['responseData']['applicationsList'] as $app){
	    if(isset($app['appName'])){
		if($app['appName'] == $params['appname']){
		    $appFound = true;
		    $appDetail = $app;
		}
	    }
	}

	if(!$appFound){
	    echo "App not found or Bad response, searching for '".$params['appname']."' in list: ".$backline;
	    var_dump($result['responseData']['applicationsList']);
	    exit(1);
	}

	//Check app deployment status

	if(stristr($appDetail["status"], "error") !== false){
	    echo "Error during deployment ".$backline;
	    if(!isset($appDetail["messageList"])){
		echo "Unknown error ".$backline;
	    }
	    echo "Return deployment log : \n";
	    var_dump($appDetail["messageList"]);
	    
	    exit(1);
	}

	echo "App deployement status : ".$appDetail["status"].$backline;
	if(isset($appDetail["messageList"])){
		var_dump($appDetail["messageList"]);
	} 

	if($appDetail["status"] == "deployed"){
		echo "delpoyed!".$backline;
		echo $backline."-------- End checkDeployZPK ----------".$backline;  
		exit(0);     
	}
}

echo "Deployment toot long, time out !".$backline;
exit(1);
          

