<?php
 ini_set('memory_limit', '-1');

$config  =  parse_ini_file( __DIR__ .'/application/configs/local.ini');

$servername = $config['resources.db.params.host'];
$username   = $config['resources.db.params.username'];
$password   = $config['resources.db.params.password'];
$dbname     = $config['resources.db.params.dbname'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}     
    $sqlDoublant = "SELECT `id`, `respondent_id`, `question_id`, `value`, `created_at`, `updated_at`
        FROM `statflow_answer`
        WHERE
            `id` NOT IN ( SELECT id FROM
                        `statflow_answer`
                      GROUP BY
                        `question_id`, `respondent_id`
                      HAVING
                         COUNT(`question_id`) = 1
                    )
        ORDER BY `respondent_id` , `question_id`, `created_at`
    ";
    $result = $conn->query($sqlDoublant); 
    $question = '';  
    for ($resultSet = []; $row = $result->fetch_assoc(); $resultSet[] = $row );
    foreach($resultSet as $key => $element) {
        if ($question == $element['question_id']) {
            if ($resultSet[$key-1]['value'] === $element['value']) {
                $deleteSqlAnswer  = "DELETE FROM `statflow_answer` WHERE id = " . $element['id'] ;
                if ($conn->query($deleteSqlAnswer) == TRUE) {
                    echo "Record " . $element['question_id'] . "deleted successful " ;
                }
                $deleteSqlAnswerEntry   = "DELETE FROM `statflow_answer_entry` WHERE answer_id = " . $element['id'] ;
                if ($conn->query($deleteSqlAnswerEntry) == TRUE) {
                    echo "Record with answer_id " . $element['id'] . "deleted successful\n" ;
                }
            }
            else {
                if (isset($element['value']) && $element['value'] !== NULL) {
                    $deleteSqlAnswer        = "DELETE FROM `statflow_answer` WHERE id = " . $resultSet[$key-1]['id'] ;
                    if ($conn->query($deleteSqlAnswer) == TRUE) {
                        echo "Record " . $resultSet[$key-1]['question_id'] . "deleted successful" ; 
                    }
                    $deleteSqlAnswerEntry   = "DELETE FROM `statflow_answer_entry` WHERE answer_id = " . $resultSet[$key-1]['id'] ;
                    if ($conn->query($deleteSqlAnswerEntry) == TRUE) {
                        echo "Record with answer_id " . $resultSet[$key-1]['id'] . "deleted successful\n" ;
                    }
                }
                else {
                    $deleteSqlAnswer        = "DELETE FROM `statflow_answer` WHERE id = " . $element['id'] ;
                    if ($conn->query($deleteSqlAnswer) == TRUE) {
                        echo "Record " . $element['question_id'] . "deleted successful " ;
                    }
                    $deleteSqlAnswerEntry   = "DELETE FROM `statflow_answer_entry` WHERE answer_id = " . $element['id'] ;
                    if ($conn->query($deleteSqlAnswerEntry) == TRUE) {
                        echo "Record with answer_id " . $element['id'] . "deleted successful\n" ;
                    }
                }
            }
        } else {
            $question = $element['question_id'];
        }
    }
    $responseEmpty = "SELECT id FROM `statflow_answer` WHERE `value` = ''";
    $resultEmptyValue = $conn->query($responseEmpty);
    for ($resultEmptyValueSet = []; $row = $resultEmptyValue->fetch_assoc(); $resultEmptyValueSet[] = $row );
    foreach ($resultEmptyValueSet as $key => $value ) {
        $cleanTableEntryAnswerSql = " DELETE FROM `statflow_answer_entry`  WHERE `answer_id` = {$value['id']} ";
        if ($conn->query($cleanTableEntryAnswerSql) == TRUE) {
            echo "\nfinished clean {$value['id']} from statflow_answer_entry !\n";
        }
    }
    $cleanTableAnswerSql = " DELETE FROM `statflow_answer` WHERE `value` = '' ";
    
    if ($conn->query($cleanTableAnswerSql) == TRUE) {
        echo "\nfinished clean statflow_answer table !\n";
    }
    

$conn->close();
?>
