function isPublish(id, element)
{
    if ($('#isPublish').is(':checked')) {
        window.location.href = '/statflow/' + element + '/publish/id/' + id;
    } else {
        window.location.href = '/statflow/' + element + '/unpublish/id/' + id;
    }
}

function isVisible(id, element)
{
    if ($('#isVisible').is(':checked')) {
        window.location.href = '/statflow/' + element + '/enable/id/' + id;
    } else {
        window.location.href = '/statflow/' + element + '/disable/id/' + id;
    }
}

function exportHandle(id)
{
    var urlExport = '/statflow/study/index?';
    urlExport += 'export=1&';
    urlExport += 'form_id=' + id + '&';
    window.location = urlExport;
}

function applyColorPicker(selector)
{
    $(selector).each(function() {
        $(this).minicolors({
            control: $(this).attr('data-control') || 'hue',
            defaultValue: $(this).attr('data-defaultValue') || '',
            inline: $(this).attr('data-inline') === 'true',
            letterCase: $(this).attr('data-letterCase') || 'lowercase',
            opacity: $(this).attr('data-opacity'),
            position: $(this).attr('data-position') || 'bottom left',
            change: function(hex, opacity) {
                if (!hex) return;
                if (opacity) hex += ', ' + opacity;
                if (typeof console === 'object') {
                    console.log(hex);
                }
            },
            theme: 'bootstrap'
        });

    });
}

function getStructureChecked() {
    var selectedElmsObj = {};
    var selectedElms = $('#tree_2').jstree("get_selected", true);

    $.each(selectedElms, function() {
        selectedElmsObj[this.id] = this.text;
    });

    return selectedElmsObj;
}

function initializeFilter()
{
    if (sessionStorage.length == 0) {
        return;
    }
    var itemsFilter = JSON.parse(sessionStorage.getItem('paramsFilter'));
    if (itemsFilter["period-start"]) {
        $('#response-date').val(itemsFilter["period-start"] + ' - ' + itemsFilter["period-end"]);
    }
    if (itemsFilter["purchase-start"]) {
        $('#purchase_date').val(itemsFilter["purchase-start"] + ' - ' + itemsFilter["purchase-end"]);
    }

    $('#alert').val(itemsFilter["alert"]);
    $('#sel_action').val(itemsFilter["sel_action"]);

    if (itemsFilter["filter_structure"]) {
       $('#filter_structure').val(itemsFilter["filter_structure"]);
       $('#filter_structure_name').val(itemsFilter["filter_structure_name"]);
       var structSelected = itemsFilter["filter_structure"].split(',');
       $.each(structSelected, function (key, val) {
           $('#tree_2').jstree("select_node","#" + val);
       });
    }

    $("[id^=sel_kpi_filter_]").each(function () {
        $(this).val(itemsFilter[$(this).attr('id')]);
    });
}

function updateFilter() {
    var rangPeriod = $('#response-date').val() ? $('#response-date').val().split(' - ') : ['', ''];
    var rangPurchase = $('#purchase_date').val() ? $('#purchase_date').val().split(' - ') : ['', ''];

    var structs = getStructureChecked();
    $('#filter_structure').val(Object.keys(structs));
    $('#filter_structure_name').val(Object.values(structs));
    var paramsFilter = {
    	"period-start": rangPeriod[0],
    	"period-end": rangPeriod[1],
    	"purchase-start": rangPurchase[0],
    	"purchase-end": rangPurchase[1],
    	"alert": $('#alert').val(),
    	"sel_action": $('#sel_action').val(),
    	"filter_structure": $('#filter_structure').val(),
    	"filter_structure_name": $('#filter_structure_name').val()
    };
    sessionStorage.clear();
    
    var crmFilterList = {};
    var crmFilterValue = {};

    $("[id^=sel_kpi_filters_]").each(function (k, val) {
        crmFilterList[k] = $(this).val();
    });

    $("[id^=sel_kpi_filter_]").each(function (k, val) {
    	paramsFilter[$(this).attr('id')] = $(this).val();
        crmFilterValue[k] = $(this).val();
    });

    paramsFilter.sel_kpi_filter = JSON.stringify(crmFilterValue);
    paramsFilter.sel_kpi_filters = JSON.stringify(crmFilterList);
    
    sessionStorage.setItem('paramsFilter', JSON.stringify(paramsFilter));
}


$(document).ready(function() {

    // Confirmation modal on delete
    $('.btn-delete').bind('click', function(e){
        e.preventDefault();
        $('#btn-delete-confirm').attr('href', $(this).attr('data-value'));
    });

    // Confirmation Import
    $('.btn-import').bind('click', function(e){

        e.preventDefault();
        $('#btn-import-confirm').attr('href', $(this).attr('data-value'));
    });

    // Confirmation duplicate
    $('.btn-duplicate').bind('click',function(e){
        e.preventDefault();
        $('#btn-duplicate-confirm').attr('href', $(this).attr('data-value'));
    });

    // Confirmation delete rule
    $('.btn-delete_rule').click(function(e) {
        //alert('test');
        e.preventDefault();
        $('#btn-delete-confirm-rule').attr('href', $(this).attr('data-value'));
    });

    //Confirmation modal change color

    $('.btn-color').bind('click', function(e) {
        var color = '';
        $('.form-color').attr('action', $(this).attr('data-value'));
        var div = $(this).attr('href');
        var id = $(this).attr('key');
        $.ajax({
            type:'GET',
            url:"/statflow/kpi-management/edit/id/" + id,
            dataType:'json',
            success: function(data) {
                color = $.parseJSON(data.meta).style;
                if (div == '#color-edit') {
                    $('#colorTS').val(color.colorTS);
                    $('#colorS').val(color.colorS);
                    $('#colorPS').val(color.colorPS);
                    $('#colorPSDT').val(color.colorPSDT);
                } else if (div == '#color-edit2') {
                    $('#colorDETR').val(color.colorDETR);
                    $('#colorPASS').val(color.colorPASS);
                    $('#colorPROM').val(color.colorPROM);
                }

                applyColorPicker(".demo");
            }
        });
    });

    // Create a dynamic hidden field in order to know the action made, activate or delete
    $('.btn-multi-select').bind('click', function(e){
        $('<input>').attr({
            type: 'hidden',
            name: 'selection',
            value: $(this).attr('data-value')
        }).appendTo('form');
    });

    if ($('#fieldLabel').val() != '') {
        $('#format').css('display','block');
        $('#fieldFormat').attr("disabled",false);
    }
    else {
        $('#format').css('display','none');
        $('#fieldFormat').attr("disabled",true);
    }
    $('#fieldLabel').on('keyup change', function(){
        if (this.value != ''){
            $('#format').css('display','block');
            $('#fieldFormat').attr("disabled",false);
        }
        else {
            $('#format').css('display','none');
            $('#fieldFormat').attr("disabled",true);
        }
    });

    if ($('.main-menu'))
    {
        $('.main-menu li:nth-last-child(2) a').attr('target', '_blank');
        $('.main-menu li:last-child a').attr('target', '_blank');
    }

    // Check if the "external" javascript function is defined & execute it
    if (typeof(external) === 'function') {
        external();
    }

    if (typeof(mainPagekpi) === 'function') {
        //var highchartsOptions = highcharts.setOptions(highcharts.theme);
        mainPagekpi();
    }

    if (typeof(RespondentRate) === 'function') {
        RespondentRate();
    }

    $( document ).ajaxStart(function() {
        $( ".loading" ).show();
    });
    $( document ).ajaxComplete(function( event, request, settings ) {
        $( ".loading" ).hide();
    });
});

