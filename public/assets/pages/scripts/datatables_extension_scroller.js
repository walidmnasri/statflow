/* ------------------------------------------------------------------------------
*
*  # Scroller extension for Datatables
*
*  Specific JS code additions for datatable_extension_scroller.html page
*
*  Version: 1.1
*  Latest update: Nov 10, 2015
*
* ---------------------------------------------------------------------------- */

function xhrGetTdData(url)
 {


    // Override defaults
    // ------------------------------
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        ajax:  url,
        scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        searching: false,
        paginate: false,
        bLengthChange: false
    });


    // Table setup
    // ------------------------------

    // Basic scroller demo
    var table = $('.datatable-scroller').DataTable({
    	buttons: {            
            buttons: [
                {
                    extend: 'csvHtml5',
                    className: 'btn btn-outline green-haze',
                    text: '<i class="fa fa-file-excel-o"></i> CSV',
                    fieldSeparator: ';',
                    extension: '.csv'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i> <span class="caret"></span>',
                    className: 'btn btn-outline purple-studio'
                }
            ]
        }
    }); 
    
    $('.datatable-scroller').on( 'page.dt', function () {
        var info = table.page.info();
        $('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
    } );
}
