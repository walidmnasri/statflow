define ['marionette','../services/registry', './page', 'hbs!templates/form','helpers/data-callbacks'], (marionette, registry, PageView, template, dataHelper) ->
  class extends marionette.CompositeView
    template: template
    itemView: PageView
    itemViewContainer: '.pages'


    itemViewOptions: (model, index) ->
      if index == 0 then return {active:true}

    initialize: () ->
      @collection = @model.pages
      #console.log @model.pages

    isVisible: (tab, navigation, index, clearPage = true) ->

      pageId = @$('.pages .tab-pane:nth-child('+(index+1)+')').attr('id').replace('page_','')
      page = _.find registry.pages, (p)-> p.id == pageId.toString()
      #console.log 'page', page
      if(page? == false && @totalPagesCount == index) then return true #end text page

      if clearPage
        page.reset()
        page.trigger('change')

      if page.get('meta') && page.get('meta').condition
        condition = page.get('meta').condition
        console.log 'raw cond', condition
        matches = condition.match /{.+?}/g
        console.log 'raw matches', matches
        condition = condition.replace /AND/gi,'&&'
        condition = condition.replace /OR/gi,'||'
        console.log 'parsed cond 1', condition
        inRegex = /({[^{}]*?}) IN\((.*?)\)/i
        matchRegex = /({[^{}]*?}) MATCH\((.*?)\)/i
        matchRegex2 = /(\[FILE:.+\]) MATCH\((.*?)\)/i
        matchRegex3 = /(\[STRUCT:.+\]) MATCH\((.*?)\)/i
        fileRegex = /\[FILE:(.+?)\]/i
        structRegex = /\[STRUCT:(.+?)\]/i
        while inRegex.test condition
          condition = condition.replace inRegex, '($2.indexOf($1) != -1)'
        while matchRegex.test condition
          condition = condition.replace matchRegex, '(new RegExp("$2")).test("$1")'
        while matchRegex2.test condition
          condition = condition.replace matchRegex2, '(new RegExp("$2")).test($1)'
        while matchRegex3.test condition
          condition = condition.replace matchRegex3, '(new RegExp("$2")).test($1)'
        while fileRegex.test condition
          condition = condition.replace fileRegex, 'window.formData.user["$1"]'
        while structRegex.test condition
          condition = condition.replace structRegex, 'window.formData.store["$1"]'

        console.log 'parsed cond 2', condition
        _.forEach matches, (s) =>
          code = s.replace /[{}]/g,''
          question = _.find registry.questions, (q) -> q.get('code') == code
          if question
            value = question.get('value')
            if value
              switch question.get('question_type_id')
                when "1"
                  value = value[0].value


          condition = condition.replace new RegExp('{'+code+'}'), value
          console.log 'cond', condition, code

        try
          return eval condition
        catch error
          if console? then console.log page, error.message
          return console
      else return true

    onRender: ()->
      self = this
      @totalPagesCount = @collection.length

      unless formData.answers != 'undefined'
        for question_id in Object.keys(formData.answers)
          if(formData.answers[question_id] != null)
            registry.questions[question_id].set('value', formData.answers[question_id])
            if _.isArray(formData.answers[question_id])
              formData.answers[question_id].forEach (answer) =>
                @$("input[value='"+answer.value+"'] ").prop("checked", true)
            else if registry.questions[question_id].get('question_type_id') == '2'
              @$("input[name='"+question_id+"'][value='"+formData.answers[question_id]+"']").prop("checked", true)
            else
              @$("textarea[name='"+question_id+"'] ").val(formData.answers[question_id])


      if @model.get('form').introduction_is_visible == "1"
        @totalPagesCount += 1
        @$('.pages').prepend('<div id="page_intro" class="tab-pane"><div class="intro-text">'+dataHelper(@model.get('form').introduction_text)+'</div></div>')

      @$('.pages').append('<div id="page_end" class="tab-pane"><div class="intro-text">'+dataHelper(@model.get('form').end_text)+'</div></div>')

      @$('.pages').append(
        """
          <ul class="pager wizard">
            <li class="previous" style="display:none"><a href="javascript:void(0)">&nbsp;&nbsp;&nbsp;&nbsp;#{previousQuestionText}</a></li>
            <li class="next" style="#{if @totalPagesCount == 1 then "display:none"}"><a href="javascript:void(0)">#{nextQuestionText}&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
            <li class="next finish" style="#{if @totalPagesCount > 1 then "display:none"}"><a href="javascript:void(0);">#{validateFormText}&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
        </ul>
        """
      )

      @$('.pages .tab-pane:first-child').addClass('active');
      @$('.pages .tab-pane').each (index, page) =>
        @$('.wizardRoot > ul').append("<li><a href='##{page.id}' data-toggle='tab'></a></li>")


      wizard = @$('.wizardRoot').bootstrapWizard({
        onNext: (tab, navigation, index) =>

          thisPageId = @$('.pages .tab-pane:nth-child('+(index)+')').attr('id').replace('page_','')
          thisPage = _.find registry.pages, (p)-> p.id == thisPageId.toString()
          if(thisPage && thisPage.isValid() == false)
            return false;

          until self.isVisible(tab, navigation, index, false)
            index = index + 1

          $('.wizardRoot').bootstrapWizard('show', index - 1)

        onPrevious: (tab, navigation, index) =>
          if index == 0 && @model.get('form').introduction_is_visible == "1" then return true

          until self.isVisible(tab, navigation, index, false)
            index = index - 1
          $('.wizardRoot').bootstrapWizard('show', index + 1)

        onTabShow: (tab, navigation, index) =>
          #console.log index
          #console.log tab
          window.scrollTo(0,0)
          switch true
            when index == 0
              $('.wizard .previous').hide()
              $('#bar .bar').css width:0
              if self.totalPagesCount == 1 then $('.wizard .finish').show()
              else
                $('.wizard .next').show()
                $('.wizard .finish').hide()
            when (index + 1) == self.totalPagesCount
              $('.wizard .previous').show()
              $('.wizard .next').hide()
              $('.wizard .finish').show()
              width = (index / self.totalPagesCount)*100
              $('#bar .bar').css width:width+'%'
              @model.savePage index, (data) ->
                console.log data
            when (index == self.totalPagesCount) #end text page => save form
              $('.wizard .previous').hide()
              $('.wizard .next').hide()
              $('.wizard .finish').hide()
              $('#bar .bar').css width:'100%'
              @model.saveForm () ->
            else
              $('.wizard .previous').show()
              $('.wizard .next').show()
              $('.wizard .finish').hide()
              width = (index / self.totalPagesCount)*100
              $('#bar .bar').css width:width+'%'
              @model.savePage index, (data) ->
                console.log data
          return true
      })

      if(formData.last_page >= 0)
        wizard.bootstrapWizard('show',formData.last_page)
        @$('.tab-content > div:eq(0)').removeClass('active')
        @$('.tab-content > div:eq('+formData.last_page+')').addClass('active')
        @$('.wizard .previous').show()
        width = (formData.last_page / self.totalPagesCount)*100
        $('#bar .bar').css width:width+'%'
      else if @model.get('form').introduction_is_visible != "1"
        index = 0
        until self.isVisible(null, null, index, false)
          index = index + 1

        if index > 0
          wizard.bootstrapWizard('show',index)
          @$('.tab-content > div:eq(0)').removeClass('active')
          @$('.tab-content > div:eq('+index+')').addClass('active')
          @$('.wizard .previous').show()
          width = (index / self.totalPagesCount)*100
          $('#bar .bar').css width:width+'%'