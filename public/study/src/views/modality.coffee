define [
  'underscore',
  'marionette',
  'hbs!templates/modality',
  'hbs!templates/modalities/radio-simple'
  'hbs!templates/modalities/radio-exclusive'
  'hbs!templates/modalities/radio-complementary'
  'hbs!templates/modalities/checkbox-simple'
  'hbs!templates/modalities/checkbox-exclusive'
  'hbs!templates/modalities/checkbox-complementary'
  'hbs!templates/modalities/interval'
  'hbs!templates/modalities/ordered-simple'
  'hbs!templates/modalities/ordered-complementary'
], (
  _,
  marionette,
  template
  radioSimpleModality
  radioExclusiveModality
  radioComplementaryModality
  cbxSimpleModality
  cbxExclusiveModality
  cbxComplementaryModality
  intervalModality
  orderedSimpleModality
  orderedComplementaryModality
) ->
  answerInterpolation = /class="empty"/
  class extends marionette.ItemView
    template: template
    defaults:
      value:null
    attributes:
      class: 'modality'

    modelEvents:
      'change:content':'render'
    events:
      'click .array-question-checkbox input' : 'parseQuestionValue'
    ui:
      content: '.content'
    initialize: () ->

      @attributes = _.clone @attributes
      questionType = @model.collection.question.get('question_type_id')
      switch questionType
        when "1"
          switch @model.get('entry_type_id')
            when "1" then @template = radioSimpleModality
            when "2" then @template = radioExclusiveModality
            when "3" then @template = radioComplementaryModality
          if @model.collection.question.get('meta')
            @model.set('inline', @model.collection.question.get('meta').inline)
            if @model.get('inline') then @attributes.style == 'display:inline'
        when "3"
          switch @model.get('entry_type_id')
            when "1" then @template = orderedSimpleModality
            when "3" then @template = orderedComplementaryModality
          @model.set('meta', @model.collection.question.get('meta'))
        when "5"
          @template = intervalModality
          @model.set('meta', @model.collection.question.get('meta'))
          @model.set('entries', @model.collection.question.get('entries'))
        when "6"
          switch @model.get('entry_type_id')
            when "1" then @template = cbxSimpleModality
            when "2" then @template = cbxExclusiveModality
            when "3" then @template = cbxComplementaryModality
          if @model.collection.question.get('meta')
            @model.set('inline', @model.collection.question.get('meta').inline)
            if @model.get('inline') then @attributes.style == 'display:inline'
        when "8"
          @model.set('image',true)
          switch @model.get('entry_type_id')
            when "7" then @template = cbxSimpleModality
            when "8" then @template = cbxExclusiveModality
          if @model.collection.question.get('meta')
            @model.set('inline', @model.collection.question.get('meta').inline)
            if @model.get('inline') then @attributes.style == 'display:inline'

    parseQuestionValue: () ->
      @model.set 'value': @$('input:checked').val()

    isValid: () ->
      @model.get 'value' != null

    onRender: () ->
      if answerInterpolation.test @ui.content.html() then @$el.hide()
      else
        if @model.get('inline') then @$el.css('display', 'inline')
        else @$el.show()