define [
  'underscore',
  'marionette',
  '../services/registry',
  './modality',
  'hbs!templates/question',
  'hbs!templates/questions/array-question',
  'hbs!templates/questions/ordered-question',
  'hbs!templates/questions/open-question',
  'hbs!templates/questions/slider-question',
  'hbs!templates/questions/image-question',
  '../helpers/array-question'
], (
  _,
  marionette,
  registry,
  ModalityView,
  template,
  arrayQuestionTemplate
  orderedQuestionTemplate
  openQuestionTemplate
  sliderQuestionTemplate
  imageQuestionTemplate
) ->
  class extends marionette.CompositeView
    template: template
    itemView: ModalityView
    itemViewContainer: '.modalities'
    attributes:
      class: "question"
    defaults:
      value:null
    modelEvents:
      'change:content':'render'
      'change:isValid':'validation'
    events:
      'click .simple input': 'selectSimple'
      'click .complementary input[type=checkbox]' : 'selectComplementary'
      'click .complementary input[type=radio]' : 'selectComplementary'
      'click .exclusive input' : 'selectExclusive'
      'click .complementary input[type=text]' : 'checkComplementary'
      'change .complementary input[type=text]' : 'parseQuestionValue'
      'change textarea' : 'parseQuestionValue'
      'click .array-question-checkbox input' : 'parseQuestionValue'
      'change .image input[type=checkbox]' : 'updateImage'
      'mousedown .ordered input[type="radio"]': 'checkOrdered'
      'change .ordered input[type="text"]':'parseQuestionValue'

    itemViewOptions: (item, index) ->
      switch @model.get('question_type_id')
        when "5" then options = {tagName: 'tr'} #array
        when "3" then options = {tagName: 'tr'} #ordered
        else options = {}
      if( options.tagName && options.tagName == 'tr')
        if index % 2 == 0 then options.attributes = {class: 'even'}
        else options.attributes = {class: 'odd'}
      return options

    validation: () ->
      if @model.isValid() then @$el.removeClass 'invalid'
      else @$el.addClass 'invalid'

    initialize: () ->
      @collection = @model.modalities
      switch @model.get('question_type_id')
        when "2"
          @template = sliderQuestionTemplate
        when "3"
          @template = orderedQuestionTemplate
          @itemViewContainer = '.modalities tbody'
        when "4"
          @template = openQuestionTemplate
        when "5"
          @template = arrayQuestionTemplate
          @itemViewContainer = '.modalities tbody'
          @collection = @model.subquestions
        when "7"
          @template = imageQuestionTemplate
        else
          #console.log 'unknown q type',@model.get('question_type_id')

    onRender: () ->

      if @model.get('meta')
        if(@model.get('meta').inline) then @$(@itemViewContainer).addClass('inline')
        if(@model.get('meta').condition)
          condition = @model.get('meta').condition
          matches = condition.match /{.+?}/g
          condition = condition.replace /( AND )/gi,' && '
          condition = condition.replace /( OR )/gi,' || '
          inRegex = /({[^{}]*?}) IN\((.*?)\)/i
          matchRegex = /({[^{}]*?}) MATCH\((.*?)\)/i
          matchRegex2 = /(\[FILE:.+\]) MATCH\((.*?)\)/i
          matchRegex3 = /(\[STRUCT:.+\]) MATCH\((.*?)\)/i
          fileRegex = /\[FILE:(.+?)\]/i
          structRegex = /\[STRUCT:(.+?)\]/i
          while inRegex.test condition
            condition = condition.replace inRegex, '($2.indexOf($1) != -1)'
          while matchRegex.test condition
            condition = condition.replace matchRegex, '(new RegExp("$2")).test("$1")'
          while matchRegex2.test condition
            condition = condition.replace matchRegex2, '(new RegExp("$2")).test($1)'
          while matchRegex3.test condition
            condition = condition.replace matchRegex3, '(new RegExp("$2")).test($1)'
          while fileRegex.test condition
            condition = condition.replace fileRegex, 'window.formData.user["$1"]'
          while structRegex.test condition
            condition = condition.replace structRegex, 'window.formData.store["$1"]'

          _.forEach matches, (s) =>
            code = s.replace /[{}]/g,''
            registry.once code+':change', @render
            question = _.find registry.questions, (q) -> q.get('code') == code
            if question
              value = question.get('value')
              if value
                switch question.get('question_type_id')
                  when "1"
                    value = value[0].value
                    condition = condition.replace new RegExp('{'+code+'}'), value
                  when "6"
                    if value.length
                      c = condition
                      condition = new Array
                      value.forEach (v) ->
                        condition.push(c.replace new RegExp('{'+code+'}'), v.value)
                    else
                      condition = condition.replace new RegExp('{'+code+'}'), value
              else
                condition = condition.replace new RegExp('{'+code+'}'), value

            else
                condition = condition.replace new RegExp('{'+code+'}'), value
          try
            if _.isArray(condition)
              test = false
              condition.forEach (c) ->
                test = true if eval(c) == true
            else
              test = eval condition
            if test
              @$el.show()
              @model.set({'visible': true})
            else
              @$el.hide()
              @model.set({'visible': false})
              @model.set 'value': null
          catch error
            if console? then console.log error


    parseQuestionValue: () ->
      value = null
      switch @model.get('question_type_id')
        when "1"
          value = []
          @$('input:checked').each (i, e) =>
            value.push {
              value : @$(e).val()
              content: @$('input[type=text]').val() || ''
            }
        when "3"
          value = []
          for i in [1..@model.get('meta').max]
            val = @$( 'input[name="'+@model.get('id')+'['+i+']"]:checked').val();
            value.push {
              value: val
              content: @$('input[name="'+@model.get('id')+'['+val+'][content]"]').val()
            }
        when "4"
          value = @$('textarea').val()
        when "6"
          value = []
          @$('input:checked').each (i, e) =>
            value.push {
              value : @$(e).val()
              content: @$('input[type=text]').val() || ''
            }
        when "7"
          return true
        when "8"
          value = []
          @$('input:checked').each (i, e) =>
            value.push {
              value : @$(e).val()
              content: @$(e).next('input[type=text]').val() || ''
            }

      @model.set 'value': value
      if @model.get('isValid')? then @model.validate()
      return true

    selectExclusive: (e) ->
      # uncheck every other modality
      el = $(e.currentTarget)
      if el.is(':checked') then el.closest('div.modality').siblings().find('input').prop('checked', false).trigger('change')
      if el.is(':checked') then el.closest('div.modality').siblings().find('.complementary [type=text]').val('').trigger('change')
      @parseQuestionValue()

    selectSimple: (e) ->
      # uncheck exclusive modalities
      el = $(e.currentTarget)
      if el.is(':checked') then el.closest('div.modality').siblings().find('.exclusive input').prop('checked' , false).trigger('change')
      # if el.is(':checked') then el.closest('div.modality').siblings().find('.complementary [type=text]').val('').trigger('change')
      @parseQuestionValue()

    selectComplementary: (e) ->
      # uncheck exclusive
      @selectSimple(e)
      el = $(e.currentTarget)
      unless el.is(':checked') then el.closest('div.modality').find('.complementary [type=text]').val('').trigger('change')

    updateImage: (e) ->
      input = $(e.currentTarget);
      if(input.is(':checked')) then input.next('img').addClass('selected')
      else input.next('img').removeClass('selected')

    checkOrdered: (e) ->
      $(e.currentTarget).closest('tr').find('input:checked').prop('checked',false)
      return true

    checkComplementary: (e) ->
      e.preventDefault()
      $(e.currentTarget).focus()
      checkBoxName = $(e.currentTarget).attr('name').replace('content', 'value')
      checkbox = $(e.currentTarget).siblings('[name="'+checkBoxName+'"]')
      if (checkbox.length > 1) then checkbox = checkbox.first()
      checkbox.prop('checked',true)
      e.currentTarget = checkbox
      @selectSimple(e)