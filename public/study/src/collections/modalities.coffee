define ['backbone','models/modality'], (backbone, Modality) ->
  class extends backbone.Collection
    model: Modality

    comparator: (modality) ->
      order = parseInt(modality.id,10)
      if modality.collection.question
        question = modality.collection.question
        meta = modality.collection.question.get('meta')
        modalityType =  modality.get('entry_type_id')

        if meta && meta.random
          modalityMeta = modality.get('meta')
          if modalityMeta && modalityMeta.fixed == true
            order = "1"+modality.id
          else if modalityType == "2" && question.get('question_type_id') =="1"
            order = "0"+Math.random();
          else if modalityType == "3" || modalityType == "2"
            order = "1"+modality.id
          else order = "0"+Math.random();
      return order