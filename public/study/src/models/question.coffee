define [
  'underscore','backbone','../services/registry', '../collections/modalities'
], (_, backbone, registry, ModalitiesCollection) ->
  answerInterpolation = /\[ANSWER:(.+?)\]/
  class Question extends backbone.Model

    constructor: (attributes, options)->
      if attributes.id
        if registry.questions[attributes.id.toString()]
          registry.questions[attributes.id.toString()].set(attributes)
          registry.questions[attributes.id.toString()].modalities.reset(_.values(attributes.entries) || [])
          registry.questions[attributes.id.toString()].subquestions.reset(_.values(attributes.subquestions) || [])
          return registry.questions[attributes.id.toString()]
        else
          registry.add 'questions', attributes.id.toString(), this
      super attributes, options

    initialize: (attributes, options) ->
      @modalities = new ModalitiesCollection
      @modalities.question = @
      @subquestions = new ModalitiesCollection
      @subquestions.question = @
      if attributes.entries then @modalities.reset _.values(attributes.entries)
      if attributes.subquestions then @subquestions.reset _.values(attributes.subquestions)
      if answerInterpolation.test(attributes.content) || answerInterpolation.test(attributes.helps[0].content)
        if answerInterpolation.test(attributes.content) then code = attributes.content.match(answerInterpolation)[1].split('|')[0]
        else code = attributes.helps[0].content.match(answerInterpolation)[1].split('|')[0]
        registry.on (code + ':change'), () =>
          @trigger 'change:content'
      @on 'change:value', () =>
        if @get('isValid')? then @validate()

    validate: ()->
      valid = @isValid()
      @set('isValid', valid)
      return valid

    isValid: ()->
      if @get('visible') == false then return true
      meta = @get('meta');
      required = meta.required || false
      value = @get('value');
      switch @get('question_type_id')
        when "1"
          if (value? == false || value.length == 0) && required then return false
          if value?
            for val in value
              modality = registry.modalities[val.value.toString()]
              if modality.get('entry_type_id') == '3' && val.content.trim().length == 0 then return false
        when "3"
          if (value? == false || value.length == 0) && required then return false
          if value?
            if value.length < meta.max then return false
            if _.compact(_.uniq(_.map(value, (e)->e.value))).length != meta.max
              return false
            for val in value
              if val.content == "" then return false
        when "4"
          if (value? == false || value.trim().length == 0) && required then return false

        when "5"
          valid = true
          @subquestions.forEach (m) ->
            if m.validate() == false then valid = false
          if valid == false then return false
        when "6"
          if (value? == false || value.length == 0) && required then return false
          if value?
            for val in value
              modality = registry.modalities[val.value.toString()]
              if modality.get('entry_type_id') == '3' && val.content.trim().length == 0 then return false
        when "8"
          if (value? == false || value.length == 0) && required then return false
        else return true
      return true

  return Question