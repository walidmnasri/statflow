define [
  'underscore','backbone','../services/registry','../collections/questions'
], (_, backbone, registry, QuestionsCollection) ->
  class Page extends backbone.Model
    constructor: (attributes, options) ->
      if registry.pages[attributes.id.toString()]
        registry.pages[attributes.id.toString()].set(attributes)
        registry.pages[attributes.id.toString()].children.reset(_.values(attributes.entries) || [])
        return registry.pages[attributes.id.toString()]
      else
        registry.add 'pages', attributes.id.toString(), this
      super attributes, options
    initialize: (attributes, options) ->
      @questions = new QuestionsCollection()
      @questions.page = @
      @questions.reset _.values(attributes.children)

    isValid: () ->
      valid = true
      @questions.forEach (q) ->
        qValid = q.validate();
        if qValid == false
          valid = false
        return true
      return valid
    reset: () ->
      @questions.forEach (q) ->
        q.set({'value': null,'isValid':null})
  return Page