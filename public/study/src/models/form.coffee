define ['underscore','backbone', 'services/registry','../collections/pages'], (_, backbone, registry, PagesCollection) ->
  class Form extends backbone.Model
    urlRoot: document.location.pathname.replace('/form/','/save/')

    initialize: (attributes, options) ->
      @pages = new PagesCollection();
      @pages.reset _.values(attributes.pages)


    savePage: (index, done) ->
      @urlRoot = document.location.pathname.replace('/form/','/save-page/')
      #@clear silent:true
      data = { last_page: index, current_state: @getResults()}

      $.ajax({
        type: "POST",
        url: @urlRoot,
        data: data,
        success: done
      });
      #@save @getResults(), success: done

    saveForm: (done) ->
      @urlRoot = document.location.pathname.replace('/form/','/save/')
      @clear silent:true
      @save @getResults(), success: done

    getResults: () ->
      results = {}
      for code of registry.questions
        q = registry.questions[code]
        unless q.get('question_type_id') == "5" || q.get('question_type_id') == "7"
          results[code] = q.get('value')
      return results

  return Form