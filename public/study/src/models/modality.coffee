define ['backbone','../services/registry'], (backbone, registry) ->
  answerInterpolation = /\[ANSWER:(.+?)\]/
  class Modality extends backbone.Model

    constructor: (attributes, options)->
      if attributes.question_type_id # array subquestions
        if registry.questions[attributes.id.toString()]
          registry.questions[attributes.id.toString()].set(attributes)
          return registry.questions[attributes.id.toString()]
        else
          registry.add 'questions', attributes.id.toString(), this
      else
        if registry.modalities[attributes.id.toString()]
          registry.modalities[attributes.id.toString()].set(attributes)
          return registry.modalities[attributes.id.toString()]
        else
          registry.modalities[attributes.id.toString()] = this
      super attributes, options

    initialize: (attributes) ->
      if answerInterpolation.test attributes.content
        code = attributes.content.match(answerInterpolation)[1]
        registry.on (code + ':change'), () =>
          @trigger 'change:content'

    validate: () ->
      return @isValid();
    isValid: () ->
      unless @get('question_type_id') then return true
      if @get('value') then return true
      else if @get('meta').required == true then return false

  return Modality