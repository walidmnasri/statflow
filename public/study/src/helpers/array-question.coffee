define ['underscore','handlebars'], (_, Handlebars) ->
  Handlebars.registerHelper 'arrayQuestionHeader', (meta) ->
    headers = (value for value in [meta.min..meta.max])
    out = _.map( headers,
      (value) -> "<th>#{value}</th>"
    ).join('')
    if meta.dontKnow then out+="<th>I don't know</th>"
    return new Handlebars.SafeString(out);

  Handlebars.registerHelper 'arrayQuestionModality', ( modality) ->
    headers = (value for value in [modality.meta.min..modality.meta.max])
    out = _.map( headers,
      (value) -> '<td class="array-question-checkbox"><input type="radio" value="'+value+'" name="'+modality.id+'"></td>'
    ).join('')
    if modality.meta.dontKnow then out+='<td><input type="radio" value="" name="'+modality.id+'"></td>'
    return new Handlebars.SafeString(out);

  Handlebars.registerHelper 'orderedQuestionModality', ( modality) ->
    headers = (value for value in [modality.meta.min..modality.meta.max])
    out = _.map(headers, 
      (value) -> '<td class="array-question-checkbox"><input type="radio" value="'+modality.id+'" name="'+modality.question_id+'['+value+']"></td>'
    ).join('')
    return new Handlebars.SafeString(out);