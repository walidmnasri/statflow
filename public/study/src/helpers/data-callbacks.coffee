define ['underscore', 'handlebars', '../services/registry'], (_, Handlebars, registry) ->

  respondentInterpolation = /\[FILE:(.+?)\]/g
  structureInterpolation = /\[STRUCT:(.+?)\]/g
  answerInterpolation = /\[ANSWER:(.+?)\]/

  callback = (string) ->
    #console.log(typeof string)
    out = _.template( string, window.formData.user, interpolate: respondentInterpolation)
    if(typeof out == 'function') then out = out()
    out = _.template out, window.formData.store, interpolate: structureInterpolation
    if(typeof out == 'function') then out = out()

    if answerInterpolation.test(out)
      until out.match(answerInterpolation) == null
        matches = out.match(answerInterpolation)
        codeComponents = matches[1].split('|')
        code = codeComponents[0];
        answerNumber = if codeComponents.length == 2 then parseInt(codeComponents[1],10) - 1 else 0

        question = _.find registry.questions, (q) -> q.attributes.code == code
        if question
          if question.get 'value'
            value = question.get 'value'
            switch question.get 'question_type_id'
              when "1"
                if value.length < (answerNumber + 1) then out = out.replace answerInterpolation, '<i class="empty"></i>'
                else
                  modality = _.find registry.modalities, (m) -> m.id == value[answerNumber].value
                  switch true
                    when modality? == false
                      out = out.replace answerInterpolation, '<i class="empty"></i>'
                    when modality.get('entry_type_id') == "3"
                      out = out.replace answerInterpolation, value[answerNumber].content
                    else
                      out = out.replace answerInterpolation, callback modality.get('content')
              when "6"
                if value.length < (answerNumber + 1) then out = out.replace answerInterpolation, '<i class="empty"></i>'
                else
                  modality = _.find registry.modalities, (m) -> m.id == value[answerNumber].value
                  switch true
                    when modality? == false
                      out = out.replace answerInterpolation, '<i class="empty"></i>'
                    when modality.get('entry_type_id') == "3"
                      out = out.replace answerInterpolation, value[answerNumber].content
                    else
                      out = out.replace answerInterpolation, callback modality.get('content')
              when "3"
                if value.length < (answerNumber + 1) then out = out.replace answerInterpolation, '<i class="empty"></i>'
                else
                  modality = _.find registry.modalities, (m) -> m.id == value[answerNumber].value
                  switch true
                    when modality? == false
                      out = out.replace answerInterpolation, '<i class="empty"></i>'
                    when modality.get('entry_type_id') == "3"
                      out = out.replace answerInterpolation, value[answerNumber].content
                    else
                      out = out.replace answerInterpolation, callback modality.get('content')
              when "2"
                out = out.replace answerInterpolation, value
              when "4"
                out = out.replace answerInterpolation, value
          else
            out = out.replace answerInterpolation, '<i class="empty"></i>'
        else
          out = out.replace answerInterpolation, '<i class="empty"></i>'
    return new Handlebars.SafeString out


  Handlebars.registerHelper 'dataCallback', callback

  return callback;