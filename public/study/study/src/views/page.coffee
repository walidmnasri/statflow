define ['underscore','marionette', './question', 'hbs!templates/page'], (_, marionette, QuestionView, template) ->
  class extends marionette.CompositeView
    template: template
    itemView: QuestionView
    itemViewContainer: '.questions'
    attributes:
      class:"tab-pane"
    modelEvents:
      change: 'render'

    constructor: (options) ->
      attributes = {
        id:  'page_' + options.model.id
        class: 'tab-pane'
      }
      @attributes = attributes
      super options

    initialize: () ->
      @collection = @model.questions