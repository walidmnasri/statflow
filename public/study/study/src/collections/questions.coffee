define ['backbone','models/question'], (backbone, Question) ->
  class Questions extends backbone.Collection
    model: Question

  return Questions