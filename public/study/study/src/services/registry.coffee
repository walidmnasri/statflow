define ['underscore','backbone'],(_, backbone) ->
  class Registry
    pages: {}
    questions: {}
    modalities: {}
    add: (registry, key, model ) ->
      @[registry][key] = model
      if registry == 'questions'
        model.on('change:value', this._onValueChange, this);
    get: (registry, key) ->
      @[registry][key]

    _onValueChange: (model, value) ->
      eventName = model.get('code')+':change'
      @trigger(eventName)

  _.extend Registry.prototype, backbone.Events

  return new Registry