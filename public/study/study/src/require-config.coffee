require.config
  baseUrl: '/study/js'
  urlArgs: '?bust='+(new Date).getTime()
  paths:
    requirejs: "vendor/requirejs/require"
    jquery: "vendor/jquery/jquery-1.10.2.min"
    json2: "vendor/json2/index"
    hbs: "vendor/require-handlebars-plugin/hbs"
    i18n: "vendor/requirejs-i18n/i18n"
    underscore: "vendor/lodash/lodash.compat"
    backbone: "vendor/backbone-amd/backbone"
    'backbone.wreqr': "vendor/marionette.wreqr/index"
    'backbone.babysitter': "vendor/marionette.babysitter/index"
    marionette: "vendor/marionette/index"
    handlebars: "vendor/handlebars/handlebars"
    i18nprecompile: "vendor/require-handlebars-plugin/hbs/i18nprecompile"
    bootstrap: "vendor/bootstrap/bootstrap"
    wizard: "vendor/bootstrap/wizard"
    'bootstrap.slider': "vendor/bootstrap/bootstrap-slider"

  shim:
    json2:
      exports: 'JSON'
    handlebars:
      exports: 'Handlebars'
    bootstrap:
      deps: ['jquery']
    wizard:
      deps:['jquery','bootstrap','bootstrap.slider']

  hbs:
    disableI18n: true
    disableHelpers: true
    templateExtension: 'hbs'
