window.console = window.console || log:()->

require ['wizard','backbone','marionette','models/form','views/form', 'helpers/data-callbacks'], ($,backbone,marionette, FormModel, FormView) ->
  backbone.emulateJSON = true
  backbone.emulateHTTP = true

  marionette.Renderer.render = (template, data) ->
    data.invalidQuestionText = window.invalidQuestionText
    return template(data)

  window.app = app = new marionette.Application();

  app.addRegions {
    'page':'#page'
  }

  app.addInitializer () ->
    @form = new FormModel(formData)
    app.page.show( new FormView({model: app.form}))


  app.start()
