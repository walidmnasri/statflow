// Generated by CoffeeScript 1.7.1
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(['underscore', 'backbone', '../services/registry', '../collections/modalities'], function(_, backbone, registry, ModalitiesCollection) {
  var Question, answerInterpolation;
  answerInterpolation = /\[ANSWER:(.+?)\]/;
  Question = (function(_super) {
    __extends(Question, _super);

    function Question(attributes, options) {
      if (attributes.id) {
        if (registry.questions[attributes.id.toString()]) {
          registry.questions[attributes.id.toString()].set(attributes);
          registry.questions[attributes.id.toString()].modalities.reset(_.values(attributes.entries) || []);
          registry.questions[attributes.id.toString()].subquestions.reset(_.values(attributes.subquestions) || []);
          return registry.questions[attributes.id.toString()];
        } else {
          registry.add('questions', attributes.id.toString(), this);
        }
      }
      Question.__super__.constructor.call(this, attributes, options);
    }

    Question.prototype.initialize = function(attributes, options) {
      var code;
      this.modalities = new ModalitiesCollection;
      this.modalities.question = this;
      this.subquestions = new ModalitiesCollection;
      this.subquestions.question = this;
      if (attributes.entries) {
        this.modalities.reset(_.values(attributes.entries));
      }
      if (attributes.subquestions) {
        this.subquestions.reset(_.values(attributes.subquestions));
      }
      if (answerInterpolation.test(attributes.content) || answerInterpolation.test(attributes.helps[0].content)) {
        if (answerInterpolation.test(attributes.content)) {
          code = attributes.content.match(answerInterpolation)[1].split('|')[0];
        } else {
          code = attributes.helps[0].content.match(answerInterpolation)[1].split('|')[0];
        }
        registry.on(code + ':change', (function(_this) {
          return function() {
            return _this.trigger('change:content');
          };
        })(this));
      }
      return this.on('change:value', (function(_this) {
        return function() {
          if (_this.get('isValid') != null) {
            return _this.validate();
          }
        };
      })(this));
    };

    Question.prototype.validate = function() {
      var valid;
      valid = this.isValid();
      this.set('isValid', valid);
      return valid;
    };

    Question.prototype.isValid = function() {
      var meta, modality, required, val, valid, value, _i, _j, _k, _len, _len1, _len2, dataFormat;
      if (this.get('visible') === false) {
        return true;
      }
      meta = this.get('meta');
      required = meta.required || false;
      value = this.get('value');
      switch (this.get('question_type_id')) {
     
        case "1":
          if (((value != null) === false || value.length === 0) && required) {
            return false;
          }
          if (value != null) {
            for (_i = 0, _len = value.length; _i < _len; _i++) {
              val = value[_i];
              modality = registry.modalities[val.value.toString()];
              if (modality.get('entry_type_id') === '3' && val.content.trim().length === 0) {
                return false;
              }
            }
          }
          break;
        case "3":
          if (((value != null) === false || value.length === 0) && required) {
            return false;
          }
          if (value != null) {
            if (value.length < meta.max) {
              return false;
            }
            if (_.compact(_.uniq(_.map(value, function(e) {
              return e.value;
            }))).length !== meta.max) {
              return false;
            }
            for (_j = 0, _len1 = value.length; _j < _len1; _j++) {
              val = value[_j];
              if (val.content === "") {
                return false;
              }
            }
          }
          break;
        case "4":
          if (((value != null) === false || value.trim().length === 0) && required) {
           	  return false;
          }
          else if (value && value.trim().length !== 0 && this.get('meta').dataFormat){
        	  dataFormat = new RegExp(this.get('meta').dataFormat);
        	  if (dataFormat.test(value) === false){
        		  return false;
        	  }
          }
          break;
        case "5":
          valid = true;
          this.subquestions.forEach(function(m) {
            if (m.validate() === false && m.get('visible') != false) {         	
              return valid = false;
            }
          });
          if (valid === false) {
            return false;
          }
          break;
        case "6":
        	
          if (((value != null) === false || value.length === 0) && required) {
            return false;
          }
          if (value != null) {
            for (_k = 0, _len2 = value.length; _k < _len2; _k++) {
              val = value[_k];
              modality = registry.modalities[val.value.toString()];
              if (modality.get('entry_type_id') === '3' && val.content.trim().length === 0) {
                return false;
              }
            }
          }

          if ((this.get('meta').nbModaliteMax != null) && (this.get('meta').nbModaliteMax != 0)){
                
        	  if (value != null) {
	              var countCheckedSimple = $('[name="' + modality.get('question_id')+"[]"  + '"]:checked').length;
	              var countCheckedComplement = $('[name="' + modality.get('question_id')+"[value]" + '"]:checked').length;
	        	  var countChecked = countCheckedSimple + countCheckedComplement;
	        
	        	  if (this.get('meta').strict){
	        		  if(countChecked != this.get('meta').nbModaliteMax)
	        		      return false;
	        	  }else if(countChecked > this.get('meta').nbModaliteMax)
	        		      return false;
        	  }else{
        		  if (this.get('meta').strict)
        		    return false;
        	  }
          }
     
         
          break;
        case "8":
          if (((value != null) === false || value.length === 0) && required) {
            return false;
          }
          break;
        default:
          return true;
      }
      return true;
    };

    return Question;

  })(backbone.Model);
  return Question;
});
